workDir = cd;

A = dlmread('timeProcessing.txt', ' ', 1, 0);

plot(A(:,1), A(:,2), 'r', A(:,1), A(:,3), 'g', A(:,1), A(:,4), 'b');
legend('detection','matching','optimalization');

title('processing time');
ylabel('time [sec]');
xlabel('feature points per image')

%% read orientation BouJou files
format1 = '%f %f %f %f %f %f %f %f %f %f %f %f %f';
format2 = '%f %f %f';

list = ls('*.txt');
confList = [];
for i = 1:length(list)
    confName = deblank(list(i,:));
    confName = str2num(confName(1:end-7));
    confList = cat(1, confList, confName);
end
confList = sort(confList, 'Ascend');

bundle = {};
for i = 1:length(list)
    if ~isletter(list(i,1))
        %% find place in structure
        ptsConfig = deblank(list(i,:));
        ptsConfig = str2num(ptsConfig(1:end-7));
        pos = find(confList == ptsConfig);
        
        %% read orientation parameters
        fid = fopen(list(i,:)); 
        B = textscan(fid, format1, 'Delimiter', '\t', 'HeaderLines', 14);
        fclose(fid);
        T = cat(2, B{end-3}, B{end-2}, B{end-1});% 
        R = cat(2, B{1}, B{2}, B{3}, ...
            B{4}, B{5}, B{6}, ...
            B{7}, B{8}, B{9});
        
        %% read coordinates
        fid = fopen(list(i,:)); 
        C = textscan(fid, format2, 'Delimiter', '\t', 'HeaderLines', 45);
        XYZ = cat(2, C{1}, C{2}, C{3});
        fclose(fid);
        
        %% structure
        bundle.name{pos} = ptsConfig;
        bundle.T{pos} = T;
        bundle.R{pos} = R;
        bundle.XYZ{pos} = XYZ;
    end
end

clear R T C XYZ
%% read through 
figure(101); hold on;
for i = 1:length(bundle.name)
    scatter(bundle.T{i}(:,1),bundle.T{i}(:,2)); axis equal;
end

%% new points and iterative closest point and look at coordinate change

cd('C:\Matlab');
load('ARCcmap.mat');
ax1 = [187867.977484907 188155.386596071 175175.268314129 175289.535451878];
ax2 = [187867.991826318 188155.081635354 80.2139636566529 105.869451957432];
for i = 1:length(bundle.name)-1
    morePts = unique(bundle.XYZ{i+1}, 'rows');
    lessPts = unique(bundle.XYZ{i}, 'rows'); % master points
    T = delaunayn(morePts);
    [idx, dist] = dsearchn(morePts, T, lessPts);

    % sigma 3 filter

    newIdx = ones(size(morePts,1),1);
    newIdx(idx) = 0;
    selecIdx = 1:size(morePts,1);
    selecIdx = selecIdx(logical(newIdx));
    newPts = morePts(selecIdx,1:3); % the new points which were added in the bba

    nextPts = morePts(idx,:); % slave points
%     figure('units','normalized','outerposition',[0 0 1 1]);
%     quiver(lessPts(:,1),lessPts(:,2),...
%         lessPts(:,1)-nextPts(:,1),lessPts(:,2)-nextPts(:,2)); 
%     axis equal; axis(ax1);
%     title(['keypoint difference between ' num2str(bundle.name{i}) ' & ' num2str(bundle.name{i+1})]);
%     xlabel('X [{\itmtr}]'); ylabel('Y [{\itmtr}]');
%     SaveFigure(['dXY' num2str(bundle.name{i})]);
%     close(gcf);
% 
%     figure('units','normalized','outerposition',[0 0 1 1]);
%     quiver(lessPts(:,1),lessPts(:,3),...
%         lessPts(:,1)-nextPts(:,1),lessPts(:,3)-nextPts(:,3)); 
%     axis equal; axis(ax2);
%     xlabel('X [{\itmtr}]'); ylabel('Z [{\itmtr}]');
%     SaveFigure(['dZ' num2str(bundle.name{i})]);
%     close(gcf);
%     
%     histThres = (2.*mad(dist)) + median(dist);
%     figure('units','normalized','outerposition',[0 0 1 1]);
%     hist(dist(dist<histThres), 200);
%     xlabel('Difference [{\itmtr}]'); ylabel('amount [{\it#}]');
%     SaveFigure(['dHist' num2str(bundle.name{i})]);
%     close(gcf);
    dX = lessPts(:,1)-nextPts(:,1);
    dY = lessPts(:,2)-nextPts(:,2);
    dZ = lessPts(:,3)-nextPts(:,3);
    
    figure('units','normalized','outerposition',[0 0 1 1]);
    subplot(2,1,1);
    title('difference in X component');
    scatter(lessPts(:,1),lessPts(:,2),1,dX);
    caxis([-0.02 0.02]); colorbar;
    colormap(ARCmap); axis equal; axis(ax1);
    xlabel('X [{\itmtr}]'); ylabel('Y [{\itmtr}]');
    
    subplot(2,1,2);
    title('difference in X component');
    scatter(lessPts(:,1),lessPts(:,2),1,dY);
    caxis([-0.02 0.02]); colorbar;
    colormap(ARCmap); axis equal; axis(ax1);
    xlabel('X [{\itmtr}]'); ylabel('Y [{\itmtr}]');
    
    SaveFigure(['diff' num2str(bundle.name{i})]);
    close(gcf);
end
cd(workDir);



