function varargout = netDesign(varargin)
% NETDESIGN M-file for netDesign.fig
%      NETDESIGN, by itself, creates a new NETDESIGN or raises the existing
%      singleton*.
%
%      H = NETDESIGN returns the handle to a new NETDESIGN or the handle to
%      the existing singleton*.
%
%      NETDESIGN('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in NETDESIGN.M with the given input arguments.
%
%      NETDESIGN('Property','Value',...) creates a new NETDESIGN or raises
%      the existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before netDesign_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to netDesign_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help netDesign

% Last Modified by GUIDE v2.5 27-Jan-2014 13:55:39

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @netDesign_OpeningFcn, ...
                   'gui_OutputFcn',  @netDesign_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
end
% End initialization code - DO NOT EDIT

function netDesign_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to netDesign (see VARARGIN)

% Choose default command line output for netDesign
handles.output = hObject;

camIco = double(imread('fig/folder-cam.bmp', 'bmp'))./2^8;
set(handles.cameraDataBase, 'cdata', camIco);

theoIco = double(imread('fig/totalStation.bmp', 'bmp'))./2^8;
set(handles.toggleStation, 'cdata', theoIco);

theoIco = double(imread('fig/V10.bmp', 'bmp'))./2^8;
set(handles.visualRod, 'cdata', theoIco);

radIco = double(imread('fig/lens.bmp', 'bmp'))./2^8;
set(handles.radialDistortion, 'cdata', radIco);

blurIco = double(imread('fig/blur.bmp', 'bmp'))./2^8;
set(handles.motionBlur, 'cdata', blurIco);

sampIco = double(imread('fig/sample.bmp', 'bmp'))./2^8;
set(handles.samplingInterv, 'cdata', sampIco);

openIco = double(imread('fig/folder-remote.bmp', 'bmp'))./2^8;
set(handles.openShape, 'cdata', openIco);

loadIco = double(imread('fig/emblem-web-2.bmp', 'bmp'))./2^8;
set(handles.laden, 'cdata', loadIco);

zoomIco = double(imread('fig/system-search-3.bmp', 'bmp'))./2^8;
set(handles.zoomAction, 'cdata', zoomIco);

panIco = double(imread('fig/transform-move.bmp', 'bmp'))./2^8;
set(handles.panAction, 'cdata', panIco);

homeIco = double(imread('fig/user-home.bmp', 'bmp'))./2^8;
set(handles.selectBuilding, 'cdata', homeIco);

homeAddIco = double(imread('fig/add-home.bmp', 'bmp'))./2^8;
set(handles.addObject, 'cdata', homeAddIco);

homeAdjustIco = double(imread('fig/adjust-home.bmp', 'bmp'))./2^8;
set(handles.adjustObject, 'cdata', homeAdjustIco);

homeClearIco = double(imread('fig/clear-home.bmp', 'bmp'))./2^8;
set(handles.clearObject, 'cdata', homeClearIco);

camXyIco = double(imread('fig/camera-photo-5.bmp', 'bmp'))./2^8;
set(handles.placeMarker, 'cdata', camXyIco);

camAdjIco = double(imread('fig/view-fullscreen-3.bmp', 'bmp'))./2^8;
set(handles.adjustCam, 'cdata', camAdjIco);

camAdjRot = double(imread('fig/transform-rotate-2.bmp', 'bmp'))./2^8;
set(handles.rotateCam, 'cdata', camAdjRot);

gcpIco = double(imread('fig/GNSS.bmp', 'bmp'))./2^8;
set(handles.duidtaan, 'cdata', gcpIco);

baakIco = double(imread('fig/baak.bmp', 'bmp'))./2^8;
set(handles.baak, 'cdata', baakIco);

calcIco = double(imread('fig/accessories-calculator-5.bmp', 'bmp'))./2^8;
set(handles.verwerk, 'cdata', calcIco);

wmsIco = double(imread('fig/Map_pin.bmp', 'bmp'))./2^8;
set(handles.WMSbutton, 'cdata', wmsIco);

viewIco = double(imread('fig/streetView.bmp', 'bmp'))./2^8;
set(handles.StreetView, 'cdata', viewIco);

paintIco = double(imread('fig/kolourpaint.bmp', 'bmp'))./2^8;
set(handles.illustreer, 'cdata', paintIco);

moveIco = double(imread('fig/move3.bmp', 'bmp'))./2^8;
set(handles.move3, 'cdata', moveIco);

uitIco = double(imread('fig/document-export.bmp', 'bmp'))./2^8;
set(handles.export, 'cdata', uitIco);

helpIco = double(imread('fig/htmlhelp.bmp', 'bmp'))./2^8;
set(handles.htmlhelp, 'cdata', helpIco);

mailIco = double(imread('fig/kuMail.bmp', 'bmp'))./2^8;
set(handles.contactMe, 'cdata', mailIco);

% jIcon= double(imread('fig/foto-lens.png', 'png',...
%     'BackgroundColor', [1 1 1]))./2^8;
javaFrame = get(hObject,'JavaFrame');
javaFrame.setFigureIcon(javax.swing.ImageIcon('fig/perian.png'));

setappdata(0, 'hMainGui', gcf);
setappdata(gcf, 'fhUpdateCam', @updateCam);
setappdata(gcf, 'fhUpdateProj', @updateProj);

% Update handles structure
guidata(hObject, handles);

initialize_gui(hObject, handles, false);
end

function varargout = netDesign_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
end

function focal_CreateFcn(hObject, eventdata, handles)
% hObject    handle to focal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

function focal_Callback(hObject, eventdata, handles)
% hObject    handle to focal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of focal as text
%        str2double(get(hObject,'String')) returns contents of focal as a double
focal = str2double(get(hObject, 'String'));
if isnan(focal)
    set(hObject, 'String', 0);
    errordlg('Gelieve een getal in te voeren');
end

% Save the new focal value
handles.metricdata.focal = focal;
guidata(hObject,handles)
end

function resolution1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to resolution1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

function resolution1_Callback(hObject, eventdata, handles)
% hObject    handle to resolution1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of resolution1 as text
%        str2double(get(hObject,'String')) returns contents of resolution1 as a double
resolution1 = str2double(get(hObject, 'String'));
if isnan(resolution1)
    set(hObject, 'String', 0);
    set(handles.bottomBar, 'String', 'Gelieve een getal in te voeren');
end

% Save the new resolution1 value
handles.metricdata.resolution1 = resolution1;
guidata(hObject,handles)
end

function resolution2_Callback(hObject, eventdata, handles)
% hObject    handle to resolution2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of resolution2 as text
%        str2double(get(hObject,'String')) returns contents of resolution2 as a double
resolution2 = str2double(get(hObject, 'String'));
if isnan(resolution2)
    set(hObject, 'String', 0);
    errordlg('Gelieve een getal in te voeren');
end

% Save the new resolution1 value
handles.metricdata.resolution2 = resolution2;
guidata(hObject,handles)
end

function resolution2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to resolution2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

function pixelsize_Callback(hObject, eventdata, handles)
pixelsize = str2double(get(hObject, 'String'));
if isnan(pixelsize)
    set(hObject, 'String', 0);
    errordlg('Gelieve een getal in te voeren');
end

% Save the new resolution1 value
handles.metricdata.pixelsize = pixelsize;
guidata(hObject,handles)
end

function pixelsize_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

function sigma_Callback(hObject, eventdata, handles)
sigma = str2double(get(hObject, 'String'));
if isnan(sigma)
    set(hObject, 'String', 0);
    errordlg('Gelieve een getal in te voeren');
end

% Save the new resolution1 value
handles.metricdata.sigma = sigma;
guidata(hObject,handles)
end

function sigma_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

function GCP_Callback(hObject, eventdata, handles)
GCP = str2double(get(hObject, 'String'));
if isnan(sigma)
    set(hObject, 'String', 0);
    errordlg('Gelieve een getal in te voeren');
end

% Save the new resolution1 value
handles.metricdata.gcp = GCP;
guidata(hObject,handles)
end

function GCP_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

%% 2. open topografische data

% --- Executes on button press in cameraDataBase.
function cameraDataBase_Callback(hObject, eventdata, handles)
setappdata(0, 'spiegelreflex', get(handles.spiegelReflex, 'Value'))
setappdata(0, 'focal', get(handles.focal, 'Value'))
setappdata(0, 'resolution1', get(handles.resolution1, 'Value'))
setappdata(0, 'resolution2', get(handles.resolution2, 'Value'))
setappdata(0, 'pixelsize', get(handles.pixelsize, 'Value'))
setappdata(0, 'handles', handles);
cameraDb();
end

function updateCam
handles = getappdata(0, 'handles');
set(handles.focal, 'String', getappdata(0, 'focal'));
set(handles.resolution1,  'String', getappdata(0, 'resolution1'));
set(handles.resolution2,  'String', getappdata(0, 'resolution2'));
set(handles.pixelsize,  'String', getappdata(0, 'pixelsize'));

switch getappdata(0, 'camType')
    case 'metrisch'
        set(handles.metrisch, 'Value', true);
    case 'spiegelReflex'
        set(handles.spiegelReflex, 'Value', true);
    case 'consument'
        set(handles.consument, 'Value', true);
end
switch getappdata(0, 'totalStation')
    case 0
        set(handles.toggleStation, 'Value', false);
    case 1
        set(handles.toggleStation, 'Value', true);
end
switch getappdata(0, 'pano')
    case 0
        set(handles.visualRod, 'Value', false);
    case 1
        set(handles.visualRod, 'Value', true);
end

rmappdata(0, 'handles');
hMainGui = getappdata(0, 'hMainGui');
guidata(hMainGui, handles);
end

function openShape_Callback(hObject, eventdata, handles)
setappdata(0, 'handles', handles);
projDb();
end

function updateProj
handles = getappdata(0, 'handles');

% topography update
set(handles.shapePath,  'String', getappdata(0, 'shapePath'));
handles.metricdata.shapePath = getappdata(0, 'shapePath');

% projection update
handles.epsg.FalseEasting = getappdata(0, 'FalseEasting');
handles.epsg.FalseNorthing = getappdata(0, 'FalseNorthing');
handles.epsg.StandPar1 = getappdata(0, 'StandPar1');
handles.epsg.StandPar2 = getappdata(0, 'StandPar2');
handles.epsg.LatOrigin = getappdata(0, 'LatOrigin');
handles.epsg.LonOriginCM = getappdata(0, 'LonOriginCM');
handles.epsg.ProjScaleFac = getappdata(0, 'ProjScaleFac');
handles.epsg.SemiMajAx = getappdata(0, 'SemiMajAx');
handles.epsg.InvFlatt = getappdata(0, 'InvFlatt');
handles.epsg.EPSG = getappdata(0, 'EPSG');

rmappdata(0, 'handles');
hMainGui = getappdata(0, 'hMainGui');
guidata(hMainGui, handles);
end

function shapePath_Callback(hObject, eventdata, handles)
shapePath = get(hObject, 'String');

% Save the new resolution1 value
handles.metricdata.shapePath = shapePath;
guidata(hObject,handles)
end

function shapePath_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

function   [M] = m_shaperead(fname,handles,UBR)
% M_SHAPEREAD reads an ESRI-format shapefile
%
%   M=M_SHAPEREAD(FNAME) reads the 3 files
%   that have name FNAME with added .dbf, .shp,
%   and .shx extensions. Data is returned in a
%   structure M, whose format includes several
%   fixed fields that contain the data, as well
%   as an arbitrary number of fields that
%   contain the database information for this
%   particular file.
%
%   Since the shapefile is a self-describing
%   format you have to look at M to discover
%   what is there. The data itself is usually
%   in the .ncst subfield.
% 
%   Note that individual polygons or polylines in
%   a shapefile sometimes come in parts, these
%   are translated into a continuous segment with
%   NaNs separating the different parts.
%
% Format info as per:
%
%   Shapefile (.shx,.shp):  Wikipedia and www.esri.com
%   dbf file:  http://www.clicketyclick.dk/databases/xbase/format/index.html
%
%   M_SHAPEREAD(FNAME,UBR) returns only those
%   elements with a minimum bounding rectangle (MBR)
%   that intersects with the User-specified minimum
%   bounding rectangle (UBR), in the format
%      [minX  minY  maxX maxY]
%

% R. Pawlowicz rpawlowicz@eos.ubc.ca 24/Jan/2011


if nargin<3,
 UBR=[-Inf -Inf Inf Inf];
end;
 
%
% Read the .shx file. Some parts are little-endian, and
% some parts big-endian. It turns out that in Windows
% I can't open a file for reading twice and get different
% FIDs (it seems to return the same one for each fopen)
% So I have to do different separate reads.
% (thanks to Doug Popken for reporting this)

% Read header 
%--------------

fidb=fopen([fname '.shx'],'r','b');  % big-endian open


if fidb==-1,
 error(['Cannot file filename: ' fname '.shx']);
end;
 
head1=fread(fidb,7,'int32'); % unused stuff + file length

% Read index data
status=fseek(fidb,100,'bof');
recn16=fread(fidb,[2 Inf],'int32')';
lrec=size(recn16,1);

set(handles.bottomBar, 'String', [int2str(lrec) ' objecten in bestand, laden']);

fclose(fidb);

%--------------

fidl=fopen([fname '.shx'],'r','l');  % little-endian open

status=fseek(fidl,28,'bof');
head2=fread(fidl,2,'int32'); % Version and shape type
head3=fread(fidl,8,'double'); % A bunch of bounding info

fclose(fidl);
%--------------


% Read the dbf file. If it isn't there
% we should stil try to read the shp file.
 
fidl=fopen([fname '.dbf'],'r','l'); % little-endian read

if fidl==-1,
 disp(['Cannot file filename: ' fname '.dbf']);
 dbf={};
 fnam={};
 nfield=0;
 mdat=[NaN NaN NaN];
else

  ver=fread(fidl,1,'int8');
  mdat=fread(fidl,3,'int8')';
  nrec=fread(fidl,1,'int32');
  hlen=fread(fidl,1,'int16');
  rlen=fread(fidl,1,'int16');

  nfield=(hlen-32-1)/32;
  for k=1:nfield,    % 
    status=fseek(fidl,32*k,'bof');
    fnam{k}=char(fread(fidl,11,'uchar')');
    fnam{k}=fnam{k}(fnam{k}>0);
    ftype(k)=char(fread(fidl,1,'uchar'));
    faddr(k)=fread(fidl,1,'int32');
    flen(k)=fread(fidl,1,'uchar');
  end;

  dbf=cell(nrec,nfield);
  for k=1:nrec,
    if rem(k,500)==0, fprintf('-'); end;
    
    status=fseek(fidl,hlen+1+(k-1)*rlen,'bof');
    for l=1:nfield,
    
      switch ftype(l),
	case 'N',
          str=char([fread(fidl,flen(l),'uchar')']);
          dbf{k,l}=sscanf(str,'%f');          % was %d but found a file with E+XXX numbers
	case 'F',
          str=char([fread(fidl,flen(l),'uchar')']);
          dbf{k,l}=sscanf(str,'%f');
	case 'I',
          dbf{k,l}=fread(fidl,4,'int32');
	case 'C',
          str=char(fread(fidl,flen(l),'uchar')');
	  str(str==13)=' ';  % CR added in some strings, change to space
          dbf{k,l}= deblank(str);
	case 'D',
       	  str=char(fread(fidl,8,'uchar')');
          dbf{k,l}=datenum(str,'yyyymmdd');
	otherwise
          disp(['Unknown field type: ' ftype(l)]);
       end; 
    end;
  end;  
  fprintf('\n');
  fclose(fidl);
end;

% Set up the data structure so far

M=struct('version',head2(1),'shape_type',head2(2),...
         'MBRx',head3([1 3]),...
         'MBRy',head3([2 4]),...
         'MBRz',head3(5:6),...
         'MBRm',head3(7:8),...
	 'filelength16',head1(7),...
         'type',NaN,'ctype',' ',...
	 'ncst',cell(1,1),'mbr',NaN(lrec,4),...
	 'dbfversion',ver,...
	 'dfbdate',mdat+[1900 0 0],'fieldnames',cell(1,1),...
         'dbfdata',cell(1,1));

M.fieldnames=fnam;
M.dbfdata=dbf;
for k=1:nfield,
  M=setfield(M,fnam{k},dbf(:,k));
end;


% Read the .shp file

fidl=fopen([fname '.shp'],'r','l');

if fidl==-1,
 error(['Cannot find filename: ' fname '.shp']);
end;

ikp=ones(lrec,1);

for k=1:lrec,
  if rem(k,500)==0, fprintf('.'); end;
  
  fseek(fidl,recn16(k,1)*2+8,'bof');
  stype=fread(fidl,1,'int32');
  M.type=stype;
  switch stype,
    case 0, % null
    
    case 1, % point
      M.ctype='point';
      pt=fread(fidl,2,'double')';
      M.ncst{k,1}=pt;
  
    case 3, % polyline
      M.ctype='polyline';
      mbr=fread(fidl,4,'double');

      if mbr(1)<UBR(3) & mbr(3)>UBR(1) & mbr(2)<UBR(4) & mbr(4)>UBR(2),
	nparts=fread(fidl,1,'int32');
	npts=fread(fidl,1,'int32');
	parts=fread(fidl,nparts,'int32');
	pts=fread(fidl,[2 npts],'double')';
	% Separate in Nan-separated list.
	% Add 'last' offset
	parts=[parts;length(pts)];

	ncst=NaN(length(pts)+length(parts)-2,2);
	for k2=1:length(parts)-1,
          ncst([parts(k2)+1:parts(k2+1)]+(k2-1),:)=pts([parts(k2)+1:parts(k2+1)],:);
	end;
	M.mbr(k,:)=mbr;	
	M.ncst{k,1}=ncst;
      else
	M.ncst{k,1}=[];
        ikp(k)=0;
      end;	  
      
    case 5, % polygon
      M.ctype='polygon';
      mbr=fread(fidl,4,'double');
      if mbr(1)<UBR(3) & mbr(3)>UBR(1) & mbr(2)<UBR(4) & mbr(4)>UBR(2),
	 nparts=fread(fidl,1,'int32');
	 npts=fread(fidl,1,'int32');
	 parts=fread(fidl,nparts,'int32');
	 pts=fread(fidl,[2 npts],'double')';

	 % Separate in Nan-separated list.
	 % Add 'last' offset
	 parts=[parts;length(pts)];

	 ncst=NaN(length(pts)+length(parts)-2,2);
	 for k2=1:length(parts)-1,
           ncst([parts(k2)+1:parts(k2+1)]+(k2-1),:)=pts([parts(k2)+1:parts(k2+1)],:);
	 end;	
	 M.mbr(k,:)=mbr;	
	 M.ncst{k,1}=ncst;
      else
	M.ncst{k,1}=[];
        ikp(k)=0;
      end;	  
      
    case 8, % multipoint
      M.ctype='multipoint';
      mbr=fread(fidl,4,'double');
      if mbr(1)<UBR(3) & mbr(3)>UBR(1) & mbr(2)<UBR(4) & mbr(4)>UBR(2),
	 npts=fread(fidl,1,'int32');
	 pts=fread(fidl,[2 npts],'double')';
	 M.mbr(k,:)=mbr;	
	 M.ncst{k,1}=pts;
      else
	M.ncst{k,1}=[];
        ikp(k)=0;
      end;	  
      
    case 11, % pointZ
      M.ctype='pointZ';
      pt=fread(fidl,3,'double')';
      M.ncst{k,1}=pt;

    case 13, % polylineZ
      M.ctype='polylineZ';
      mbr=fread(fidl,4,'double')';
      if mbr(1)<UBR(3) & mbr(3)>UBR(1) & mbr(2)<UBR(4) & mbr(4)>UBR(2),
	 nparts=fread(fidl,1,'int32');
	 npts=fread(fidl,1,'int32');
	 parts=fread(fidl,nparts,'int32');
	 pts=fread(fidl,[2 npts],'double')';
	 mbrZ=fread(fidl,2,'double')';
	 ptsZ=fread(fidl,[npts],'double')';

	 % Separate in Nan-separated list.
	 % Add 'last' offset
	 parts=[parts;length(pts)];

	 ncst=NaN(length(pts)+length(parts)-2,2);
	 for k2=1:length(parts)-1,
           ncst([parts(k2)+1:parts(k2+1)]+(k2-1),1:2)=pts([parts(k2)+1:parts(k2+1)],:);
           ncst([parts(k2)+1:parts(k2+1)]+(k2-1),3)=ptsZ([parts(k2)+1:parts(k2+1)] );
	 end;
	 if size(M.mbr,2)==4, M.mbr=NaN(size(M.mbr,1),6); end;	
	 M.mbr(k,:)=[mbr mbrZ];	
	 M.ncst{k,1}=ncst;
      else
	M.ncst{k,1}=[];
        ikp(k)=0;
      end;	  

    case 15, % polygonZ
      M.ctype='polygonZ';
      mbr=fread(fidl,4,'double')';
      if mbr(1)<UBR(3) & mbr(3)>UBR(1) & mbr(2)<UBR(4) & mbr(4)>UBR(2),
	 nparts=fread(fidl,1,'int32');
	 npts=fread(fidl,1,'int32');
	 parts=fread(fidl,nparts,'int32');
	 pts=fread(fidl,[2 npts],'double')';
	 mbrZ=fread(fidl,2,'double')';
	 ptsZ=fread(fidl,[npts],'double')';

	 % Separate in Nan-separated list.
	 % Add 'last' offset
	 parts=[parts;length(pts)];

	 ncst=NaN(length(pts)+length(parts)-2,2);
	 for k2=1:length(parts)-1,
           ncst([parts(k2)+1:parts(k2+1)]+(k2-1),1:2)=pts([parts(k2)+1:parts(k2+1)],:);
           ncst([parts(k2)+1:parts(k2+1)]+(k2-1),3)=ptsZ([parts(k2)+1:parts(k2+1)] );
	 end;
	 if size(M.mbr,2)==4, M.mbr=NaN(size(M.mbr,1),6); end;	
	 M.mbr(k,:)=[mbr mbrZ];	
	 M.ncst{k,1}=ncst;
      else
	M.ncst{k,1}=[];
        ikp(k)=0;
      end;	  

    case 18, % multipointZ
      M.ctype='multipointZ';
      mbr=fread(fidl,4,'double');
      if mbr(1)<UBR(3) & mbr(3)>UBR(1) & mbr(2)<UBR(4) & mbr(4)>UBR(2),
	 npts=fread(fidl,1,'int32');
	 pts=fread(fidl,[2 npts],'double')';
	 mbrZ=fread(fidl,2,'double')';
	 ptsZ=fread(fidl,[npts],'double')';
	 if size(M.mbr,2)==4, M.mbr=NaN(size(M.mbr,1),6); end;	
	 M.mbr(k,:)=[mbr mbrZ];	
	 M.ncst{k,1}=[pts ptsZ];
      else
	M.ncst{k,1}=[];
        ikp(k)=0;
      end;	  

       
    otherwise
      disp(['Unknown record type: ' int2str(stype)])
  end;
end;
fclose(fidl);

irem=find(~ikp);
M.dbfdata(irem,:)=[];
M.ncst(irem,:)=[];
M.mbr(irem,:)=[];
for k=1:nfield,
  M.(fnam{k})(irem,:)=[];
end;
end

function setTarget(sourceHandle, targetHandle)
% retrieve handles structure in the source gui
handles = guidata(sourceHandle);  
% assign target handle as an item in the handles structure 
handles.target = targetHandle;
% save the handles s
guidata(sourceHandle, handles);
end

function toggleStation_Callback(hObject, eventdata, handles)
if get(handles.toggleStation, 'Value')==1
    handles.options.imagingstation = true;
else
    handles.options.imagingstation = false;
end
guidata(hObject,handles)
end

% --- Executes on button press in visualRod.
function visualRod_Callback(hObject, eventdata, handles)
if get(handles.toggleStation, 'Value')==1
    handles.options.imagingrod = true;
else
    handles.options.imagingrod = false;
end
guidata(hObject,handles)
end

% --- Executes on button press in radialDistortion.
function radialDistortion_Callback(hObject, eventdata, handles)
% hObject    handle to radialDistortion (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if get(handles.radialDistortion, 'Value')==1
    handles.options.radialdistortion = true;
else
    handles.options.radialdistortion = false;
end
guidata(hObject,handles)
end

% --- Executes on button press in motionBlur.
function motionBlur_Callback(hObject, eventdata, handles)
% hObject    handle to motionBlur (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of motionBlur
if get(handles.motionBlur, 'Value')==1
    handles.options.blur = true;
else
    handles.options.blur = false;
end
guidata(hObject,handles)
end


% --- Executes on button press in samplingInterv.
function samplingInterv_Callback(hObject, eventdata, handles)
% hObject    handle to samplingInterv (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of samplingInterv
if get(handles.samplingInterv, 'Value')==1
    % Checkbox is checked-take approriate action
    handles.metricdata.samplingOnOff  = 1;
else
    % Checkbox is not checked-take approriate action
    handles.metricdata.samplingOnOff  = 0;
end

% set(handles.samplingOnOff, 'String', handles.metricdata.samplingOnOff);
guidata(hObject,handles)
end

% --- Executes on button press in samplingOnOff.
function samplingOnOff_Callback(hObject, eventdata, handles)
% hObject    handle to samplingOnOff (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if (get(hObject,'Value') == get(hObject,'Max'))
    % Checkbox is checked-take approriate action
    handles.metricdata.samplingOnOff  = 1;
else
    % Checkbox is not checked-take approriate action
    handles.metricdata.samplingOnOff  = 0;
end

% set(handles.samplingOnOff, 'String', handles.metricdata.samplingOnOff);
guidata(hObject,handles)
end

function samplingInterval_Callback(hObject, eventdata, handles)
% hObject    handle to samplingInterval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of samplingInterval as text
%        str2double(get(hObject,'String')) returns contents of samplingInterval as a double
sampInterv = str2double(get(hObject, 'String'));
if isnan(sampInterv)
    set(hObject, 'String', 0);
    errordlg('Gelieve een getal in te voeren');
end

% Save the new focal value
handles.metricdata.samplingInterval = sampInterv;
guidata(hObject,handles)
end

% --- Executes during object creation, after setting all properties.
function samplingInterval_CreateFcn(hObject, eventdata, handles)
% hObject    handle to samplingInterval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

% --- Executes on button press in laden.
function [handles] = laden_Callback(hObject, eventdata, handles)
% hObject    handle to laden (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

cla;
hold on;

GRB = m_shaperead(handles.metricdata.shapePath, handles);


%% plot GRB
objects = length(GRB.UIDN);
for i = 1:objects
    xy = GRB.ncst{i};
    patch(xy(:,1),xy(:,2),[1 0.35 0.15],'EdgeColor','none');
end
axis on; axis xy; axis equal;
grid on; axis tight;

x = get(gca,'XTick'); set(gca,'XTick',x); set(gca,'XTickLabel',sprintf('%3.0f|',x));
y = get(gca,'YTick'); set(gca,'YTick',y); set(gca,'YTickLabel',sprintf('%3.0f|',y));
xlabel('X [m]'); ylabel('Y [m]');

handles.metricdata.shape = GRB;
guidata(hObject,handles)
set(handles.bottomBar, 'String', ...
    'Vergroot en verplaats in kaartblad naar object om op te meten');
set(handles.zoomAction,'Enable','on');
set(handles.panAction,'Enable','on');
set(handles.addObject,'Enable','on');
set(handles.clearObject,'Enable','on');
set(handles.adjustObject,'Enable','on');
set(handles.selectBuilding,'Enable','on');
end

% --- Executes on button press in zoomAction.
function zoomAction_Callback(hObject, eventdata, handles)
% hObject    handle to zoomAction (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if get(handles.zoomAction, 'Value');
    set(handles.panAction, 'Value', false);
    set(handles.adjustCam, 'Value', false);
    set(handles.rotateCam, 'Value', false);
    set(handles.clearObject, 'Value', false);
    set(handles.addObject, 'Value', false);
    set(handles.adjustObject, 'Value', false);
    zoom on;
else
    zoom off;
end
end

% --- Executes on button press in panAction.
function panAction_Callback(hObject, eventdata, handles)
% hObject    handle to panAction (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if get(handles.panAction, 'Value');
    set(handles.zoomAction, 'Value', 0);
    set(handles.adjustCam, 'Value', false);
    set(handles.rotateCam, 'Value', false);
    set(handles.clearObject, 'Value', false);
    set(handles.addObject, 'Value', false);
    set(handles.adjustObject, 'Value', false);
    pan on;
else
    pan off;
end
end

% --- Executes on button press in selectBuilding.
function selectBuilding_Callback(hObject, eventdata, handles)
% hObject    handle to selectBuilding (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of selectBuilding

% load spatial data

noTerrain = false;
try 
    handles.objData.terrain;
catch
    noTerrain = true;
end
if noTerrain
    GRB = handles.metricdata.shape;
    objects = length(GRB.ncst);
else
    GRB = handles.objData.terrain;
    objects = length(GRB.ncst);
end

if get(handles.selectBuilding, 'Value');
    set(handles.panAction, 'Value', false); pan off;
    set(handles.zoomAction, 'Value', false); zoom off;
    set(handles.clearObject, 'Value', false);
    set(handles.addObject, 'Value', false);
    set(handles.adjustObject, 'Value', false);
    set(handles.bottomBar, 'String', ...
        'Linkermuisknop voor toevoegen van object, middelste muisknop voor verwijderen, rechtermuisknop sluiten');
    
    % look if the extent is to big to do a global search
    if abs(GRB.MBRy(1)-GRB.MBRy(2))*abs(GRB.MBRx(1)-GRB.MBRx(2)) > 1000000
        bigFile = true;
    else
        bigFile = false;
    end
    endSelection = false;
    object = {};
    while ~endSelection
        [xPoly,yPoly,button] = ginput(1);
        if button == 1
            if bigFile % radius verhaal
                %% get polygon
                matVer = version('-release');
                matVer = matVer(1:end-1);

                radius = 400;
                if matVer < 2011
                    a = [0:18:360];
                    x = cosd(a);
                    y = sind(a);
                    v = [radius*x+xPoly; radius*y+yPoly];

                    clear x y a
                else
                    ci = rsmak('circle', radius, [xPoly yPoly]); 
                    in = fnbrk(ci,'interv');
                    t = linspace(in(1),in(2),21); t(end)=[];
                    v = fntlr(ci,3,t);

                    clear ci in t
                end

                %% get neighborhood
                terrain = {};
                terrain.MBRy(1) = 0; terrain.MBRy(2) = 0;
                terrain.MBRx(1) = 0; terrain.MBRx(2) = 0;
                counter = 1;
                for i = 1:objects
                    xy = GRB.ncst{i};
                    IN = inpolygon(xy(:,1),xy(:,2),v(1,:)',v(2,:)');
                    if sum(IN)>0
                        terrain.ncst{counter} = xy;
                        counter = counter+1;
                    end
                end
                objects = counter-1;

                clear counter IN xy
                GRB = terrain;
                bigFile = false;
            end
            subjectID = [];
            for i = 1:objects
                xy = GRB.ncst{i};
                IN = inpolygon(xPoly,yPoly,xy(:,1),xy(:,2));
                if sum(IN)>0
                    subjectID = i;
                end
            end

            objectDeselect = [];
            if ~isempty(subjectID)
                % loop through objects
                objectLoop = size(object,2);
                if ~isempty(objectLoop)
                    for i = 1:objectLoop
                        IN = inpolygon(xPoly,yPoly,object{i}(:,1),object{i}(:,2));
                        if sum(IN)>0
                            objectDeselect = i;
                        end
                    end
                end
                if isempty(objectDeselect)
                    object{objectLoop+1} = GRB.ncst{subjectID};
                    patch(object{objectLoop+1}(:,1),object{objectLoop+1}(:,2),...
                        [0 0 0.35],'EdgeColor','none');
                end
            end
         elseif button == 2 % delete object
             if size(object,2) > 1;
                closestPoint = zeros(size(object,2),1);
                for i = 1:size(object,2)
                    xy = GRB.ncst{i};
                    delta = xy-repmat([xPoly yPoly], size(xy,1),1);
                    [C,K] = min(sqrt((sum(delta.^2,2)).^0.5));
                    closestPoint(i)= C;
                end

                [C,K] = min(closestPoint);
                patch(object{K}(:,1),object{K}(:,2),...
                    [1 0.35 0.15],'EdgeColor','none');
                
                if K == size(object,2) % clear object
                    sizeObjects = size(object,2);
                    keep = [1:sizeObjects-1];
                    object = object(keep);
                else
                    keep = [1:K-1 K+1:size(object,2)];
                    object = object(keep);
                end
                
               
             end
        elseif button == 3 % done with selection
                endSelection = true;
                handles.object = object;
                handles.objData.terrain = GRB;
                guidata(hObject,handles);
                set(handles.selectBuilding, 'Value', 0);
        end
    end
end
set(handles.placeMarker,'Enable','on');
set(handles.adjustCam,'Enable','on');
set(handles.rotateCam,'Enable','on');
set(handles.duidtaan,'Enable','on');
set(handles.baak,'Enable','on');
set(handles.WMSbutton,'Enable','on');
set(handles.StreetView,'Enable','on');

end

% --- Executes on button press in addObject.
function addObject_Callback(hObject, eventdata, handles)
% hObject    handle to addObject (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of addObject

if get(handles.addObject, 'Value') == 1;
    set(handles.panAction, 'Value', false); pan off;
    set(handles.zoomAction, 'Value', false); zoom off;
    set(handles.rotateCam, 'Value', false);
    set(handles.adjustCam, 'Value', false);
    set(handles.adjustObject, 'Value', false);
    set(handles.clearObject, 'Value', false);
    set(handles.bottomBar, 'String', ...
        'Linkermuisknop voor toevoegen van objectpunt, rechter muisknop voor sluiten van object, druk de knop voor beeindiging');
    
    
    h = impoly(gca,[]);
    
%     set(handles.options.newObject,'ButtonDownFcn',{@pointButtonDown,handles});   
%     api = iptgetapi(handles.options.newObject);
%     api.setColor('yellow');
%     
%     fcn = makeConstrainToRectFcn('impoly',get(gca,'XLim'),get(gca,'YLim'));
%     api.setPositionConstraintFcn(fcn);
    api = iptgetapi(h);
    handles.options.newObject = api.getPosition();
    guidata(hObject,handles);
    set(handles.addObject, 'Value', 0);
    addObject_Callback(hObject, eventdata, handles);
else
%     set(handles.options.newObject,'Visible', 'off'); 
    noTerrain = false;
    noObjectSelection = false;
    try 
        handles.objData.terrain;
    catch
        noTerrain = true;
    end
    if noTerrain
        terrain = handles.metricdata.shape;
    else
        terrain = handles.objData.terrain;
    end
    try 
        handles.object
    catch
        noObjectSelection = true;
    end
    if noObjectSelection
        object = [];
    else
        object = handles.object;
    end
    objects = length(terrain.ncst);
    gcp = handles.options.gcp;
    baak = handles.options.baak;
    camXY = handles.camXY;
    camDir = handles.camDir;
    
    newObject = handles.options.newObject;

    %% plot new view
    hold off
    ax = gcf;
    xAxis = xlim; yAxis = ylim;

    newplot;
    
    objects = objects+1;
    terrain.ncst{objects} = newObject;
    
    for i = 1:objects % objects
        xyTerrain = terrain.ncst{i};
        if ~isempty(xyTerrain)
            patch(xyTerrain(:,1),xyTerrain(:,2),[1 0.35 0.15],'EdgeColor','none');
            hold on;
        end
    end
    
    if ~isempty(object)
        for i = 1:length(object)
            patch(object{i}(:,1),object{i}(:,2), [0 0 0.35],'EdgeColor','none');
        end
    end
    
    for i = 1:size(camXY,1) 
        scatter(camXY(i,1),camXY(i,2),5,'ok','filled');
        line([camXY(i,1); camXY(i,1)+(5*camDir(i,1))], ...
            [camXY(i,2); camXY(i,2)+(5*camDir(i,2))], 'Color','k');
    end

    if ~isempty(baak)
        scatter(baak(1,:), baak(2,:), '.r');
        for i = 1:2:size(baak,1)-1;
            line(baak(i:i+1,1), baak(i:i+1,2));
        end
    end
    if ~isempty(gcp)
        scatter(gcp(:,1),gcp(:,2),6,'+b');
    end

    axis equal; axis tight;

    axis([xAxis yAxis]);
    x = get(gca,'XTick'); set(gca,'XTick',x); set(gca,'XTickLabel',sprintf('%3.0f|',x));
    y = get(gca,'YTick'); set(gca,'YTick',y); set(gca,'YTickLabel',sprintf('%3.0f|',y));
    xlabel('X [m]'); ylabel('Y [m]');
    
    handles.metricdata.shape  = terrain;
    handles.objData.terrain = terrain;
    guidata(hObject,handles);
end

end

% --- Executes on button press in clearObject.
function clearObject_Callback(hObject, eventdata, handles)
% hObject    handle to clearObject (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of clearObject

if get(handles.clearObject, 'Value') == 1
    set(handles.panAction, 'Value', false); pan off;
    set(handles.zoomAction, 'Value', false); zoom off;
    set(handles.rotateCam, 'Value', false);
    set(handles.adjustCam, 'Value', false);
    set(handles.adjustObject, 'Value', false);
    set(handles.addObject, 'Value', false);
    set(handles.bottomBar, 'String', ...
        'Linkermuisknop voor verwijderen van object, rechter muisknop voor beeindiging');

    noTerrain = false;
    noObjectSelection = false;
    try 
        handles.objData.terrain;
    catch
        noTerrain = true;
    end
    if noTerrain
        terrain = handles.metricdata.shape;
    else
        terrain = handles.objData.terrain;
    end
    try 
        handles.object
    catch
        noObjectSelection = true;
    end
    if noObjectSelection
        object = [];
    else
        object = handles.object;
    end

    objects = length(terrain.ncst);
    
    stopDeleting = false;
    while ~stopDeleting
        [xPoly,yPoly,button] = ginput(1);
        if button == 1
            for i = 1:objects % find clicked
                xy = terrain.ncst{i};
                IN = inpolygon(xPoly,yPoly,xy(:,1),xy(:,2));
                if sum(IN)>0
                    subjectID = i;
                end
            end
            if ~isempty(subjectID)
                ncstMat = deal(terrain.ncst);
                if subjectID == 1
                    ncstMat = ncstMat(2:end,:);
                elseif subjectID == size(ncstMat,1)
                    ncstMat = ncstMat(1:end-1,:);
                else
                    ncstMat = ncstMat([1:subjectID-1 subjectID+1:end],:);
                end
                terrain.ncst = ncstMat;
                objects = objects-1;
                
                gcp = handles.options.gcp;
                baak = handles.options.baak;
                camXY = handles.camXY;
                camDir = handles.camDir;

                %% plot new view
                hold off
                ax = gcf;
                xAxis = xlim; yAxis = ylim;

                newplot;

                for i = 1:objects % objects
                xyTerrain = terrain.ncst{i};
                    if ~isempty(xyTerrain)
                        patch(xyTerrain(:,1),xyTerrain(:,2),[1 0.35 0.15],'EdgeColor','none');
                        hold on;
                    end
                end

                if ~isempty(camXY)
                    for i = 1:size(camXY,1) 
                        scatter(camXY(i,1),camXY(i,2),5,'ok','filled');
                        line([camXY(i,1); camXY(i,1)+(5*camDir(i,1))], ...
                        [camXY(i,2); camXY(i,2)+(5*camDir(i,2))], 'Color','k');
                    end
                end

                if ~isempty(baak)
                    scatter(baak(1,:), baak(2,:), '.r');
                    for i = 1:2:size(baak,1)-1;
                        line(baak(i:i+1,1), baak(i:i+1,2));
                    end
                end
                if ~isempty(gcp)
                    scatter(gcp(:,1),gcp(:,2),6,'+b');
                end

                axis equal; axis tight;

                axis([xAxis yAxis]);
                x = get(gca,'XTick'); set(gca,'XTick',x); set(gca,'XTickLabel',sprintf('%3.0f|',x));
                y = get(gca,'YTick'); set(gca,'YTick',y); set(gca,'YTickLabel',sprintf('%3.0f|',y));
                xlabel('X [m]'); ylabel('Y [m]');
                
                handles.objData.terrain = terrain;
                guidata(hObject,handles);
            end
        else
            stopDeleting = true;
            set(handles.clearObject, 'Value', 0);
        end
    end
    
end

end


% --- Executes on button press in adjustObject.
function adjustObject_Callback(hObject, eventdata, handles)
% hObject    handle to adjustObject (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(handles.adjustObject, 'Value') == 1;
    
%     get value, set value, callback
    set(handles.panAction, 'Value', false); pan off;
    set(handles.zoomAction, 'Value', false); zoom off;
    set(handles.rotateCam, 'Value', false);
    set(handles.adjustCam, 'Value', false);
    
    noTerrain = false;
    try 
        handles.objData.terrain;
    catch
        noTerrain = true;
    end
    if noTerrain
        terrain = handles.metricdata.shape;
    else
        terrain = handles.objData.terrain;
    end
   
    objects = length(terrain.ncst);
    
    [subjectID, objectID] = pickObject;
    
    set(gcf,'Pointer','arrow');
    
    h = impoly(gca,[terrain.ncst{subjectID}]);
    api = iptgetapi(h);
    handles.options.newObject = api;
    handles.options.newObjectNumber = subjectID;
    handles.options.newSelectedNumber = objectID;
    guidata(hObject,handles);
else
    noTerrain = false;
    noObjectSelection = false;
    try 
        handles.objData.terrain;
    catch
        noTerrain = true;
    end
    if noTerrain
        terrain = handles.metricdata.shape;
    else
        terrain = handles.objData.terrain;
    end
    try 
        handles.object
    catch
        noObjectSelection = true;
    end
    if noObjectSelection
        object = [];
    else
        object = handles.object;
    end

    objects = length(terrain.ncst);
    gcp = handles.options.gcp;
    baak = handles.options.baak;
    camXY = handles.camXY;
    camDir = handles.camDir;
    
    newObjectApi = handles.options.newObject;
    newObject = newObjectApi.getPosition();
    subjectID = handles.options.newObjectNumber;
    terrain.ncst{subjectID} = newObject;
    if ~noObjectSelection
        objectID = handles.options.newSelectedNumber;    
        object.ncst{objectID} = newObject;
    end
    
    %% plot new view
    hold off
    ax = gcf;
    xAxis = xlim; yAxis = ylim;

    newplot;
    
    for i = 1:objects % objects
        xyTerrain = terrain.ncst{i};
        if ~isempty(xyTerrain)
            patch(xyTerrain(:,1),xyTerrain(:,2),[1 0.35 0.15],'EdgeColor','none');
            hold on;
        end
    end
    
    if ~isempty(object)
        for i = 1:length(handles.object)
            patch(object{i}(:,1),object{i}(:,2), [0 0 0.35],'EdgeColor','none');
        end
    end
    
    for i = 1:size(camXY,1) 
        scatter(camXY(i,1),camXY(i,2),5,'ok','filled');
        line([camXY(i,1); camXY(i,1)+(5*camDir(i,1))], ...
            [camXY(i,2); camXY(i,2)+(5*camDir(i,2))], 'Color','k');
    end

    if ~isempty(baak)
        scatter(baak(1,:), baak(2,:), '.r');
        for i = 1:2:size(baak,1)-1;
            line(baak(i:i+1,1), baak(i:i+1,2));
        end
    end
    if ~isempty(gcp)
        scatter(gcp(:,1),gcp(:,2),6,'+b');
    end

    axis equal; axis tight;

    axis([xAxis yAxis]);
    x = get(gca,'XTick'); set(gca,'XTick',x); set(gca,'XTickLabel',sprintf('%3.0f|',x));
    y = get(gca,'YTick'); set(gca,'YTick',y); set(gca,'YTickLabel',sprintf('%3.0f|',y));
    xlabel('X [m]'); ylabel('Y [m]');
    
    if ~noObjectSelection
        object = handles.object;
    end
    
    handles.objData.terrain = terrain;
    guidata(hObject,handles);
end

    function [subjectID objectID] = pickObject()
        [xPoly,yPoly] = ginput(1);
        set(gcf,'Pointer','watch');
        subjectID = [];
        for i = 1:objects % find clicked
            xy = terrain.ncst{i};
            IN = inpolygon(xPoly,yPoly,xy(:,1),xy(:,2));
            if sum(IN)>0
                subjectID = i;
            end
        end
        if isempty(subjectID)
            subjectID = pickObject();
        end
        
        noObjectSelection = false; % is also a object of interest selected
        try 
            handles.object
        catch
            noObjectSelection = true;
            objectID = [];
        end
        if ~noObjectSelection
            for i = 1:length(handles.object)
                xy = handles.object{i};
                IN = inpolygon(xPoly,yPoly,xy(:,1),xy(:,2));
                if sum(IN)>0
                    objectID = i;
                else
                    objectID = [];
                end
            end
        end

    end

end

% --- Executes on button press in placeMarker.
function placeMarker_Callback(hObject, eventdata, handles)
% hObject    handle to placeMarker (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(handles.placeMarker, 'Value') == 1;
    set(handles.adjustCam, 'Value', false);
    set(handles.panAction, 'Value', false); pan off;
    set(handles.zoomAction, 'Value', false); zoom off;
    set(handles.bottomBar, 'String', ...
        'Linkermuisknop voor toevoegen van camera, middelste muisknop voor verwijderen, rechtermuisknop sluiten');


    GRB = handles.objData.terrain;
    objects = length(GRB.ncst);
    object = handles.object;

    [matRelease matVer] = version;
    %% get centerpoints of objects
    objectLoop = length(object);
    centers = [];
    if datenum(matVer) < datenum([2011 01 01])
        for i = 1:objectLoop
            xy = object{i};
            [vx, vy] = voronoi(xy(1:end-1,1),xy(1:end-1,2));
            voronoiPoints = [vx(1,:)',vy(1,:)'];
            IN = inpolygon(voronoiPoints(:,1),voronoiPoints(:,2),xy(:,1),xy(:,2));
            % selection of centerpoints (only in polygon)
            centers = cat(1,centers, voronoiPoints(IN,:)); 
        end

        clear r
    else
        for i = 1:objectLoop
            xy = object{i};
            DT = DelaunayTri(xy(1:end-1,1),xy(1:end-1,2));
            CC = circumcenters(DT); % centers of triangles
            IN = inpolygon(CC(:,1),CC(:,2),xy(:,1),xy(:,2));
            % selection of centerpoints (only in polygon)
            centers = cat(1,centers, CC(IN,:)); 
        end
    end


    %% pick camera's in mapview
    if datenum(matVer) > datenum([2011 01 01])
        dtCC = DelaunayTri(centers(:,1),centers(:,2));
    end

    camXY = handles.camXY; 
    camDir = handles.camDir;

    set(handles.bottomBar, 'String', 'Linkermuisknop voor camera, rechter muisknop voor laatste camera standplaats');
    finished = 0;
    camAmount = 0;
    while ~finished
        % position of camera
        [xCam,yCam,click] = ginput(1);
        
        if click == 1 % add camera
            camXY = cat(1,camXY, [xCam yCam]);

            % direction of camera
            if length(centers)>4
                if datenum(matVer) < datenum([2011 01 01])
                    delta = centers-repmat([xCam yCam], length(centers),1);
                    [C,K] = min(sqrt((sum(delta.^2,2)).^0.5));
                    centroid = centers(K,:);
                else
                    K = nearestNeighbor(dtCC,[xCam,yCam]);
                    centroid = centers(K,:);
                end
            else
                centroid = mean(centers);
            end
            xDir = centroid(1)-xCam; yDir = centroid(2)-yCam;
            xDirNew = xDir./(xDir.^2 + yDir.^2).^0.5; 
            yDirNew = yDir./(xDir.^2 + yDir.^2).^0.5;
            camDir = cat(1,camDir, [xDirNew yDirNew]);

            scatter(xCam,yCam,5,'ok','filled');
            line([xCam; xCam+(5*xDirNew)], [yCam; yCam+(5*yDirNew)], 'Color','k');
            camAmount = camAmount + 1;
        elseif click == 2
            sizeCam = size(camXY,1)
            if sizeCam>=2
                delta = camXY-repmat([xCam yCam], size(camXY,1),1);
                [C,K] = min(sqrt((sum(delta.^2,2)).^0.5));
                scatter(camXY(K,1),camXY(K,2),6,get(handles.mapWindow, 'Color'),'o','filled');
                line([camXY(K,1); camXY(K,1)+(5*camDir(K,1))], ...
                    [camXY(K,2); camXY(K,2)+(5*camDir(K,2))], 'Color', get(handles.mapWindow, 'Color'));
                if K == sizeCam % clear camera position
                    camXY = camXY(1:end-1,:); 
                    camDir = camDir(1:end-1,:);
                else
                    keep = [1:K-1 K+1:sizeCam];
                    camXY = camXY(keep,:);
                    camDir = camDir(keep,:);
                end
            end
        elseif click == 3
            finished = true;
            set(handles.placeMarker, 'Value', 0)
        end
    end
    handles.camXY = camXY;
    handles.camDir = camDir;
    guidata(hObject,handles);
end
set(handles.verwerk,'Enable','on');
end

% --- Executes on button press in adjustCam.
function adjustCam_Callback(hObject, eventdata, handles)
% hObject    handle to adjustCam (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(handles.adjustCam, 'Value') == 1;
    set(handles.panAction, 'Value', false); pan off;
    set(handles.zoomAction, 'Value', false); zoom off;
    set(handles.rotateCam, 'Value', false);

    camXY = handles.camXY;

    % drag camera postion
    set(gcf, 'WindowButtonMotionFcn',@figButtonMotion);
    
    ax = gcf;
%     set(ax, 'Units', 'normalized');
    point_clicked = 0;
    this_p = camXY(1);
    this_k = 1;

    % convert mapaxis and window axis
    xAxis = xlim; yAxis = ylim;
    deltaX = xAxis(2) - xAxis(1);
    deltaY = yAxis(2) - yAxis(1);
    

    for i = 1:size(camXY,1)
        x{i} = camXY(i,1);
        y{i} = camXY(i,2);
        p(i) = scatter(camXY(i,1), camXY(i,2), 10, [1 0.35 0.15]);
        set(p(i),'UserData',i,'ButtonDownFcn',{@pointButtonDown,p(i)});
    end
else % end of moving
    terrain = handles.objData.terrain;
    objects = length(terrain.ncst);
    object = handles.object;
    gcp = handles.options.gcp;
    baak = handles.options.baak;
    camXY = handles.camXY;
    camDir = handles.camDir;

    %% plot new view
    hold off
    ax = gcf;
    xAxis = xlim; yAxis = ylim;

    newplot;

    for i = 1:objects % objects
        xyTerrain = terrain.ncst{i};
        patch(xyTerrain(:,1),xyTerrain(:,2),[1 0.35 0.15],'EdgeColor','none');
        hold on;
    end
    for i = 1:size(object,2)
        patch(object{i}(:,1),object{i}(:,2), [0 0 0.35],'EdgeColor','none');
    end

    for i = 1:size(camXY,1) 
        scatter(camXY(i,1),camXY(i,2),5,'ok','filled');
        line([camXY(i,1); camXY(i,1)+(5*camDir(i,1))], ...
            [camXY(i,2); camXY(i,2)+(5*camDir(i,2))], 'Color','k');
    end

    if ~isempty(baak)
        scatter(baak(1,:), baak(2,:), '.r');
        for i = 1:2:size(baak,1)-1;
            line(baak(i:i+1,1), baak(i:i+1,2));
        end
    end
    if ~isempty(gcp)
        scatter(gcp(:,1),gcp(:,2),6,'+b');
    end

    axis equal; axis tight;

    axis([xAxis yAxis]);
    x = get(gca,'XTick'); set(gca,'XTick',x); set(gca,'XTickLabel',sprintf('%3.0f|',x));
    y = get(gca,'YTick'); set(gca,'YTick',y); set(gca,'YTickLabel',sprintf('%3.0f|',y));
    xlabel('X [m]'); ylabel('Y [m]');
end

% Function for Identifying Clicked Patch
function pointButtonDown(this,varargin)
    this_p = this;
    point_clicked = ~point_clicked;
end

% Function for Moving Selected camera
function figButtonMotion(varargin)
    if point_clicked
        % Get the mouse Location
        currPt = get(handles.mapWindow, 'CurrentPoint');
       
        % Change the position of the camera
        set(this_p,'XData', currPt(1,1), 'YData', currPt(1,2));
        
        camNr = find(p == this_p);
        camXY(camNr(1),:) = [currPt(1,1) currPt(1,2)];
        handles.camXY = camXY;
        guidata(hObject,handles);
    end
end
end

function rotateCam_Callback(hObject, eventdata, handles)
if get(handles.rotateCam, 'Value') == 1;
    set(handles.adjustCam, 'Value', false);
    set(handles.panAction, 'Value', false); pan off;
    set(handles.zoomAction, 'Value', false); zoom off;
 
    camXY = handles.camXY;
    camDir = handles.camDir;

    % drag camera postion
    set(gcf, 'WindowButtonMotionFcn',@figButtonMotion);

    ax = gcf;
%     set(ax, 'Units', 'normalized');
    point_clicked = 0;
    this_p = camXY(1);
    this_k = 1;

    % convert mapaxis and window axis
    xAxis = xlim; yAxis = ylim;
    deltaX = xAxis(2) - xAxis(1);
    deltaY = yAxis(2) - yAxis(1);
    

    for i = 1:size(camXY,1)
        x{i} = [camXY(i,1); camXY(i,1)+10.*camDir(i,1)];
        y{i} = [camXY(i,2); camXY(i,1)+10.*camDir(i,1)];
        p(i) = line([camXY(i,1) camXY(i,1)+10.*camDir(i,1)], ...
            [camXY(i,2) camXY(i,2)+10.*camDir(i,2)], ...
            'Color', 'k', 'LineWidth', 2);
        set(p(i),'UserData',i,'ButtonDownFcn',{@pointButtonDown,p(i)});
    end
else % end of moving
    terrain = handles.objData.terrain;
    objects = length(terrain.ncst);
    object = handles.object;
    gcp = handles.options.gcp;
    baak = handles.options.baak;
    camXY = handles.camXY;
    camDir = handles.camDir;

    %% plot new view
    hold off
    ax = gcf;
    xAxis = xlim; yAxis = ylim;

    newplot;

    for i = 1:objects % objects
        xyTerrain = terrain.ncst{i};
        patch(xyTerrain(:,1),xyTerrain(:,2),[1 0.35 0.15],'EdgeColor','none');
        hold on;
    end
    for i = 1:size(object,2)
        patch(object{i}(:,1),object{i}(:,2), [0 0 0.35],'EdgeColor','none');
    end

    for i = 1:size(camXY,1) 
        scatter(camXY(i,1),camXY(i,2),5,'ok','filled');
        line([camXY(i,1); camXY(i,1)+(5*camDir(i,1))], ...
            [camXY(i,2); camXY(i,2)+(5*camDir(i,2))], 'Color','k');
    end

    if ~isempty(baak)
        scatter(baak(1,:), baak(2,:), '.r');
        for i = 1:2:size(baak,1)-1;
            line(baak(i:i+1,1), baak(i:i+1,2));
        end
    end
    if ~isempty(gcp)
        scatter(gcp(:,1),gcp(:,2),6,'+b');
    end

    axis equal; axis tight;

    axis([xAxis yAxis]);
    x = get(gca,'XTick'); set(gca,'XTick',x); set(gca,'XTickLabel',sprintf('%3.0f|',x));
    y = get(gca,'YTick'); set(gca,'YTick',y); set(gca,'YTickLabel',sprintf('%3.0f|',y));
    xlabel('X [m]'); ylabel('Y [m]');
end

% Function for Identifying Clicked Patch
function pointButtonDown(this,varargin)
    this_p = this;
    point_clicked = ~point_clicked;
end

% Function for Moving Selected camera
function figButtonMotion(varargin)
    if point_clicked
        % Get the mouse Location
        currPt = get(handles.mapWindow, 'CurrentPoint');
        camNr = find(p == this_p);
       
        % Change the position of the camera
        set(this_p,'XData', [camXY(camNr(1),1) currPt(1,1)], ...
            'YData', [camXY(camNr(1),2) currPt(1,2)]);
        
        xDir = currPt(1,1) - camXY(camNr(1),1); 
        yDir = currPt(1,2) - camXY(camNr(1),2);
        xDirNew = xDir./(xDir.^2 + yDir.^2).^0.5; 
        yDirNew = yDir./(xDir.^2 + yDir.^2).^0.5;
        
        camDir(camNr(1),:) = [xDirNew yDirNew];
        handles.camDir = camDir;
        guidata(hObject,handles);
    end
end

end

%% 3. vereffenen
% --- Executes on selection change in kkMethode.
function kkMethode_Callback(hObject, eventdata, handles)
end

% --- Executes during object creation, after setting all properties.
function kkMethode_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
set(hObject, 'String', {'Vrij netwerk', 'Aangesloten netwerk'});
end

% --- Executes on button press in duidtaan.
function duidtaan_Callback(hObject, eventdata, handles)
% hObject    handle to duidtaan (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(handles.duidtaan, 'Value') == 1;
    set(handles.adjustCam, 'Value', false);
    set(handles.baak, 'Value', false);
    set(handles.panAction, 'Value', false); pan off;
    set(handles.zoomAction, 'Value', false); zoom off;
    set(handles.bottomBar, 'String', ...
        'Linkermuisknop voor toevoegen van paspunt, rechtermuisknop voor laatste paspunt');

    finished = 0;
    gcp = handles.options.gcp;
    while ~finished
        [xGCP, yGCP, button] = ginput(1);
        if button == 1 % add GCP
            scatter(xGCP,yGCP,6,'+b');
            gcp = cat(1, gcp, [xGCP yGCP]);
        elseif button == 2 % delete GCP
            gcpSize = size(gcp,1);
            if gcpSize>=2 % only delete if there are more than 2 gcp's
                delta = gcp-repmat([xGCP yGCP], size(gcp,1),1);
                [C,K] = min(sqrt((sum(delta.^2,2)).^0.5));
                scatter(gcp(K,1),gcp(K,2),6,get(handles.mapWindow, 'Color'),'+');
                if K == gcpSize % clear gcp position
                    gcp = gcp(1:end-1,:); 
                else
                    keep = [1:K-1 K+1:gcpSize];
                    gcp = gcp(keep,:);
                end
            else
                gcp = [];
            end
        elseif button == 3
            finished = true;
            set(handles.duidtaan, 'Value', 0)
        end
        handles.options.gcp = gcp;
        guidata(hObject, handles);
    end
end
end

% --- Executes on button press in baak.
function baak_Callback(hObject, eventdata, handles)
% hObject    handle to baak (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of baak
if get(handles.baak, 'Value') == 1;
    set(handles.adjustCam, 'Value', false);
    set(handles.duidtaan, 'Value', false);
    set(handles.panAction, 'Value', false); pan off;
    set(handles.zoomAction, 'Value', false); zoom off;
    set(handles.bottomBar, 'String', ...
        'Linkermuisknop voor toevoegen van paspunt, rechtermuisknop voor laatste paspunt');

    finished = 0;
    baak = handles.options.baak;
    baakGrote = handles.metricdata.baakGrote;
    
    while ~finished
        [xBaak, yBaak, button] = ginput(1);
        if button == 1 % add Baak
            scatter(xBaak, yBaak, '.r');
            w = waitforbuttonpress;
            if w == 0
                currPt = get(handles.mapWindow, 'CurrentPoint');
                xDir = currPt(1,1) - xBaak; 
                yDir = currPt(1,2) - yBaak;
                xDirNew = baakGrote.*(xDir./(xDir.^2 + yDir.^2).^0.5); 
                yDirNew = baakGrote.*(yDir./(xDir.^2 + yDir.^2).^0.5);
                newBaak = [xBaak yBaak; xBaak+xDirNew yBaak+yDirNew];
                
                scatter(newBaak(2,1), newBaak(2,2), '.r');
                line(newBaak(:,1), newBaak(:,2));
                baak = cat(1, baak, newBaak);
                handles.options.baak = baak;
                guidata(hObject,handles);
            end
        elseif button == 2 % delete Baak
            baakSize = size(baak,1);
            if baakSize>=1 % only delete if there are more than 2 gcp's
                delta = baak-repmat([xBaak yBaak], size(baak,1),1);
                [C,K] = min(sqrt((sum(delta.^2,2)).^0.5));
                scatter(baak(K,1),baak(K,2),6,get(handles.mapWindow, 'Color'),'o', 'filled');
                if K == baakSize || K == baakSize-1% clear gcp position
                    baak = baak(1:end-2,:); 
                else
                    if mod(x,2)==0 %even
                        keep = [1:K-2 K+1:baakSize];
                        baak = baak(keep,:);
                    else
                        keep = [1:K-1 K+2:baakSize];
                        baak = baak(keep,:);
                    end
                end
                handles.options.baak = baak;
                guidata(hObject, handles);
            end
        elseif button == 3
            finished = true;
            set(handles.baak, 'Value', 0)
        end
    end
end
end

function verwerk_Callback(hObject, eventdata, handles)
set(handles.bottomBar, 'String', ...
        'Begin verwerking');

%% viewed
camXY = handles.camXY;
camDir = handles.camDir;
camFov = [];%handles.adjustment.camFov;

object = handles.object;
gcp = handles.options.gcp;
baak = handles.options.baak;

terrain = handles.objData.terrain;
objects = size(terrain.ncst,2);

%% extent point file (xy) if sampling interval is checked
if (handles.metricdata.samplingOnOff) == 1
    set(handles.bottomBar, 'String', ...
        'Toevoegen van extra bemonsteringspunten');
    xyNew = [];
    sampInterval = handles.metricdata.samplingInterval;
    for j = 1:size(object,2)
        xy = object{j};
        for i = 1:length(xy) % for every line find length and interval
            A = i;
            if i == length(xy)
                B = 1;
            else
                B = i + 1;
            end
            dx = xy(A,1) - xy(B,1);
            dy = xy(A,2) - xy(B,2);
            n = floor(((dx.^2+dy.^2).^0.5)./sampInterval);
            if n == 0
                dots = [xy(B,1) xy(B,2) j];
            else
                if i == 1
                    dots = [xy(B,1)+(0:n)'.*(dx./n) xy(B,2)+(0:n)'.*(dy./n) repmat(j, n+1, 1)];
                else
                    dots = flipud([xy(B,1)+(1:n)'.*(dx./n) xy(B,2)+(1:n)'.*(dy./n) repmat(j, n, 1)]);
                end
            end
            xyNew = cat(1, xyNew, dots);
        end
    end
    xy = xyNew;
else
    xy = [];
    objectsAmount = size(object,2);
    for i = 1:objectsAmount
        obj = object{i};
        xy = cat(1, xy, [obj(1:end-1,:) repmat(i, size(obj,1)-1, 1)]);
    end
end

%% find the field of view from all camera's
if get(handles.visualRod, 'Value')==1
    fov = 2.*pi();
else
    fov = atan(((handles.metricdata.resolution2.* ...
        handles.metricdata.pixelsize .* 0.001) ./ ...
        handles.metricdata.focal)).*2;
end
set(handles.bottomBar, 'String', ...
    'Uitrekenen wat het gezichtsveld van de camera is');

camAmount = size(camXY,1);
for i = 1:camAmount
    [theta rho] = cart2pol(camDir(i,1),camDir(i,2));
    thetaPlus = theta + fov./2;
    thetaMinus = theta - fov./2;
    [xCamMax yCamMax] = pol2cart(thetaPlus, rho);
    [xCamMin yCamMin] = pol2cart(thetaMinus, rho);
    camFov = cat(1,camFov, [xCamMin yCamMin xCamMax yCamMax]);
    
    line([camXY(i,1); camXY(i,1)+(10*xCamMin)], ...
        [camXY(i,2); camXY(i,2)+(10*yCamMin)], 'Color','r');
        line([camXY(i,1); camXY(i,1)+(10*xCamMax)], ...
        [camXY(i,2); camXY(i,2)+(10*yCamMax)], 'Color','b');
end


%% viewed of camera's with objects
set(handles.bottomBar, 'String', ...
    'Vind zichtbaarheid van punten op de verschillende camera''s');

h = waitbar(0,'Bereken zichtbaarheid...');
totalSteps = camAmount.*size(xy,1);
A = zeros(camAmount,size(xy,1)); % design matrix
for i = 1:camAmount;
    for j = 1:size(xy,1);
        waitbar(((i-1)*size(xy,1)+j)/totalSteps)
        inView = 0;
        xDir = xy(j,1)-camXY(i,1); 
        yDir = xy(j,2)-camXY(i,2);

        if det([0 0 1; camFov(i,1) camFov(i,2) 1; xDir yDir 1])>=0 == det([0 0 1; camFov(i,1) camFov(i,2) 1; camFov(i,3) camFov(i,4) 1])>=0
            if det([0 0 1; camFov(i,3) camFov(i,4) 1; xDir yDir 1])>=0 == det([0 0 1; camFov(i,3) camFov(i,4) 1; camFov(i,1) camFov(i,2) 1])>=0
                inView = 1;
            end
        end
        
        if inView == 1; %% in direct view
            
            Obj = repmat(xy(j,3), size(xy,1), 1)==xy(:,3);
            xyObj = xy(Obj,:);
            
            [xi, yi] = polyxpoly(xyObj(:,1), xyObj(:,2), ...
                [xy(j,1) camXY(i,1)],[xy(j,2) camXY(i,2)]);
            duplicate = sum((round(xi.*10))./10 == (round(xy(j,1).*10))./10);
            crossings = length(xi) - duplicate;
        
            if crossings == 0 %% not obstructed by others
                obstruction = 0;
                for k = 1:objects
                    uv = terrain.ncst{k};
                    [xi, yi] = polyxpoly(uv(:,1), uv(:,2), ...
                        [xy(j,1) camXY(i,1)],[xy(j,2) camXY(i,2)]);
                    duplicate = sum((round(xi.*10))./10 == (round(xy(j,1).*10))./10);
                    crossing = length(xi) - duplicate;
                    obstruction = obstruction + crossing;
                end
                if obstruction == 0
                    A(i,j) = 1;
                end
            end
        end
    end
end
close(h)
clear rho
%% find ground control points
set(handles.bottomBar, 'String', ...
    'Vind zichtbaarheid van paspunten op de verschillende camera''s');

if ~isempty(gcp);
    Agcp = zeros(size(camXY,1),size(gcp,1)); % design matrix
    h = waitbar(0,'Bereken zichtbaarheid paspunten...');
    totalSteps = length(Agcp).*size(gcp,1);

    for i = 1:length(Agcp);
        [thetaPlus rho] = cart2pol(camFov(i,1),camFov(i,2));
        [thetaMinus rho] = cart2pol(camFov(i,3),camFov(i,4));
        for j = 1:size(gcp,1);
            inView = 0;
            waitbar(((i-1)*length(Agcp)+j)/totalSteps)
            xDir = gcp(j,1)-camXY(i,1); 
            yDir = gcp(j,2)-camXY(i,2);
            xDirNew = xDir./(xDir.^2 + yDir.^2).^0.5; 
            yDirNew = yDir./(yDir.^2 + yDir.^2).^0.5;

            [theta rho] = cart2pol(xDirNew,yDirNew);
            if thetaMinus < thetaPlus %% seen through lens
                if thetaMinus<theta && theta<thetaPlus; inView = 1; end
            else           
                if thetaMinus>theta && theta>thetaPlus; inView = 1; end
            end
            if inView == 1; %% in direct view
                [xi, yi] = polyxpoly(xy(:,1), xy(:,2), ...
                    [gcp(j,1) camXY(i,1)],[gcp(j,2) camXY(i,2)]);
                duplicate = sum((round(xi.*10))./10 == (round(xy(j,1).*10))./10);
                crossings = length(xi) - duplicate;

                if crossings == 0 %% not obstructed by others
                    obstruction = 0;
                    for k = 1:objects
                        uv = terrain.ncst{k};
                        [xi, yi] = polyxpoly(uv(:,1), uv(:,2), ...
                            [gcp(j,1) camXY(i,1)],[gcp(j,2) camXY(i,2)]);
                        duplicate = sum((round(xi.*10))./10 == (round(xy(j,1).*10))./10);
                        crossing = length(xi) - duplicate;
                        obstruction = obstruction + crossing;
                    end
                    if obstruction == 0
                        Agcp(i,j) = 1;
                    end
                end
            end
        end
    end
    handles.options.Agcp = Agcp;
    close(h);
end
clear rho

%% find baak placements in photo's
set(handles.bottomBar, 'String', ...
    'Vind zichtbaarheid van baken op de verschillende camera''s');
if ~isempty(baak)
    Abaak = zeros(size(camXY,1),size(baak,1)); % design matrix
    
    h = waitbar(0,'Bereken zichtbaarheid paspunten...');
    totalSteps = length(Abaak).*size(baak,1);
    for i = 1:length(Abaak);
        [thetaPlus rho] = cart2pol(camFov(i,1),camFov(i,2));
        [thetaMinus rho] = cart2pol(camFov(i,3),camFov(i,4));
        for j = 1:size(baak,1);
            waitbar((i+j)/totalSteps)
            inView = 0;
            xDir = baak(j,1)-camXY(i,1); 
            yDir = baak(j,2)-camXY(i,2);
            xDirNew = xDir./(xDir.^2 + yDir.^2).^0.5; 
            yDirNew = yDir./(yDir.^2 + yDir.^2).^0.5;

            [theta rho] = cart2pol(xDirNew,yDirNew);
            if thetaMinus < thetaPlus %% seen through lens
                if thetaMinus<theta && theta<thetaPlus; inView = 1; end
            else           
                if thetaMinus>theta && theta>thetaPlus; inView = 1; end
            end
            if inView == 1; %% in direct view
                [xi, yi] = polyxpoly(xy(:,1), xy(:,2), ...
                    [baak(j,1) camXY(i,1)],[baak(j,2) camXY(i,2)]);
                duplicate = sum((round(xi.*10))./10 == (round(xy(j,1).*10))./10);
                crossings = length(xi) - duplicate;

                if crossings == 0 %% not obstructed by others
                    obstruction = 0;
                    for k = 1:objects
                        uv = terrain.ncst{k};
                        [xi, yi] = polyxpoly(uv(:,1), uv(:,2), ...
                            [baak(j,1) camXY(i,1)],[baak(j,2) camXY(i,2)]);
                        duplicate = sum((round(xi.*10))./10 == (round(xy(j,1).*10))./10);
                        crossing = length(xi) - duplicate;
                        obstruction = obstruction + crossing;
                    end
                    if obstruction == 0
                        Abaak(i,j) = 1;
                    end
                end
            end
        end
    end
    handles.options.Abaak = Abaak;
    close(h);
end
clear rho

%% plot new view
hold off
ax = gcf;
xAxis = xlim; yAxis = ylim;

newplot;

for i = 1:objects % objects
    xyTerrain = terrain.ncst{i};
    patch(xyTerrain(:,1),xyTerrain(:,2),[1 0.35 0.15],'EdgeColor','none');
    hold on;
end
for i = 1:size(object,2)
    patch(object{i}(:,1),object{i}(:,2), [0 0 0.35],'EdgeColor','none');
end
for i = 1:camAmount 
    line([camXY(i,1); camXY(i,1)+(10*camFov(i,1))], ...
        [camXY(i,2); camXY(i,2)+(10*camFov(i,2))], 'Color','r');
        line([camXY(i,1); camXY(i,1)+(10*camFov(i,3))], ...
        [camXY(i,2); camXY(i,2)+(10*camFov(i,4))], 'Color','b');
end
if ~isempty(baak)
    scatter(baak(1,:), baak(2,:), '.r');
    for i = 1:2:size(baak,1)-1;
        line(baak(i:i+1,1), baak(i:i+1,2));
    end
end
if ~isempty(gcp)
    scatter(gcp(:,1),gcp(:,2),6,'+b');
end

axis equal; axis tight;

axis([xAxis yAxis]);
x = get(gca,'XTick'); set(gca,'XTick',x); set(gca,'XTickLabel',sprintf('%3.0f|',x));
y = get(gca,'YTick'); set(gca,'YTick',y); set(gca,'YTickLabel',sprintf('%3.0f|',y));
xlabel('X [m]'); ylabel('Y [m]');

%% clean up and save
set(handles.bottomBar, 'String', ...
        'Klaar met verwerken');

set(handles.illustreer,'Enable','on');
set(handles.move3,'Enable','on');
set(handles.export,'Enable','on');

handles.adjustment.A = A;
handles.objData.xy = xy;
guidata(hObject, handles);
end

% --- Executes on button press in move3.
function move3_Callback(hObject, eventdata, handles)
% hObject    handle to move3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[file,path] = uiputfile({'*.prj','Move3 project'}, 'Voer ontwerp uit');
fileName = strcat(path,file);
workDir = cd;
cd(path);
obsMove3(handles, file);
ctoMove3(handles, file);
prjMove3(handles, file);
cd(workDir);
end

function obsMove3(handles, file)
% convert data to .obs Move3 format
%
% B. Altena - Sep. 2013

camXY = handles.camXY;
keyXY = handles.objData.xy; 
A = handles.adjustment.A;
camDir = handles.camDir;
focal = handles.metricdata.focal;

gcpUse = ~isempty(handles.options.gcp);
baakUse = ~isempty(handles.options.baak);

% radUse = true; % radiale vertekening meenemen
% planUse = true; % planimetrische vertekening meenemen


fid = fopen([file(1:end-4) '.obs'], 'wt');
formatSpec = '%s\t%s\t%s\t%1.4f\t%1.4f\t%s\t%3.5f\t%1.5f\t%1.5f\t%s \n';
formatSpec2 = '%s\t%s\t%s\t%s\t%1.5f\t%1.5f \n';
formatSpec3 = '%s\t%s\t%s\t%1.4f\t%1.4f\t%s\t%3.5f%s\t%1.5f\t%1.5f\t%s \n';

fprintf(fid,'%s\n','MOVE3 V4.2 OBS file');
fprintf(fid,'%s\n','$'); 
fprintf(fid,'%s\n',upper(file(1:end-4))); 
fprintf(fid,'%s\n','$');
fprintf(fid,'%s\n','ANGLEUNIT GON'); 
fprintf(fid,'%s\n','$');
fprintf(fid,'%s\n','');

% 1. observation type -> TS total station
% 2. from -> image name of observation
% 3. to -> keypoints
% 4. instrumental height -> none
% 5. vertical height above known point -> none
% 6. azimuth
% 7. direction -> x of keypoint
% 8. St Afw Abs -> std camera calibration
% 9. St Afw Rel -> std camera calibration in relation to position
% 10. Dimension of network

obsType = 'TS'; % 1
obsType2 = 'GR';
instrHgt = 0; % 4
gcpHgt = 0; % 5
azi = 'R0'; % 6
% argu = 'A0'; % 6b
dim = '2D'; % 10
meetType = 'TD';

from = 3000;
to = 2000;

if isempty(handles.options.gcp);
    gcpUse = false;
else
    gcpUse = true;
end

if isempty(handles.options.baak);
    baakUse = false;
else
    baakUse = true;
end

if handles.options.radialdistortion == true
    if handles.metrisch == true
        stdRad = 1;
    elseif handles.spiegelReflex == true
        stdRad = 1;
    elseif handles.consument == true
        stdRad = 1;
    end
else
    stdRad = 0;
end

if handles.options.radialdistortion == true
    if handles.metrisch == true
        stdRad = 1;
    elseif handles.spiegelReflex == true
        stdRad = 1;
    elseif handles.consument == true
        stdRad = 1;
    end
else
    stdRad = 0;
end

BlurdX = 1; % blur is one pixel

if sum(A(:))> 0
%     argIDX = argumentSearch(A);
    obs = find(A ==1);
    twoViews = sum(A,1)>1;
    for k = 1:length(obs)
        [i j] = ind2sub([size(A,1) size(A,2)], obs(k));
       
        stat = camXY(i,:);   
        pnt = keyXY(j,:);

        deltaX = pnt(1) - stat(1);
        deltaY = pnt(2) - stat(2);
        [azimuth rho] = cart2gon(deltaX, deltaY);
        [azimuth0 rho] = cart2gon(camDir(i,1),camDir(i,2));
        
        azimuth = azimuth - azimuth0;
        if azimuth < 0
            azimuth = azimuth + 400;
        end
        
        stdPix = handles.metricdata.pixelsize .* 1e-3 .* ...
            handles.metricdata.sigma; % [mm]
        dX = tand(azimuth*360/400)*focal; % [mm]
        
        [minAzimuth rho] = cart2gon(dX+stdPix, focal);
        [maxAzimuth rho] = cart2gon(dX-stdPix, focal);
        
%         [minRadAzimuth rho] = cart2gon(stdRad*dX+stdPix, focal);
%         [maxRadAzimuth rho] = cart2gon(stdRad*dX-stdPix, focal);
        
        if handles.options.blur == 1
            [minBlurAzimuth rho] = cart2gon(BlurdX+stdPix, focal);
            [maxBlurAzimuth rho] = cart2gon(BlurdX-stdPix, focal);
            stdXabs = sqrt((maxAzimuth - minAzimuth).^2+(maxBlurAzimuth - minBlurAzimuth).^2);
        else    
            stdXabs = abs(maxAzimuth - minAzimuth); % 8
        end
        stdXrel = 0; % 9
        
        if twoViews(j) % seen by at least two camera's
            fprintf(fid, formatSpec, ...
                obsType, num2str(from + i), num2str(to + j), instrHgt, gcpHgt, ...
                azi, azimuth, stdXabs, stdXrel, dim);
        else
            fprintf(fid, formatSpec3, ...
                obsType, num2str(from + i), num2str(to + j), instrHgt, gcpHgt, ...
                azi, azimuth, '#', stdXabs, stdXrel, dim);
        end
%         if ~isempty(find(obs(k)==argIDX))
%             fprintf(fid, formatSpec, ...
%                 obsType2, num2str(from + i), num2str(to + j), instrHgt, gcpHgt, ...
%                 argu, azimuth0, 0.00001, 0.00000, dim);
%         end
    end
end
    
if gcpUse
    gcp = handles.options.gcp;
    Agcp = handles.options.Agcp;
    from = 3000;
    to = 4000;
    if sum(Agcp(:))> 0
        obs = find(Agcp ==1);
        for k = 1:length(obs)
            [i j] = ind2sub([size(Agcp,1) size(Agcp,2)], obs(k));

            pnt = gcp(j,:);
            stat = camXY(i,:);   

            deltaX = pnt(1) - stat(1);
            deltaY = pnt(2) - stat(2);
            [azimuth0 rho] = cart2gon(camDir(i,1),camDir(i,2));

            [azimuth rho] = cart2gon(deltaX, deltaY);

            azimuth = azimuth - azimuth0;
            if azimuth < 0
                azimuth = azimuth + 400;
            end

            stdPix = handles.metricdata.pixelsize .* 1e-3 .* ...
                handles.metricdata.sigma; % [mm]
            dX = tand(azimuth*360/400)*focal; % [mm]
            [minAzimuth rho] = cart2gon(dX+stdPix, focal);
            [maxAzimuth rho] = cart2gon(dX-stdPix, focal);

            if handles.options.blur == 1
                [minBlurAzimuth rho] = cart2gon(BlurdX+stdPix, focal);
                [maxBlurAzimuth rho] = cart2gon(BlurdX-stdPix, focal);
                stdXabs = sqrt((maxAzimuth - minAzimuth).^2+(maxBlurAzimuth - minBlurAzimuth).^2);
            else    
                stdXabs = abs(maxAzimuth - minAzimuth); % 8
            end
            
            stdXrel = 0; % 9

            fprintf(fid, formatSpec, ...
                obsType, num2str(from + i), num2str(to + j), instrHgt, gcpHgt, ...
                azi, azimuth, stdXabs, stdXrel, dim);
        end
    end
end

if baakUse
    baak = handles.options.baak;
    Abaak = handles.options.Abaak;
    baakGrote = handles.metricdata.baakGrote;
    from = 3000;
    to = 5000;
    if sum(Abaak(:))> 0
        obs = find(Abaak ==1);
        for k = 1:length(obs)
            [i j] = ind2sub([size(Abaak,1) size(Abaak,2)], obs(k));

            pnt = baak(j,:);
            stat = camXY(i,:);   

            deltaX = pnt(1) - stat(1);
            deltaY = pnt(2) - stat(2);
            [azimuth0 rho] = cart2gon(camDir(i,1),camDir(i,2));

            [azimuth rho] = cart2gon(deltaX, deltaY);

            azimuth = azimuth - azimuth0;
            if azimuth < 0
                azimuth = azimuth + 400;
            end

            stdPix = handles.metricdata.pixelsize .* 1e-3 .* ...
                handles.metricdata.sigma; % [mm]
            dX = tand(azimuth*360/400)*focal; % [mm]

            [minAzimuth rho] = cart2gon(dX+stdPix, focal);
            [maxAzimuth rho] = cart2gon(dX-stdPix, focal);

            if handles.options.blur == 1
                [minBlurAzimuth rho] = cart2gon(BlurdX+stdPix, focal);
                [maxBlurAzimuth rho] = cart2gon(BlurdX-stdPix, focal);
                stdXabs = sqrt((maxAzimuth - minAzimuth).^2+(maxBlurAzimuth - minBlurAzimuth).^2);
            else    
                stdXabs = abs(maxAzimuth - minAzimuth); % 8
            end
            stdXrel = 0; % 9
            
            fprintf(fid, formatSpec, ...
                obsType, num2str(from + i), num2str(to + j), instrHgt, gcpHgt, ...
                azi, azimuth, stdXabs, stdXrel, dim);
        end
        to = 5000;
        for k = 1:size(baak,1)/2
            fprintf(fid, formatSpec2, ...
                obsType2, num2str(to + 1), num2str(to + 2), meetType, ...
                baakGrote, 0.001);
            to = to + 2;
        end
    end
end

fprintf(fid,'%s','$');
fclose(fid);
end

function ctoMove3(handles, file)
% convert data to .cto Move3 format
%
% B. Altena - Sep. 2013

camXY = handles.camXY;
keyXY = handles.objData.xy;
A = handles.adjustment.A;

if isempty(handles.options.gcp);
    gcpUse = false;
else
    gcpUse = true;
end

if isempty(handles.options.baak);
    baakUse = false;
else
    baakUse = true;
end

fid = fopen([file(1:end-4) '.tco'], 'wt');
formatSpec = '%s\t%0.4f%s\t%0.4f%s\t%0.4f\t%0.4f\t%0.4f\t%0.4f\n';
formatSpec2 = '    %s\t%0.4f\t%0.4f\t%0.4f\n';
formatSpec3 = '%s\t%0.4f%s\t%0.4f%s\t%0.4f\t%0.4f\t%0.4f\t%0.4f\n';
formatSpec4 = '%s   %s\t%0.4f\t%0.4f\t%0.4f\n';

fprintf(fid,'%s\n','MOVE3 V4.2 TCO file');
fprintf(fid,'%s\n','$'); 
fprintf(fid,'%s\n',upper(file(1:end-4))); 
fprintf(fid,'%s\n','$');
if ~isempty(strfind(handles.epsg.projName, 'Stereographic'))
    fprintf(fid,'%s %s\n', 'PROJECTION', 'STEREOGRAPHIC');
elseif~isempty(strfind(handles.epsg.projName, 'Mercator'))
    fprintf(fid,'%s %s\n', 'PROJECTION', 'TM');
elseif ~isempty(strfind(handles.epsg.projName, 'Lambert')) && ...
        (handles.epsg.StandPar2 == 0)
    fprintf(fid,'%s %s\n', 'PROJECTION', 'LAMBERT1'); % 1 parallel
elseif ~isempty(strfind(handles.epsg.projName, 'Lambert')) && ...
        (handles.epsg.StandPar2 ~= 0)
    fprintf(fid,'%s %s\n', 'PROJECTION', 'LAMBERT2'); % 2 parallels
else
    fprintf(fid,'%s t%s\n', 'PROJECTION', 'LOCAL');
end
fprintf(fid,'%s\n','$');
fprintf(fid,'%s\n','');

from = 3000;
if get(handles.toggleStation, 'Value')==1
    for i = 1:length(camXY) % known imaging stations acquisitions
        tsSigma = str2num(num2str(get(handles.Opstel, 'String'))).*1e-2;
        fprintf(fid, formatSpec3, ...
                num2str(from + i), ...
                camXY(i,1), '*', ...
                camXY(i,2), '*', ...
                0, ...
                tsSigma, tsSigma, 0);
    end
else
    for i = 1:size(camXY,1) % known camera acquisitions
        fprintf(fid, formatSpec2, ...
                num2str(from + i), ...
                camXY(i,1), ...
                camXY(i,2), ...
                0);
    end
end

twoViews = sum(A,1)>1;
% oneView = and(A,repmat(~twoViews, size(A,1),1));
% oneKeyView = oneView(A(:)==1);
oneKeyView = and(sum(A,1)>0,~twoViews);

to = 2000;
for i = 1:size(keyXY,1) % unknown keypoints
    if oneKeyView(i)
        fprintf(fid, formatSpec2, ...
                num2str(to + i), ...
                keyXY(i,1), ...
                keyXY(i,2), ...
                0);
    else
        fprintf(fid, formatSpec4, ...
            '#', ...
            num2str(to + i), ...
            keyXY(i,1), ...
            keyXY(i,2), ...
            0);
    end
end

if gcpUse
    gcp = handles.options.gcp;
    Agcp = handles.options.Agcp;
    to = 4000;
    if sum(Agcp(:))> 0
        gcpSigma = str2double(get(handles.GCP, 'String')).*1e-2;
        for i = 1:length(gcp) % unknown keypoints
            fprintf(fid, formatSpec3, ...
                num2str(to + i), ...
                gcp(i,1), '*', ...
                gcp(i,2), '*', ...
                0, ...
                gcpSigma, gcpSigma, 0);
        end
    end
end

if baakUse
    to = 5000;
    baak = handles.options.baak;
    for i = 1:size(baak,1) % unknown baakpoints
        fprintf(fid, formatSpec2, ...
                num2str(to + i), ...
                baak(i,1), ...
                baak(i,2), ...
                0);
    end
end

fprintf(fid,'%s','$');
fclose(fid);
end

function prjMove3(handles, file)
% make .prj file
% 

if isempty(handles.options.gcp);
    gcpUse = false;
else
    gcpUse = true;
end

fid = fopen([file(1:end-4) '.prj'], 'wt');

fprintf(fid,'%s\n','MOVE3 V4.2.0    PRJ file');
fprintf(fid,'%s\n','$');
fprintf(fid,'%s\n', file(1:end-4));
fprintf(fid,'%s\n', '$ INCLUDED FILE TYPES');
fprintf(fid,'%s\t%s\n', 'TerCoord', 'YES');
% if gcpUse
%     fprintf(fid,'%s\t%s\n', 'GPSCoord', 'YES');
% else
fprintf(fid,'%s\t%s\n', 'GPSCoord', 'NO');
% end
fprintf(fid,'%s\t%s\n', 'TerObserv', 'YES');
% if gcpUse
%     fprintf(fid,'%s\t%s\n', 'GPSObserv', 'YES');
% else
fprintf(fid,'%s\t%s\n', 'GPSObserv', 'NO');
% end
fprintf(fid,'%s\t%s\n', 'GeoidModel', 'NO');
fprintf(fid,'%s\t%s\n', 'FeatureCode', 'NONE');
fprintf(fid,'%s\n', '$ GEOMETRY PARAMETERS');
fprintf(fid,'%s\t%s\n', 'Dimension', '2');

if ~isempty(strfind(handles.epsg.projName, 'Stereographic'))
    fprintf(fid,'%s\t%s\n', 'Projection', 'STEREOGRAPHIC');
elseif~isempty(strfind(handles.epsg.projName, 'Mercator'))
    fprintf(fid,'%s\t%s\n', 'Projection', 'TM');
elseif ~isempty(strfind(handles.epsg.projName, 'Lambert')) && ...
        (handles.epsg.StandPar2 == 0)
    fprintf(fid,'%s\t%s\n', 'Projection', 'LAMBERT1'); % 1 parallel
elseif ~isempty(strfind(handles.epsg.projName, 'Lambert')) && ...
        (handles.epsg.StandPar2 ~= 0)
    fprintf(fid,'%s\t%s\n', 'Projection', 'LAMBERT2'); % 2 parallels
else
    fprintf(fid,'%s\t%s\n', 'Projection', 'LOCAL');
end
% fprintf(fid,'%s\t%s\n', 'Projection', 'LAMBERT72');
fprintf(fid,'%s\t%s\n', 'ProjName', ['EPSG' num2str(handles.epsg.EPSG)]);
LonOrigi = handles.epsg.LonOriginCM;
if LonOrigi<0
    LonOrigi = LonOrigi + 360;
end
% angles in degrees minutes seconds
dms = degrees2dms(LonOrigi);
fprintf(fid,'%s\t%u %u %f\n', 'LonOriginCM', dms(1), dms(2), dms(3));
dms = degrees2dms(handles.epsg.LatOrigin);
fprintf(fid,'%s\t%u %u %f\n', 'LatOrigin',  dms(1), dms(2), dms(3));
dms = degrees2dms(handles.epsg.StandPar1);
fprintf(fid,'%s\t%u %u %f\n', 'StandPar1',  dms(1), dms(2), dms(3));
dms = degrees2dms(handles.epsg.StandPar2);
fprintf(fid,'%s\t%u %u %f\n', 'StandPar2',  dms(1), dms(2), dms(3));
fprintf(fid,'%s\t%1.9f\n', 'ProjScaleFac', handles.epsg.ProjScaleFac);
fprintf(fid,'%s\t%8.5f\n', 'FalseEasting', handles.epsg.FalseEasting );
fprintf(fid,'%s\t%8.5f\n', 'FalseNorthing', handles.epsg.FalseNorthing );
fprintf(fid,'%s\t%s\n', 'Ellipsoid', 'USER');
fprintf(fid,'%s\t%8.9f\n', 'SemiMajAx', handles.epsg.SemiMajAx );
fprintf(fid,'%s\t%3.9f\n', 'InvFlatt', handles.epsg.InvFlatt );
fprintf(fid,'%s\t%s\n', 'TransProj', 'NONE');
fprintf(fid,'%s\t%s\n', 'GPSCoordType', 'XYZ');
fprintf(fid,'%s\n', '$ ADJUSTMENT PARAMETERS');
fprintf(fid,'%s\t%s\n', 'AdjDesign', 'DESIGN');
if gcpUse
    fprintf(fid,'%s\t%s\n', 'Phase', '3');
else
    fprintf(fid,'%s\t%s\n', 'Phase', '1');
end
fprintf(fid,'%s\t%s\n', 'InnerConstraint', 'FALSE');
fprintf(fid,'%s\t%s\n', 'AngleUnit', 'GON');
fprintf(fid,'%s\t%s\n', 'LinearUnit', 'm  1.0000000000000  METER');
fprintf(fid,'%s\t%s\n', 'IterMax', '3');
fprintf(fid,'%s\t%s\n', 'Epsilon', '0.0001');
fprintf(fid,'%s\t%s\n', 'Delta', '1.0e-006');
fprintf(fid,'%s\t%s\n', 'CovMatrix', 'NONE');
fprintf(fid,'%s\t%s\n', 'APostVarFac', 'DONOT');
fprintf(fid,'%s\t%s\n', 'VarComponent', 'NONE');
fprintf(fid,'%s\t%s\n', 'VarIterMax', '5');
fprintf(fid,'%s\t%s\n', 'VarEpsilon', '0.0100');
fprintf(fid,'%s\t%s\n', 'FilterFreeStations', 'FALSE');
fprintf(fid,'%s\t%s\n', 'EstAddTrf', 'FREENET');
fprintf(fid,'%s\n', '$ PRECISION AND TESTING PARAMETERS');
fprintf(fid,'%s\t%s\n', 'Sigma0', '1.0e+000');
fprintf(fid,'%s\t%s\n', 'Alfa0', '0.0010');
fprintf(fid,'%s\t%s\n', 'Beta', '0.80');
fprintf(fid,'%s\t%s\n', 'ConfidenceLevel1D', '0.683');
fprintf(fid,'%s\t%s\n', 'ConfidenceLevel2D', '0.394');
fprintf(fid,'%s\t%s\n', 'C0', '0.000');
fprintf(fid,'%s\t%s\n', 'C1', '1.000');
fprintf(fid,'%s\n', '$ DEFAULT STANDARD DEVIATIONS');
fprintf(fid,'%s\t%s\n', 'SigmaAbsR', '0.00100'); % variabel niet?
fprintf(fid,'%s\t%s\n', 'SigmaRelR', '0.00000');
fprintf(fid,'%s\t%s\n', 'SigmaAbsS', '0.0100');
fprintf(fid,'%s\t%s\n', 'SigmaRelS', '0.0');
fprintf(fid,'%s\t%s\n', 'SigmaAbsZ', '0.00100');
fprintf(fid,'%s\t%s\n', 'SigmaRelZ', '0.00000');
fprintf(fid,'%s\t%s\n', 'SigmaAbsA', '0.00100');
fprintf(fid,'%s\t%s\n', 'SigmaRelA', '0.00000');
fprintf(fid,'%s\t%s\n', 'SigmaDHA', '0.00');
fprintf(fid,'%s\t%s\n', 'SigmaDHB', '1.00');
fprintf(fid,'%s\t%s\n', 'SigmaDHC', '0.00');
fprintf(fid,'%s\t%s\n', 'SigmaAbsDX', '0.0100');
fprintf(fid,'%s\t%s\n', 'SigmaRelDX', '1.0');
fprintf(fid,'%s\t%s\n', 'SigmaAbsX', '0.0100');
fprintf(fid,'%s\t%s\n', 'SigmaLatLon', '0.0100');
fprintf(fid,'%s\t%s\n', 'SigmaHgt', '0.01000');
fprintf(fid,'%s\t%s\n', 'SigmaXYZ', '0.0100');% variabel niet?
fprintf(fid,'%s\t%s\n', 'SigmaCentr', '0.0000');
fprintf(fid,'%s\t%s\n', 'SigmaInstr', '0.00000');
fprintf(fid,'%s\t%s\n', 'SigmaTape', '0.0100');
fprintf(fid,'%s\t%s\n', 'SigmaOrthogonal', '0.0100');% variabel niet?
fprintf(fid,'%s\t%s\n', 'SigmaDistLine', '0.0150');
fprintf(fid,'%s\t%s\n', 'SigmaAngle', '0.10000');
fprintf(fid,'%s\t%s\n', 'SigmaIdealXY', '0.0000');% variabel niet?
fprintf(fid,'%s\t%s\n', 'SigmaIdealH', '0.0000');
fprintf(fid,'%s\n', '$ ADDITIONAL PARAMETERS');
fprintf(fid,'%s\t%s\t%1.7f\n', 'ScaleFac0', 'FREE', 1);
fprintf(fid,'%s\t%s\t%1.7f\n', 'ScaleFac1', 'FREE', 1);
fprintf(fid,'%s\t%s\t%1.7f\n', 'ScaleFac2', 'FREE', 1);
fprintf(fid,'%s\t%s\t%1.7f\n', 'ScaleFac3', 'FREE', 1);
fprintf(fid,'%s\t%s\t%1.7f\n', 'ScaleFac4', 'FREE', 1);
fprintf(fid,'%s\t%s\t%1.7f\n', 'ScaleFac5', 'FREE', 1);
fprintf(fid,'%s\t%s\t%1.7f\n', 'ScaleFac6', 'FREE', 1);
fprintf(fid,'%s\t%s\t%1.7f\n', 'ScaleFac7', 'FREE', 1);
fprintf(fid,'%s\t%s\t%1.7f\n', 'ScaleFac8', 'FREE', 1);
fprintf(fid,'%s\t%s\t%1.7f\n', 'ScaleFac9', 'FREE', 1);
fprintf(fid,'%s\t%s\t%1.3f\n', 'VertRefr0', 'FIXED', 0.13);
fprintf(fid,'%s\t%s\t%1.3f\n', 'VertRefr1', 'FIXED', 0.13);
fprintf(fid,'%s\t%s\t%1.3f\n', 'VertRefr2', 'FIXED', 0.13);
fprintf(fid,'%s\t%s\t%1.3f\n', 'VertRefr3', 'FIXED', 0.13);
fprintf(fid,'%s\t%s\t%1.3f\n', 'VertRefr4','FIXED', 0.13);
fprintf(fid,'%s\t%s\t%1.3f\n', 'VertRefr5','FIXED', 0.13);
fprintf(fid,'%s\t%s\t%1.3f\n', 'VertRefr6','FIXED', 0.13);
fprintf(fid,'%s\t%s\t%1.3f\n', 'VertRefr7','FIXED', 0.13);
fprintf(fid,'%s\t%s\t%1.3f\n', 'VertRefr8','FIXED', 0.13);
fprintf(fid,'%s\t%s\t%1.3f\n', 'VertRefr9','FIXED', 0.13);
fprintf(fid,'%s\t%s\t%1.5f\n', 'AzimOffs0', 'FIXED', 0);
fprintf(fid,'%s\t%s\t%1.5f\n', 'AzimOffs1', 'FIXED', 0);
fprintf(fid,'%s\t%s\t%1.5f\n', 'AzimOffs2', 'FIXED', 0);
fprintf(fid,'%s\t%s\t%1.5f\n', 'AzimOffs3', 'FIXED', 0);
fprintf(fid,'%s\t%s\t%1.5f\n', 'AzimOffs4', 'FIXED', 0);
fprintf(fid,'%s\t%s\t%1.5f\n', 'AzimOffs5', 'FIXED', 0);
fprintf(fid,'%s\t%s\t%1.5f\n', 'AzimOffs6', 'FIXED', 0);
fprintf(fid,'%s\t%s\t%1.5f\n', 'AzimOffs7', 'FIXED', 0);
fprintf(fid,'%s\t%s\t%1.5f\n', 'AzimOffs8', 'FIXED', 0);
fprintf(fid,'%s\t%s\t%1.5f\n', 'AzimOffs9', 'FIXED', 0);
fprintf(fid,'%s\t%s\t%1.4f\t%1.4f\n', 'GPSTrfTX', 'FREE', 0, 0);
fprintf(fid,'%s\t%s\t%1.4f\t%1.4f\n', 'GPSTrfTY', 'FREE', 0, 0);
fprintf(fid,'%s\t%s\t%1.4f\t%1.4f\n', 'GPSTrfTZ', 'FREE', 0, 0);
fprintf(fid,'%s\t%s\t%1.4f\t%1.4f\n', 'GPSTrfRX', 'FREE', 0, 0);
fprintf(fid,'%s\t%s\t%1.4f\t%1.4f\n', 'GPSTrfRY', 'FREE', 0, 0);
fprintf(fid,'%s\t%s\t%1.4f\t%1.4f\n', 'GPSTrfRZ', 'FREE', 0, 0);
fprintf(fid,'%s\t%s\t%1.4f\t%1.4f\n', 'GPSTrfSc', 'FREE', 1, 0);
fprintf(fid,'%s\t%s\n', 'LocalTrfType', 'NONE'); % ff kijken wat dit is
fprintf(fid,'%s\n', '$ PRINT OUTPUT SWITCHES');
fprintf(fid,'%s\t%s\n', 'PrProjConst', 'YES');
fprintf(fid,'%s\t%s\n', 'PrInpCoords', 'NO');
fprintf(fid,'%s\t%s\n', 'PrAddParms', 'YES');
fprintf(fid,'%s\t%s\n', 'PrInpObsv', 'NO');
fprintf(fid,'%s\t%s\n', 'PrAdjCoords', 'NO');
if gcpUse
    fprintf(fid,'%s\t%s\n', 'PrExtReliab', 'YES'); % if baak is only on change
else
    fprintf(fid,'%s\t%s\n', 'PrExtReliab', 'NO'); % if baak is only on change
end
fprintf(fid,'%s\t%s\n', 'PrAbsStandEll', 'YES');
fprintf(fid,'%s\t%s\n', 'PrRelStandEll', 'NO');
fprintf(fid,'%s\t%s\n', 'PrTestCoords', 'NO');
fprintf(fid,'%s\t%s\n', 'PrErrCoords', 'NO');
fprintf(fid,'%s\t%s\n', 'PrAdjParms', 'YES');
fprintf(fid,'%s\t%s\n', 'PrAdjObsv', 'NO');
fprintf(fid,'%s\t%s\n', 'PrTestObsv', 'NO');
fprintf(fid,'%s\t%s\n', 'PrErrObsv', 'NO');
fprintf(fid,'%s\t%s\n', 'LogFile', 'ASCII');
fprintf(fid,'%s\n', '$');
fclose(fid);
end

function [th r] = cart2gon(x,y)
%CART2POL Transform Cartesian to goniometric polar coordinates (400).
%   [TH,R] = CART2POL(X,Y) transforms corresponding elements of data
%   stored in Cartesian coordinates X,Y to polar coordinates (angle TH
%   and radius R).  The arrays X and Y must be the same size (or
%   either can be scalar). TH is returned in gon. 
%
% B. Altena - sep 2013
th = ((atan2(y,x))*400)/(2.*pi());
r = hypot(x,y);
end

function [IDX] = argumentSearch(A)
%ARGUMENTSEARCH Find last entry within design matrix
%
% B. Altena - sep 2013
Iidx = 1:size(A,1);
for j = 1:size(A,2)
    sweep = ones(Iidx(end),1);
    I(Iidx(and(sweep,A(:,j)))) = j;
end
J = Iidx'; I = I';
IDX = sub2ind([size(A,1) size(A,2)], J(and(I,J)), I(and(I,J)));
end

% --- Executes on button press in Vereffen.
function Vereffen_Callback(hObject, eventdata, handles)
% hObject    handle to Vereffen (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
methode = get(handles.kkMethode, 'Value');

camXY = handles.camData.camXY;
camFov = handles.camData.camFov;
xy = handles.objData.xy;
CC = handles.objData.centers;
A = handles.adjustment.A;
focalLength = handles.metricdata.focal*1000;
pixelSize = handles.metricdata.pixelsize;
sigma = handles.metricdata.sigma;

switch methode
    case 1 % vrij netwerk
        set(handles.bottomBar, 'String', 'vereffen in vrije configuratie');
        %% construct design matrix
        measurements = find(A == 1);
        [station,point] = ind2sub(size(A), measurements);

        A0 = [];
        for k = 1:length(measurements);
            %% initialize
            i = station(k);
            j = point(k);
            stat = camXY(i,:);
            pnt = xy(j,:);
            aX = zeros(1,2*size(A,2));
            aY = zeros(1,2*size(A,2));
            %% entry computation
            deltaX = pnt(1) - stat(1);
            deltaY = pnt(2) - stat(2);
            sX = deltaX.^2; 
            sY = deltaY.^2;
            sXY = deltaX.*deltaY;

            aX((2*j)-1:(2*j)) = [deltaX/sX deltaX/sXY];
            aY((2*j)-1:(2*j)) = [deltaX/sXY deltaY/sY];
            A0 = cat(1, A0, aX, aY);
        end

        angularPrec = atan(pixelSize/focalLength);
        Qyy = sigma.*angularPrec.*eye(length(A0));

        Qxx = inv(A0'*inv(Qyy)*A0);
        qxx = diag(Qxx);

        % redundancy matrix
        R = eye(length(Qyy))-A0*Qxx*A0'*inv(Qyy);
        r = spdiags(R,0);
        clear angularPrec         
    case 2 % aangesloten netwerk
        set(handles.bottomBar, 'String', 'vereffen ge�ntegreerd met grondpunten');
        
end

handles.adjustment.Qxx = Qxx;
handles.adjustment.qxx = qxx;
handles.adjustment.R = R;
handles.adjustment.r = r;
guidata(hObject, handles);
end

%% Resultaten

% --- Executes on selection change in tekenVorm.
function tekenVorm_Callback(hObject, eventdata, handles)
% hObject    handle to tekenVorm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if get(hObject, 'Value') == 2
    handles.scaleOn = 1;
    set(handles.scaleSlider, 'visible', 'on');
    set(handles.textScale, 'visible', 'on');
else
    handles.scaleOn = 0;
    set(handles.scaleSlider, 'visible', 'off');
    set(handles.textScale, 'visible', 'off');
end
end

% --- Executes during object creation, after setting all properties.
function tekenVorm_CreateFcn(hObject, eventdata, handles)
% hObject    handle to tekenVorm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

set(hObject, 'String', {'Aansluitingen', 'Standaard ellipse', 'Redudantie', 'Begin scherm'});
end

% --- Executes on button press in teken.
function teken_Callback(hObject, eventdata, handles)
% hObject    handle to teken (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
vorm = get(handles.tekenVorm, 'Value');

camXY = handles.camData.camXY;
xy = handles.objData.xy;
A = handles.adjustment.A;
% gcp = handles.adjustment.gcp;

measurements = find(A == 1);
[station,point] = ind2sub(size(A), measurements);
pointList = unique(point);

cla
patch(xy(:,1),xy(:,2),'b','EdgeColor','none'); alpha(0.5); hold on;
axis off; colorbar('off');

xlm = get(gca,'xlim');
dx = xlm(2)-xlm(1);
dl = round(dx./10);
xSpacing = dx./20;

ylm=get(gca,'ylim');
dy = ylm(2)-ylm(1);
ySpacing = dy./20;

switch vorm
    case 1 % Aansluitingen
        N = sum(A,1)';
        minN = min(N); maxN = max(N); % range
        dN = max(N)-min(N);
%         minN = minN-(dN./100); maxN = maxN+(dN./100); dN = maxN-minN;
        load('ARCcmap.mat');
        for k = 1:length(measurements);
            j = point(k);
            scatter(xy(j,1),xy(j,2), 10, ...
                ARCmap(ceil(((N(j)-minN)./dN)*63)+1,:),'o', 'filled');
        end
        colorLabels = minN:dN./5:maxN;
        labels = {};
        for k = 1:length(colorLabels)
            labels{k} = sprintf('%1.1f', colorLabels(k));
        end
        colormap(ARCmap);
        colorbar('location','WestOutside','YTickLabel',labels);
    case 2 % Standaard ellipse
        qxx = handles.adjustment.qxx;
        Qxx = handles.adjustment.Qxx;
        scale = handles.metricdata.scaleSlider;

        a = [0:2:360];
        for k = 1:length(pointList)
            i = pointList(k);
            u = xy(i,1) + scale*qxx((k.*2)-1)*sind(a);
            v = xy(i,2) + scale*qxx((k.*2))*cosd(a);
            plot(u,v,'-b');
        end
        sigma = 1;
        u = (xlm(2)-2.*xSpacing) + scale*sigma*sind(a);
        v = (ylm(1)+3.*ySpacing) + scale*sigma*cosd(a);
        plot(u,v,'-k');
        text(xlm(2)-2.*xSpacing, ylm(1)+4.*ySpacing, ['\sigma = ' sprintf('%1.1f', sigma) ' mtr'])
    case 3 % Redudantie
        r = handles.adjustment.r;
        minR = min(r); maxR = max(r); dR = max(r)-min(r);
        minR = minR-(dR./100); maxR = maxR+(dR./100); dR = maxR-minR;
        load('ARCcmap.mat');
        for k = 1:length(measurements);
            i = station(k);
            j = point(k);
            stat = camXY(i,:);
            pnt = xy(j,:);
            line([stat(1); pnt(1)], [stat(2); pnt(2)], 'Color', ...
            ARCmap(ceil(((r(k)-minR)./dR)*64),:));
        end
        colorLabels = minR:0.1:maxR;
        labels = {};
        for k = 1:length(colorLabels)
            labels{k} = sprintf('%1.1f', colorLabels(k));
        end
        colormap(ARCmap);
        colorbar('location','WestOutside','YTickLabel',labels);
    case 4 % Begin scherm
        terrain = handles.objData.terrain;
        for i = 1:length(terrain);
            uv = terrain{i};
            patch(uv(:,1),uv(:,2),'r','EdgeColor','none');
        end
        patch(xy(:,1),xy(:,2),'b','EdgeColor','none'); alpha(0.5);
end
line([xlm(2)-dl-xSpacing; xlm(2)-dl-xSpacing; xlm(2)-xSpacing; xlm(2)-xSpacing], ...
    [ylm(1)+1.5.*ySpacing; ylm(1)+ySpacing; ylm(1)+ySpacing; ylm(1)+1.5.*ySpacing], ...
    'Color','k');
text(xlm(2)-dl-xSpacing, ylm(1)+2.*ySpacing,'0')
text(xlm(2)-xSpacing, ylm(1)+2.*ySpacing,[sprintf('%1.1f',dl) ' m'])
xlabel('X [m]'); ylabel('Y [m]');
end

% --- Executes on slider movement.
function scaleSlider_Callback(hObject, eventdata, handles)
% hObject    handle to scaleSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

scale = get(hObject, 'Value');
set(handles.textScale, 'String', ['Schaal: ' sprintf('%3.1f', scale)]);
handles.metricdata.scaleSlider = scale;
guidata(hObject,handles);
end

% --- Executes during object creation, after setting all properties.
function scaleSlider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to scaleSlider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
end

%% Main
% --------------------------------------------------------------------
function initialize_gui(fig_handle, handles, isreset)
% If the metricdata field is present and the reset flag is false, it means
% we are we are just re-initializing a GUI by calling it from the cmd line
% while it is up. So, bail out as we dont want to reset the data.
if isfield(handles, 'metricdata') && ~isreset
    return;
end

setappdata(0, 'baakGrote', 1);
setappdata(0, 'focal', 35);
setappdata(0, 'resolution1', 3456);
setappdata(0, 'resolution2', 5184);
setappdata(0, 'pixelsize', 4.3);
setappdata(0, 'sigma', 1);
setappdata(0, 'gcp', 3);
setappdata(0, 'opstel', 1);
setappdata(0, 'rad1', 0.027);
setappdata(0, 'shapePath', 'sample\Gbge33779');
setappdata(0, 'samplingOnOff', false);
setappdata(0, 'samplingInterval', 5);
setappdata(0, 'scaleSlider', 1);
setappdata(0, 'FalseEasting', 150000.013);
setappdata(0, 'FalseNorthing', 5400088.438);
setappdata(0, 'StandPar1', 49.8333339);
setappdata(0, 'StandPar2', 51.16666723);
setappdata(0, 'LatOrigin', 90);
setappdata(0, 'LonOriginCM', 4.367486667);
setappdata(0, 'ProjScaleFac', 0);
setappdata(0, 'SemiMajAx', 6378388);
setappdata(0, 'InvFlatt', 297);
setappdata(0, 'EPSG', 31370);

handles.metricdata.baakGrote = 1;
handles.metricdata.focal = 35;
handles.metricdata.resolution1  = 3456;
handles.metricdata.resolution2  = 5184;
handles.metricdata.pixelsize  = 4.3;
handles.metricdata.sigma  = 1;
handles.metricdata.gcp  = 3;
handles.metricdata.rad1 = 0.027;
handles.metricdata.shapePath  = 'sample\Gbge33779';
handles.metricdata.samplingOnOff  = 0;
handles.metricdata.samplingInterval  = 5;
handles.metricdata.scaleSlider = 1;
handles.metricdata.shape = {};

handles.epsg.FalseEasting = 150000.013;
handles.epsg.FalseNorthing = 5400088.438;
handles.epsg.StandPar1 = 49.8333339;
handles.epsg.StandPar2 = 51.16666723;
handles.epsg.LatOrigin = 90;
handles.epsg.LonOriginCM = 4.367486667;
handles.epsg.ProjScaleFac = 0;
handles.epsg.SemiMajAx = 6378388;
handles.epsg.InvFlatt = 297;
handles.epsg.EPSG = 31370;
handles.epsg.projName = 'Lambert 1SP';

handles.camXY = [];
handles.camDir = [];
handles.options.imagingstation = false;
handles.options.imagingrod = false;
handles.options.blur = false;
handles.options.radialdistortion = false;
handles.options.gcp = [];
handles.options.baak = [];
handles.options.newObject = [];

handles.adjustment.camFov = [];

set(handles.baakGrote, 'String', getappdata(0, 'baakGrote'));
set(handles.spiegelReflex, 'Value', true);
set(handles.focal, 'String', getappdata(0, 'focal'));
set(handles.resolution1,  'String', getappdata(0, 'resolution1'));
set(handles.resolution2,  'String', getappdata(0, 'resolution2'));
set(handles.pixelsize,  'String', getappdata(0, 'pixelsize'));
set(handles.sigma,  'String', getappdata(0, 'sigma'));
set(handles.GCP,  'String', getappdata(0, 'gcp'));
set(handles.Opstel,  'String', getappdata(0, 'opstel'));
set(handles.shapePath,  'String', getappdata(0, 'shapePath'));
set(handles.stdK1,  'String', getappdata(0, 'rad1'));
set(handles.samplingInterval, 'String', getappdata(0, 'samplingInterval'));
set(handles.scaleSlider, 'String', getappdata(0, 'scaleSlider'));
set(handles.scaleSlider, 'Value', getappdata(0, 'scaleSlider'));
set(handles.textScale, 'String', ['Schaal: ' int2str(getappdata(0, 'scaleSlider'))]);

set(handles.zoomAction,'Enable','off');
set(handles.panAction,'Enable','off');
set(handles.addObject,'Enable','off');
set(handles.adjustObject,'Enable','off');
set(handles.clearObject,'Enable','off');
set(handles.selectBuilding,'Enable','off');
set(handles.placeMarker,'Enable','off');
set(handles.adjustCam,'Enable','off');
set(handles.rotateCam,'Enable','off');
set(handles.duidtaan,'Enable','off');
set(handles.baak,'Enable','off');
set(handles.verwerk,'Enable','off');
set(handles.illustreer,'Enable','off');
set(handles.move3,'Enable','off');
set(handles.export,'Enable','off');
set(handles.WMSbutton,'Enable','off');
set(handles.StreetView,'Enable','off');

set(handles.uipanel12, 'visible', 'off');
set(handles.uipanel13, 'visible', 'off');
set(handles.scaleSlider, 'visible', 'off');
set(handles.textScale, 'visible', 'off');
set(handles.bottomBar, 'String', 'Geef camera parameters en topografisch bronbestand op');

set(handles.text4, 'String', '[mm]');
set(handles.text6, 'String', '[�m]');
imshow('fig\3d4.png', 'InitialMagnification', 1);
load('fig\ARCcmap.mat');
hold off;

% Update handles structure
guidata(handles.figure1, handles);
end

% --- Executes on button press in radial.
function radial_Callback(hObject, eventdata, handles)
% hObject    handle to radial (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
end

% --- Executes on button press in tangent.
function tangent_Callback(hObject, eventdata, handles)
% hObject    handle to tangent (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
end

function edit14_Callback(hObject, eventdata, handles)
% hObject    handle to edit14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
end

% --- Executes during object creation, after setting all properties.
function edit14_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end


function stdK1_Callback(hObject, eventdata, handles)
% hObject    handle to stdK1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
end

% --- Executes during object creation, after setting all properties.
function stdK1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to stdK1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end


function radial2_Callback(hObject, eventdata, handles)
% hObject    handle to radial2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
end    

% --- Executes during object creation, after setting all properties.
function radial2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to radial2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end


% --- Executes on button press in export.
function export_Callback(hObject, eventdata, handles)
% hObject    handle to export (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% --- Executes on button press in pb_send.
% get the handle to the target gui 
[file,path] = uiputfile({'*.xls','Excel tabel';'*.txt','Tekst bestand'}, 'Sla grondslag op');
filename = strcat(path,file);

if ~isempty(filename) % op annuleren gedrukt
    measurements = find(handles.adjustment.A == 1);
    [station,point] = ind2sub(size(handles.adjustment.A), measurements);
    pointList = unique(point);
    
    a = []; b = []; psi = [];
    for i = 1:length(pointList)
        C = handles.adjustment.Qxx(i*2-1:i*2,i*2-1:i*2); % correlatie matrix
        [V,D]=eig(C); % Eigenwaarden & vectoren
        r=sqrt(diag(D))'; % Lengte van assen
        d=diag(D);
        inda=find(max(d)==d);
        indb=find(min(d)==d);
        Ia=sqrt(d(inda(1)));
        Ib=sqrt(d(indb(1)));
        Ipsi=atan2(V(2,inda(1)),V(1,inda(1)));
        
        a = cat(1,a,Ia);
        b = cat(1,b,Ib);
        psi = cat(1,psi,Ipsi);
    end
    m = [handles.objData.xy(pointList,:) a b psi];

    if strcmp(file(end-2:end),'txt')
        header = ['Xcoord Ycoord sigmaA sigmaB \psi \n'];
        fid = fopen(filename, 'wt');
        fprintf(fid, header);
        fclose(fid);
        dlmwrite(filename, m, '-append', 'delimiter', ' ', 'precision', '%.3f');
    else
        header = 'NetDesign';
        colnames = {'Xcoord','Ycoord','sigmaA','sigmaB','sigmaPsi'};
        xlswrite(file, m)
    end
end
end

% --- Executes on button press in pushbutton18.
function pushbutton18_Callback(hObject, eventdata, handles)
end

% --- Executes on selection change in popupmenu5.
function popupmenu5_Callback(hObject, eventdata, handles)
end

% --- Executes during object creation, after setting all properties.
function popupmenu5_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end


% --- Executes on button press in pushbutton19.
function pushbutton19_Callback(hObject, eventdata, handles)
end

% --- Executes on slider movement.
function slider2_Callback(hObject, eventdata, handles)
end

% --- Executes during object creation, after setting all properties.
function slider2_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
end

% --- Executes on button press in consument.
function consument_Callback(hObject, eventdata, handles)
end

% --- Executes on button press in metrisch.
function metrisch_Callback(hObject, eventdata, handles)
end

% --- Executes on button press in spiegelReflex.
function spiegelReflex_Callback(hObject, eventdata, handles)
end


% --- Executes on button press in WMSbutton.
function WMSbutton_Callback(hObject, eventdata, handles)
if handles.epsg.EPSG == 31370 % belgium Lambert
    radius = 200;

    object = handles.object;
    xy = object{1}(1,:);

    urlStr = ['http://geo-vlaanderen.gisvlaanderen.be/geo-vlaanderen/kleurenortho/?startup=zex%28', ...
        num2str(floor(xy(1)-radius)), ... 
        ',' num2str(floor(xy(2)-radius)), ...
        ',' num2str(ceil(xy(1)+radius)), ...
        ',' num2str(ceil(xy(2)+radius)), '%29'];

    web(urlStr, '-browser');
elseif handles.epsg.EPSG == 28992 % RD nederland
    
    urlSTR = ['http://kaart.pdok.nl/api/api.html?zoom=12&baselayer=BRT&loc=', ...
        num2str(xy(1)), '%2C%20', ...
        num2str(xy(2)), '&pdoklayers=BRT%2CLUFO&markersdef=http%3A%2F%2Fkaart.pdok.nl', ...
        '%2Fapi%2Fjs%2Fpdok-markers.js&layersdef=http%3A%2F%2Fkaart.pdok.nl%2Fapi', ...
        '%2Fjs%2Fpdok-layers.js&mimg=http%3A%2F%2Fsocialcompare.com%2Fu%2F1211%2F3d4', ...
        '-1hy1856x.png&mloc=', ...
        num2str(xy(1)), '%2C',...
        num2str(xy(2)), '&titel=%26nbsp%3B&mt=mt0'];
    
    web(urlStr, '-browser');
end
end

% --- Executes on button press in StreetView.
function StreetView_Callback(hObject, eventdata, handles)
% hObject    handle to StreetView (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of StreetView

if handles.epsg.EPSG == 31370 %only working for belgium
    if ~isempty(ver('map'))
        object = handles.object;
        xy = object{1}(1,:);
        %% converteer naar lat lon

        % voor nu Lambert 72
        mstruct = defaultm('lambertstd');
        mstruct.origin = [90 4.367486667 0];
        mstruct.falseeasting = 150000.013;
        mstruct.falsenorthing = 5400088.438;
        mstruct.mapparallels = [49.8333339 51.16666723];
        mstruct.geoid = almanac('earth','international','meter');
        mstruct.scalefactor = 1;

        [objLat,objLon] = minvtran(mstruct,xy(1),xy(2));

        fin = fopen('doc/streetview.htm');
        fout = fopen('doc/streetviewNew.htm', 'w');

        while ~feof(fin)
           s = fgetl(fin);
           [str en] = regexp(s, 'LatLng(\w*.\w*', 'match');
           oldLat = s(en+7:en+numel(str{:})-1);
           s = strrep(s, oldLat, num2str(objLat));

           [str en] = regexp(s, '\w*.\w*);', 'match');
           oldLon = s(en(1):en(1)+numel(str{1})-3);
           s = strrep(s, oldLon, num2str(objLon));
           fprintf(fout,'%s\n',s);
        end
        fclose(fin);
        fclose(fout);
        web('doc/streetviewNew.htm', '-browser');
    else
        object = handles.object;
        xy = object{1}(1,:);
        [objLat,objLon] = BLambert2WGS84(xy(1), xy(2));
        fin = fopen('doc/streetview.htm');
        fout = fopen('doc/streetviewNew.htm', 'w');

        while ~feof(fin)
           s = fgetl(fin);
           [str en] = regexp(s, 'LatLng(\w*.\w*', 'match');
           oldLat = s(en+7:en+numel(str{:})-1);
           s = strrep(s, oldLat, num2str(objLat));

           [str en] = regexp(s, '\w*.\w*);', 'match');
           oldLon = s(en(1):en(1)+numel(str{1})-3);
           s = strrep(s, oldLon, num2str(objLon));
           fprintf(fout,'%s\n',s);
        end
        fclose(fin);
        fclose(fout);
        web('doc/streetviewNew.htm', '-browser');    
    end
end
end

%In : Geographical coordinats :
% x and y coordinates(m)
%Out : coordinates in WGS84 format (Longitude and lattitude)
% (c) Vrije universiteit Brussel, dept ELEC
% $Date: 2005/05/20 13:45:30 $
function [Lat,Long] = BLambert2WGS84(x, y)
format long;
% constants:
a = 6378388;%Equatorial Radius
e = 0.08199189;%eccentricity
e2=0.006722670;%e^2;
lamdaF = 0.0760429434637049260;%Longitude False Origin
phi1 = 0.86975574;%First Standard Parallel 49.83333333
phi2 = 0.89302680;%Second Standard Parallel 51.16666667
phiF =1.57079633;%Latitude False Origin
Ef=150000.01256;%Easting at false origin
Nf=5400088.5378;%Northing at false origin
alpha = 29.2985/3600*pi/180;
% =========================================================
m1 = cos(phi1) / sqrt((1 - e2*sin(phi1)*sin(phi1)));
m2 = cos(phi2) / sqrt(( 1 - e2*sin(phi2)*sin(phi2)));
to = tan(pi/4-(phiF/2)) / (((1-e*sin(phiF))...
/(1+e*sin(phiF)))^(e/2));
t1 = tan(pi/4-(phi1/2)) / (((1-e*sin(phi1))...
/(1+e*sin(phi1)))^(e/2));
t2 = tan(pi/4-(phi2/2)) / (((1-e*sin(phi2))/...
(1+e*sin(phi2)))^(e/2));
n=(log10(m1)-log10(m2))/(log10(t1)-log10(t2));
F=m1/(n*(t1^n));
rf=a*F*to^n;
r_dash = sqrt((x-Ef)^2 + (rf-(y-Nf))^2);
t_dash= (r_dash/(a*F))^(1/n);
theta_dash= atan((x-Ef)/(rf-(y-Nf)))+alpha;
% theta_dash=n*(lambda-lambdaF);
lamda = theta_dash/n + lamdaF;% Long in Radians
phi0 = pi/2 - 2*atan(t_dash);
phi1 = pi/2 - 2*atan(t_dash*(((1-e*sin(phi0))...
/(1+e*sin(phi0)))^(e/2)));
phi2 = pi/2 - 2*atan(t_dash*(((1-e*sin(phi1))...
/(1+e*sin(phi1)))^(e/2)));
phi = pi/2 - 2*atan(t_dash*(((1-e*sin(phi2))...
/(1+e*sin(phi2)))^(e/2)));
%Lat in Radians
Lat=real(phi*180/pi);
Long=real(lamda*180/pi);
% =========================================================
end

%In : Geographical coordinats :
% Longitude : lambda_dd in decimal degrees (E/W)
% Lattitude : phi_dd in decimal degrees (N/S)
%Out : Lambert coordinates : x, y (m)
% (c) Vrije universiteit Brussel, dept ELEC
% Mussa Bshara
function [x,y] = WGSdd2Lambert(lambda_dd, phi_dd)
%constants
a = 6378388;
e = 0.08199188998;
lambdaF = 0.0760429434637049260;
phi1 = 0.86975574;
phi2 = 0.89302680;
phiF = 1.57079633;
alpha = 29.2985/3600*pi/180;
%alpha=29.2985 sec => needs to be converted to rad
% =========================================================
%Convert lambda_dms and phi_dms to radians
lambda = dd_rad(lambda_dd);
phi = dd_rad(phi_dd);
m1=cos(phi1)/sqrt(1-(e*sin(phi1))^2);
m2=cos(phi2)/sqrt(1-(e*sin(phi2))^2);
t=cot(pi/4+phi/2)/(((1-e*sin(phi))...
    /(1+e*sin(phi)))^(e/2));
t1=cot(pi/4+phi1/2)/(((1-e*sin(phi1))...
    /(1+e*sin(phi1)))^(e/2));
t2=cot(pi/4+phi2/2)/(((1-e*sin(phi2))...
    /(1+e*sin(phi2)))^(e/2));
tF=cot(pi/4+phiF/2)/(((1-e*sin(phiF))...
    /(1+e*sin(phiF)))^(e/2));
n=(log10(m1)-log10(m2))/(log10(t1)-log10(t2));
F=m1/(n*(t1^n));
rho=a*F*(t^n);
rhoF=a*F*(tF^n);
theta=n*(lambda-lambdaF);
format long;
x = 150000.01 + rho*sin(theta-alpha);
y = abs(5400088.44 + rhoF - rho*cos(theta-alpha));
end

function [x_rad] = dd_rad(x_dd)
x_rad = x_dd * pi / 180;
end

function illustreer_Callback(hObject, eventdata, handles)
% hObject    handle to illustreer (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
h = figure('Name','Meetopzet','NumberTitle','off');

load('fig\ARCcmap');
object = handles.object;
camXY = handles.camXY;
camDir = handles.camDir;
A = handles.adjustment.A;
keyXY = handles.objData.xy;

hold on;
for i = 1:size(object,2)
%         patch(object.ncst{i}(:,1),object.ncst{i}(:,2), [0 0 0.35],'EdgeColor','none');
        patch(object{i}(:,1),object{i}(:,2), [0.6 0.6 1],'EdgeColor','none');
end

for i = 1:size(camXY,1) 
        scatter(camXY(i,1),camXY(i,2),5,'+k');
%         line([camXY(i,1); camXY(i,1)+(5*camDir(i,1))], ...
%             [camXY(i,2); camXY(i,2)+(5*camDir(i,2))], 'Color','k');
end

axis on; axis xy; axis equal;

measurements = find(A == 1);
[station,point] = ind2sub(size(A), measurements);
pointList = unique(point);

N = sum(A,1)';
minN = min(N); maxN = max(N); % range
dN = max(N)-min(N);
%         minN = minN-(dN./100); maxN = maxN+(dN./100); dN = maxN-minN;

for k = 1:length(measurements);
    j = point(k);
    scatter(keyXY(j,1),keyXY(j,2), 10, ...
        flipud(ARCmap(ceil(((N(j)-minN)./dN)*63)+1,:)),'o', 'filled');
end
colormap(ARCmap);
hcb = colorbar('location','EastOutside');
colorTicks = get(hcb,'YTickLabel');

colorStep = dN./(length(colorTicks)-1);

labels = {};
for k = 1:length(colorTicks)
    labels{k} = sprintf('%1.1f', minN + colorStep.*(k-1));
end
set(hcb,'YTickLabel', labels);

x = get(gca,'XTick'); set(gca,'XTick',x); set(gca,'XTickLabel',sprintf('%3.0f|',x));
y = get(gca,'YTick'); set(gca,'YTick',y); set(gca,'YTickLabel',sprintf('%3.0f|',y));
xlabel('X [m]'); ylabel('Y [m]');

hold off;

end

function htmlhelp_Callback(hObject, eventdata, handles)
% hObject    handle to htmlhelp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
winopen('doc/netDesign.chm');
end

function contactMe_Callback(hObject, eventdata, handles)
% hObject    handle to contactMe (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
msgbox('Het programma is ontwikkeld door Bas Altena, mochten er enige fouten of opmerkingen zijn over netDesign, schroom dan niet mij te mailen (bas.altena@kuleuven.be), nieuwere versies van het programma zijn te vinden op de project website (eavise.be/3d4sure).', ...
    'Contact',...
    'custom',double(imread('fig/logo_kuleuven2.png', ...
    'BackgroundColor', get(handles.contactMe, 'BackgroundColor')))./2^8);
end

function Opstel_Callback(hObject, eventdata, handles)
% hObject    handle to Opstel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Opstel as text
%        str2double(get(hObject,'String')) returns contents of Opstel as a double
end

% --- Executes during object creation, after setting all properties.
function Opstel_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Opstel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

function [xi,yi,ii,ri] = polyxpoly(varargin)

%POLYXPOLY  Line or polygon intersection points.
%
%   [XI,YI] = POLYXPOLY(X1,Y1,X2,Y2) returns the intersection points of
%   two sets of lines and/or polygons.
%
%   [XI,YI] = POLYXPOLY(...,'unique') returns only unique intersections.
%
%   [XI,YI,II] = POLYXPOLY(...) also returns a two-column index of line
%   segment numbers corresponding to the intersection points.
%
% See also NAVFIX, CROSSFIX, SCXSC, GCXGC, GCXSC, RHXRH
	
%  Written by:  A. Kim
%  Copyright 1996-2002 Systems Planning and Analysis, Inc. and The MathWorks, Inc.
%  $Revision: 1.6 $    $Date: 2002/03/20 21:26:10 $
	
% set input variables
if nargin==4 | nargin==5
        x1 = varargin{1}(:);  y1 = varargin{2}(:);
        x2 = varargin{3}(:);  y2 = varargin{4}(:);
else
        error('Incorrect number of arguments')
end
if nargin==5,
        strcode = varargin{nargin};
else
        strcode = 'all';
end

% check for valid strcode
validtypes = {'all';'unique';'filter'};
if isempty(strmatch(strcode,validtypes))
        error('Valid options are ''all'',''unique'' or ''filter''')
end

% check x and y vectors
msg = inputcheck('xyvector',x1,y1); if ~isempty(msg); error(msg); end
msg = inputcheck('xyvector',x2,y2); if ~isempty(msg); error(msg); end


% determine if both are polygons
if (x1(1)==x1(end) & y1(1)==y1(end)) & (x2(1)==x2(end) & y2(1)==y2(end))
        datatype = 'polygon';
else
        datatype = 'line';
end

% compute all intersection points
[xi,yi,ii] = intptsall(x1,y1,x2,y2);
ri = [];
if isempty([xi yi ii]),  return;  end

% format intersection points according to type and strcode
if ~isempty(strmatch(strcode,'filter'))
        if strcmp(datatype,'line')
                error('Line data cannot be filtered for polygon boolean operations')
        end
        [xi,yi,ii,ri] = filterintpts(x1,y1,x2,y2,xi,yi,ii);
elseif ~isempty(strmatch(strcode,'unique'))
        [a,i,j] = uniqueerr(flipud([xi yi]),'rows',eps*1e4);
        i = length(xi)-flipud(sort(i))+1;
        xi = xi(i);  yi = yi(i);  ii = ii(i,:);
end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [xi,yi,ii] = intptsall(x1,y1,x2,y2)
%INTPTSALL  Unfiltered line or polygon intersection points.
%   [XI,YI,II] = INTPTSALL(X1,Y1,X2,Y2) returns the unfiltered intersection
%   points of two sets of lines or polygons, along with a two-column index
%   of line segment numbers corresponding to the intersection points.
%   Note: intersection points are ordered from lowest to hightest line
%   segment numbers.

%  Written by:  A. Kim


err = eps*1e5;

% form line segment matrices
xs1 = [x1 [x1(2:end); x1(1)]];
ys1 = [y1 [y1(2:end); y1(1)]];
xs2 = [x2 [x2(2:end); x2(1)]];
ys2 = [y2 [y2(2:end); y2(1)]];

% remove last segment (for self-enclosed polygons, this is a non-segment;
% for lines, there are n-1 line segments)
xs1 = xs1(1:end-1,:);  ys1 = ys1(1:end-1,:);
xs2 = xs2(1:end-1,:);  ys2 = ys2(1:end-1,:);

% tile matrices for vectorized intersection calculations
N1 = length(xs1(:,1));  N2 = length(xs2(:,1));
X1 = reshape(repmat(xs1,1,N2)',2,N1*N2)';
Y1 = reshape(repmat(ys1,1,N2)',2,N1*N2)';
X2 = repmat(xs2,N1,1);
Y2 = repmat(ys2,N1,1);

% compute slopes
w = warning;
warning off
m1 = (Y1(:,2) - Y1(:,1)) ./ (X1(:,2) - X1(:,1));
m2 = (Y2(:,2) - Y2(:,1)) ./ (X2(:,2) - X2(:,1));
% m1(find(m1==-inf)) = inf;  m2(find(m2==-inf)) = inf;
m1(find(abs(m1)>1/err)) = inf;  m2(find(abs(m2)>1/err)) = inf;
warning(w)
	
% compute y-intercepts (note: imaginary values for vertical lines)
b1 = zeros(size(m1));  b2 = zeros(size(m2));
i1 = find(m1==inf);  if ~isempty(i1),  b1(i1) = X1(i1)*i;  end
i2 = find(m2==inf);  if ~isempty(i2),  b2(i2) = X2(i2)*i;  end
i1 = find(m1~=inf);  if ~isempty(i1),  b1(i1) = Y1(i1) - m1(i1).*X1(i1);  end
i2 = find(m2~=inf);  if ~isempty(i2),  b2(i2) = Y2(i2) - m2(i2).*X2(i2);  end
	
% zero intersection coordinate arrays
sz = size(X1(:,1));  x0 = zeros(sz);  y0 = zeros(sz);

% parallel lines (do not intersect except for similar lines)
% for similar lines, take the low and high points
% idx = find(m1==m2);
idx = find( abs(m1-m2)<err | (isinf(m1)&isinf(m2)) );
if ~isempty(idx)
% non-similar lines
%       sub = find(b1(idx)~=b2(idx));  j = idx(sub);
        sub = find(abs(b1(idx)-b2(idx))>err);  j = idx(sub);
        x0(j) = nan;  y0(j) = nan;
% similar lines (non-vertical)
%       sub = find(b1(idx)==b2(idx) & m1(idx)~=inf);  j = idx(sub);
        sub = find(abs(b1(idx)-b2(idx))<err & m1(idx)~=inf);  j = idx(sub);
        Xlo = max([min(X1(j,:),[],2) min(X2(j,:),[],2)],[],2);
        Xhi = min([max(X1(j,:),[],2) max(X2(j,:),[],2)],[],2);
        if ~isempty(j)
                j0 = find(abs(Xlo-Xhi)<=err);
                j1 = find(abs(Xlo-Xhi)>err);
                x0(j(j0)) = Xlo(j0);
                y0(j(j0)) = Y1(j(j0)) + m1(j(j0)).*(Xlo(j0) - X1(j(j0)));
                x0(j(j1)) = Xlo(j1) + i*Xhi(j1);
                y0(j(j1)) = (Y1(j(j1)) + m1(j(j1)).*(Xlo(j1) - X1(j(j1)))) + ...
                                         i*(Y1(j(j1)) + m1(j(j1)).*(Xhi(j1) - X1(j(j1))));
%               if Xlo==Xhi
%               if abs(Xlo-Xhi)<=eps
%                       x0(j) = Xlo;
%                       y0(j) = Y1(j) + m1(j).*(Xlo - X1(j));
%               else
%                       x0(j) = Xlo + i*Xhi;
%                       y0(j) = (Y1(j) + m1(j).*(Xlo - X1(j))) + ...
%                                        i*(Y1(j) + m1(j).*(Xhi - X1(j)));
%               end
        end
% similar lines (vertical)
%       sub = find(b1(idx)==b2(idx) & m1(idx)==inf);  j = idx(sub);
        sub = find(abs(b1(idx)-b2(idx))<err & m1(idx)==inf);  j = idx(sub);
        Ylo = max([min(Y1(j,:),[],2) min(Y2(j,:),[],2)],[],2);
        Yhi = min([max(Y1(j,:),[],2) max(Y2(j,:),[],2)],[],2);
        if ~isempty(j)
                y0(j) = Ylo + i*Yhi;
                x0(j) = X1(j) + i*X1(j);
        end
end

% non-parallel lines
% idx = find(m1~=m2);
idx = find(abs(m1-m2)>err);
if ~isempty(idx)
% non-vertical/non-horizontal lines
%       sub = find(m1(idx)~=inf & m2(idx)~=inf & m1(idx)~=0 & m2(idx)~=0);
        sub = find(m1(idx)~=inf & m2(idx)~=inf & ...
                           abs(m1(idx))>eps & abs(m2(idx))>eps);
        j = idx(sub);
        x0(j) = (Y1(j) - Y2(j) + m2(j).*X2(j) - m1(j).*X1(j)) ./ ...
                        (m2(j) - m1(j));
        y0(j) = Y1(j) + m1(j).*(x0(j)-X1(j));
% first line vertical
        sub = find(m1(idx)==inf);  j = idx(sub);
        x0(j) = X1(j);
        y0(j) = Y2(j) + m2(j).*(x0(j)-X2(j));
% second line vertical
        sub = find(m2(idx)==inf);  j = idx(sub);
        x0(j) = X2(j);
        y0(j) = Y1(j) + m1(j).*(x0(j)-X1(j));
% first line horizontal, second line non-vertical
%       sub = find(m1(idx)==0 & m2(idx)~=inf);  j = idx(sub);
        sub = find(abs(m1(idx))<=eps & m2(idx)~=inf);  j = idx(sub);
        y0(j) = Y1(j);
        x0(j) = (Y1(j) - Y2(j) + m2(j).*X2(j)) ./ m2(j);
% second line horizontal, first line non-vertical
%       sub = find(m2(idx)==0 & m1(idx)~=inf);  j = idx(sub);
        sub = find(abs(m2(idx))<=eps & m1(idx)~=inf);  j = idx(sub);
        y0(j) = Y2(j);
        x0(j) = (Y1(j) - y0(j) - m1(j).*X1(j)) ./ -m1(j);
% connecting line segments (exact solution)
%       sub1 = find(X1(idx,1)==X2(idx,1) & Y1(idx,1)==Y2(idx,1));
%       sub2 = find(X1(idx,1)==X2(idx,2) & Y1(idx,1)==Y2(idx,2));
%       sub3 = find(X1(idx,2)==X2(idx,1) & Y1(idx,2)==Y2(idx,1));
%       sub4 = find(X1(idx,2)==X2(idx,2) & Y1(idx,2)==Y2(idx,2));
%       j1 = idx(sort([sub1; sub2]));
%       j2 = idx(sort([sub3; sub4]));
%       x0(j1) = X1(j1,1);  y0(j1) = Y1(j1,1);
%       x0(j2) = X1(j2,2);  y0(j2) = Y1(j2,2);
end
	
% throw away points that lie outside of line segments
dx1 = [min(X1,[],2)-x0, x0-max(X1,[],2)];
dy1 = [min(Y1,[],2)-y0, y0-max(Y1,[],2)];
dx2 = [min(X2,[],2)-x0, x0-max(X2,[],2)];
dy2 = [min(Y2,[],2)-y0, y0-max(Y2,[],2)];
% [irow,icol] = find([dx1 dy1 dx2 dy2]>1e-14);
[irow,icol] = find([dx1 dy1 dx2 dy2]>err);
idx = sort(unique(irow));
x0(idx) = nan;
y0(idx) = nan;

% retrieve only intersection points (no nans)
idx = find(~isnan(x0));
xi = x0(idx);  yi = y0(idx);
	
% determine indices of line segments that intersect
i1 = ceil(idx/N2);  i2 = rem(idx,N2);
if ~isempty(i2),  i2(find(i2==0)) = N2;  end
ii = [i1 i2];

% combine all intersection points
indx = union(find(imag(xi)),find(imag(yi)));
% indx = find(imag(xi));
for n=length(indx):-1:1
        j = indx(n);
        ii = [ii(1:j-1,:); ii(j,:); ii(j:end,:)];
        xi = [xi(1:j-1); imag(xi(j)); real(xi(j:end))];
        yi = [yi(1:j-1); imag(yi(j)); real(yi(j:end))];
end

% round intersection points
% xi = round(xi/1e-9)*1e-9;
% yi = round(yi/1e-9)*1e-9;

% check for identical intersection points (numerical error in epsilon)
[xt,ixt,jxt] = uniqueerr(xi,[],err);
[yt,iyt,jyt] = uniqueerr(yi,[],err);
xi = xt(jxt);  yi = yt(jyt);
[xi,yi] = ptserr(xi,yi,[x1;x2],[y1;y2],err);
% if ~isempty([xi yi])
%       N = length(xi);
%       xi1 = repmat(xi,N,1);  yi1 = repmat(yi,N,1);
%       xi2 = repmat(xi',N,1);  yi2 = repmat(yi',N,1);
%       xi2 = xi2(:);  yi2 = yi2(:);
%       dxi = abs(xi1 - xi2);  dyi = abs(yi1 - yi2);
%       indx = find((dxi>0 & dxi<eps) | dyi>0 & dyi<eps);
%       itmp = rem(indx,N);
%       if ~isempty(itmp),  itmp(find(itmp==0)) = N;  end
%       ixyi = [ceil(indx/N) itmp];
%       while ~isempty(ixyi)
%               idx =  find(ismember([xi(ixyi(1,:)) yi(ixyi(1,:))],...
%                                                        [x1 y1; x2 y2],'rows'));
%               xi(ixyi(1,:)) = xi(ixyi(1,idx));
%               yi(ixyi(1,:)) = yi(ixyi(1,idx));
%               iz = find(ixyi(:,1)==ixyi(1,2) & ixyi(:,2)==ixyi(1,1));
%               ixyi([1 iz],:) = [];
%       end
% end

% figure; hold; plotpoly(x1,y1,'b*-'); plotpoly(x2,y2,'ro--')
% [xi yi ii]
% keyboard
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [xi,yi,ii,ri] = filterintpts(x1,y1,x2,y2,xi,yi,ii)
%FILTERINTPTS  Filter polygon intersection points.
%   [XI,YI,II,RI] = FILTERINTPTS(X1,Y1,X2,Y2,XI,YI,II) filters the intersection
%   points from INTPTSALL for the polygon boolean operations.

%  Written by:  A. Kim


err = eps*1e5;

% extract unique intersection points
[a,i,j] = uniqueerr(flipud([xi yi]),'rows',err);
i = length(xi)-flipud(sort(i))+1;
ixy0 = [ii(i,:) xi(i) yi(i)];

% sort all unique intersection points along polygon 1
i1 = sort(unique(ixy0(:,1)));
for n=1:length(i1)
        indx1 = find(ixy0(:,1)==i1(n));
        dist = sqrt((x1(i1(n))-ixy0(indx1,3)).^2 + ...
                                        (y1(i1(n))-ixy0(indx1,4)).^2);
        [dist,isort] = sort(dist);  indx2 = indx1(isort);
        ixy0(indx1,:) = ixy0(indx2,:);
end

% identify vertex intersection points
nvtx(:,1) = ismembererr(ixy0(:,3:4),[x1 y1],'rows',err);
nvtx(:,2) = ismembererr(ixy0(:,3:4),[x2 y2],'rows',err);
ivtx = find(nvtx(:,1) | nvtx(:,2));

% procedure for determining which vertex intersection points to add:
%       1.  for each vertex intersection point, determine whether the points
%               before and after the intersection points on polygon 1 are inside
%               or outside polygon 2
%   2.  check conditions to add point

ri = [];  % initialize reverse intersection points index

if ~isempty(ivtx)

% polygon 1 point inside/outside of polygon 2
% for each vertex intersection point, check the last and next midpoints
% on polygon 1 to see if they are inside of polygon 2
        for n=1:length(ivtx)
% determine last point before current vertex intersection point
% (will either be the last point on polygon 1 or an intersection point)
                ilastpt1 = ixy0(ivtx(n),1);
                lastpt = [x1(ilastpt1) y1(ilastpt1)];   % on polygon 1
                if ivtx(n)>1
                        if ixy0(ivtx(n)-1,1)==ilastpt1
                                lastpt = ixy0(ivtx(n)-1,3:4);   % intersection point
                        end
                end
% determine next point after current vertex intersection point
% (will either be the next point on polygon 1 or an intersection point)
                if nvtx(ivtx(n),1)
                        inextpt1 = ixy0(ivtx(n),1) + 2;
                else
                        inextpt1 = ixy0(ivtx(n),1) + 1;
                end
                nextpt = [x1(inextpt1) y1(inextpt1)];   % on polygon 1
                if ivtx(n)<length(ixy0(:,1))
                        if ixy0(ivtx(n)+1,1)==inextpt1-1
                                nextpt = ixy0(ivtx(n)+1,3:4);   % intersection point
                        end
                end
% compute midpoints and determine whether inside or outside
                lastmidpt = lastpt + .5*(ixy0(ivtx(n),3:4) - lastpt);
                nextmidpt = nextpt + .5*(ixy0(ivtx(n),3:4) - nextpt);
% evaluate last and next points at 9 points: [nw,n,ne;w,c,e;sw,s,se]
                xa = lastmidpt(:,1);  ya = lastmidpt(:,2);
                xb = nextmidpt(:,1);  yb = nextmidpt(:,2);
                p1in(n,1) = inpolygonerr(xa,ya,x2,y2,err);
                p1in(n,2) = inpolygonerr(xb,yb,x2,y2,err);
%               xca = lastmidpt(:,1);  yca = lastmidpt(:,2);
%               xcb = nextmidpt(:,1);  ycb = nextmidpt(:,2);
%               xa = zeros(3,3);  ya = zeros(3,3);
%               xb = zeros(3,3);  yb = zeros(3,3);
%               xa(:,1) = xca - xca*eps;  xa(:,2) = xca;  xa(:,3) = xca + xca*eps;
%               ya(1,:) = yca + yca*eps;  ya(2,:) = yca;  ya(3,:) = yca - yca*eps;
%               xb(:,1) = xcb - xcb*eps;  xb(:,2) = xcb;  xb(:,3) = xcb + xcb*eps;
%               yb(1,:) = ycb + ycb*eps;  yb(2,:) = ycb;  yb(3,:) = ycb - ycb*eps;
%               ina = inpolygon(xa,ya,x2,y2);
%               inb = inpolygon(xb,yb,x2,y2);
%               if all(ina(:))
%                       ina = 1;
%               elseif ~any(ina(:))
%                       ina = 0;
%               else
%                       ina = .5;
%               end
%               if all(inb(:))
%                       inb = 1;
%               elseif ~any(inb(:))
%                       inb = 0;
%               else
%                       inb = .5;
%               end
%               xa = lastmidpt(:,1);  ya = lastmidpt(:,2);
%               xb = nextmidpt(:,1);  yb = nextmidpt(:,2);
%               p1in(n,:) = [ina inb];
        end

% determine which simple (no borders) vertex intersection points to add
%       - check whether points before and after vertex are same or different
% add point for same
        iadd1 = find(ismember(p1in,[0 0; 1 1],'rows'));
%       nbrdr1 = zeros(size(iadd1));

% determine which complex (borders) vertex intersection points to add:
%       - check whether points before and after border are same or different
%       - check whether number of borders are odd or even
% add point: diff & odd, same & even
        iadd2 = [];
        indx = [find(p1in(:,2)==.5) find(p1in(:,1)==.5)];
        if ~isempty(indx)
                if length(indx(:,1))==1
                        ichk = indx;
                elseif length(indx(:,1))>1
                        itmp = find(diff(indx(:,1))>1);
                        if isempty(itmp)
                                ichk = [1 length(p1in(:,1))];
                        else
                                ichk = [1 indx(itmp(1),2)];
                                for n=1:length(itmp)-1
                                        ichk = [ichk; indx(itmp(n)+1,1) indx(itmp(n+1),2)];
                                end
                                ichk = [ichk; indx(itmp(end)+1,1) indx(end)];
                        end
                end
                binout = [p1in(ichk(:,1),1) p1in(ichk(:,2),2)];
                bnum = ichk(:,2) - ichk(:,1);
                ipts = find( (binout(:,1)==~binout(:,2) & mod(bnum,2)) | ...
                                          (binout(:,1)==binout(:,2) & ~mod(bnum,2)) );
                if ~isempty(ipts)
                        iadd2 = ichk(ipts,2);
                end
        end

% procedure for adding additional vertex intersection points:
%       1.  for each vertex intersection point to be added, determine ordering
%       2.  add points with correct ordering

% determine ordering for non-border vertex intersection points:
% for each non-border vertex intersection point to add, check the last and
% next midpoints on polygon 2 to see if they are inside of polygon 1 to
% determine ordering of intersection points
        rvsordr1 = [];
        if ~isempty(iadd1)
                for n=1:length(iadd1)
% specify current vertex point to add
                        ivtxpt = ivtx(iadd1(n));
% determine last point before current vertex intersection point or border
% point (last point on polygon 2 or an intersection point)
                        ilastpt2 = ixy0(ivtxpt,2);
                        lastpt = [x2(ilastpt2) y2(ilastpt2)];   % on polygon 2
                        if ivtxpt>1
                                if ixy0(ivtxpt-1,2)==ilastpt2
                                        lastpt = ixy0(ivtxpt-1,3:4);    % intersection point
                                end
                        end
% determine next point after current vertex intersection point
% (will either be the next point on polygon 1 or an intersection point)
                        if nvtx(ivtxpt,2)
                                inextpt2 = ixy0(ivtxpt,2) + 2;
                        else
                                inextpt2 = ixy0(ivtxpt,2) + 1;
                        end
                        nextpt = [x2(inextpt2) y2(inextpt2)];   % on polygon 2
                        if ivtxpt<length(ixy0(:,1))
                                if ixy0(ivtxpt+1,2)==inextpt2-1
                                        nextpt = ixy0(ivtxpt+1,3:4);    % intersection point
                                end
                        end
% compute midpoints and determine whether inside or outside
                        lastmidpt = lastpt + (ixy0(ivtxpt,3:4) - lastpt) / 2;
                        nextmidpt = nextpt + (ixy0(ivtxpt,3:4) - nextpt) / 2;
                        xa = lastmidpt(:,1);  ya = lastmidpt(:,2);
                        xb = nextmidpt(:,1);  yb = nextmidpt(:,2);
%                       p2in(n,:) = inpolygon([xa xb],[ya yb],x1,y1);
                        p2in(n,1) = inpolygonerr(xa,ya,x1,y1,err);
                        p2in(n,2) = inpolygonerr(xb,yb,x1,y1,err);
                end
% if polygon 2 is counter-clockwise (intersection and union), reverse
% order if similar in/out states; if polygon 2 is clockwise (subtraction),
% reverse oder if opposite in/out states
                if sparea(x2,y2)<0
                        rvsordr1 = p1in(iadd1,1)==p2in(:,1);
                else
                        rvsordr1 = p1in(iadd1,1)~=p2in(:,1);
                end
        end

% determine ordering for border vertex intersection points:
%       - if the point on polygon 2 before the current vertex intersection point
%         is on the border of polygon 1, then the two polygons follow the same
%         direction along their common border
%       - if the point on polygon 2 before the current vertex intersection point 
%         is not on the border of polygon 1, then the two polygons do not follow 
%         the same direction along their common border
        rvsordr2 = [];
        if ~isempty(iadd2)
                for n=1:length(iadd2)
% specify current vertex point to add
                        ivtxpt = ivtx(iadd2(n));
% point on polygon 2 before the current vertex intersection point
                        ipt = ixy0(ivtxpt,1:2);
% reverse order border segments are in opposite direction
                        dir1 = atan2(y1(ipt(1)+1)-y1(ipt(1)),x1(ipt(1)+1)-x1(ipt(1)));
                        dir2 = atan2(y2(ipt(2)+1)-y2(ipt(2)),x2(ipt(2)+1)-x2(ipt(2)));
%                       if dir1~=dir2
                        if abs(dir1-dir2)>err
                                rvsordr2(n,1) = 1;
                        else
                                rvsordr2(n,1) = 0;
                        end
                end
%               rvsordr2 = inpolygon(x2(ipt),y2(ipt),x1,y1)~=.5;
        end

% add points using add points index and reverse order flag
        [iaddpts,isort] = sort(ivtx([iadd1; iadd2]));
        rvsordr = [rvsordr1; rvsordr2];
        rvsordr = rvsordr(isort);
        ixy = ixy0;
        for n=1:length(iaddpts)
                iins = find(ismembererr(ixy(:,3:4),ixy0(iaddpts(n),3:4),'rows',err));
                iadd = find(ismembererr([xi yi],ixy0(iaddpts(n),3:4),'rows',err));
                if rvsordr(n)
                        if length(iadd(:)) > 1
                                iadd = flipud(iadd(1:2));
                        end
                        ri = [ri; iins];  % store reverses
                else
                        iadd = iadd(1:2);
                end
                addvtxpts = [ii(iadd,:) xi(iadd) yi(iadd)];
                ixy = [ixy(1:iins-1,:); addvtxpts; ixy(iins+1:end,:)];
        end

% all intersection points
        ixy0 = ixy;     

end

% set outputs
ii = ixy0(:,1:2);  xi = ixy0(:,3);  yi = ixy0(:,4);
% check for epsilon error
[xi,yi] = ptserr(xi,yi,[x1;x2],[y1;y2],err); 
end
function msg = inputcheck(varargin)
%INPUTCHECK checks for proper mapping toolbox inputs
%
% msg = INPUTCHECK('type',arg1,arg1,...) returns an error
% message string if the arguments are inconsistent with the
% mapping toolbox datatype. The following calling forms list
% the datatypes checked by INPUTCHECK.
%
% msg = INPUTCHECK('scalar',lat,lon)
% msg = INPUTCHECK('vector',lat,lon)
% msg = INPUTCHECK('xyvector',lat,lon)
% msg = INPUTCHECK('cellvector',latc,lonc);
% msg = INPUTCHECK('cellxyvector',latc,lonc);
% msg = INPUTCHECK('rmm',map,maplegend);
% msg = INPUTCHECK('gmm',lat,lon,map)  general matrix map

% As needed, add
% msg = INPUTCHECK('xycellvector',latc,lonc);
% msg = INPUTCHECK('geostruct',s) geographic data structure
% msg = INPUTCHECK('mstruct',mstruct) map projection structure
%
	
%  Copyright 1996-2002 Systems Planning and Analysis, Inc. and The MathWorks, Inc.
%  $Revision: 1.3 $    $Date: 2002/03/20 21:26:47 $
	
if nargin < 2; error('Incorrect number of input arguments'); end
type = varargin{1};
varargin(1) = [];

switch type
case 'scalar'
   msg = checkscalar(varargin{:});
case 'vector'
   msg = checkvector(varargin{:});
case 'xyvector'
   msg = checkxyvector(varargin{:});
case 'cellvector'
   msg = checkcellvector(varargin{:});
case 'cellxyvector'
   msg = checkxycellvector(varargin{:});
case 'rmm' % regular matrix map
   msg = checkrmm(varargin{:});
case 'gmm' % general matrix map
   msg = checkgmm(varargin{:});
case 'geostruct' % geographic data structure
   msg = checkgeostruct(varargin{:});
case 'mstruct' % map projection structure
   msg = checkcellmstruct(varargin{:});
otherwise
   error('Unrecognized argument type')
end
end	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function msg = checkscalar(varargin);
	
if nargin ~= 2;
   error('Incorrect number of input arguments');
elseif nargin == 2
   [lat,lon]= deal(varargin{:});
end
	
	
msg = [];
	
if ~isa(lat,'numeric') | ~isa(lon,'numeric') | ...
      length(lat) > 1 | length(lon) > 1
   
   msg = 'Latitude and longitude must be scalars';
   
end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function msg = checkvector(varargin);
	
if nargin ~= 2;
   error('Incorrect number of input arguments');
elseif nargin == 2
   [lat,lon]= deal(varargin{:});
end
	
	
msg = [];
	
if ~isa(lat,'numeric') | ~isa(lon,'numeric')
	   
   msg = 'Latitude and longitude must be numeric vectors';
	   
elseif any([min(size(lat))    min(size(lon))]    ~= 1) | ...
      any([ndims(lat) ndims(lon)] > 2)
	   
   msg = 'Latitude and longitude inputs must be vectors';
	   
elseif ~isequal(size(lat),size(lon))
	   
   msg = 'Inconsistent dimensions on latitude and longitude input';
	   
elseif ~isequal(find(isnan(lat)),find(isnan(lon)))
   
   msg = 'Inconsistent NaN locations in latitude and longitude input';
	   
end
end	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function msg = checkxyvector(varargin);
	
if nargin ~= 2;
   error('Incorrect number of input arguments');
elseif nargin == 2
   [lat,lon]= deal(varargin{:});
end
	
	
msg = [];
	
if ~isa(lat,'numeric') | ~isa(lon,'numeric')
	   
   msg = 'x and y must be numeric vectors';
	   
elseif any([min(size(lat))    min(size(lon))]    ~= 1) | ...
      any([ndims(lat) ndims(lon)] > 2)
	   
   msg = 'x and y inputs must be vectors';
	   
elseif ~isequal(size(lat),size(lon))
	   
   msg = 'Inconsistent dimensions on x and y input';
	   
elseif ~isequal(find(isnan(lat)),find(isnan(lon)))
	   
   msg = 'Inconsistent NaN locations in x and y input';
	   
end
end	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function msg = checkcellvector(varargin);
	
if nargin ~= 2;
   error('Incorrect number of input arguments');
elseif nargin == 2
   [lat,lon]= deal(varargin{:});
end
	
	
msg = [];
	
if ~isa(lat,'cell') | ~isa(lon,'cell')
   
   msg = 'Latitude and longitude must be cell arrays';
   return
   
elseif ~isequal(size(lat),size(lon))
   
   msg = 'Inconsistent dimensions on latitude and longitude input';
end


for i=1:length(lat)
   if ~isequal(size(lat{i}),size(lon{i}))
      msg = 'Inconsistent latitude and longitude dimensions within a cell';
   end
end
end	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function msg = checkxycellvector(varargin);
	
if nargin ~= 2;
   error('Incorrect number of input arguments');
elseif nargin == 2
   [lat,lon]= deal(varargin{:});
end
	
	
msg = [];
	
if ~isa(lat,'cell') | ~isa(lon,'cell')
	   
   msg = 'X and y must be cell arrays';
   return
	   
elseif ~isequal(size(lat),size(lon))
   
   msg = 'Inconsistent dimensions on x and y input';
end
	
	
for i=1:length(lat)
   if ~isequal(size(lat{i}),size(lon{i}))
      msg = 'Inconsistent x and y dimensions within a cell';
   end
end
end
	
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function msg = checkrmm(varargin);

msg = [];

[map,maplegend] = deal(varargin{1:2});

if ndims(map) > 2
    msg = 'Input map can not have pages';
end
	 
if ~isequal(sort(size(maplegend)),[1 3])
    msg = 'Input maplegend must be a 3 element vector';
end
	 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end
function msg = checkgmm(varargin);
	
if nargin ~= 3;
   error('Incorrect number of input arguments');
elseif nargin == 3
   [lat,lon,map]= deal(varargin{:});
end


msg = [];
	
if ~isa(lat,'numeric') | ~isa(lon,'numeric') | ~isa(map,'numeric')
   
   msg = 'Latitude and longitude  and map matrices must be numeric vectors';
   
elseif any([min(size(lat))    min(size(lon))]    < 2) | ...
      any([ndims(lat) ndims(lon)] > 2)
   
   msg = 'Latitude and longitude inputs must be two-dimensional matrices';
   
elseif ~isequal(size(lat),size(lon))
   
   msg = 'Inconsistent dimensions on latitude and longitude input';
   
elseif ~isequal(find(isnan(lat)),find(isnan(lon)))
   
   msg = 'Inconsistent NaN locations in latitude and longitude input';
   
end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function msg = checkgeostruct(varargin);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end

function msg = checkcellmstruct(varargin);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
end

function [b,ndx,pos] = uniqueerr(a,flag,err)
%UNIQUEERR set unique with erro
	
% Copyright 1996-2002 Systems Planning and Analysis, Inc. and The MathWorks, Inc.
% $Revision: 1.4 $ $Date: 2002/03/20 21:26:58 $
	
if isempty(a), b = a; ndx = []; pos = []; return, end
	
if nargin<3,  err = 0;  end  %%%
	
rowvec = 0;
if nargin==1 | isempty(flag),
  rowvec = size(a,1)==1;
  [b,ndx] = sort(a(:));
  % d indicates the location of matching entries
  db = abs(b((1:end-1)') - b((2:end)'));
  d = db>=0 & db<=err;
  n = length(a);
else
  if ~isstr(flag) | ~strcmp(flag,'rows'), error('Unknown flag.'); end
  [b,ndx] = sortrows(a);
  n = size(a,1);
  if n > 1,
    % d indicates the location of matching entries
        db = abs(b(1:end-1,:) - b(2:end,:));
    d = db>=0 & db<=err;
    if ~isempty(d), d = all(d,2); end
  else
    d = [];
  end
end
	
if nargout==3, % Create position mapping vector
  pos(ndx) = cumsum([1;~d]);
  pos = pos';
end
	
b(d,:) = [];
ndx(d) = [];
	
if rowvec,
  b = b.';
  ndx = ndx.';
  if nargout==3, pos=pos.'; end
end 
end
function [xi,yi] = ptserr(xi,yi,xa,ya,err)
% PTSERR removes floating point errors of xi and yi by setting them to
% x1 y1 values

%  Copyright 1996-2002 Systems Planning and Analysis, Inc. and The MathWorks, Inc.
%  $Revision: 1.4 $ $Date: 2002/03/20 21:26:52 $
	
ni = length(xi);  na = length(xa);
Xi = reshape(reshape(repmat(xi,na,1),ni,na)',ni*na,1);
Yi = reshape(reshape(repmat(yi,na,1),ni,na)',ni*na,1);
Xa = repmat(xa,ni,1);
Ya = repmat(ya,ni,1);
ix = find( (abs(Xi-Xa)>0 & abs(Xi-Xa)<=err) );
iy = find( (abs(Yi-Ya)>0 & abs(Yi-Ya)<=err) );
xi(ceil(ix/na)) = Xa(ix);
yi(ceil(iy/na)) = Ya(iy); 
end



function baakGrote_Callback(hObject, eventdata, handles)
% hObject    handle to baakGrote (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of baakGrote as text
%        str2double(get(hObject,'String')) returns contents of baakGrote as a double

baakGrote = str2double(get(hObject, 'String'));
if isnan(baakGrote)
    set(hObject, 'String', 0);
    errordlg('Gelieve een getal in te voeren');
end

% Save the new focal value
handles.metricdata.baakGrote = baakGrote;
guidata(hObject,handles)
end

% --- Executes during object creation, after setting all properties.
function baakGrote_CreateFcn(hObject, eventdata, handles)
% hObject    handle to baakGrote (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end
