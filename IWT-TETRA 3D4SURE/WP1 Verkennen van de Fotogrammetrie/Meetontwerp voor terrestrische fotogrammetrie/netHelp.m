iconHelp = fullfile(matlabroot,'/toolbox/matlab/icons/helpicon.gif');
iconBook = fullfile(matlabroot,'/toolbox/matlab/icons/help_ug.gif');
iconBook2 = fullfile(matlabroot,'/toolbox/matlab/icons/bookicon.gif');
iconGui = fullfile(matlabroot,'/toolbox/matlab/icons/figureicon.gif');

% workflow
workflow = uitreenode('v0', 'Workflow', 'Workflow', iconBook2, false);
workflow.add(uitreenode('v0', 'Camera',  'Camera instellingen',  [], true));
workflow.add(uitreenode('v0', 'ImagingStation', 'Opstelling met totaal station', ...
    'fig\totalStationSmall.png', true));
workflow.add(uitreenode('v0', 'Banana', 'Banana', [], true));
workflow.add(uitreenode('v0', 'Orange', 'Exporteer naar Move3', 'fig\move3Small.png', true));
 
% info
info = uitreenode('v0', 'Informatie', 'Informatie', iconHelp, false);
info.add(uitreenode('v0', 'Camera calibratie', 'Camera calibratie', ...
    iconBook, true));
info.add(uitreenode('v0', 'Lens vertekening', 'Lens vertekening', ...
    iconBook, true));
info.add(uitreenode('v0', 'Statistisch testen', 'Statistisch testen', ...
    iconBook, true));
 
% Root node
root = uitreenode('v0', 'NetDesign', 'NetDesign', iconGui, false);
root.add(workflow);
root.add(info);
 
% Tree
figure('pos',[300,300,150,150]);
mtree = uitree('v0', 'Root', root);