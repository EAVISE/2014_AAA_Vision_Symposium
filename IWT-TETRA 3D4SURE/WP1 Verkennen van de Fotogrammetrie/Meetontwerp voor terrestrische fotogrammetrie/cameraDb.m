function cameraDb(varargin)
% CAMERADB MATLAB code for cameraDb.fig
%      CAMERADB, by itself, creates a new CAMERADB or raises the existing
%      singleton*.
%
%      H = CAMERADB returns the handle to a new CAMERADB or the handle to
%      the existing singleton*.
%
%      CAMERADB('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CAMERADB.M with the given input arguments.
%
%      CAMERADB('Property','Value',...) creates a new CAMERADB or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before cameraDb_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to cameraDb_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help cameraDb

% Last Modified by GUIDE v2.5 12-Sep-2013 16:10:57

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @cameraDb_OpeningFcn, ...
                   'gui_OutputFcn',  @cameraDb_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT
setappdata(0, 'camDb', true);



% --- Executes just before cameraDb is made visible.
function cameraDb_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to cameraDb (see VARARGIN)

% Choose default command line output for cameraDb
handles.output = hObject;

[num,txt,raw] = xlsread('doc\camera','Camera');

spaces = {};
for i = 1:length(txt)
    spaces{i} = '  ';
end

set(handles.listbox1,'string',strcat({txt{:,1}}, spaces,{txt{:,2}}));

handles.spiegelReflex = getappdata(0, 'spiegelReflex');
handles.focal = getappdata(0, 'focal');
handles.resolution1 = getappdata(0, 'resolution1');
handles.resolution2 = getappdata(0, 'resolution2');
handles.pixelsize = getappdata(0, 'pixelsize');

handles.db.num = num;
handles.db.txt = txt;

javaFrame = get(hObject,'JavaFrame');
javaFrame.setFigureIcon(javax.swing.ImageIcon('fig/camera-photo-5.png'));

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes cameraDb wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = cameraDb_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1
index = get(handles.listbox1,'value');
num = handles.db.num;
set(handles.text1, 'string', num2str(num(index,1))); % pixels
set(handles.text2, 'string', num2str(num(index,2))); % pixels
set(handles.text3, 'string', num2str(num(index,3))); % pixel size
set(handles.text4, 'string', num2str(num(index,4))); % focal length

switch num(index,5)
    case 1
        typeCam = 'Metrisch';
    case 2
        typeCam = 'Spiegel reflex';
    case 3
        typeCam = 'Consument';
end
set(handles.text5, 'string', typeCam);
switch num(index,6)
    case 0
        ImagingStat = '.';
    case 1
        ImagingStat = 'Totaal station met camera';
end
set(handles.text6, 'string', ImagingStat);


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in selectCam.
function selectCam_Callback(hObject, eventdata, handles)
% hObject    handle to selectCam (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
index = get(handles.listbox1,'value');

num = handles.db.num;
setappdata(0, 'resolution1', num(index,1));
setappdata(0, 'resolution2', num(index,2));
setappdata(0, 'pixelsize', num(index,3));

if ~isnan(num(index,4))
    setappdata(0, 'focal', num(index,4)); % focal length
end

switch num(index,5)
    case 1
        setappdata(0, 'camType', 'metrisch');
    case 2
        setappdata(0, 'camType', 'spiegelReflex');
    case 3
        setappdata(0, 'camType', 'consument');
end
switch num(index,6)
    case 0
        setappdata(0, 'totalStation', false);
    case 1
        setappdata(0, 'totalStation', true);
end
switch num(index,6)
    case 0
        setappdata(0, 'pano', false);
    case 1
        setappdata(0, 'pano', true);
end
setappdata(0, 'camDb', false);

hMainGui = getappdata(0, 'hMainGui');
fhUpdateCam = getappdata(hMainGui, 'fhUpdateCam');
feval(fhUpdateCam)

close(gcf);
