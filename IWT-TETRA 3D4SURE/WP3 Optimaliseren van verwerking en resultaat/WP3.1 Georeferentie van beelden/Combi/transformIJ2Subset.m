function [u,v] = transformIJ2Subset(i,j,I,boundingSize,xData,yData)
%TRANSFORMIJ2SUBSET Summary of this function goes here
%   Detailed explanation goes here
[m n b] = size(I);
switch nargin
  case 4
     xData = [1 n];
     yData = [1 m];
  case 5
     xData = [1 n];
     yData = [1 m];
end
iY = i; jX = j;
i = ((yData(2)-yData(1)+1)./m) * (i-yData(1)+1);
j = ((xData(2)-xData(1)+1)./n) * (j-xData(1)+1);

iRaw = i; jRaw = j;
i = round(i); j = round(j);

iMin = floor(i) - boundingSize; 
if iMin < 1, iMin = 1; end

jMin = floor(j) - boundingSize; 
if jMin < 1, jMin = 1; end

u = iRaw - iMin +1;
v = jRaw - jMin +1;
end

