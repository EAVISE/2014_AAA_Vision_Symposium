function [ output_args ] = adjustGCPs(session)
%ADJUSTGCPS Summary of this function goes here
%   Detailed explanation goes here
%
% Bas Altena

imShow = false;
workDir = cd();
affineDir = [matlabroot '\toolbox\images\images\private'];

[projName, pathProj] = uigetfile({'*.p4d','Pix4D project (*.p4d)'}, ...
   'Open photogrammetry project...', ...
   pwd, ...
   'MultiSelect', 'off');

d = dir(pathProj);
nameFolds = {d([d(:).isdir]).name}';
if sum(ismember(nameFolds,{'1_initial'})) ~= 0
    pixDir = [pathProj projName(1:end-4) '\params'];
else
    pixDir = [pathProj projName(1:end-4) '\1_initial\params'];
end

[dummy, imDir] = uigetfile({'*.*',  'All Files (*.*)'}, ...
   'Select an image from the project...', ...
   pwd, ...
   'MultiSelect', 'off');

clear dummy
%% import photo order Close-Up pole
cd('C:\3D4Sure\Kiezegem\CloseUp');
closeDir = cd();

fid = fopen('photoOrder.txt');
closeUp = {};
counter = 1;
while ~feof(fid)
    tline=fgetl(fid);
    sline = regexp(tline, ' ', 'split');
    aline = cellfun(@str2num, sline(2:end));
    closeUp.GCP(counter) = aline(1);
    closeUp.height(counter) = aline(2);
    closeUp.imFile{counter} = sline{1};
    counter = counter + 1;
end
fclose(fid);
closeUp.focal = 2376.62; % in Pixels
closeUp.pixelSize = 0.00883608; % in [mm]

cd(workDir);
clear sline aline tline fid counter
%% extract estimate of GCP in closeup imagery
cd(closeDir);
fid = fopen('photoOrder2.txt');

counter = 1;
while ~feof(fid)
    tline=fgetl(fid);
    sline = regexp(tline, ' ', 'split');   
    aline = cellfun(@str2num, sline(2:end));
    closeUp.Target(counter,1) = aline(1);
    closeUp.Target(counter,2) = aline(2);
    counter = counter + 1;
end
fclose(fid);

cd(workDir);
clear sline aline tline fid counter
%% read camera internals
cd(pixDir);
fid = fopen([projName(1:end-4) '_pix4d_calibrated_internal_camera_parameters.cam']);

counter = 1;
while ~feof(fid)
    tline=fgetl(fid);
    sline = regexp(tline, ' ', 'split');
    switch sline{1}
        case 'F' % focal length
            F  = cellfun(@str2double, sline(2));
        case 'Px' % Principle point
            Px  = cellfun(@str2double, sline(2));
        case 'Py' % Principle point
            Py  = cellfun(@str2double, sline(2));
        case 'Sw' % image size [mm]
            Sw  = cellfun(@str2double, sline(2));
        case 'Sh' % image size [mm]
            Sh  = cellfun(@str2double, sline(2));
        case 'K1' % radial distortion
            K(1) = cellfun(@str2double, sline(2));
        case 'K2' % radial distortion
            K(2) = cellfun(@str2double, sline(2));
        case 'K3' % radial distortion
            K(3) = cellfun(@str2double, sline(2));
        case 'T1' % tangential distortion
            T(1) = cellfun(@str2double, sline(2));
        case 'T2' % tangential distortion
            T(2) = cellfun(@str2double, sline(2));
    end
end
fclose(fid);

if ~exist('Sw','var') % normal full frame camera
    Sw = 35.99999888;
end
if ~exist('Sh','var')
    Sh = 23.921823360000001;
end


%% read camera positions
fid = fopen([projName(1:end-4) '_calibrated_external_camera_parameters.txt']);
camXYZ = textscan(fid, '%s %f %f %f %*[^\n]', 'headerLines', 1);
fclose(fid);

%% read control points positions
fid = fopen([projName(1:end-4) '_measured_estimated_gcps_position.txt']);
gcpXYZ = textscan(fid, '%s %f %f %f %*[^\n]', ...
    'delimiter', {',',' '}, 'MultipleDelimsAsOne', 1, 'headerLines', 1);
fclose(fid);

cd(workDir);
clear tline sline
%% import Bingo file

cd(pixDir);
[fileName, pathName] = uigetfile({'*.txt','Text ASCII Raster (*.txt)'; ...
   '*.*',  'All Files (*.*)'}, ...
   'Select GCP file...', ...
   pwd, ...
   'MultiSelect', 'off');
fid = fopen([pathName fileName]);

cd(imDir);
imGCP = {};
counter = 1;

tline=fgetl(fid);
sline = regexp(tline, ' ', 'split');
while ~feof(fid)
    imfile = sline{1};
    gcpHeader = sline{2};
    tline=fgetl(fid); %-99 or GCP
    if tline == -1 
        break 
    end
    sline = regexp(tline, ' ', 'split');
    num = cellfun(@str2num, sline(1));
    if num ~= -99
        endList = false;
        while ~endList
            imGCP.file{counter} = imfile;
            imGCP.gcp{counter} = num;
            imGCP.X(counter) = cellfun(@str2num, sline(2));
            imGCP.Y(counter) = cellfun(@str2num, sline(3));
            counter = counter + 1;
            
            tline=fgets(fid);
            if tline == -1, 
                break 
            end
            sline = regexp(tline, ' ', 'split');
            if cellfun(@str2num, sline(1)) == -99
                tline=fgets(fid);
                if tline == -1, 
                    break 
                end
                sline = regexp(tline, ' ', 'split');
                endList = true;
            else
                num = cellfun(@str2num, sline(1));
            end
        end
    else
        tline=fgets(fid);
        if tline == -1 
            break 
        end
        sline = regexp(tline, ' ', 'split');
    end
end
fclose(fid);

cd(workDir);
clear counter imfile endList
%% connect close-up cameras to each other

%% connect close-up's to UAV imagery
for i = 1:length(imGCP.file) %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    tic;
    %% UAV imagery
    camFile = imGCP.file{i};
    cd(imDir);
    I1 = imread([camFile '.jpg']); % UAV image
    
    cd(workDir);
    x = imGCP.X(i); y = imGCP.Y(i); % image coordinates
%     [x1 y1] = undistortImageCoordinate(x,y,I1,K,T,Px,Py,Sw,Sh);
    xi = x * size(I1,2) / Sw;
    yi = y * size(I1,1) / Sh;
    x1 = xi + (Sw/2)* size(I1,2) / Sw; % Px = Sw/2: point changes with calibration
    y1 = (Sh/2) * size(I1,1) / Sh - yi; % Py = Sh/2
    clear x y
    %% Pole imagery
    gcpName = imGCP.gcp{i};
    poleIm = find(closeUp.GCP==gcpName);
    [maxHeight Idx] = max(closeUp.height(poleIm));
    poleFile = closeUp.imFile{poleIm(Idx)};
    
    cd(closeDir);
    I2 = imread(poleFile); % close-up image
    if (size(I1,3) == 1) && (size(I2,3) ~= 1)
        I2 = I2(:,:,2);
    end
    cd(workDir);
    
    xyTarget = closeUp.Target(poleIm(Idx),1:2);
    x2 = xyTarget(1); y2 = xyTarget(2); % image coordinates
        
    clear poleIm xyTarget
    %% Estimate rough scale
    camNr = find(ismember(camXYZ{1},{[camFile '.JPG']}) == 1);
    if isempty(camNr)
        camNr = find(ismember(camXYZ{1},{[camFile '.jpg']}) == 1);
    end
    camZ = camXYZ{4}(camNr); % flight heigt of the UAV
    
   
    gcpNr = find(ismember(gcpXYZ{1},{num2str(gcpName)}) == 1);
    gcpZ = gcpXYZ{4}(gcpNr); % flight heigt of the UAV
    
    diffZ = camZ-gcpZ;
    pixelSize = Sw / size(I1,2);
    
    gsdClose = 1.2.*(maxHeight/(closeUp.focal.*closeUp.pixelSize*1e-3)).*closeUp.pixelSize*1e-3;
    gsdUav = (diffZ/(F*1e-3)).* pixelSize.*1e-3;
    D = gsdClose/gsdUav;
    %% Create subset UAV imagery
    I1Sub = createImSubset(y1,x1,I1,100);
    disp(['i = ' num2str(i)]);
    [I2New] = equalizeImages(I1Sub, imresize(I2, D));   
    
    delta = 15; % spacing away from center
    I1Sub = createImSubset(y1,x1,I1,delta);
    [y1Sub,x1Sub] = transformXY2Subset(y1, x1,I1, delta);
    I2Sub = createImSubset(y2.*D, x2.*D, I2New, delta);
    [y2Sub,x2Sub] = transformXY2Subset(y2.*D, x2.*D,I2New, delta);  
    %% Estimate rotation
    
    mserEst = true;
    
    if mserEst
        % use MSER to get rough orientation
%         [I1rough,I2rough] = imPercentile(rgb2gray(I1Sub),rgb2gray(createImSubset(y2.*D, x2.*D, ...
%             imresize(I2, D), delta)), 80);
        if size(I1,3)==1
            [theta,Dsub] = combineWithMSER(I1Sub, createImSubset(y2.*D, x2.*D, ...
                imresize(I2, D), delta));
        else
            [theta,Dsub] = combineWithMSER(rgb2gray(I1Sub),rgb2gray(createImSubset(y2.*D, x2.*D, ...
                imresize(I2, D), delta)));
        end
        r = [cosd(theta) -sind(theta); ...
        sind(theta) cosd(theta)];
        tranRot = maketform('affine', [double(r); [0 0]]);
    else  % optimize with least squares matching
        [optimizer, metric]  = imregconfig('multimodal'); % 'monomodal'
        optimizer = registration.optimizer.RegularStepGradientDescent;
        metric = registration.metric.MeanSquares;
        cd(affineDir);
        foundEstimate = false;
        while ~foundEstimate
            diary([workDir '\diary.txt']);
            [r, t] = regmex(double(rgb2gray(I1Sub)), [1 1], [1 1], ...
                double(rgb2gray(I2Sub)), [1 1], [1 1], ...
                0, 'rigid', ...
                [Rsub(1,1) Rsub(1,2) 0 Rsub(2,2) Rsub(2,1) 0], ... % [1 0 0 1 0 0], ... 
                optimizer, ...
                metric,...
                3);
            diary off
            A = importdata([workDir '\diary.txt']);
            errorLines = size(A,1);
            if errorLines == 0
                foundEstimate = true;
            else
                if strfind([A{:}],'MaximumStepLength') > 0
                    optimizer.MaximumStepLength = ...
                        optimizer.MaximumStepLength./2;
                else
                    foundEstimate = true;
                end
            end
            delete([workDir '\diary.txt']);
        end
        tranRot = maketform('affine', [r'; [0 0]]);
    end
    cd(workDir);
    
    if exist('Dsub','var')
        if Dsub > 0.5 && Dsub < 2
            % the rough estimate is not that wrong
            D = D./Dsub;
        end
    end
    
    [x2Rot, y2Rot] = tformfwd(tranRot, x2, y2);
    [I2Rot, xRot, yRot] = imtransform(I2, tranRot, 'bilinear');
    if imShow
        imagesc(xRot,yRot,I2Rot); axis square;
        hold on;
        scatter(x2Rot, y2Rot, '+b');
        set(gcf,'Units','Normalized');
        set(gcf, 'OuterPosition', [0 0.5 0.5 0.5])
    end
    %% Estimate shear and skew

    alpha = atan((x1.*pixelSize-Px)/F);
    Sx = tan(alpha)/sin(alpha);
    beta = atan((y1.*pixelSize-Py)/F);
    Sy = tan(beta)/sin(beta);
      
    clear alpha beta pixelSize
    S = [1 Sx-1 0; Sy-1 1 0; 0 0 1]; %skew
    tSkew = maketform('affine', S);

    [I2Skew, xSkew, ySkew] = imtransform(I2Rot,tSkew, 'bilinear', ...
        'Udata',xRot,'Vdata',yRot);
    [xMark, yMark] = tformfwd(tSkew, x2Rot, y2Rot); % transform point center
    
    searchRange = 10; % search extent of pixel movement back and forward
    xRuler = linspace(xSkew(1),xSkew(2),size(I2Skew,2));
    yRuler = linspace(ySkew(1),ySkew(2),size(I2Skew,1));
    pixelSpace = delta./D;
    tScale = maketform('affine', [double(D) 0; 0 double(D); 0 0]); % changed 15:36
    C = zeros(length(-searchRange:searchRange));
    for j = -searchRange:searchRange
        for k = -searchRange:searchRange
            if size(I1Sub,3) == 1
                I1piece = double(I1Sub);
            else
                I1piece = double(I1Sub(:,:,3));
            end
            I2bigSub = createImSubset(yMark+j, xMark+k, I2Skew, ...
                round(pixelSpace), xSkew, ySkew);           
%             I2Sub = imtransform(I2bigSub, tScale, 'bilinear');
            I2Sub = imtransform(I2bigSub, tScale, 'bicubic', ...
                'Size', size(I1piece));
            if size(I2Sub,3) == 1
                I2piece = double(I2Sub);
            else
                I2piece = double(I2Sub(:,:,3));
            end
            
            if size(I1piece,1)~=size(I2piece,1)
                if y1+delta < size(I1,1) % top needs extension
                    I2piece = I2piece(round(delta-y1)+2:end,1:end);
                else % bottom needs extension
                    I2piece = I2piece(1:end-round(delta+y1 - size(I1,1)),1:end);
                end
            end

            if size(I1piece,2)~=size(I2piece,2)
                if x1+delta < size(I1,2) % leftside needs extension
                    I2piece = I2piece(1:end,round(delta-x1)+2:end);
                else % rightside needs extension
                    I2piece = I2piece(1:end,1:end-round(delta+x1 - size(I1,2)));
                end
            end
            
            % normalized cross correlation
            correlation = (1/length(I2piece(:))) .* sum( (I2piece(:)- mean(I2piece(:))).* ...
                (I1piece(:)- mean(I1piece(:))) )./ (std(I1piece(:))*std(I2piece(:))) ;
            C(j+1+searchRange,k+1+searchRange) = correlation;
        end
    end
    I2bigSub = createImSubset(yMark, xMark, I2Skew, ...
                round(pixelSpace), xSkew, ySkew);           
    I2Sub = imtransform(I2bigSub, tScale, 'bicubic');
            
    
    [c1,c2] = meshgrid(-searchRange:searchRange, -searchRange:searchRange);
    f = fit( [c1(:) c2(:)], C(:), 'poly22' );
    g = @(c) max(C(:))-f(c);
    [cfit,fval,exitflag] = fminsearch(g,[1 1],optimset('Display','off')); % peak of the correlation metric
    if imShow
        figure();
        set(gcf,'Units','Normalized');
        plot(f, [c1(:),c2(:)], C(:));
        set(gcf, 'OuterPosition', [0 0 0.5 0.5])
    end
    
    if exitflag<1 % deviates out of template
        x1Adj = x1;
        y1Adj = y1;
    else
        x1Adj = x1 - cfit(1).*D; % new estimates
        y1Adj = y1 - cfit(2).*D;
    end
    
    disp('Adjusted GCP by: ');
    disp([cfit(1) cfit(2)].*D);
    if imShow
        figure();
        set(gcf,'Units','Normalized');
        set(gcf, 'OuterPosition', [0.5 0 0.5 1])
        title('blue measured, red estimated');
        subplot(2,1,1); imagesc(I1Sub); axis equal; axis tight; ax = axis;
        hold on; scatter(x1Sub, y1Sub, '+b');
        if fval==1
            scatter(x1Sub-(cfit(1).*D), y1Sub-(cfit(2).*D), 'or'); 
        end
        hold off;
        subplot(2,1,2); imagesc(I2Sub); axis equal; axis tight; 
        hold on; scatter(x2Sub, y2Sub, '+b'); hold off;
        pause(3); close all;
    end

    % transform to image coordinates
    [yNew,xNew] = transformSubset2XY(y1Sub,x1Sub,y1,x1, delta);
    
    xi = x1Adj - (Sw/2)* size(I1,2)/Sw;
%     xi = (Sw/2)* size(I1,2)/Sw - x1Adj;
    xCross = xi * Sw / size(I1,2) ;
    
    yi = (Sh/2) * size(I1,1) / Sh - y1Adj;
%     yi = y1Adj - (Sh/2) * size(I1,1) / Sh;
    yCross =  yi * Sh / size(I1,1);
    
    SubPix{i} = {camFile, gcpName, yCross, xCross};
    toc;
end

% write to GCP file
cd(imDir)
imFiles = ls('*.JPG');
fid = fopen([pathName fileName(1:end-4) 'New' fileName(end-3:end)], 'w');

obs = [SubPix{:}];
for i = 1:size(imFiles) % create new Bingo file
    fprintf(fid, '%s %s', imFiles(i,1:end-4), gcpHeader);

    % find image in gcp estimation
    ind = find(ismember({obs{1,1:4:end}},imFiles(i,1:end-4)));
    if ~isempty(ind)
        for j = 1:length(ind)
            if isstr(SubPix{ind(j)}{2})
                fprintf(fid, '%s %2.6f %2.6f\n', ...
                    SubPix{ind(j)}{2}, SubPix{ind(j)}{4}, SubPix{ind(j)}{3});
            else
                fprintf(fid, '%s %2.6f %2.6f\n', ...
                    int2str(SubPix{ind(j)}{2}), SubPix{ind(j)}{4}, SubPix{ind(j)}{3});
            end
        end
    end
    fprintf(fid, '%i\n', -99);
end

fclose(fid);

cd(workDir);
end
function [i j] = pickPoint()
gotPoint = false;
while ~gotPoint
    [i, j, button] = ginput(1);
    switch button
        case 1
            gotPoint = true;
        case 2
            zoom(0.5);
        case 3  
            lim = axis;
            zoom(2);
            lim = [i+diff(lim(1:2))/2*[-1 1] j+diff(lim(3:4))/2*[-1 1]]; 
            axis(lim);
            zoom(2);
    end
end
end
function [u v] = undistortImageCoordinate(x,y,I,K,T,Px,Py,Sw,Sh)
% remove radial and tangential distortion form image coordinate
% x,y are in image coordinates, originating at the topleft
%
% B. Altena - November 2013
scaling = 1e-3;

xi = x * size(I,2) / Sw;
x = xi*scaling;
yi = y * size(I,1) / Sh;
y = yi*scaling;
r = sqrt(x.^2 + y.^2);  

deltaX = K(1)*x*r^2 + K(2)*x*r^4 + K(3)*x*r^6 + ...
    T(1)*2*x*y + T(2)*(r^2+2*x^2);
u = xi + deltaX + Px* size(I,2) / Sw;

deltaY = K(1)*y*r^2 + K(2)*y*r^4 + K(3)*y*r^6 + ...
    T(1)*(r^2+2*y^2) + T(2)*2*x*y;
v = Py * size(I,1) / Sh - yi - deltaY;
end
function [I2New,tNew] = equalizeImages(I1, I2)
% equalize the histogram of image I2 to the histogram of I1
%
% B.Altena - November 2013

% check dimensions of imagery
[dummy1,dummy2,b1] = size(I1);
[dummy1,dummy2,b2] = size(I2);

switch b1
    case 1
        monochrome = true;
    case 3
        monochrome = false;
end
if b2 == 1
    monochrome = true;
end

if monochrome
    histI1 = hist(double(I1(:)),256);
    histI1 = histI1./max(histI1);
    I2New = histeq(I2, histI1);
else
    histR = hist(reshape(double(I1(:,:,1)),[],1),256);
    histR = histR./max(histR);
    histG = hist(reshape(double(I1(:,:,2)),[],1),256);
    histG = histG./max(histG);
    histB = hist(reshape(double(I1(:,:,3)),[],1),256);
    histB = histB./max(histB);

    [I2R,tR] = histeq(I2(:,:,1), histR);
    [I2G,tG] = histeq(I2(:,:,2), histG);
    [I2B,tB] = histeq(I2(:,:,3), histB);
    I2New = cat(3, I2R, I2G, I2B);
    tNew = cat(3, tR, tG, tB);
end

end
function [Isub] = createImSubset(i,j,I,boundingSize,xData,yData)
[m,n,b] = size(I);
switch nargin
  case 4
     xData = [1 n];
     yData = [1 m];
  case 5
     xData = [1 n];
     yData = [1 m];
end

i = ((yData(2)-yData(1)+1)./m) * (i-yData(1)+1);
j = ((xData(2)-xData(1)+1)./n) * (j-xData(1)+1);
i = round(i); j = round(j);

iMin = floor(i) - boundingSize; 
if iMin < 1, iMin = 1; end
iMax = floor(i) + boundingSize; 
if iMax > m, iMax = m; end

jMin = floor(j) - boundingSize; 
if jMin < 1, jMin = 1; end
jMax = floor(j) + boundingSize; 
if jMax > n, jMax = n; end

Isub = I(iMin:iMax,jMin:jMax,:);
end
function [u,v] = transformXY2Subset(i,j,I,boundingSize,xData,yData)
[m,n,b] = size(I);
switch nargin
  case 4
     xData = [1 n];
     yData = [1 m];
  case 5
     xData = [1 n];
     yData = [1 m];
end
i = ((yData(2)-yData(1)+1)./m) * (i-yData(1)+1);
j = ((xData(2)-xData(1)+1)./n) * (j-xData(1)+1);

iRaw = i; jRaw = j;
i = round(i); j = round(j);

iMin = floor(i) - boundingSize; 
if iMin < 1, iMin = 1; end

jMin = floor(j) - boundingSize; 
if jMin < 1, jMin = 1; end

u = iRaw - iMin +1;
v = jRaw - jMin +1;
end
function [iNew,jNew] = transformSubset2XY(i,j,iOld,jOld,boundingsize)
iDiff = i - (boundingsize + 1);
jDiff = j - (boundingsize + 1);

iNew = round(iOld) + iDiff;
jNew = round(jOld) + jDiff;
end
function [bw1,bw2] = imPercentile(I1Sub,I2Sub,percentile)
% find the main orientation to orient two imagery
% I1 - UAV imagery
% I2 - Close Up imagery
% R - rotational matrix of I2 to I1
%
% B. Altena - November 2013
visIm = true; % visualization of imagery is off

thresh1 = prctile(I1Sub(:),percentile);
bw1 = zeros(size(I1Sub,1), size(I1Sub,2));
highVal = (I1Sub >= thresh1);
bw1(highVal) = 1;

thresh2 = prctile(I2Sub(:),percentile);
bw2 = zeros(size(I2Sub,1), size(I2Sub,2));
highVal = (I2Sub >= thresh2);
bw2(highVal) = 1;

for i = 1:4
    bw1 = imerode(bw1, strel('ball',3,3));
    bw2 = imerode(bw2, strel('ball',3,3));
end
end
function [theta,D] = combineWithMSER(I1Sub,I2Sub)

% use MSER to get rough orientation
MSER1 = detectMSERFeatures(I1Sub, 'ThresholdDelta', 1);
MSER2 = detectMSERFeatures(I2Sub, 'ThresholdDelta', 1);

%% image of close-up - find square, or double square
classification = zeros(length(MSER2), 2); % one for a square, two for a double
centroid = zeros(length(MSER2), 2);
compactness = zeros(length(MSER2), 1);
for i = 1:length(MSER2)
    subjectIm = zeros(size(I2Sub,1),size(I2Sub,2));
    subjectRegion = MSER2.PixelList{i};
    subjectInd = sub2ind([size(I2Sub,1) size(I2Sub,2)], ...
    subjectRegion(:,2), subjectRegion(:,1));
    subjectIm(subjectInd) = 1;
    
    if length(bwboundaries(subjectIm, 'holes')) ~= length(bwboundaries(subjectIm, 'noholes'))
        % there are holes in the selection
        classification(i,1) = 1e8;
        classification(i,2) = 1e8;
    elseif length(bwboundaries(subjectIm, 'holes')) > 1
        % there are segments within the selection
        classification(i) = 1e8;
    else
    %     imagesc(subjectIm); pause(1)

        s = regionprops(subjectIm, 'centroid'); % estimate centroid
        centroid(i,:) = fliplr(cat(1, s.Centroid)); clear s

        s = regionprops(subjectIm, 'Orientation'); % estimate major orientation
        direction = cat(1, s.Orientation); clear s
        delta = 100;

        % get point far away
        outerend = centroid(i,:) + [-sind(direction)*delta ...
            cosd(direction)*delta];
        % get approximation of the shape
        elems = find(subjectIm==1);
        [iE,jE] = ind2sub(size(subjectIm), elems(1));
        try
            firstContour = bwtraceboundary(subjectIm, [iE, jE], 'E', 8);
        catch err
            firstContour = bwtraceboundary(subjectIm, [iE, jE], 'W', 8);
        end
        clear iE jE


        areaRegion = size(subjectRegion,1);
        periRegion = bwperim(subjectIm, 8);
        periRegion = sum(double(periRegion(:)));
        compactness(i) = periRegion./areaRegion; % as big as possible

        [xi, yi] = polyxpoly(firstContour(:,1)', firstContour(:,2)', ...
            [centroid(i,1) outerend(1)], [centroid(i,2) outerend(2)]);

        if isempty(xi) % if it is not hitting any point, due to convex shape
            outerend = centroid(i,:) + [cosd(direction)*delta ...
                -sind(direction)*delta];
            [xi, yi] = polyxpoly(firstContour(:,1)', firstContour(:,2)', ...
                [centroid(i,1) outerend(1)], [centroid(i,2) outerend(2)]);
        end

        if isempty(xi) % the point is to far out anyway
            classification(i,1) = 1e8;
            classification(i,2) = 1e8;
        else
            % the point should be inside the convexhull of the region
            s = regionprops(subjectIm, 'ConvexHull');
            convexhull = cat(1, s.ConvexHull); clear s
            center2 = [((size(I2Sub,1)-1)./2)+1 ((size(I2Sub,2)-1)./2)+1];
            if inpolygon(center2(1),center2(2),convexhull(:,1),convexhull(:,2))
                % multiple crossings, take the crossing most far away
                if length(xi)>1
                    d = sqrt(sum(([xi yi]-repmat(centroid(i,:),length(xi),1)).^2,2));
                    [dMax dIdx] = max(d);
                    xi = xi(dIdx);
                    yi = yi(dIdx);
                    clear d dMax dIdx
                end
                [iIm,jIm] = ind2sub(size(subjectIm), elems);
                d = sqrt(sum(([iIm jIm]-repmat([xi yi],length(iIm),1)).^2,2));
                [dMin dIdx] = min(d);

    %             boundIm = bwtraceboundary(subjectIm,[iIm(dIdx) jIm(dIdx)], 'N', 8);
                try
                    boundIm = bwtraceboundary(subjectIm,[iIm(dIdx) jIm(dIdx)], 'N', 8);
                catch err
                    boundIm = bwtraceboundary(subjectIm,[iIm(dIdx) jIm(dIdx)], 'S', 8);
                end
                distBound = sqrt(sum((boundIm-repmat(centroid(i,:),length(boundIm),1)).^2,2)); 
                dMax = max(distBound);
                
                %% find curvature along the curve
                xf = fft(boundIm(:,1));
                yf = fft(boundIm(:,2)); % fourier transform
                
                nx = length(xf); % scaling
                hx = ceil(nx/2)-1;
                ftdiff = (2i*pi/nx)*(0:hx);
                ftdiff(nx:-1:nx-hx+1) = -ftdiff(2:hx+1);
                ftddiff = (-(2i*pi/nx)^2)*(0:hx);
                ftddiff(nx:-1:nx-hx+1) = ftddiff(2:hx+1);
                
                xf(25:end-24) = 0; % low pass filtering
                yf(25:end-24) = 0;
                
                dx = real(ifft(xf.*ftdiff')); % estimate derivatives
                dy = real(ifft(yf.*ftdiff'));
                ddx = real(ifft(xf.*ftddiff'));
                ddy = real(ifft(yf.*ftddiff'));
                
                % line([boundIm(:,1) boundIm(:,1)+10.*ddx]',[boundIm(:,2) boundIm(:,2)+10.*ddy]')
                % scatter(boundIm(:,1),boundIm(:,2),3,k,'filled'); colorbar
                curv = sqrt((ddy.*dx - ddx.*dy).^2) ./ ((dx.^2 + dy.^2).^(3/2));
%                 dir = atan2(dy,dx);
                %rotate along primair axis
                dirRot = atan2(-sind(direction).*dx+cosd(direction)*dy,...
                    cosd(direction).*dx+sind(direction).*dy);
                meanDir = mean(dirRot); stdDir = std(dirRot);
                
                shiftVec = -2:2;
                orientCorr = -1;
                for k = 1:5 % move a bit back and forth to get good fit

                    crossCorr = (1/length(dirRot)) .* ...
                        sum( (circshift(-flipud(dirRot(:)),shiftVec(k)) - meanDir).* ...
                        (dirRot- meanDir) )./ (stdDir*stdDir) ;
                    if crossCorr > orientCorr
                        orientCorr = crossCorr;
                    end
                end
                % vectorial dot product
                % plot(atand(dot([dx dy],[-flipud(dx) -flipud(dy)],2)))
                    
%                 
%                 gradientCorr = zeros(length(dx),1);
%                 stdDy = std(dy); stdDx = std(dx);
%                 meanDy = mean(dy); meanDx = mean(dx);
%                 for k = 1:length(dx)
%                     % cross correlation
%                     gradientCorr(k) = (1/length(dx)) .* sum( (circshift(dy,k-1) - meanDx).* ...
%                         (dx- meanDy) )./ (stdDx*stdDy) ;
%                 end
%                 [maxGrad,idxGrad] = max(gradientCorr);
%                 distShift = min(abs(1./8 - idxGrad./length(dx)),...
%                     abs((1-1./8) - 13./length(dx)));
%                 
%                 %% look at hough lines
%                 
%                 traceIm = zeros(size(I2Sub,1),size(I2Sub,2));
%                 traceIm(sub2ind([size(I2Sub,1) size(I2Sub,1)],boundIm(:,1),boundIm(:,2))) = 1;
%                 [H,Theta,Rho] = hough(traceIm);              
% %                 imshow(H,[],'XData',Theta,'YData',Rho,'InitialMagnification','fit');
% %                 xlabel('theta'), ylabel('rho');
% %                 axis on; hold on; axis tight
% %                 plot(Theta(P(:,2)),Rho(P(:,1)),'s','color','white');
%                 
%                 P  = houghpeaks(H,2,'NHoodSize',[3 7]);
%                 [peak1Val,peak1Loc] = findpeaks(H(:,P(1,2)),'npeaks',3,'sortstr','descend');
%                 [peak2Val,peak2Loc] = findpeaks(H(:,P(2,2)),'npeaks',3,'sortstr','descend');
%                 
%                 if length(peak1Val) == 3 && length(peak2Val) == 3
%                     peak1Diff = max(peak1Loc(2:3))-min(peak1Loc(2:3));
%                     peak2Diff = max(peak2Loc(2:3))-min(peak2Loc(2:3));
% 
%                     peakOrient = Theta(P(:,2));
%                     angleLines = max(peakOrient)-min(peakOrient); % difference in angle  (degrees) between both crossing lines

%                 if sum(curv > 0.2)>6 || sum(curv > 0.2)<3
                    % maximum 6 points need to have a strong ouwards corner
%                     classification(i,1) = 1e8;
%                 elseif distShift > 0.1 || maxGrad < 0.6
%                     % there should be a similar repeatance at 1/8 of the
%                     % track and the correlation score should be high
%                     classification(i,1) = 1e8;
                if orientCorr<0.3
%                 elseif abs(angleLines-90)>6 || abs(peak1Diff-peak2Diff)>2
                    classification(i,1) = 1e8;
                else
                    %% create double square model
                    highPoint = dMax;
                    midPoint = sqrt(2.*((dMax/2)^2));
                    lowPoint = 0;

                    lenSect = length(distBound)/8;
                    err = 0;
                    for j = 1:length(distBound)
                        switch ceil(j/8)
                            case 1
                                D = [0 highPoint 1; lenSect midPoint 1; j distBound(j) 1];
                            case 2
                                D = [lenSect midPoint 1; 2.*lenSect 0 1; j distBound(j) 1];
                            case 3
                                D = [2.*lenSect 0 1; 3.*lenSect midPoint 1; j distBound(j) 1];
                            case 4
                                D = [3.*lenSect midPoint 1; 4.*lenSect highPoint 1; j distBound(j) 1];
                            case 5
                                D = [4.*lenSect highPoint 1; 5.*lenSect midPoint 1; j distBound(j) 1];
                            case 6
                                D = [5.*lenSect midPoint 1; 6.*lenSect 0 1; j distBound(j) 1];
                            case 7
                                D = [6.*lenSect 0 1; 7.*lenSect midPoint 1;  j distBound(j) 1];
                            case 8
                                D = [7.*lenSect midPoint 1; 8.*lenSect highPoint 1;  j distBound(j) 1];
                        end
                        dist = sqrt((D(1,1)-D(2,1)).^2+(D(1,2)-D(2,2)).^2);
                        err = err + abs(det(D)./dist).^2;
                    end

                    classification(i,1) = err./length(distBound);
                end
            else
                classification(i,1) = 1e8;
            end
%         else
%             classification(i,1) = 1e8;
        end
        %% create square model

        % enclosed regions are present, which are also sensative to square
        % detection, thus these should be excluded

        if ~exist('boundIm', 'var')
            if length(xi)>1
                d = sqrt(sum(([xi yi]-repmat(centroid(i,:),length(xi),1)).^2,2));
                [dMax dIdx] = max(d);
                xi = xi(dIdx);
                yi = yi(dIdx);
                clear d dMax dIdx
            end
            
            [iIm,jIm] = ind2sub(size(subjectIm), elems);
            d = sqrt(sum(([iIm jIm]-repmat([xi yi],length(iIm),1)).^2,2));
            [dMin dIdx] = min(d);

            try
                boundIm = bwtraceboundary(subjectIm,[iIm(dIdx) jIm(dIdx)], 'N', 8);
            catch err
                boundIm = bwtraceboundary(subjectIm,[iIm(dIdx) jIm(dIdx)], 'S', 8);
            end
            distBound = sqrt(sum((boundIm-repmat(centroid(i,:),length(boundIm),1)).^2,2));
            dMax = max(distBound);
            highPoint = dMax;
            midPoint = sqrt(2.*((dMax/2)^2));
            lowPoint = 0;
        end

        periRegion = bwperim(subjectIm, 8);
        periIdx = find(periRegion~=0);
        boundIdx = unique(sub2ind(size(subjectIm),boundIm(:,1),boundIm(:,2)));
        [periDup, boundDup] = findDuplicate(periIdx,boundIdx);

        bound2idx = 0;
        polygoninpolygon = false;
        if sum(periDup==0)>0
            % find the other polygons
            leftOver = zeros(size(subjectIm,1),size(subjectIm,2));
            leftOver(boundIdx) = 1;
            subjectRest = subjectIm-imdilate(leftOver,ones(3));

            restPix = find(periDup == 0);
            for j = 1:length(restPix)
                % points can be inside the polygon and cut off
                if subjectRest(periIdx(j))~=0 
                    % left over pixels can already be included by other
                    % pixels
                    if isempty(find(bound2idx==periIdx(j)))
                        [periIdxI,periIdxJ] = ind2sub(size(subjectRest), periIdx(j));
                        try
                            bound = bwtraceboundary(subjectIm, ...
                                [periIdxI periIdxJ], 'N', 8);
                        catch err
                            % find all boundaries, then find the
                            % intersection point, in the collection
                            [B,L,N,A] = bwboundaries(subjectIm);
                            bound = B{L(periIdxI,periIdxJ)};
                        end
                        % include if a polyon is made
                        if size(bound,1)>2 
                            bound2idx = cat(1, bound2idx, ...
                                sub2ind(size(subjectRest), bound(:,1), bound(:,2)));
                        end
                        clear periIdxI periIdxJ
                    end
                end
            end

            bound2idx = bound2idx(bound2idx(:)>0); % remove zeros
            [iPoly,jPoly] = ind2sub(size(subjectRest), bound2idx);
            IN = inpolygon(iPoly,jPoly, boundIm(:,1),boundIm(:,2));
            if sum(IN)>0 % no simple polyon, it is another polygon
                polygoninpolygon = true;
            end
        end

        if ~polygoninpolygon % redundant
            
            if ~exist('midPoint', 'var')
                highPoint = dMax;
                midPoint = sqrt(2.*((dMax/2)^2));
                lowPoint = 0;
            end
            lenSect = length(distBound)/8;
            err = 0;
            for j = 1:length(distBound)
                switch mod(ceil(j/8),2)
                    case 0 % 1,3,5,7
                        D = [(ceil(j/8)-1).*lenSect highPoint 1; ...
                            ceil(j/8).*lenSect midPoint 1; ...
                            j distBound(j) 1];
                    case 1 % 2,4,6,8
                        D = [(ceil(j/8)-1).*lenSect midPoint 1; ...
                            ceil(j/8).*lenSect highPoint 1; ...
                            j distBound(j) 1];
                end
                dist = sqrt((D(1,1)-D(2,1)).^2+(D(1,2)-D(2,2)).^2);
                err = err + abs(det(D)./dist).^2;
            end

            classification(i,2) = err./length(distBound); 
%             else
%                 classification(i,2) = 1e8;
%             end
        end     
    end
end
center2 = repmat([((size(I2Sub,1)-1)./2)+1 ((size(I2Sub,2)-1)./2)+1], length(MSER2),1);
dist = sqrt(sum((centroid-center2).^2,2))./sqrt(size(I2Sub,1)*size(I2Sub,2));
classification(dist>0.1,1) = 1e8;

if sum(classification(:,1)<1000)>0
    [minVal, minIdx] = min(classification(:,1));
    subjectIm = zeros(size(I2Sub,1),size(I2Sub,2));
    subjectRegion = MSER2.PixelList{minIdx};
    subjectInd = sub2ind([size(I2Sub,1) size(I2Sub,2)], ...
    subjectRegion(:,1), subjectRegion(:,2));
    subjectIm(subjectInd) = 1;

    subjectCenter = repmat(MSER2(minIdx).Location,size(subjectRegion,1),1);

    primairHist = cos(MSER2.Orientation(minIdx)).*(double(subjectRegion(:,1)) - subjectCenter(:,1)) + ...
        -sin(MSER2.Orientation(minIdx)).*(double(subjectRegion(:,2)) - subjectCenter(:,2));
    roundHist = round(primairHist);
    axisHist = [min(roundHist):max(roundHist)];
    projHist = histc(primairHist,axisHist);
    [pks,locs]= findpeaks(projHist);
    centers = axisHist(locs);

    %% Gaussian mixture parameter estimation
    obj = gmdistribution.fit(primairHist,2);
    peakDist2 = abs(obj.mu(1))+abs(obj.mu(2));
    peakOrient2 = MSER2.Orientation(minIdx);
    peaksCenter2 = MSER2.Location(minIdx,:);
% look at a right angle if a better estimate is available
%
% primairHist = sin(MSER2.Orientation(minIdx)).*(double(subjectRegion(:,1)) - subjectCenter(:,1)) + ...
%     cos(MSER2.Orientation(minIdx)).*(double(subjectRegion(:,2)) - subjectCenter(:,2));
% obj = gmdistribution.fit(primairHist,2);
% if abs(obj.mu(1))+abs(obj.mu(2)) > peakDist2
%     peakDist2 = abs(obj.mu(1))+abs(obj.mu(2));
% end

else % the double square was not detected, thus look at individual squares
    [squareVal squareIdx] = sort(classification(:,2), 'ascend');
    squareIdx = squareIdx(1:2);
    
    squareCentr = []; meanIntens = [];
    for k = 1:2
        subjectIm = zeros(size(I2Sub,1),size(I2Sub,2));
        subjectRegion = MSER2.PixelList{squareIdx(k)};
        subjectInd = sub2ind([size(I2Sub,1) size(I2Sub,2)], ...
        subjectRegion(:,2), subjectRegion(:,1));
        subjectIm(subjectInd) = 1;
        
        s = regionprops(subjectIm, 'centroid'); % estimate centroid
        squareCentr(k,:) = cat(1, s.Centroid); clear s
        meanIntens(k) = median(I2Sub(logical(subjectIm(:))));
    end
    
    center2 = repmat([((size(I2Sub,1)-1)./2)+1 ((size(I2Sub,2)-1)./2)+1], k,1);
    peakDist2 = sum(sqrt(sum((squareCentr-center2).^2,2)));
    
    [whitVal,whiteIdx] = max(meanIntens); % choose the white square
    
    peakOrient2 = atan2(center2(1,2)-squareCentr(whiteIdx,2), squareCentr(whiteIdx,1)-center2(2,1));

    if whitVal<240 % only black squares are found, rotate a quatre
        if peakOrient2>=0
            peakOrient2 = peakOrient2 - (pi()./2);
        else
            peakOrient2 = peakOrient2 + (pi()./2);
        end
    end
%     peakOrient2 = atan2(center2(1,1)-squareCentr(whiteIdx,1), ...
%         squareCentr(whiteIdx,2)-center2(1,2));
    disp('used square-model in stead of lemniscate-model');
end


%% image of UAV - find double square
classification = zeros(length(MSER1), 1); % one for a square, two for a double
for i = 1:length(MSER1)
    subjectIm = zeros(size(I1Sub,1),size(I1Sub,2));
    subjectRegion = MSER1.PixelList{i};
    subjectInd = sub2ind([size(I1Sub,1) size(I1Sub,2)], ...
    subjectRegion(:,2), subjectRegion(:,1));
    subjectIm(subjectInd) = 1;
    if length(bwboundaries(subjectIm, 'holes')) ~= length(bwboundaries(subjectIm, 'noholes'))
        % there are holes in the selection
        classification(i) = 1e8;
    elseif length(bwboundaries(subjectIm, 'holes')) > 1
        % there are segments within the selection
        classification(i) = 1e8;
    else
    %     imagesc(subjectIm); pause(1)

        s = regionprops(subjectIm, 'centroid');
        centroid = cat(1, s.Centroid); clear s
        centroid = fliplr(centroid);

        s = regionprops(subjectIm, 'Orientation');
        direction = cat(1, s.Orientation); clear s
        delta = 100;
        
        s = regionprops(subjectIm, 'ConvexHull');
        convexhull = cat(1, s.ConvexHull); clear s
        center1 = [((size(I1Sub,1)-1)./2)+1 ((size(I1Sub,2)-1)./2)+1];
        if inpolygon(center1(1),center1(2),convexhull(:,1),convexhull(:,2))
        
            % get point far away
            outerend = centroid + [-sind(direction)*delta ...
                cosd(direction)*delta];
            % get approximation of the shape
            elems = find(subjectIm==1);
            [iE,jE] = ind2sub(size(subjectIm), elems(1));
            try
                firstContour = bwtraceboundary(subjectIm, [iE, jE], 'E', 8);
            catch err
                firstContour = bwtraceboundary(subjectIm, [iE, jE], 'W', 8);
            end
            clear iE jE

            [xi, yi] = polyxpoly(firstContour(:,2)', firstContour(:,1)', ...
                [centroid(2) outerend(2)], [centroid(1) outerend(1)]);

            if isempty(xi) % if it is not hitting ay point, due to convex shape
                outerend = centroid + [cosd(direction)*delta ...
                    sind(direction)*delta];
                [xi, yi] = polyxpoly(firstContour(:,2)', firstContour(:,1)', ...
                    [centroid(2) outerend(2)], [centroid(1) outerend(1)]);
            end

            % multiple crossings, take the crossing most far away
            if length(xi)>1
                d = sqrt(sum(([xi yi]-repmat(centroid,length(xi),1)).^2,2));
                [dMax dIdx] = max(d);
                xi = xi(dIdx);
                yi = yi(dIdx);
                clear d dMax dIdx
            end
            [jIm,iIm] = ind2sub(size(subjectIm), elems);
            d = sqrt(sum(([iIm jIm]-repmat([xi yi],length(iIm),1)).^2,2));
            [dMin dIdx] = min(d);

            try
                boundIm = bwtraceboundary(subjectIm,[jIm(dIdx) iIm(dIdx)], 'N', 8);
            catch err
                boundIm = bwtraceboundary(subjectIm,[jIm(dIdx) iIm(dIdx)], 'S', 8);
            end
            distBound = sqrt(sum((boundIm-repmat(centroid,length(boundIm),1)).^2,2));
            dMax = max(distBound);

            % create model
            highPoint = dMax;
            midPoint = sqrt(2.*((dMax/2)^2));
            lowPoint = 0;

            lenSect = length(distBound)/8;
            err = 1e8;       
            for k = -round(length(distBound)./6):round(length(distBound)./6)
                distBoundShift = circshift(distBound,k);
                errNew = 0;
                for j = 1:length(distBound)
                    switch ceil(j/8)
                        case 1
                            D = [0 highPoint 1; lenSect midPoint 1; j distBoundShift(j) 1];
                        case 2
                            D = [lenSect midPoint 1; 2.*lenSect 0 1; j distBoundShift(j) 1];
                        case 3
                            D = [2.*lenSect 0 1; 3.*lenSect midPoint 1; j distBoundShift(j) 1];
                        case 4
                            D = [3.*lenSect midPoint 1; 4.*lenSect highPoint 1; j distBoundShift(j) 1];
                        case 5
                            D = [4.*lenSect highPoint 1; 5.*lenSect midPoint 1; j distBoundShift(j) 1];
                        case 6
                            D = [5.*lenSect midPoint 1; 6.*lenSect 0 1; j distBoundShift(j) 1];
                        case 7
                            D = [6.*lenSect 0 1; 7.*lenSect midPoint 1;  j distBoundShift(j) 1];
                        case 8
                            D = [7.*lenSect midPoint 1; 8.*lenSect highPoint 1;  j distBoundShift(j) 1];
                    end
                    dist = sqrt((D(1,1)-D(2,1)).^2+(D(1,2)-D(2,2)).^2);
                    errNew = errNew + abs(det(D)./dist).^2;
                end
                if errNew < err
                    err = errNew;
                end
            end

            classification(i) = err./length(distBound);
        else
            classification(i) = 1e8;
        end
    end
end

[minVal,minIdx] = min(classification);

subjectIm = zeros(size(I1Sub,1),size(I1Sub,2));
subjectRegion = MSER1.PixelList{minIdx};
subjectInd = sub2ind([size(I1Sub,1) size(I1Sub,2)], ...
subjectRegion(:,2), subjectRegion(:,1));
subjectIm(subjectInd) = 1;

subjectCenter = repmat(MSER1(minIdx).Location,size(subjectRegion,1),1);

primairHist = cos(MSER1.Orientation(minIdx)).*(double(subjectRegion(:,1)) - subjectCenter(:,1)) + ...
    -sin(MSER1.Orientation(minIdx)).*(double(subjectRegion(:,2)) - subjectCenter(:,2));
roundHist = round(primairHist);
axisHist = [min(roundHist):max(roundHist)];
projHist = histc(primairHist,axisHist);
[pks,locs]= findpeaks(projHist);
centers = axisHist(locs);
    
% Gaussian mixture parameter estimation
obj = gmdistribution.fit(primairHist,2);
peakDist1 = abs(obj.mu(1))+abs(obj.mu(2));
peakOrient1 = MSER1.Orientation(minIdx);
peaksCenter1 = MSER1.Location(minIdx,:);

% look at a right angle if a better estimate is available
%
% primairHist = sin(MSER1.Orientation(minIdx)).*(double(subjectRegion(:,1)) - subjectCenter(:,1)) + ...
%     cos(MSER1.Orientation(minIdx)).*(double(subjectRegion(:,2)) - subjectCenter(:,2));
% obj = gmdistribution.fit(primairHist,2);
% if abs(obj.mu(1))+abs(obj.mu(2)) > peakDist2
%     peakDist2 = abs(obj.mu(1))+abs(obj.mu(2));
% end

D = peakDist2./peakDist1;
% theta = rad2deg(min((2 * pi()) - abs(peakOrient1 - peakOrient2), abs(peakOrient1 - peakOrient2))); % rotation angle from I1 to I2 in degrees
theta = rad2deg(min((2 * pi()) - peakOrient1 - peakOrient2, peakOrient1 - peakOrient2));
%% old stuff
% %% search for main orientation - circle in the middle 
% a1 = 0; a2 = 0; % major axis of MSER ellipses
% 
% [m1,n1] = size(I1Sub);
% center1 = repmat([((m1-1)./2)+1 ((n1-1)./2)+1], length(MSER1),1);
% offCenter = sqrt(sum(((MSER1.Location - center1)./center1).^2,2));
% selec = offCenter < 0.1;
% if sum(selec) > 0 % selection on axis
%     ax = MSER1.Axes(selec,:);
%     axOrder = sort(ax,2,'descend');
%     a = axOrder(:,1);
%     b = axOrder(:,2);
%     g = 1 - b./a; % flattening factor
%     ellipses = find(g > 0.1);
%     f = find(selec>0);
%     orientation1 = rad2deg(median(MSER1.Orientation(f(ellipses))));
%     offset1 = median(MSER1.Location(f(ellipses)));
%     a1 = median(a);
%     clear a b g ax axOrder f ellipses
% %     eccentricity =  sqrt(1 - b.^2./a.^2);
% else % look at others and find checkerboard blobs
%     lineAB = robustfit(MSER1.Location(~selec,1),MSER1.Location(~selec,2));
%     orientation1 = atand(lineAB(2));
%     offset1 = lineAB(1);
%     clear lineAB
% end
% 
% [m2 n2] = size(I2Sub);
% center2 = repmat([((m2-1)./2)+1 ((n2-1)./2)+1], length(MSER2),1);
% offCenter = sqrt(sum(((MSER2.Location - center2)./center2).^2,2));
% selec = offCenter < 0.1;
% if sum(selec) > 0 % selection on axis
%     ax = MSER2.Axes(selec,:);
%     axOrder = sort(ax,2,'descend');
%     a = axOrder(:,1);
%     b = axOrder(:,2);
%     g = 1 - b./a; % flattening factor
%     ellipses = find(g > 0.1);
%     f = find(selec>0);
%     orientation2 = rad2deg(median(MSER2.Orientation(f(ellipses))));
%     offset2 = median(MSER2.Location(f(ellipses)));
%     a2 = median(a);
%     clear a b g ax axOrder f ellipses
% %     eccentricity =  sqrt(1 - b.^2./a.^2);
% else % look at others and find checkerboard blobs
%     lineAB = robustfit(MSER2.Location(~selec,1),MSER2.Location(~selec,2));
%     orientation2 = atand(lineAB(2));
%     offset2 = lineAB(1);
%     clear lineAB
% end
% 
% 
% % if the orientation is known, look at scale estimate
% if a1 ~= 0 && a2 ~= 0
%     D = a1./a2;
% else
%     D = 1;
% end

% %% find rotation
% % image of close-up
% targetIm = zeros(size(I2Sub,1),size(I2Sub,2));
% targetRegion = MSER2.PixelList{1};
% targetInd = sub2ind([size(I2Sub,1) size(I2Sub,2)], ...
%     targetRegion(:,1), targetRegion(:,2));
% targetIm(targetInd) = 1;
% squareFound = false;
% for i = 2:length(MSER2)
%     subjectIm = zeros(size(I2Sub,1),size(I2Sub,2));
%     subjectRegion = MSER2.PixelList{i};
%     subjectInd = sub2ind([size(I2Sub,1) size(I2Sub,2)], ...
%     subjectRegion(:,1), subjectRegion(:,2));
%     subjectIm(subjectInd) = 1;
%     imagesc(subjectIm); pause(1)
%     overlay = and(targetIm,subjectIm);
%     if sum(overlay(:))==0 && ~squareFound
%         otherSquare2 = i;
%         squareFound = true;
%         break
%     end
% end
%
% cornList = corner(subjectIm);
% areaRegion = size(subjectRegion,1);
% periRegion = bwperim(subjectIm, 8);
% periRegion = sum(double(periRegion(:)));
%
% v1 = MSER1.Location(1,:)-MSER1.Location(otherSquare1,:);
% v2 = MSER2.Location(2,:)-MSER2.Location(otherSquare2,:);
% u1 = v1./sum(sqrt(sum(v1.^2)));
% u2 = v2./sum(sqrt(sum(v2.^2)));

end
function [doubleA,doubleB] = findDuplicate(a,b)
% find duplicate numbers in two vectors
%
%   doubleA - vector indicating elements also present in b
%
%   doubleB - vector indicating elements also present in a
%
% B.Altena - March 2014

[mA,nA] = size(a);
if mA==1 || nA == 1
    if mA==1
        a = a';
    end
    [mB,nB] = size(b);
    if mB==1 || nB == 1
        if nB==1
            b = b';
        end
        A = repmat(a,1,length(b));
        B = repmat(b,length(a),1);
        
        % construct counter
        doubles = A==B;
        doubleA = sum(doubles,2);
        doubleB = sum(doubles,1);
    else
        error('The second element is not a vector.');
    end
else
    error('The first element is not a vector.');
end

end
function [theta] = combineWithSIFT(I1,x1,y1, I2,x2,y2, scaleMultipla)
% find the main orientation to orient two imagery
% I1 - UAV imagery
% I2 - Close Up imagery
% R - rotational matrix of I2 to I1
%
% B. Altena - November 2013
visIm = true; % visualization of imagery is off
scale = 8;

% make sift descriptor of UAV image
fc = [x1;y1;scale;0] ;
[f1,d1] = vl_sift(single(rgb2gray(I1)),'frames',fc,'orientations');

% make sift descriptor of close-up image
fc = [x2;y2;scale*scaleMultipla;0] ;
[f2,d2] = vl_sift(single(rgb2gray(I2)),'frames',fc,'orientations');

if size(f1,2)>1
    noOrient = size(f1,2);
    [val,idx] = min(sum(d1-repmat(d2,1,size(f1,2)),1));
    f1 = f1(:,idx); d1 = d1(:,idx);
end

if visIm
    figure();
    subplot(1,2,1)
    imshow(I1); hold on;
    h = vl_plotsiftdescriptor(d1,f1) ;
    set(h,'color','r') ; hold off;
    
    subplot(1,2,2)
    imshow(I2); hold on;
    h = vl_plotsiftdescriptor(d2,f2) ;
    set(h,'color','r') ; hold off;
end

theta = rad2deg(f2(4)) - rad2deg(f1(4)); % rotation angle from I2 to I1 in degrees
% R = [cos(f2(4)-f1(4)) -sin(f2(4)-f1(4)); ...
%     sin(f2(4)-f1(4)) cos(f2(4)-f1(4))];
end
function [I2Turn, x2, y2] = findScaleWithSift(I1,x1,y1, I2,x2,y2)
% Correspondence search to estimate affine scale and rotation
%
% B.Altena - November 2013

I1sub = createImSubset(y1,x1,I1,100);
% I2sub = createImSubset(y2,x2,I2,400);
I1Sub = single(rgb2gray(I1sub));
I2small = imresize(I2, 0.1);
I2Sub = single(rgb2gray(I2small));
x2 = x2./10; y2 = y2./10;

[f1, d1] = vl_sift(I1Sub) ;
[f2, d2] = vl_sift(I2Sub) ;
[matches,scores] = vl_ubcmatch(d1, d2) ;

xy = f1(1:2,matches(1,:))';
uv = f2(1:2,matches(2,:))';

[F,inlierIdx] = estimateFundamentalMatrix(xy,uv);
xy = xy(inlierIdx,:);
uv = uv(inlierIdx,:);

clear f1 d1 f2 d2
% For an affine transformation:
%                     [ A D 0 ]
% [u v 1] = [x y 1] * [ B E 0 ] There are 6 unknowns: A,B,C,D,E,F
%                     [ C F 1 ]
% Another way to write this is:
%                   [ A D ]
% [u v] = [x y 1] * [ B E ]
%                   [ C F ]
% Rewriting the above matrix equation:
% U = X * T, where T = reshape([A B C D E F],3,2)
%
% With 3 or more correspondence points we can solve for T,
% T = X\U which gives us the first 2 columns of T, and
% we know the third column must be [0 0 1]'.

M = size(xy,1);
X = [xy ones(M,1)];

% just solve for the first two columns of T
U = uv;

% We know that X * T = U
if rank(X)>=3
    Tinv = X \ U;
else
    error(message('images:cp2tform:rankError', K, 'affine'))
end

% add third column
Tinv(:,3) = [0 0 1]';

A = inv(Tinv);
% clear xy uv M X U

A(:,3) = [0 0 1]';

A(3,1) = 0; A(3,2) = 0;
tRot = maketform('affine', A);
outbounds = findbounds(tRot,[1 1;size(I2,2) size(I2,1)]);

[I2Turn,xData,yData] = imtransform(I2small, tRot);
[xNew, yNew] = tformfwd(tRot, x2, y2);
deltaNew = outbounds(1,:)-1;
y2 = yNew-deltaNew(2); x2 = xNew-deltaNew(1);

I2Turn = createImSubset(y2,x2,I2Turn,100);
end
function [degrees] = rad2deg(radians)
degrees = radians * 180/pi;
while degrees < 0
    degrees = degrees + 360;
end
while degrees > 360
    degrees = degrees - 360;
end
end
function [radians] = deg2rad(degrees)
while degrees < 0
    degrees = degrees + 360;
end
while degrees > 360
    degrees = degrees - 360;
end
radians = degrees * pi/180;
end