function varargout = SkyFIlterGUI(varargin)
% GUI_TESTING123 M-file for GUI_testing123.fig
%      GUI_TESTING123, by itself, creates a new GUI_TESTING123 or raises the existing
%      singleton*.
%
%      H = GUI_TESTING123 returns the handle to a new GUI_TESTING123 or the handle to
%      the existing singleton*.
%
%      GUI_TESTING123('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI_TESTING123.M with the given input arguments.
%
%      GUI_TESTING123('Property','Value',...) creates a new GUI_TESTING123 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before GUI_testing123_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to GUI_testing123_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help GUI_testing123

% Last Modified by GUIDE v2.5 14-Mar-2014 10:56:09

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @SkyFIlterGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @SkyFIlterGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before GUI_testing123 is made visible.
function SkyFIlterGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to GUI_testing123 (see VARARGIN)

% Choose default command line output for GUI_testing123
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes GUI_testing123 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = SkyFIlterGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
temp=cd;
folder_name = uigetdir;
path=folder_name;
cd(path);
global imcell;
names={'empty', 'leeg'};
D = dir('*.jpg');
imcell = cell(1,numel(D));
for i = 1:numel(D)
  imcell{i} = imread(D(i).name);
  str=D(i).name;
  names{i}=str;
end
imshow(imcell{1});
cd(temp);
handles.names=names;
handles.tekst=D(1).name;
handles.imseq=imcell;
handles.folder_name=folder_name;
guidata(hObject,handles);
handles.currentview=1;
handles.B_min=255;
handles.B_max=0;

function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
tekst=handles.tekst;
imseq=handles.imseq;
number = str2num(get(handles.edit1,'string'));
handles.number=number;
imshow(imseq{number});
handles.currentview=number;
guidata(hObject,handles);





% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

disp('Select Sky by clicking on 10 evenly distributed sky points');
list=[];
list= ginput(10);
currentview=handles.currentview;
imseq=handles.imseq;
B_min=255;
B_max=0;
som=0.0001;
image=imseq{currentview};
for i=1:10
    %B=image(round(list(i,2)),round(list(i,1)),3);
    %som=som+uint32(B);
    %if B<B_min
       % B_min=B;
    %end   
   % if B>B_max
       % B_max=B;
    %end   
    array(i)=uint32(image(round(list(i,2)),round(list(i,1)),3));
end
%avgB=som/10;
B_min=min(array);
B_max=max(array);
avgB=mean(array);
handles.B_min=B_min;
handles.B_max=B_max;
handles.avgB=avgB;
handles.delta=uint32(avgB)-uint32(B_min);
test=3;
guidata(hObject,handles);




% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
imgCell=handles.imseq;
s=size(imgCell);
n=s(1,2);
for z=1:n
image=imgCell{z};
work_image=image;
[r,k]=size(image);
k=k/3;
%initialize
detectedblue=zeros(r,k);
%detect if pixel is skyish blue
B_min=double(handles.B_min);
B_max=double(handles.B_max);
disp(B_min);
disp(B_max);
for i=1:r
    for j=1:k
        R=double(image(i,j,1));
        G=double(image(i,j,2));
        B=double(image(i,j,3));
        if((abs(R-G)<30)&&(abs(G-B)<70)&&(B>=R)&&(B>=G)&&(B>B_min-22.5)&&(B<B_max+30))
            detectedblue(i,j)=1;
        else
            %display (R-G);
            %display (G-B);
        end
    end
end

%figure(1), imshow(image);
%figure(2), imshow(detectedblue);
figure (z), imshow(detectedblue);
connect=zeros(r,k);
  
lim=round(0.05*r);
    %top 5% of arrays search for blue
    for i=1:lim
        for j=1:k
            if detectedblue(lim,j)==1
              connect(lim,j)=1;
            end
        end
    end
    
    % lower array search for connected blue
    %first: from up to down
    for i=2:r
        for j=1:k
            if detectedblue(i,j)==1
                %checking top connection
                if connect(i-1,j)==1
                    connect(i,j)=1;
                end
                %checking leftside connection
                if((j>1)&&(connect(i,j-1)==1))
                    connect(i,j)=1;
                end
            end
        end
        
        for j=k-1:-1:1
            if detectedblue(i,j)==1
                %checking rightside connection
                if ((j<k)&&(connect(i,j+1)==1))
                    connect(i,j)=1;
                end
            end
            
        end
    end
   %second: from down to top 
  for i=r-1:-1:2
        for j=1:k
            if ((detectedblue(i,j)==1)&&connect(i,j)==0)
                %checking top connection
                if ((connect(i+1,j)==1)&&(i<r))
                    connect(i,j)=1;
                end
                %checking leftside connection
                if((j>1)&&(connect(i,j-1)==1))
                    connect(i,j)=1;
                end
            end
        end
        
        for j=k-1:-1:1
            if detectedblue(i,j)==1
                %checking rightside connection
                if ((j<k)&&(connect(i,j+1)==1))
                    connect(i,j)=1;
                end
            end
            
        end
  end

for i=1:r
    for j=1:k
        if connect(i,j)==1
            connectInv(i,j)=0;
        else
            connectInv(i,j)=1;
        end
    end
end

%figure(1), imshow(imgCell{1});
%figure(2), imshow(imgCell{2});
%figure(3), imshow(imgCell{3});
%figure(4), imshow(imgCell{4});

%figure(5), imshow(imgCellOutput{1});
%figure(6), imshow(imgCellOutput{2});
%figure(7), imshow(imgCellOutput{3});
%figure(8), imshow(imgCellOutput{4});
folder_name=handles.folder_name;
names=handles.names;
cd(folder_name);
mkdir('mask')
file_name=names{z};
[pathstr,name,ext] = fileparts(file_name)
file_name2=['mask/',name, '_mask',ext];
imwrite(connectInv,file_name2);
end
guidata(hObject,handles);
