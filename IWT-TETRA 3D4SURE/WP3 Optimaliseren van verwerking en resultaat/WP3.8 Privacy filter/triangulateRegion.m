function [ Iresult ] = triangulateRegion( Im, ROI, varargin )
%TRIANGULATEREGION Triangulate image taking image edges into account
%
% Bas Altena

%% initialize and check input
p = inputParser;

expectedDetec = {'harris', 'local', 'eigen'};
addRequired(p,'Im',@(x)validateattributes(x,...
        {'numeric'},{'3d'}));
addOptional(p,'median', true, @islogical);
addOptional(p,'iterate', 10, @isnumeric);
addOptional(p,'blur', false, @islogical);
addOptional(p,'detector', 'eigen',...
             @(x) any(validatestring(x,expectedDetec)));
addOptional(p,'neighborhood', 100, @isnumeric);
addOptional(p,'percent', 0.5, @isnumeric);
addOptional(p,'angle', 0.2, @isnumeric);
addOptional(p,'scale', 1.0, @isnumeric);
addOptional(p,'plot', false, @islogical);

parse(p,Im,varargin{:});

imMedian = p.Results.median;
imIter = p.Results.iterate;
imBlur = p.Results.blur;
imScaling = p.Results.scale;
cornerMethod = p.Results.detector;
boundingSize = p.Results.neighborhood;
angleThresh = p.Results.angle;
edgeThresh = p.Results.percent;
imShow = p.Results.plot;

%% image manipulation
% bbox

minJ = round(min(ROI(:,1))); minI = round(min(ROI(:,2)));
maxJ = round(max(ROI(:,1))); maxI = round(max(ROI(:,2)));

[m,n] = size(Im);
buffer = 10; % extend the region a bit
if minJ-buffer<1
    minJ = 1;
else
    minJ = minJ-buffer;
end
if minI-buffer<1
    minI = 1;
else
    minI = minI-buffer;
end
if maxJ+buffer>n
    maxJ = n;
else
    maxJ = maxJ+buffer;
end
if maxI+buffer>m
    maxI = 1;
else
    maxI = maxI+buffer;
end

Isub = Im(minI:maxI,minJ:maxJ,:);

if imScaling~= 1
    Isub = imresize(Isub, imScaling, 'bilinear');
end

[m,n] = size(Isub);

imLimit = 16e4;
if m*n > imLimit % processing sometimes takes a while
    crimpFactor = 1./sqrt((m*n)/imLimit);
    Isub = imresize(Isub, crimpFactor, 'bilinear');
    crimping = true;
    localROI = crimpFactor.*(ROI - repmat([minJ minI], length(ROI),1));
else
    crimping = false;
    localROI = ROI - repmat([minJ minI], length(ROI),1);
end

Ihsv = rgb2hsv(Isub);
I = rgb2gray(Isub);
[m,n] = size(I);

if imBlur %%blur
    h = fspecial('gaussian');
    I = imfilter(I,h,'replicate');
end

%% corner detection 
switch lower(cornerMethod)
    case 'harris'
        hcornerdet = vision.CornerDetector('Method', 'Harris corner detection (Harris & Stephens)', ...
            'MetricMatrixOutputPort', true, ...
            'Sensitivity', 0.02, 'CornerThreshold', 0.0000005, ...
            'MaximumCornerCount', round((m*n)./10)); 
        [ptsArray,metArray] = step(hcornerdet, Ihsv(:,:,2));
    case 'local'
        hcornerdet = vision.CornerDetector('Method', ...
            'Local intensity comparison (Rosten & Drummond)');
        [ptsArray,metArray] = step(hcornerdet, Ihsv(:,:,2));
    case 'eigen'
        corners = detectMinEigenFeatures(Ihsv(:,:,2), ...
            'MinQuality', 0.005, ...
            'FilterSize', 15);
        ptsArray = corners.Location;
        metArray = corners.Metric;
end

%% exclude points outside ROI
IN = inpolygon(ptsArray(:,1),ptsArray(:,2),localROI(:,1),localROI(:,2));
ptsArray = ptsArray(IN,:);
ptsArray = cat(1, localROI, ptsArray);

if imShow
    figure();
    set(gcf,'numbertitle','off','name','Normal Triangulation');
    % delaunay
    TRI = delaunay(double(ptsArray(:,1)),double(ptsArray(:,2)));
    imshow(I); hold on;
    triplot(TRI,double(ptsArray(:,1)),double(ptsArray(:,2))); hold off;
end

if imMedian
    Ir = double(Isub(:,:,1));
    Ig = double(Isub(:,:,2));
    Ib = double(Isub(:,:,3));

    % median filter
    for i = 1:imIter
        Ir = medfilt2(Ir, [5 5]);
        Ig = medfilt2(Ig, [5 5]);
        Ib = medfilt2(Ib, [5 5]);
    end
    Inew = cat(3, Ir, Ig, Ib);
end

%% edge detection
if imMedian
    IhsvNew = rgb2hsv(uint8(Inew));
    BW = edge(IhsvNew(:,:,2),'canny');
else
    BW = edge(Ihsv(:,:,2),'canny');
end

if imShow
    figure();
    set(gcf,'numbertitle','off','name','Edge detection result');
    imshow(Isub); hold on;
    alpha(double(~BW));
end

%% edge connection

C = cat(2,(1:size(ROI,1))',circshift((1:size(ROI,1))',-1)); % Constrained delaunay matrix
%run through all points
h1 = waitbar(0, 'find edge connections'); 
for i = 1:size(ptsArray,1)
    waitbar(i/length(ptsArray),h1,'find edge connections');
    % create subsets around point of interest
    [Iminor] = createImSubset(ptsArray(i,2),ptsArray(i,1),...
        BW,boundingSize);
    % find optional directions, that is,other points
    [ptsX, ptsY] = transformXY2Subset(ptsArray(i,1),ptsArray(i,2), ...
        I,boundingSize);
    selec = and(and(ptsArray(:,1)> (ptsArray(i,1)-(boundingSize+(ptsX-boundingSize))), ...
        ptsArray(:,1) < (ptsArray(i,1) + (size(Iminor,2)-ptsX))), ...
        and(ptsArray(:,2) > (ptsArray(i,2)-(boundingSize+(ptsY-boundingSize))), ...
        ptsArray(:,2) < (ptsArray(i,2) + (size(Iminor,1)-ptsY))));
    [dummy,selecIdx] = sort(selec, 'descend');
    ptsSel = ptsArray(selec,:);
    % transform to local coordinatesystem
    ptsLocal = double(ptsSel) - double(repmat(...
        [ptsArray(i,1)-ptsX ptsArray(i,2)-ptsY], size(ptsSel,1), 1));

    %% how much percent is an edge along each direction
  
    if ptsX < 1
        ptsX = 1;
    end
    if ptsY < 1
        ptsY = 1;
    end
    for j = 1:size(ptsLocal,1)
        dist = sqrt(sum((double(ptsLocal(j,:))-[ptsX ptsY]).^2));
        if dist ~= 0
            if double(ptsLocal(j,1))<1
                dX = linspace(1, ptsX, dist);
            elseif double(ptsLocal(j,1))>size(Isub,2)
                dX = linspace(double(ptsLocal(j,1)-1), ptsX, dist);
            else
                dX = linspace(double(ptsLocal(j,1)), ptsX, dist);
            end
            if double(ptsLocal(j,2))<1
                dY = linspace(1, ptsY, dist);
            elseif double(ptsLocal(j,2))>size(Isub,1)
                dY = linspace(double(ptsLocal(j,2)-1), ptsY, dist);
            else
                dY = linspace(double(ptsLocal(j,2)), ptsY, dist);
            end
           vline = sub2ind(size(Isub), round(dY), round(dX));
           edgeScore = Isub(vline);
           if sum(edgeScore)./length(edgeScore) >= edgeThresh % more than 50 percent is edge
               %  bring into constrained matrix
               C = cat(1, C, [i selecIdx(j)]);
           end
        end
    end
    
end
close(h1);
C = unique(C,'rows');
nodes = unique(C(:));
Csel = [];

%% erase duplicates connected vertices along line.
h1 = waitbar(0, 'removing duplicate edge connections');
for i = 1:length(nodes) % loop through vertices
    % find connected points, with coordinates
%     pntNrs = cat(1,C(C(:,1)==i,2), C(C(:,2)==i,1));
    pntNrs = C(C(:,1)==nodes(i),2);
    subject = double(ptsArray(i,:));
    verts = double(ptsArray(pntNrs,:));
    
    waitbar(i/length(nodes),h1,'removing duplicate edge connections');
    for j = 1:size(verts,1)
        includeNode = true;
        for k = 1:size(verts,1)
            if j ~= k || j < k
                %% point distance/angle test
                Q = [verts(j,1) verts(j,2) 1; ...
                    verts(k,1) verts(k,2) 1
                    subject(1) subject(2) 1];
                dist = sqrt(sum((verts(j,:)-verts(k,:)).^2));
                pointOrient = det(Q);
                pointDistQ = pointOrient./dist;
                
                dist = sqrt(sum((verts(j,:)-subject).^2));
                P = circshift(Q, 1);
                pointOrient = det(P);
                pointDistP = pointOrient./dist;
                
                dist = sqrt(sum((verts(k,:)-subject).^2));
                R = circshift(Q, 2);
                pointOrient = det(R);
                pointDistR = pointOrient./dist;

                if abs(pointDistQ) < angleThresh 
                    % inbetween this triple
                    
                    includeNode = false;
                elseif (abs(pointDistP) > angleThresh) || (abs(pointDistR) > angleThresh)
                    % different outer ends, take only the longest
                    
                    if abs(pointDistP) < abs(pointDistR)
                        includeNode = false;
                    end
                end
            end
        end
        if includeNode
            Csel = cat(1, Csel, [nodes(i) pntNrs(j)]);
        end
    end
end
close(h1);

%% constrained delaunay triangulation 
dt = DelaunayTri(double(ptsArray(:,1)),double(ptsArray(:,2)), Csel);

if imShow
    figure();
    set(gcf,'numbertitle','off','name','Constrained Triangulation result');
    imshow(Isub); hold on;
    triplot(dt);
end

if imMedian
    TRI = dt.Triangulation;
    % voronoi
    center = round((ptsArray(TRI(:,1),:) + ...
        ptsArray(TRI(:,2),:) + ...
        ptsArray(TRI(:,3),:))./3);
    vInd = sub2ind(size(Ir),center(:,2),center(:,1));
    Col = cat(2, Ir(vInd), Ig(vInd), Ib(vInd));
else
    TRI = dt.Triangulation;
    center = round((ptsArray(TRI(:,1),:) + ...
        ptsArray(TRI(:,2),:) + ...
        ptsArray(TRI(:,3),:))./3);
    vIndR = sub2ind(size(Im), center(:,2), center(:,1), ...
        1.*ones(length(center(:,1)),1));
    vIndG = sub2ind(size(Im), center(:,2), center(:,1), ...
        2.*ones(length(center(:,1)),1));
    vIndB = sub2ind(size(Im), center(:,2), center(:,1), ...
        3.*ones(length(center(:,1)),1));
    Col = double(cat(2, Im(vIndR), Im(vIndG), Im(vIndB)));
end

%% allocate color of triangle to new image
[subI,subJ] = meshgrid(1:m,1:n);
IsubNew = Isub;
for i = 1:length(TRI)
    node1 = ptsArray(TRI(i,1),:);
    node2 = ptsArray(TRI(i,2),:);
    node3 = ptsArray(TRI(i,3),:);
    IN = inpolygon(subI(:),subJ(:),...
        [node1(2); node2(2); node3(2)], ...
        [node1(1); node2(1); node3(1)]);
    trSel = (reshape(IN,n, m))';
    trInd = find(trSel == 1);
    IsubNew(trInd) = Col(i,1);
    IsubNew(trInd+m*n) = Col(i,2);
    IsubNew(trInd+m*n+m*n) = Col(i,3);
end
Iresult = Im;

if crimping
%     minI = round(crimpFactor.*minI);
%     maxI = round(crimpFactor.*maxI);
%     minJ = round(crimpFactor.*minJ);
%     maxJ = round(crimpFactor.*maxJ);
    newI = imresize(IsubNew, ...
        1./crimpFactor, 'nearest');
    Iresult(minI:maxI,minJ:maxJ,:) = newI(1:maxI-minI+1,1:maxJ-minJ+1,:);
else
    Iresult(minI:maxI,minJ:maxJ,:) = IsubNew;
end

%% plot some results

if imShow
    figure();
    set(gcf,'numbertitle','off','name','Final triangulation result');
    p = patch('Faces',TRI,'Vertices',dt.X,...
        'FaceColor','b');
    clear cdata
    set(p,'FaceColor','flat',...
    'FaceVertexCData', Col./256, ...
    'EdgeColor', 'none');
    axis ij
    axis tight

    rgbColors = get(p, 'CData');
    rgb = permute(rgbColors, [1 3 2]);
    [c, map] = rgb2ind(permute(Col, [1 3 2])./256, 64);
    set(p,'FaceColor','flat',...
    'FaceVertexCData', double(c), ...
    'EdgeColor', 'none');
    colormap(map);

    axis equal
    axis off
end

end

