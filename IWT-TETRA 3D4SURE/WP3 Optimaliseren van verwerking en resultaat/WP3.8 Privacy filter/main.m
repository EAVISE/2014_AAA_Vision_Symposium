workDir = cd();

%% import orientation camera's and camera specifications

[projName, pathProj] = uigetfile({'*.jpg';'*.tif';'*.png';'*.bmp';'*.*'}, ...
   'Open image...', ...
   pwd, ...
   'MultiSelect', 'off');

d = dir(pathProj);
nameFolds = {d([d(:).isdir]).name}';
if sum(ismember(nameFolds,{'1_initial'})) ~= 0
    pixDir = [pathProj projName(1:end-4) '\params'];
else
    pixDir = [pathProj projName(1:end-4) '\1_initial\params'];
end

I = imread(strcat(pathProj, '\', projName));
%%
figure('units','normalized','outerposition',[0 0 1 1]);
imshow(I); hold on;
set(gcf,'numbertitle','off','name','Left click to add node, right click to close polygon');
h = impoly(gca,[]);
api = iptgetapi(h);
ROI = api.getPosition();
hold off;
%%

Inew = triangulateRegion(I,ROI);
figure('units','normalized','outerposition',[0 0 1 1]);
set(gcf,'numbertitle','off','name','Final triangulated result');
imshow(Inew);
