function [ output_args ] = pix4theIndustry( delta )
%PIX4THEINDUSTRY Summary of this function goes here
%   Detailed explanation goes here

if nargin < 1
    delta = 0.01; % resolution of grid
elseif ~isnumeric(delta)
    delta = 0.01;
end

workDir = cd;
addpath(workDir); % then ply_read can be read

[projName, pathProj] = uigetfile({'*.p4d','Pix4D project (*.p4d)'}, ...
   'Open photogrammetry project...', ...
   pwd, ...
   'MultiSelect', 'off');

d = dir(pathProj);
nameFolds = {d([d(:).isdir]).name}';
if sum(ismember(nameFolds,{'1_initial'})) ~= 0
    pixDir = [pathProj projName(1:end-4) '\params'];
else
    pixDir = [pathProj projName(1:end-4) '\2_densification\1_densified'];
end

cd(pixDir);
cloudList = ls('*ply');

for i = 1:size(cloudList,1)
    %% load data
    cd(pixDir);
    [dat,comm] = ply_read(cloudList(i,:));
    XYZ = cat(2, dat.vertex.x, dat.vertex.y, dat.vertex.z);
    
    cd(workDir);
    clear comm
    %% transform to voxels
    minXYZ = min(XYZ,[],1);
    maxXYZ = max(XYZ,[],1);
    

    dim = ceil((maxXYZ-minXYZ)./delta);
    cubInd = round(((XYZ-repmat(minXYZ,size(XYZ,1),1))./delta));
    cubInd((cubInd(:,1)==0),1) = 1;
    cubInd((cubInd(:,2)==0),2) = 1;
    cubInd((cubInd(:,3)==0),3) = 1;

    linInd = sub2ind(dim, cubInd(:,1), cubInd(:,2), cubInd(:,3));

    voxel = logical(zeros(dim(1),dim(2),dim(3)));
    voxel(linInd) = true;

    clear cubInd linInd
    %% distance transform
    [dist,idx] = bwdist(voxel, 'euclidean');
    dist = smooth3(dist,'gaussian');
    div = del2(dist);
    
    %% caluclate hessians
    [Hx,Hy,Hz] = gradient(div);
    [Hxx,Hxy,Hxz] = gradient(Hx);
    [Hyx,Hyy,Hyz] = gradient(Hy);
    [Hzx,Hzy,Hzz] = gradient(Hz);

    Hstack = cat(3, cat(2, Hxx(:), Hxy(:), Hxz(:)), ...
        cat(2, Hyx(:), Hyy(:), Hyz(:)), ...
        cat(2, Hzx(:), Hzy(:), Hzz(:)));
    clear Hxx Hxy Hxz Hyx Hyy Hyz Hzx Hzy Hzz
    Hstack = permute(Hstack, [3 2 1]);


    %% calculate eigenvalues
    % Given a real symmetric 3x3 matrix A, compute the eigenvalues

    p1 = Hstack(1,2,:).^2 + Hstack(1,3,:).^2 + Hstack(2,3,:).^2;
    % Hessian is diagonal.
    diagHess = (p1==0);

    q = (Hstack(1,1,:) + Hstack(2,2,:) + Hstack(3,3,:))./3;
    p2 = (Hstack(1,1,:) - q).^2 + (Hstack(2,2,:) - q).^2 + (Hstack(3,3,:) - q).^2 + 2.*p1;
    p = sqrt(p2 ./ 6);
    clear p2

    qI = zeros(3,3,length(Hstack));
    qI(1,1,:) = q; qI(2,2,:) = q; qI(3,3,:) = q;

    diffqI = (Hstack - qI); clear qI

    invP = 1./p;
    pI = ones(3,3,length(p));
    pI(1,1,:) = invP; pI(1,2,:) = invP; pI(1,3,:) = invP;
    pI(2,1,:) = invP; pI(2,2,:) = invP; pI(2,3,:) = invP;
    pI(3,1,:) = invP; pI(3,2,:) = invP; pI(3,3,:) = invP;
    clear invP

    B = pI .* diffqI;
    clear pI diffqI

    % determinant
    detB = B(1,1,:).*B(2,2,:).*B(3,3,:) + ...
        B(1,2,:).*B(2,3,:).*B(3,1,:) + ...
        B(1,3,:).*B(2,1,:).*B(3,2,:) - ...
        B(1,3,:).*B(2,2,:).*B(3,1,:) - ...
        B(1,2,:).*B(2,1,:).*B(3,3,:) - ...
        B(1,1,:).*B(2,3,:).*B(3,2,:);
    r = detB./2;
    clear detB B

    phi = acos(r)./3;
    % In exact arithmetic for a symmetric matrix  -1 <= r <= 1
    % but computation error can leave it slightly outside this range.
    phi(r<=-1) = pi./ 3;
    phi(r>=1) = 0;
    clear r

    % the eigenvalues satisfy eig3 <= eig2 <= eig1
    eig1 = q + 2.* p .* cos(phi);
    eig1(diagHess) = Hstack(1,1,diagHess);
    eig1 = reshape(eig1,dim);

    centrLine = and(div<-0.1, eig1>0.12);
    voxelD = imclose(centrLine,ones(5,5,5));

    voxelLabel = bwlabeln(voxelD);

    centrVox = [];
    for j = 1:max(voxelLabel(:));
        region = find(voxelLabel == j);
        regionDistances = dist(region);
        if std(regionDistances) < 3 && mean(regionDistances) > (0.02/delta) % diameter bigger than 2 centimeter
            centrVox = cat(1, centrVox, region);
        end 
    end


    [x,y,z]=ind2sub(dim,centrVox);
    
    %% transform to world coordinates and write file
    xVox = x.*delta + minXYZ(1);
    yVox = y.*delta + minXYZ(2);
    zVox = z.*delta + minXYZ(3);
    
    centerPoints = cat(2, xVox, yVox, zVox, dist(centrVox).*delta);
    
    dlmwrite([cloudList(i,1:end-33) '_extracted_center_points.txt'], centerPoints, '-append');
end

%% from text to laz

system(['lastools\txt2las -i ' cloudList(i,1:end-33) '_extracted_center_points.txt' ...
    '-o ' cloudList(i,1:end-33) '_extracted_center_points.las -parse xyzu']);

end

