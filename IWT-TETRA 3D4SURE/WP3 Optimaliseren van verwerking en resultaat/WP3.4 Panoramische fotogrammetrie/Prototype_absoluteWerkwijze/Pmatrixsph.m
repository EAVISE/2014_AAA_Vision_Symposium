function[P] = Pmatrixsph(A,B)
 
%Inlezen van de 3D co�rdinaten punten en normalisatie van de 3D
%co�rdinaten.

% bolcoord = P*3D_objectcoord

P3 = A;
P3(:,4)=1;
%[P3D,T3] = normalise3dpts_(P3);
P3D = P3

P2 = B;
P2(:,4)=1;
%[P2D,T2] = normalise3dpts_(P2);
P2D = P2

 
[rows,cols]=size(P3D);
 
%Opstellen van matrix waaruit nadien met behulp van singuliere waarde
%ontbinding de projectie matrix P kan worden gevonden.
for i=1:rows
    M(3*i-2,:)=[0 0 0 0 -P2D(i,3)*P3D(i,1) -P2D(i,3)*P3D(i,2) -P2D(i,3)*P3D(i,3) -P2D(i,3) P2D(i,2)*P3D(i,1) P2D(i,2)*P3D(i,2) P2D(i,2)*P3D(i,3) P2D(i,2)];
    M(3*i-1,:)=[P2D(i,3)*P3D(i,1) P2D(i,3)*P3D(i,2) P2D(i,3)*P3D(i,3) P2D(i,3) 0 0 0 0 -P2D(i,1)*P3D(i,1) -P2D(i,1)*P3D(i,2) -P2D(i,1)*P3D(i,3) -P2D(i,1)];
    M(3*i,:)=[-P2D(i,2)*P3D(i,1) -P2D(i,2)*P3D(i,2) -P2D(i,2)*P3D(i,3) -P2D(i,2) P2D(i,1)*P3D(i,1) P2D(i,1)*P3D(i,2) P2D(i,1)*P3D(i,3) P2D(i,1) 0 0 0 0];
end    
[U, W, V]=svd(M,0);
for i=1:12
    P(i,1)=V(i,12)/V(12,12);
end
P=[P(1,1) P(2,1) P(3,1) P(4,1);P(5,1) P(6,1) P(7,1) P(8,1);P(9,1) P(10,1) P(11,1) P(12,1)];

%denormaliseren:
%probeersel
%P = T2\P*T3;
[r,c]=size(P3D);
for i=1:r
    test_projectie(i,:)=(P*[P3D(i,1) P3D(i,2) P3D(i,3) P3D(i,4)]')';
end
test_projectie(:,4)=1;
test_projectie
D=P2D-test_projectie
end

