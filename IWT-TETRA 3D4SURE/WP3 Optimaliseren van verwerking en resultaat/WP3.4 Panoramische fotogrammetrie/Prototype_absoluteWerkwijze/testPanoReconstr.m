%% Inlezen van 3D controlepunten.
%A = load('3D Panorama binnen minder.txt');
 
path='D:\3D4SURE (D)\__Panorama_WerkMap\Thesis Fabian Van Meert\Data_thesisFabian\test_metDataJasper';
cd(path);

%% Inlezen eerste panorama foto en pixelco�rdinaten.
%im1 = imread('pano1.jpg');
imc = load('pano111_pix minder.txt');
A=load('XYZ minder1en3.txt');
A
 
 %Omzetten van pixelco�rdinaten naar 3D co�rdinaten.
[sph] = im2sph(im1, imc);
sph1 = sph;
sph1
 %Aanmaken van de projectie matrix van het eerste panorama.
[P]=Pmatrixsph(A,sph1);
P1 = P;
%[K,R,t] = PMatrixDecompQR(P1)
%t1=t
A(:,4)=1;
 
 %% Inlezen tweede panorama foto en pixelco�rdinaten.
im2 = imread('pano3.jpg');
imc = load('pano333_pix minder.txt');
 
 %Omzetten van pixelco�rdinaten naar 3D co�rdinaten.
[sph] = im2sph(im2, imc);
sph2 = sph;
sph2
%Aanmaken van de projectie matrix van het tweede panorama.
[P]=Pmatrixsph(A,sph2);
P2 = P;
P1;
P2;
%[K,R,t] = PMatrixDecompQR(P2)
%t2=t


%% Inlezen van te reconstrueren pixelco�rdinaten van het eerste panorama.
%im = imread('Panorama binnen 1 laag.jpg');
imc = load('pano111_pix alles.txt');
 
 %Aanmaken van de 3D co�rdinaten van het camera assenstelsel van het eerste panorama.
[sph] = im2sph(im1, imc);
sph1 = sph;
 %% Inlezen van te reconstrueren pixelco�rdinaten van het tweede panorama.
%im = imread('Panorama binnen 2 laag.jpg');
imc = load('pano333_pix alles.txt');
 
 %Aanmaken van de 3D co�rdinaten van het camera assenstelsel van het tweede panorama.
[sph] = im2sph(im2, imc);
sph2 = sph;
 
 x1 = sph1;
 x2 = sph2;
 
 %% Reconstructie van de 3D punten uit 2 paar sferische co�rdinaten.
[X]=Reconstructionsph(P1,P2,x1,x2);
 
 %Plotten van de gereconstrueerde 3D punten.
figure,plot3(X(1,:), X(2,:), X(3,:),'r+'); axis equal;

Xecht=load('XYZ alles.txt');
Xecht(1,4)=1;
[rXecht,kXecht]=size(Xecht);
%Xecht(2,4)=1;
%Xecht(3,4)=1;
echte_punten=Xecht';
format short;
%% PRINT ERRORS ON COORDINATES
display ('RESULTAAT:  ');
display ('__________  ');
verschil=echte_punten-X
    dX=0;
    dY=0;
    dZ=0;
aantal=0;
for i=1:rXecht
    if (abs(verschil(1,i))<0.5 && abs(verschil(2,i))<0.5 && abs(verschil(3,i))<0.5)
    dX=abs(verschil(1,i))+dX;
    dY=abs(verschil(2,i))+dY;
    dZ=abs(verschil(3,i))+dZ;
    aantal=aantal+1;
    end
end
disp('GEMIDDELDE ABSOLUTE FOUTEN (ZONDER UITSCHIETERS) in meter: ');
dXgem=dX/aantal
dYgem=dY/aantal
dZgem=dZ/aantal

% wegschrijven berekende coordinaten naar txt
%save('reconstructie3DRESULT.txt', 'reconstructie X(m) Y(m) Z (m)', '-ascii');
for i=1:29
    rec3D= [X(1,i) X(2,i) X(3,i)];
    dlmwrite('reconstructie3DRESULT1_3.txt',rec3D, '-append', 'precision',5,'delimiter',' ','newline','pc');
    
end



%figure(2),imshow(im1)
%figure(3),imshow(im2)