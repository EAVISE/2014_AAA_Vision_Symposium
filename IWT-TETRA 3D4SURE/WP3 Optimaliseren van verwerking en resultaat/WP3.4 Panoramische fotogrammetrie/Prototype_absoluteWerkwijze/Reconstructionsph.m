function[X]= Reconstructionsph(P1,P2,x1,x2)
 
%Inlezen 3D punten van het camera co÷rdinatenstelsel.
x1 = x1;
x2 = x2;
 
%Inlezen van de projectie matrices.
P1 = P1;
P2 = P2;
 
[rows,col] = size(x1);
 
%Reconstructie van de 3D punten.
% J1 is antisymmetrische matrix obv cameracoord van pano1
% J2 is antisymmetrische matrix obv cameracoord van pano2
% eigenlijk aan 1 set genoeg ipv 2?
for i=1:rows
   J1 = [0 -x1(i,3) x1(i,2); x1(i,3) 0 -x1(i,1); -x1(i,2) x1(i,1) 0];
   O1 = J1*P1;  
   
   J2 = [0 -x2(i,3) x2(i,2); x2(i,3) 0 -x2(i,1); -x2(i,2) x2(i,1) 0];
   O2 = J2*P2;
   
   A(1,:)=O1(1,:);
   A(2,:)=O1(2,:);
   A(3,:)=O1(3,:);
   A(4,:)=O2(1,:);
   A(5,:)=O2(2,:);
   A(6,:)=O2(3,:);
   
    [U, W, V]=svd(A);
    X(:,i) = V(:,4)./V(4,4);
end
%Weergave van de gereconstrueerde 3D punten
X
 
end

