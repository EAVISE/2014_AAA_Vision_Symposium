function[sph] = im2sph(im, imc)
 
%Panorama wordt ingelezen samen met de pixelco�rdinaten.
image = im;
imcoor = imc;
 
[rows,cols] = size(image);
[rows2,cols2] = size(imcoor);
 
%omtrek bol in pixels berekenen; 
%delen door 3 aangezien er 3 kanalen zijn (RGB)?
a = cols/3;
% omtrek cirkel = R*2pi  => R = omtrek/2pi

r=(a/(2*pi));
 
% Elke pixel co�rdinaat wordt omgezet naar een X,Y,Z co�rdinaat op de bol, deze
% bol heeft een straal gelijk aan de focusafstand van de camera.
% zodoende is er geen verlies aan resolutie
%f is 24mm in ons geval
f=0.024;
for i=1:rows2
    phi = imcoor(i, 2)/r;
    th = imcoor(i, 1)/r;
    sph(i, 1) = (f)*sin(phi)*sin(th);
    sph(i, 2) = (f)*sin(phi)*cos(th);
    sph(i, 3) = (f)*cos(phi);
end

end

