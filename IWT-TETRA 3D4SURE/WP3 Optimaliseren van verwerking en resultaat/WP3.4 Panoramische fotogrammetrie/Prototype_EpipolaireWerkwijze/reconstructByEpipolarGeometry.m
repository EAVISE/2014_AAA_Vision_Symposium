%% load pixel coordinates of matches of both panoramas (xpix, ypix)
pix1=load('matches_pano1_16.txt');
pix2=load('matches_pano3_16.txt');
[r,k]=size(pix1);
%load radius of panoramas (panorama_width/2pi)
R1=23492/(2*pi);
R2=23492/(2*pi);
%% transforming pixelcoordinates to 3D cartesian
p1=[];
p2=[];
for i=1:r
    phi1=pix1(i,2)/R1;
    phi2=pix2(i,2)/R2;
    thet1=pix1(i,1)/R1;
    thet2=pix2(i,1)/R2;
    %cartesian for first sphere
    p1(1,i)=sin(phi1)*sin(thet1);
    p1(2,i)=sin(phi1)*cos(thet1);
    p1(3,i)=cos(phi1);
    %cartesian for second sphere
    p2(1,i)=sin(phi2)*sin(thet2);
    p2(2,i)=sin(phi2)*cos(thet2);
    p2(3,i)=cos(phi2);
    
end

%% epipolar constraint p^T.E.p=0 => Q.m=0

for i=1:r
    p1x=p1(1,i);
    p1y=p1(2,i);
    p1z=p1(3,i);
    
    p2x=p2(1,i);
    p2y=p2(2,i);
    p2z=p2(3,i);
    
    %building system of equations:
    
    Q(i,:)=[p2x*p1x p2x*p1y p2x*p1z p2y*p1x p2y*p1y p2y*p1z p2z*p1x p2z*p1y p2z*p1z]
end

[u,d,v]=svd(Q);

m=v(:,9)%v(9,9);
%resulting in essential matrix E
E=[m(1,1) m(2,1) m(3,1);m(4,1) m(5,1) m(6,1);m(7,1) m(8,1) m(9,1)]

%% Optimization of E by minimizing the geodesic distance between epipolar
% plane and image points on the panoramic spheres (Pagani et al., 2011)

syms a 'E11' 'E12' 'E13' 'E21' 'E22' 'E23' 'E31' 'E32' 'E33'

for i=1:r
    p1x=p1(1,i);
    p1y=p1(2,i);
    p1z=p1(3,i);
    
    p2x=p2(1,i);
    p2y=p2(2,i);
    p2z=p2(3,i);
    
    %building system of equations:
    
    vgl(i)=(p2x*p1x*E11+p2x*p1y*E12+p2x*p1z*E13+p2y*p1x*E21+p2y*p1y*E22+p2y*p1z*E23+p2z*p1x*E31+p2z*p1y*E32+p2z*p1z*E33)/sqrt(1-(p2x*p1x*E11+p2x*p1y*E12+p2x*p1z*E13+p2y*p1x*E21+p2y*p1y*E22+p2y*p1z*E23+p2z*p1x*E31+p2z*p1y*E32+p2z*p1z*E33)^2);
end

vgl_string='';

for i=1:r
    
   vgl_string=strcat(vgl_string, char(vgl(i)));
  
   vgl_string=strcat(vgl_string, '; ');
   
end
% array of parameters
vgl_S=(['[' vgl_string ']']);
vgl_S1=regexprep(vgl_S, 'E11', 'x(1)');
vgl_S2=regexprep(vgl_S1, 'E12', 'x(2)');
vgl_S3=regexprep(vgl_S2, 'E13', 'x(3)');
vgl_S4=regexprep(vgl_S3, 'E21', 'x(4)');
vgl_S5=regexprep(vgl_S4, 'E22', 'x(5)');
vgl_S6=regexprep(vgl_S5, 'E23', 'x(6)');
vgl_S7=regexprep(vgl_S6, 'E31', 'x(7)');
vgl_S8=regexprep(vgl_S7, 'E32', 'x(8)');
vgl_S9=regexprep(vgl_S8, 'E33', 'x(9)');

%initial values
initieel=m';
%Levenberg-Marquardt
vgl_LM=inline(vgl_S9);
options = optimset('TolFun',1e-12,...
                   'Algorithm','levenberg-marquardt');
[x_S, resnorm_S, residu_S,exitflag, output]=lsqnonlin(vgl_LM, initieel,[],[],options);

%optimized values:
disp('Optimized Essential matrix');
E_nieuw=[x_S(1) x_S(2) x_S(3);x_S(4) x_S(5) x_S(6);x_S(7) x_S(8) x_S(9)]

%% Get Orientation from E

%start by svd of E
[u,d,v]=svd(E)
%2 rotations and 2 translations possible
W=[0,-1,0;1,0,0;0,0,det(u)*det(v)]
Rotation_1=u*W*v.'
Rotation_2=u*W.'*v.'
translation_1=u(:,3)
translation_2=-u(:,3)
% choose wright combination out of 4 possibilities
k1=[];
k2=[];
%based on reconstruction of first correspondence point
% two rays A and B are defined by two point: camera (1) and reconstructed 
% point P (2)

%1st option
i =1;
model=0;
while ((model<1)&&(i<r))
A1=[0,0,0];
A2=p1(:,i)';

B1=translation_1';
B2=translation_1'+(Rotation_1*p2(:,1))';

% if rays don't cross (most likely), take closest point in between (average
% of A0 and B0)
 nA = dot(cross(B2-B1,A1-B1),cross(A2-A1,B2-B1));
 nB = dot(cross(A2-A1,A1-B1),cross(A2-A1,B2-B1));
 d = dot(cross(A2-A1,B2-B1),cross(A2-A1,B2-B1));
 A0 = A1 + (nA/d)*(A2-A1);
 B0 = B1 + (nB/d)*(B2-B1);
 
 A0
 B0
 
 rec_avg1=(A0+B0)/2
 k1(1)=rec_avg1/p1(:,1)'
 temp=(Rotation_1*p2(:,1))';
 k2(1)=(rec_avg1(1,1)-translation_1(1,1))/temp(1)
  
 % second option
A1=[0,0,0];
A2=p1(:,i)';

B1=translation_2';
B2=translation_2'+(Rotation_1*p2(:,1))';

 nA = dot(cross(B2-B1,A1-B1),cross(A2-A1,B2-B1));
 nB = dot(cross(A2-A1,A1-B1),cross(A2-A1,B2-B1));
 d = dot(cross(A2-A1,B2-B1),cross(A2-A1,B2-B1));
 A0 = A1 + (nA/d)*(A2-A1);
 B0 = B1 + (nB/d)*(B2-B1);
 
  rec_avg2=(A0+B0)/2
 k1(2)=rec_avg2/p1(:,1)';
 temp=(Rotation_1*p2(:,1))';
 k2(2)=(rec_avg2(1,1)-translation_2(1,1))/temp(1);
 
  %third option

A1=[0,0,0];
A2=p1(:,i)';

B1=translation_1';
B2=translation_1'+(Rotation_2*p2(:,1))';

 nA = dot(cross(B2-B1,A1-B1),cross(A2-A1,B2-B1));
 nB = dot(cross(A2-A1,A1-B1),cross(A2-A1,B2-B1));
 d = dot(cross(A2-A1,B2-B1),cross(A2-A1,B2-B1));
 A0 = A1 + (nA/d)*(A2-A1);
 B0 = B1 + (nB/d)*(B2-B1);
 
 rec_avg3=(A0+B0)/2
 k1(3)=rec_avg3/p1(:,1)';
 temp=(Rotation_2*p2(:,1))';
 k2(3)=(rec_avg3(1,1)-translation_1(1,1))/temp(1);
  
 %4th option
A1=[0,0,0];
A2=p1(:,i)';

B1=translation_2';
B2=translation_2'+(Rotation_2*p2(:,1))';

 nA = dot(cross(B2-B1,A1-B1),cross(A2-A1,B2-B1));
 nB = dot(cross(A2-A1,A1-B1),cross(A2-A1,B2-B1));
 d = dot(cross(A2-A1,B2-B1),cross(A2-A1,B2-B1));
 A0 = A1 + (nA/d)*(A2-A1);
 B0 = B1 + (nB/d)*(B2-B1);
 
  rec_avg4=(A0+B0)/2
  k1(4)=rec_avg4/p1(:,1)';
  temp=(Rotation_2*p2(:,1))';
 k2(4)=(rec_avg4(1,1)-translation_2(1,1))/temp(1);
  
  % which of these 4 points has same direction as p1-vector?
  sum=0
  for j=1:4
      if k1(j)>0 &&k2(j)>0
          model=j;
          sum=sum+1;
      end
  end
  
i=i+1;
end 
  if model==1
      Rotation=Rotation_1;
      translation=translation_1;
  elseif model==2
      Rotation=Rotation_1;
      translation=translation_2;
    elseif model==3
      Rotation=Rotation_2;
      translation=translation_1;
   elseif model==4
      Rotation=Rotation_2;
      translation=translation_2;
  end

  
%  Rotation=Rotation_2;
%  translation=translation_2;
  %% Since we found the pose, we can reconstruct up to scale

  %load image points to reconstruct (id xpix ypix)
recc1=load('recon_pano1.txt');
recc2=load('recon_pano3.txt');
rec1=recc1(:,2:3);
rec2=recc2(:,2:3);
[r,k]=size(rec1);

%% transforming pixelcoordinates to 3D cartesian
pp1=[];
pp2=[];
for i=1:r
    phi1=rec1(i,2)/R1;
    phi2=rec2(i,2)/R2;
    thet1=rec1(i,1)/R1;
    thet2=rec2(i,1)/R2;
    %cartesian for first sphere
    pp1(1,i)=sin(phi1)*sin(thet1);
    pp1(2,i)=sin(phi1)*cos(thet1);
    pp1(3,i)=cos(phi1);
    %cartesian for second sphere
    pp2(1,i)=sin(phi2)*sin(thet2);
    pp2(2,i)=sin(phi2)*cos(thet2);
    pp2(3,i)=cos(phi2);
    
end 
  
A1=[0,0,0];
B1=translation';
% for every match
model=[];
rec=[];
for i=1:r
A2=pp1(:,i)';
B2=(translation+(Rotation*pp2(:,i)))';
% http://www.mathworks.com/matlabcentral/newsreader/view_thread/246420
 nA = dot(cross(B2-B1,A1-B1),cross(A2-A1,B2-B1));
 nB = dot(cross(A2-A1,A1-B1),cross(A2-A1,B2-B1));
 d = dot(cross(A2-A1,B2-B1),cross(A2-A1,B2-B1));
 A0 = A1 + (nA/d)*(A2-A1);
 B0 = B1 + (nB/d)*(B2-B1);
 
rec_temp=(A0+B0)/2
rec(i,:)=rec_temp;
end
rec

%% Apply Similarity Transformation

GCP=load('GCP.txt');

	% P1 is rec
    % P2 is GCP data
	
	for i=1:r
	
		P1(i,1) = rec(i,1);
		P1(i,2) = rec(i,2);
		P1(i,3) = rec(i,3);

		P2(i,1) = GCP(i,2);
		P2(i,2) = GCP(i,3);
		P2(i,3) = GCP(i,4);
    end
	
	% find the mean point in x, y and z coordinates
    meanP1=mean(P1);
    meanP2=mean(P2);
	 mean1x = meanP1(1);
	 mean1y = meanP1(2);
	 mean1z = meanP1(3);
	 mean2x = meanP2(1);
	 mean2y = meanP2(2);
	 mean2z = meanP2(3);

	% shift all the 3d coordinates so that their mean point is (0,0,0)
	for i=1:r
	
		P1(i,1) = P1(i,1)-mean1x;
		P1(i,2) =P1(i,2)-mean1y;
		P1(i,3) =P1(i,3)-mean1z;

		P2(i,1) = P2(i,1)-mean2x;
		P2(i,2) =P2(i,2)-mean2y;
		P2(i,3) =P2(i,3)-mean2z;
    end

	% rotation			P1 = R * P2
	scatter = P2' * P1;
	[U,D,V]=svd(scatter);
	 R = U * V';

	% scale
	 scale = dot(P2, P1 * R') / dot(P1, P1);
	
	% translation
	mean1(1,1) = mean1x; 
    mean1(2,1) = mean1y; 
    mean1(3,1) = mean1z;
	mean2(1,1) = mean2x; 
    mean2(2,1) = mean2y; 
    mean2(3,1) = mean2z;
	t = mean2 - scale * R * mean1;

	% create H transform 
	%              | sR       T |
	%         H =  | 0        1 |
	%
	
	H=(scale*R);
	H(1,4) = t(1,1);
	H(2,4) = t(2,1);
	H(3,4) = t(3,1);
    H
    
    rec(:,4)=1;
    
    disp('Resulting reconstructed points');
    world_points =H*rec'
    
    disp('Show errors on GCPs in meter');
    error_on_GCP=GCP(:,2:4)'-world_points
    