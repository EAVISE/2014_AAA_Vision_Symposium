function [] = projDb(varargin)
% PROJDB M-file for projDb.fig
%      PROJDB, by itself, creates a new PROJDB or raises the existing
%      singleton*.
%
%      H = PROJDB returns the handle to a new PROJDB or the handle to
%      the existing singleton*.
%
%      PROJDB('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PROJDB.M with the given input arguments.
%
%      PROJDB('Property','Value',...) creates a new PROJDB or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before projDb_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to projDb_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help projDb

% Last Modified by GUIDE v2.5 28-Sep-2013 11:31:43

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @projDb_OpeningFcn, ...
                   'gui_OutputFcn',  @projDb_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT
setappdata(0, 'projDb', true);


% --- Executes just before projDb is made visible.
function projDb_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to projDb (see VARARGIN)

% Choose default command line output for projDb
handles.output = hObject;

[numGrid,txtGrid,rawGrid] = xlsread('doc\epsg','grid','A2:N3031');
handles.epsg.numGrid = numGrid;
handles.epsg.txtGrid = txtGrid;

[numProj,txtProj,rawProj] = xlsread('doc\epsg','projection','A2:C30');
handles.epsg.numProj = numProj;
handles.epsg.txtProj = txtProj;

[numDat,txtDat,rawDat] = xlsread('doc\epsg','datum','A2:K447');
handles.epsg.numDat = numDat;
handles.epsg.txtDat = txtDat;

[numEll,txtEll,rawEll] = xlsread('doc\epsg','ellipsoid','A2:D49');
handles.epsg.numEll = numEll;
handles.epsg.txtEll = txtEll;

[numPrim,txtPrim,rawPrim] = xlsread('doc\epsg','prime','A2:C15');
handles.epsg.numPrim = numPrim;
handles.epsg.txtPrim = txtPrim;

EPSG = {};
for i = 1:length(rawGrid)
    EPSG{i} = num2str(rawGrid{i,1});
end

set(handles.listboxEpsg,'string',EPSG);

num = 2820; % kies lambert72 als opstart

handles.epsg.num = num;
% handles.epsg.txt = txt;

javaFrame = get(hObject,'JavaFrame');
javaFrame.setFigureIcon(javax.swing.ImageIcon('fig/emblem-web-2.png'));

set(handles.selectOption,'Enable','off');
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes projDb wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = projDb_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in listboxEpsg.
function listboxEpsg_Callback(hObject, eventdata, handles)
% hObject    handle to listboxEpsg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns listboxEpsg contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listboxEpsg

numGrid = handles.epsg.numGrid;
txtGrid = handles.epsg.txtGrid;
numProj = handles.epsg.numProj;
txtProj = handles.epsg.txtProj;
numDat = handles.epsg.numDat;
txtDat = handles.epsg.txtDat;
numEll = handles.epsg.numEll;
txtEll = handles.epsg.txtEll;
numPrim = handles.epsg.numPrim;
txtPrim = handles.epsg.txtPrim;

index = get(handles.listboxEpsg,'value');
% num = handles.epsg.num;
num = index;
set(handles.textName, 'string', txtGrid{num,1}); % name
set(handles.textRegion, 'string', txtGrid{num,2}); % region

nrDat = find(numGrid(num,end-1) == numDat(:,1));
set(handles.textDatum, 'string', txtDat{nrDat,1}); % datum

nrProj = find(numGrid(num,end) == numProj(:,1));
set(handles.textProjection, 'string', txtProj{nrProj,1}); % projectie

nrEll = find(numDat(nrDat,end-1) == numEll(:,1));
set(handles.textEllipsoid, 'string', txtEll{nrEll,1}); % ellipsoid

nrPrim = find(numDat(nrDat,end) == numPrim(:,1));
set(handles.textPrime, 'string', txtPrim{nrPrim,1}); % prime

handles.epsg.nrDat = nrDat;
handles.epsg.nrProj = nrProj;
handles.epsg.nrEll = nrEll;
handles.epsg.nrPrim = nrPrim;
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function listboxEpsg_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listboxEpsg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in openShape.
function openShape_Callback(hObject, eventdata, handles)
% hObject    handle to openShape (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[fileName, pathName] = uigetfile({'*.shp','ESRI shapefile (*.shp)'; ...
   '*.txt','Text ASCII Raster (*.txt)'; ...
   '*.*',  'All Files (*.*)'}, ...
   'Selecteer object bestand...', ...
   pwd, ...
   'MultiSelect', 'off');
handles.shape = [pathName fileName(1:end-4)];
set(handles.selectOption,'Enable','on');
guidata(hObject, handles);

% --- Executes on button press in selectOption.
function selectOption_Callback(hObject, eventdata, handles)
% hObject    handle to selectOption (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
num = get(handles.listboxEpsg,'value');
nrEll = handles.epsg.nrEll;

numGrid = handles.epsg.numGrid;
numEll = handles.epsg.numEll;

txtProj = handles.epsg.txtProj;
numProj = handles.epsg.numProj;
nrProj = find(numGrid(num,end) == numProj(:,1));

setappdata(0, 'shapePath', handles.shape);
setappdata(0, 'FalseEasting', numGrid(num,9));
setappdata(0, 'FalseNorthing', numGrid(num,10));
setappdata(0, 'StandPar1', numGrid(num,7));
setappdata(0, 'StandPar2', numGrid(num,6));
setappdata(0, 'LatOrigin', numGrid(num,5));
setappdata(0, 'LonOriginCM', numGrid(num,4));
setappdata(0, 'ProjScaleFac', numGrid(num,8));
setappdata(0, 'SemiMajAx', numEll(nrEll,3));
setappdata(0, 'InvFlatt', numEll(nrEll,4));
setappdata(0, 'EPSG', numGrid(num,1));
setappdata(0, 'projName', txtProj{nrProj,1});

setappdata(0, 'projDb', false);

hMainGui = getappdata(0, 'hMainGui');
fhUpdateProj = getappdata(hMainGui, 'fhUpdateProj');
feval(fhUpdateProj)

close(gcf);
