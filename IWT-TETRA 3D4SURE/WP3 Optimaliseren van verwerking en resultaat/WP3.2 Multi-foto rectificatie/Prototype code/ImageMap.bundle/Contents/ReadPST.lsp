(defun c:ReadPST ( / fn fp lst l)

    (princ "New version")
  (setvar "cmdecho" 1)
  (setvar "INSUNITS" 6)  ; meters
  (setvar 'pdmode 34)    ; point mode
  
  ;; check if the current AutoCAD file is saved
  (setq a (getvar "dwgtitled"))
  (setq path (getvar "dwgprefix"))
  (if (/= a 1)
    (princ "AutoCAD file is not saved, please save the AutoCAD file in the same folder as the image to be imported before continuing")
    (progn
     	      
	  ;; Prompt the user to select a .TXT file.
	  (setq fn (getfiled "Select ASCII file" path "PST" 4))

           ;; Open the file and create an empty list
	  (setq fp (open fn "r") lst '())

	  ;; Iterate the file, writing each line to the list
	  (setq header (read-line fp))
          (if (/= header "## IMAGEMAP PROJECT")
		(princ "Selected file is not a ImageMap Project File")
		(progn
	  		(setq dummy (read-line fp))
	  		(setq UCSName (read-line fp))
	  		(setq Origin (read-line fp))
	  		(setq Xax (read-line fp))
	  		(setq Yax (read-line fp))
	  		(setq dummy (read-line fp))
	  		(setq imageFile (read-line fp))
	  		(setq scale (read-line fp))

			  ; read the control points
       		  	 (setq dummy (read-line fp))
			 (while (setq l (read-line fp))
			    (setq lst (cons l lst))
	 		 )

		         ;; Close the file.
			 (close fp)

	   	         ;; Reverse the list
	 		 (setq lst (reverse lst))

			 ;; At this point, the data is stored in
			 ;; a variable and the file is closed.

			 ;; Turn off OSMODE
	  		 (setvar "osmode" 0)

          		 ;; check if the image to be imported is in the same folder as the current autocad drawing
          		 (if (not (findfile imageFile))
	    		 (princ "The Rectified image file cannot be found in the folder where your AutoCAD drawing is stored, please save your AutoCAD drawing to the same folder as your rectified image file")
	    		 (progn

	          	 	;; Check if UCS already exists
		  		(setq t1 (tblsearch "UCS" UCSName))
	      	  		(if t1
		    			(princ "Image was already imported into AutoCAD Drawing")
		    			(progn      	  

		          			;; add the control points
		          			(command "_ucs" "world")
		          			      
		      	  
			  			;; Create New UCS
			  			(command "_ucs" "world")
			  			(command "_ucs" "3" Origin Xax Yax)
			  			(command "_ucs" "NA" "S" UCSName)

						(foreach item lst
                             				(command "_._point" item))  

			  			;; Attach the rectified Image          
			  			(if (findfile imageFile)
			    				(progn
			     			 		(setq imageFilePath (findfile imageFile))
			      					(command "_-image" "_Attach" imageFilePath "0.00,0.00" Scale "0")
			    				)
			    				(princ "Could not find the rectified image file")
			  			)

						;; set the image to back in drawing order
						;;(setq sel1 (ssget "x" '((8 . "dims"))))
						(setq sel1 (entlast))
						(command "_draworder" sel1 "" "back")

		      	  			;; set view to the image
		      	  			(command "_plan" "C")

		          			;; delete any duplicate (control) points
		          			(c:DELDUP_PT)
		          
		      			); end else (if (= t3 UCSName)
		    		); end if (= t3 UCSName)
	      		); end else (if not (findfile imageFile)
	    		); end (if not (findfile imageFile)
		); end else if (/= header "## IMAGEMAP PROJECT")
		); end if (/= header "## IMAGEMAP PROJECT")
    ); end else (if (/= a 1)
     
  ) ; end if (if (/= a 1)
    
  ;; Close quietly
  (princ)

  ;(setvar "cmdecho" 1)
)





(defun c:deldup_pt( / ss ssdup ct len e eb
                      pt lay obj obj_list)

  ;(princ "\nSelect point objects.")             ;Select objects and filter all but valid objects.
   (setq ss (ssget "X" (list (cons 0 "POINT"))))
   (if ss                                       ;If any valid objects were selected.
   (progn
      ;(princ "\nBuilding list of objects.")
      (setq ssdup (ssadd))                      ;Initialize new selection set to hold objects to delete
      (setq len (sslength ss))                  ;Find out how many objects were selected.      
      (setq ct 0)
      (while (< ct len)                         ;Loop through selected objects
         (setq e (ssname ss ct))                ;Get an object name
         (setq eb (entget e))                   ;Get the entity list from the object name
         (setq ct (+ ct 1))                     ;Increment index into selection set
         (setq pt (cdr (assoc 10 eb)))          ;Access object's coordinate
         (setq lay (cdr (assoc 8 eb)))          ;Access object's layer
                                                ;Make list of object properties
         (setq obj (list pt lay))

         (if (not (member obj obj_list))        ;If these properties are not already in list
            (setq obj_list (cons obj obj_list)) ;Add them to the list
            (ssadd e ssdup)                     ;Else add object to selection set to delete
         )                                      ;End if
      )                                         ;End of while loop

      (if (> (sslength ssdup) 0)                ;If there are any objects in the selection set to delete
      (progn
         ;(princ "\nDeleting duplicate objects.")
         (setq len (sslength ssdup))            ;Find out how many many objects to delete.      
         (setq ct 0)
         (while (< ct len)                      ;Loop through objects and delete.
            (setq e (ssname ssdup ct))          ;Get object name
            (setq ct (+ ct 1))                  ;Increment index into selection set
            (entdel e)                          ;Delete duplicate object
         )                                      ;End of while loop

         ;(princ                                 ;Print the number of objects deleted to command line 
         ;   (strcat "\nDeleted " 
         ;           (itoa len) 
         ;           " duplicate objects."
         ;))
      )                                         ;End progn
         ;(princ "\nNo duplicates found.")       ;Else no there were no duplicates to delete.
      )                                         ;End if

   )                                            ;End progn
      ;(princ "\nNo point objects selected.")    ;Else there were no valid objects selected
   )                                            ;End if
   (princ)
)
