function h=rectangles(ra,rb,x0,y0,C,Nb)
% Check the number of input arguments 

if nargin<3,
  x0=[];
  y0=[];
end;
 
if nargin<5,
  C=[];
end

if nargin<6,
  Nb=[];
end

% set up the default values

if isempty(ra),ra=1;end;
if isempty(rb),rb=1;end;
if isempty(x0),x0=0;end;
if isempty(y0),y0=0;end;
if isempty(Nb),Nb=300;end;
if isempty(C),C=get(gca,'colororder');end;

% work on the variable sizes

x0=x0(:);
y0=y0(:);
ra=ra(:);
rb=rb(:);
Nb=Nb(:);

if isstr(C),C=C(:);end;

if length(x0)~=length(y0),
  error('length(x0)~=length(y0)');
end;

% how many inscribed elllipses are plotted

if length(ra)~=length(x0)
  maxk=length(ra)*length(x0);
else
  maxk=length(ra);
end;

% drawing loop

for k=1:maxk
  
  if length(x0)==1
    xpos=x0;
    ypos=y0;
    radm=ra(k);
    radn=rb(k);
  elseif length(ra)==1
    xpos=x0(k);
    ypos=y0(k);
    radm=ra;
    radn=rb;
  elseif length(x0)==length(ra)
    xpos=x0(k);
    ypos=y0(k);
    radm=ra(k);
    radn=rb(k);
  else
    radm=ra(fix((k-1)/size(x0,1))+1);
    radn=rb(fix((k-1)/size(x0,1))+1);
    xpos=x0(rem(k-1,size(x0,1))+1);
    ypos=y0(rem(k-1,size(y0,1))+1);
  end;

  h(k)=line([xpos - radm; ...
      xpos - radm; ...
      xpos + radm; ...
      xpos + radm; ...
      xpos - radm],...
      [ypos - radn; ...
      ypos + radn; ...
      ypos + radn; ...
      ypos - radn; ...
      ypos - radn]);
  set(h(k),'color',C(rem(k-1,size(C,1))+1,:));

end;

