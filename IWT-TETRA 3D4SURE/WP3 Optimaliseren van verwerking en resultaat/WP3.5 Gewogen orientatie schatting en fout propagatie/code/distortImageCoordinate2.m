function [ colNew,rowNew ] = distortImageCoordinate2( col,row, f, K,T,Px,Py )
%DISTORTIMAGECOORDINATE Summary of this function goes here
%   Detailed explanation goes here
xp = col - Px; % bring to center
yp = Py - row;

xn = xp./f; % bring to normal coordinates
yn = yp./f;

r2 = sqrt(xn.^2 + yn.^2);

xp = (1 + K(1)*r2 + K(2)*(r2.^2) + K(3)*(r2.^4))*xn + ...
    2.*T(1)*xn*yn + T(2).*(r2+2.*xn.^2);

yp = (1 + K(1)*r2 + K(2)*(r2.^2) + K(3)*(r2.^4))*yn + ...
    2.*T(2)*xn*yn + T(1).*(r2+2.*yn.^2);

xi = xp.*f; % to image coordinates
yi = yp.*f;

colNew = xi + Px;
rowNew = Py - yi;

end

