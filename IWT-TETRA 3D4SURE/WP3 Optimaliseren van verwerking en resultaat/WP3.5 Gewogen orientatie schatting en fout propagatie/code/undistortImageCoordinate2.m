function [colNew,rowNew] = undistortImageCoordinate2(col,row,f,K,T,Px,Py)
% remove radial and tangential distortion form image coordinate
% x,y are in image coordinates, originating at the topleft
%
% B. Altena - November 2013

xp = col - Px; % bring to center
yp = Py - row;

xn = xp./f; % bring to normal coordinates
yn = yp./f;


r2 = sqrt(xn.^2 + yn.^2);

xp = (1 - K(1)*r2 - K(2)*(r2.^2) - K(3)*(r2.^4))*xn - ...
    2.*T(1)*xn*yn - T(2).*(r2+2.*xn.^2);

yp = (1 - K(1)*r2 - K(2)*(r2.^2) - K(3)*(r2.^4))*yn - ...
    2.*T(2)*xn*yn - T(1).*(r2+2.*yn.^2);

xi = xp.*f; % to image coordinates
yi = yp.*f;

colNew = xi + Px;
rowNew = Py - yi;
end

