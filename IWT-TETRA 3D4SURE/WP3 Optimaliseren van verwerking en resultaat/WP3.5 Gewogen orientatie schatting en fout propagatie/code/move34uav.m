workDir = cd();
includeGCP = true;
includeKeypoints = true;
imShow = false;

%% import orientation camera's and camera specifications

[projName, pathProj] = uigetfile({'*.p4d','Pix4D project (*.p4d)'}, ...
   'Open photogrammetry project...', ...
   pwd, ...
   'MultiSelect', 'off');

d = dir(pathProj);
nameFolds = {d([d(:).isdir]).name}';
if sum(ismember(nameFolds,{'1_initial'})) ~= 0
    pixDir = [pathProj projName(1:end-4) '\params'];
else
    pixDir = [pathProj projName(1:end-4) '\1_initial\params'];
end

[dummy, imDir] = uigetfile({'*.*',  'All Files (*.*)'}, ...
   'Select an image from the project...', ...
   pwd, ...
   'MultiSelect', 'off');

cd(pixDir);
% fid = fopen('kiezegem100_metriccal.txt');
fid = fopen([projName(1:end-4) '_calibrated_camera_parameters.txt']);

cam = {};
counter = 1;

% while ~feof(fid)
%     tline=fgetl(fid);
%     sline = regexp(tline, ' ', 'split');
%     if regexp(sline{1},'camModelList.size()')
%         imTotal = cellfun(@str2num, sline(2));
%         break
%     end
% end

for i = 1:8
    fgetl(fid);
end

counter = 1;
while ~feof(fid)%for i = 1:imTotal
    % fileName imageWidth imageHeight
    tline=fgetl(fid);
    sline = regexp(tline, ' ', 'split');
    cam.imFile{counter} = sline{1};
    cam.size{counter} = cellfun(@str2num, sline(2:end));
    % camera matrix K [3x3]
    tline=fgetl(fid); sline = regexp(tline, ' ', 'split');
    cam.K{counter}(1,:) = cellfun(@str2double, sline);
    tline=fgetl(fid); sline = regexp(tline, ' ', 'split');
    cam.K{counter}(2,:) = cellfun(@str2double, sline);
    tline=fgetl(fid); sline = regexp(tline, ' ', 'split');
    cam.K{counter}(3,:) = cellfun(@str2double, sline);
    % radial distortion [3x1]
    tline=fgetl(fid); sline = regexp(tline, ' ', 'split');
    cam.rad{counter} = cellfun(@str2double, sline);
    % tangential distortion [2x1]
    tline=fgetl(fid); sline = regexp(tline, ' ', 'split');
    cam.tan{counter} = cellfun(@str2double, sline);
    % camera position T [3x1]
    tline=fgetl(fid); sline = regexp(tline, ' ', 'split');
    cam.T{counter} = cellfun(@str2double, sline);
    % camera matrix K [3x3]
    tline=fgetl(fid); sline = regexp(tline, ' ', 'split');
    cam.R{counter}(1,:) = cellfun(@str2double, sline);
    tline=fgetl(fid); sline = regexp(tline, ' ', 'split');
    cam.R{counter}(2,:) = cellfun(@str2double, sline);
    tline=fgetl(fid); sline = regexp(tline, ' ', 'split');
    cam.R{counter}(3,:) = cellfun(@str2double, sline);
    % camera model m = K [R|-Rt] X
    counter = counter + 1;
end
fclose(fid);

% insert world coordinates
% fid = fopen('kiezegem100_geo.txt');
fid = fopen([projName(1:end-4) '_calibrated_external_camera_parameters.txt']);
tline=fgetl(fid); % header
counter = 1;
while ~feof(fid)
    tline=fgetl(fid);
    sline = regexp(tline, ' ', 'split');
    aline = cellfun(@str2num, sline(2:end));
    cam.XYZ(counter,:) = aline;
    counter = counter + 1;
end
fclose(fid);

cd(workDir);
%% import data offset
cd(pixDir);
cd ..\..
fid = fopen(['2_densification\2_filtered\' projName(1:end-4) '_ply_offset.xyz']);
tline = fgetl(fid); fclose(fid);
dataOffset = str2num(tline);

clear fid tline
%% import internal camera calibration
cd(pixDir);
% fid = fopen('kiezegem100_pix4D_internals.cam');
fid = fopen([projName(1:end-4) '_pix4d_calibrated_internal_camera_parameters.cam']);

while ~feof(fid)
    tline=fgetl(fid);
    sline = regexp(tline, ' ', 'split');
    switch sline{1}
        case 'F'
            focal = cellfun(@str2num, sline(end));
        case 'Px'
            xPoff =  cellfun(@str2num, sline(end));
        case 'Py'
            yPoff =  cellfun(@str2num, sline(end));
        case 'Sw'
            sensorWidth = cellfun(@str2num, sline(end));
        case 'Sh'
            sensorHeight = cellfun(@str2num, sline(end));
        case 'K1'
            rad(1) = cellfun(@str2num, sline(end));
        case 'K2'
            rad(2) = cellfun(@str2num, sline(end));
        case 'K3'
            rad(3) = cellfun(@str2num, sline(end));
        case 'T1'
            tan(1) = cellfun(@str2num, sline(end));
        case 'T2'
            tan(2) = cellfun(@str2num, sline(end));
    end
end
fclose(fid);

cd(workDir);

%% import keypoints
if includeKeypoints
    cd(pixDir);
    fid = fopen([projName(1:end-4) '_bingo.txt']);
    
    cd(imDir);
    keyPoints = {};
    counter = 1;
    minKey = 1e10;
    maxKey = 1;
    keyXY = [];
    keyName = {};
    while ~feof(fid)
        tline=fgetl(fid);
        sline = regexp(tline, ' ', 'split');
        firstLine = false;
        lastLine = false;
        switch sline{1}(1:3) % walkthrough images
            case 'DSC'
                keyPoints.imFile{counter} = sline{1};
                firstLine = true;
                cd(imDir)
                % open image and find scale of keypoints at every pixel
                I = single(rgb2gray(imread([keyPoints.imFile{counter} '.JPG'])));
                Isift = I./max(I(:)); % normalize to [0-1]
            case '-99'
                keyPointEnd = true;
                keyPoints.XY{counter} = keyXY;
                lastLine = true;
                counter = counter + 1;
                if ~isempty(keyXY)
                    if max(keyXY(:,1)) > maxKey
                        maxKey = max(keyXY(:,1));
                    end
                    if min(keyXY(:,1)) < minKey
                        minKey = min(keyXY(:,1));
                    end
                end
                keyXY = [];
        end
        gcpPoint = false;
        if ~ (firstLine || lastLine)
            if strcmpi('GCP',sline{1}(1:3))
                sline{1} = [sline{1}(5:end)];
                gcpPoint = true;
            end
            keyPoint = cellfun(@str2num, sline(1:end));
            if gcpPoint
                keyPoint = cat(2,keyPoint,1);
            else
                keyPoint = cat(2,keyPoint,0);
            end
            % keypoint in image coordinates
            keyX = ((sensorWidth./2) + keyPoint(2)) ./ (sensorWidth/cam.size{1}(1));
            keyY = ((sensorHeight./2) - keyPoint(3)) ./ (sensorHeight/cam.size{1}(2));
            %% find extrema in difference of Gaussians (DoG) pyramis
            boundingSize = 40;
            cd(workDir);
            [ Isub ] = createSubset( keyY,keyX,double(Isift),boundingSize);
            [frames, descrip] = vl_covdet(im2single(Isub), ...
                'Method', 'DoG', ... % of Hessian?
                'PeakThreshold', 1e-08, 'EdgeThreshold', 100, ...
                'EstimateOrientation', true, ...
                'EstimateAffineShape', true);
            % frame(1:2) are the x,y coordiantes of the center. 
            % frame(3:6) is the column-wise stacking of a 2x2 matrix A 
            % defining the ellipse shape and orientation. 
            % The ellipse is obtaine by transforming
            % a unit circle by A as the set of points 
            % {A x + T : |x| = 1}, 
            % where T is the center.

            [ySub,xSub] = transformIJ2Subset(keyY,keyX,Isift,boundingSize);
            [val idx] = sort(sqrt(sum((frames(1:2,:) - ...
                repmat([xSub+1;ySub+1], 1, size(frames,2))).^2)),2);
            keyXY = cat(1,keyXY, ...
                [keyPoint frames(3:6,idx(1))']);
%                 sqrt(sum(frames(3:6,idx(1)).^2)) ...
%                 sqrt(sum(frames(5:6,idx(1)).^2))]);
%             figure();
%             imagesc(Isub); axis off; axis equal; colormap(gray);
%             vl_plotframe(frames); hold on;
%             scatter(xSub+1,ySub+1,'+b'); hold off;
        end
    end
    fclose(fid);

    cd(workDir);
    
    %% structure to keypoint oriented file
    keyList = [];
    counter = 1;
    for i = minKey:maxKey
        coordinateList = [];
        for j = 1:length(keyPoints.XY) % search through photos
            searchKey = keyPoints.XY{j}(:,1)==i;
            if sum(searchKey) == 1
                coordinateList = cat(2, coordinateList, ...
                    [j keyPoints.XY{j}(searchKey,2:4) i]);
            end
        end
        
        if numel(coordinateList)>=9 % select only triples
            keyList{counter} = coordinateList;
            counter = counter+1;
        end
    end
    %% calculate ground point
    
    keyXYZ = zeros(length(keyList),3);
%     key = repmat(key, 1, 4);
    for i = 1:length(keyList)
        coordinateList = reshape(keyList{i},5,[])';
        
        stackP = []; 
        stackm = []; %stackK = []; stackR = []; stackT = [];
        for j = 1:size(coordinateList,1);
            m = coordinateList(j,2:3);
            r2 = (m(1)/focal).*1e-3.^2 + (m(2)/focal).*1e-3.^2;
            
            xi = m(1)./ (sensorWidth/cam.size{1}(1)); % convert to pixel coordinates
            yi = m(2)./ (sensorHeight/cam.size{1}(2));
            differ = 1;
            affinity = 1;
            skew = 0;
            xp = xi; yp = yi;            
            while differ>1e-2  % radial distortion              
                xiStart = xi;
                yiStart = yi;
                
                x = xi.*1e-3;
                y = yi.*1e-3;
                
                dxi = -rad(1).*x*r2 -rad(2)*x*r2.^2 -rad(3)*x*r2.^4 -tan(1)*2*x*y -tan(2)*(r2+2*x.^2);
                dyi = -affinity*y -skew*x -rad(1)*y*r2 -rad(2)*y*r2.^2 -rad(3)*y*r2^4 -tan(1)*(r2+2*y^2) -tan(2)*2*x*y;
                
                xi = xp + dxi; yi = yp + dyi;
                differ = sum([xiStart - xi; yiStart - yi]);
            end
            
            mi(1) = xi.* (sensorWidth/cam.size{1}(1)); % convert back to camera coordinates
            mi(2) = yi.* (sensorHeight/cam.size{1}(2));
            
            keyX = ((sensorWidth./2) + mi(1)) ./ (sensorWidth/cam.size{1}(1));
            keyY = ((sensorHeight./2) - mi(2)) ./ (sensorHeight/cam.size{1}(2));
            
            metricStyle = false;            
            if metricStyle
                m(1) = (xi + xPoff) * (sensorWidth/cam.size{1}(1)); 
                m(2) = (yi + yPoff) * (sensorHeight/cam.size{1}(2));
                m(3) = 1;
                K = cam.K{coordinateList(j,1)}.*(sensorWidth/cam.size{1}(1)); 
                K(end) = 1;
            else
                m(1) = (xPoff./ (sensorWidth/cam.size{1}(1))) + xi;
                m(2) = (yPoff./ (sensorWidth/cam.size{1}(1))) - yi;
                m(3) = 1;
                K = cam.K{coordinateList(j,1)};
            end
            R = cam.R{coordinateList(j,1)};
            T = cam.T{coordinateList(j,1)};
            stackm = cat(1, stackm, [keyX keyY]);
            stackP = cat(1, stackP, K*cat(2,R,-R*T'));
        end
        stackedM = reshape(stackm',[],1);
        
        A = repmat(stackedM,1,4).*reshape(repmat(stackP(3:3:end,:)',2,1),4,[])'...
            -stackP(reshape(cat(1,3.*[1:length(stackedM)./2]-2,3.*[1:length(stackedM)./2]-1),[],1),:);
        
        [V,D] = eig(A'*A);
   
        keyXYZ(i,1:4) = (V(:,1)./V(end,1))';
        keyXYZ(i,1:3) = keyXYZ(i,1:3)+dataOffset;
        keyXYZ(i,4:5) = [coordinateList(1,5) coordinateList(1,4)];
    end 
end

disp('done processing keypoints');
%% import ground control points
if includeGCP
%     % image coordinates
%     cd(pixDir);
%     imDir = cd();
% 
%     [fileName, pathName] = uigetfile({'*.txt','Text ASCII Raster (*.txt)'; ...
%        '*.*',  'All Files (*.*)'}, ...
%        'Select GCP file...', ...
%        pwd, ...
%        'MultiSelect', 'off');
%     fid = fopen([pathName fileName]);
% 
%     cd(workDir);
% 
%     imGCP = {};
%     counter = 1;
% 
%     tline=fgetl(fid);
%     sline = regexp(tline, ' ', 'split');
%     while ischar(tline)
%         imfile = sline{1};
%         tline=fgetl(fid); %-99 or GCP
%         if tline == -1 
%             break 
%         end
%         sline = regexp(tline, ' ', 'split');
%         num = cellfun(@str2num, sline(1));
%         if num ~= -99
%             endList = false;
%             while ~endList
%                 imGCP.file{counter} = imfile;
%                 imGCP.gcp{counter} = num;
%                 imGCP.X(counter) = cellfun(@str2num, sline(2));
%                 imGCP.Y(counter) = cellfun(@str2num, sline(3));
%                 counter = counter + 1;
% 
%                 tline=fgets(fid);
%                 if tline == -1, 
%                     break 
%                 end
%                 sline = regexp(tline, ' ', 'split');
%                 if cellfun(@str2num, sline(1)) == -99
%                     tline=fgets(fid);
%                     sline = regexp(tline, ' ', 'split');
%                     endList = true;
%                 else
%                     num = cellfun(@str2num, sline(1));
%                 end
%             end
%         else
%             tline=fgets(fid);
%             if tline == -1 
%                 break 
%             end
%             sline = regexp(tline, ' ', 'split');
%         end
%     end
%     fclose(fid);

    % world coordinates
    cd(pixDir);
    fid = fopen([projName(1:end-4) '_measured_estimated_gcps_position.txt']);
    gcpXYZ = textscan(fid, '%u %f %f %f %*[^\n]', ...
        'delimiter', {',',' '}, 'MultipleDelimsAsOne', 1, 'headerLines', 1);
    fclose(fid);
    
    cd(workDir);
    if imShow
        figure();
        scatter(imGCP.X(:),imGCP.Y(:),5,[imGCP.gcp{:}], 'filled');
        caxis([2001 2020]); hold on;
%         plot(-0.003, 12.001-11.961, 'rx');
%         grid on;
        
%         plot(imGCP.X(:),imGCP.Y(:), 'rx')
%         labels = cellstr( num2str([imGCP.gcp{:}]') );
%         text(imGCP.X(:),imGCP.Y(:), labels, 'VerticalAlignment','bottom', ...
%                              'HorizontalAlignment','right')
        
        xlabel('x [mm]'); ylabel('y [mm]');
    end
end

%% write to Observatie bestand voor Move3
cd(pixDir); cd ..; cd ..;
fid = fopen([projName(1:end-4) '.obs'], 'wt');
formatSpec = '%s\t%s\t%s\t%1.4f\t%1.4f\t%s\t%3.5f\t%1.5f\t%1.5f\t%s\t%3.5f\t%1.5f\t%1.5f\t%s \n';

fprintf(fid,'%s\n','MOVE3 V4.2 OBS file');
fprintf(fid,'%s\n','$'); 
fprintf(fid,'%s\n', projName(1:end-4)); 
fprintf(fid,'%s\n','$');
fprintf(fid,'%s\n','ANGLEUNIT GON'); 
fprintf(fid,'%s\n','$');
fprintf(fid,'%s\n','');

% 1. observation type -> TS total station
% 2. from -> image name of observation
% 3. to -> keypoints
% 4. instrumental height -> none
% 5. vertical height above known point -> none
% 6. azimuth
% 7. direction -> x of keypoint
% 8. St Afw Abs -> std camera calibration
% 9. St Afw Rel -> std camera calibration in relation to position
% 10. afstand
% 11. afstand
% 12. St Afw Abs -> std camera calibration
% 13. St Afw Rel -> std camera calibration in relation to position
% 14. zenith
% 15. direction -> y of keypoint
% 16. St Afw Abs -> std camera calibration
% 17. St Afw Rel -> std camera calibration in relation to position
% 18. Dimension of network


imFiles = char(cam.imFile{:});

obsType = 'TS'; % 1
instrHgt = 0; % 4
gcpHgt = 0; % 5
azi = 'R0'; % 6
zen = 'Z0'; % 14
dim = '3D'; % 18

% transform from pixel deviation to angles (gon)
gonFactorX = (400 * (sensorWidth/cam.size{1}(1)))./(2.*pi().*focal); 
gonFactorY = (400 * (sensorHeight/cam.size{1}(2)))./(2.*pi().*focal);

if includeKeypoints
    for i = 1:length(keyPoints.imFile);
        from = keyPoints.imFile{i}; % 2

    %     match = strcmpi(cellstr(imFiles(:,1:end-4)), from);
    %     [ dummy matchIdx] = max(match); % index of image file in cam.imFiles
    %     rad = cam.rad{matchIdx};
    %     tan = cam.tan{matchIdx};
        for j = 1:length(keyPoints.XY{i});
            
            if keyPoints.XY{i}(j,4)
                to = ['gcp_' num2str(keyPoints.XY{i}(j,1))]; % 3
            else
                to = num2str(keyPoints.XY{i}(j,1)); % 3
            end
            
            
            % undistort image coordinates
            
            m = keyPoints.XY{i}(j,2:3);
            r2 = (m(1)/focal).*1e-3.^2 + (m(2)/focal).*1e-3.^2;
            
            xi = m(1)./ (sensorWidth/cam.size{1}(1)); % convert to pixel coordinates
            yi = m(2)./ (sensorHeight/cam.size{1}(2));
            differ = 1;
            affinity = 1;
            skew = 0;
            xp = xi; yp = yi;            
            while differ>1e-2  % radial distortion              
                xiStart = xi;
                yiStart = yi;
                
                x = xi.*1e-3;
                y = yi.*1e-3;
                
                dxi = -rad(1).*x*r2 -rad(2)*x*r2.^2 -rad(3)*x*r2.^4 -tan(1)*2*x*y -tan(2)*(r2+2*x.^2);
                dyi = -affinity*y -skew*x -rad(1)*y*r2 -rad(2)*y*r2.^2 -rad(3)*y*r2^4 -tan(1)*(r2+2*y^2) -tan(2)*2*x*y;
                
                xi = xp + dxi; yi = yp + dyi;
                differ = sum([xiStart - xi; yiStart - yi]);
            end
            
            mi(1) = xi.* (sensorWidth/cam.size{1}(1)); % convert back to camera coordinates
            mi(2) = yi.* (sensorHeight/cam.size{1}(2));
            
            x = ((sensorWidth./2) + mi(1)) ./ (sensorWidth/cam.size{1}(1));
            y = ((sensorHeight./2) - mi(2)) ./ (sensorHeight/cam.size{1}(2));
            
            
%             x = keyPoints.XY{i}(j,2); % 7
%             y = keyPoints.XY{i}(j,3); % 15
            newVec = cam.R{i}'*[x; -y; focal];
            xNew = newVec(1); yNew = newVec(2); focalNew = newVec(3);
            
            
            azimuth = atan2(xNew,yNew).*400/(2.*pi());
            if sign(azimuth)==-1
                azimuth = azimuth + 400;
            end
            zenit = atan2(sqrt(xNew.^2 + yNew.^2),focalNew).*400/(2.*pi());
            if sign(zenit)==-1
                zenit = zenit + 400;
            end
            stdXabs = sqrt(sum(keyPoints.XY{i}(j,5:6).^2)).*gonFactorX; % 8
            stdXrel = 0; % 9

            stdYabs = sqrt(sum(keyPoints.XY{i}(j,7:8).^2)).*gonFactorX; % 16
            stdYrel = 0; % 17

            fprintf(fid, formatSpec, ...
                obsType, from, to, instrHgt, gcpHgt, ...
                azi, azimuth, stdXabs, stdXrel, ...
                zen, zenit, stdYabs, stdYrel, dim);
        end
    end
end
fprintf(fid,'%s','$');
fclose(fid);

% include directions of the imagery


clear dummy imFiles match
cd(workDir);
%% write to Terrestrial coordinates file (TCO) to Move3
cd(pixDir); cd ..; cd ..
fid = fopen([projName(1:end-4) '.tco'], 'wt');
formatSpec = '%s\t%0.4f%s\t%0.4f%s\t%0.4f%s\t%0.4f\t%0.4f\t%0.4f\n';
formatSpec2 = '%s\t%0.4f\t%0.4f\t% 8.4f\n';

fprintf(fid,'%s\n','MOVE3 V4.2 TCO file');
fprintf(fid,'%s\n','$'); 
fprintf(fid,'%s\n', projName(1:end-4)); 
fprintf(fid,'%s\n','$');
fprintf(fid,'%s\n','PROJECTION LAMBERT2'); 
fprintf(fid,'%s\n','$');
% fprintf(fid,'%s\n','');

for i = 1:length(cam.imFile) % known camera acquisitions
    fprintf(fid, formatSpec, ...
            cam.imFile{i}(1:end-4), ...
            cam.XYZ(i,1), '*', ...
            cam.XYZ(i,2), '*', ...
            cam.XYZ(i,3), '*', ...
            0.100, 0.100, 0.100);
end

if includeKeypoints
%     key = [];
%     for i = 1:length(cam.imFile)
%         key = cat(1, key, keyPoints.XY{i}(:,1));
%     end
%     key = unique(key);
    for i = 1:size(keyXYZ,1) % known camera acquisitions
        if keyXYZ(i,5)
            sel = (keyXYZ(i,4)== gcpXYZ{1});
            
            fprintf(fid, formatSpec, ...
                ['gcp_' num2str(keyXYZ(i,4))], ...
                gcpXYZ{2}(sel), '*', ...
                gcpXYZ{3}(sel), '*', ...
                gcpXYZ{4}(sel), '*', ...
                0.100, 0.100, 0.100);
        else
            fprintf(fid, formatSpec2, ...
                num2str(keyXYZ(i,4)), keyXYZ(i,1), keyXYZ(i,2), keyXYZ(i,3));
        end
    end
end
fprintf(fid,'%s','$');
fclose(fid);

%% write to Project file (PRJ) to Move3

fid = fopen([projName(1:end-4) '.prj'], 'wt');

fprintf(fid,'%s\n','MOVE3 V4.2.0    PRJ file');
fprintf(fid,'%s\n','$');
fprintf(fid,'%s\n', projName(1:end-4));
fprintf(fid,'%s\n', '$ INCLUDED FILE TYPES');
fprintf(fid,'%s\t%s\n', 'TerCoord', 'YES');
fprintf(fid,'%s\t%s\n', 'GPSCoord', 'NO');
fprintf(fid,'%s\t%s\n', 'TerObserv', 'YES');
fprintf(fid,'%s\t%s\n', 'GPSObserv', 'NO');
fprintf(fid,'%s\t%s\n', 'GeoidModel', 'NO');
fprintf(fid,'%s\t%s\n', 'FeatureCode', 'NONE');
fprintf(fid,'%s\n', '$ GEOMETRY PARAMETERS');
fprintf(fid,'%s\t%s\n', 'Dimension', '3');
fprintf(fid,'%s\t%s\n', 'Projection', 'LAMBERT2'); % 2 parallels
fprintf(fid,'%s\t%s\n', 'ProjName', 'EPSG31370');
fprintf(fid,'%s\t%u %u %f\n', 'LonOriginCM', 4, 22, 2.952001);
fprintf(fid,'%s\t%u %u %f\n', 'LatOrigin',  90, 0, 0.000000);
fprintf(fid,'%s\t%u %u %f\n', 'StandPar1',  49, 50, 0.002040);
fprintf(fid,'%s\t%u %u %f\n', 'StandPar2',  51, 10, 0.002028);
fprintf(fid,'%s\t%1.9f\n', 'ProjScaleFac', 0);
fprintf(fid,'%s\t%8.5f\n', 'FalseEasting', 150000.01300 );
fprintf(fid,'%s\t%8.5f\n', 'FalseNorthing', 5400088.43800 );
fprintf(fid,'%s\t%s\n', 'Ellipsoid', 'USER');
fprintf(fid,'%s\t%8.9f\n', 'SemiMajAx', 6378388.000000000 );
fprintf(fid,'%s\t%3.9f\n', 'InvFlatt', 297.000000000 );
fprintf(fid,'%s\t%s\n', 'TransProj', 'NONE');
fprintf(fid,'%s\t%s\n', 'GPSCoordType', 'XYZ');
fprintf(fid,'%s\n', '$ ADJUSTMENT PARAMETERS');
fprintf(fid,'%s\t%s\n', 'AdjDesign', 'ADJUST');
fprintf(fid,'%s\t%s\n', 'Phase', '1');
fprintf(fid,'%s\t%s\n', 'InnerConstraint', 'FALSE');
fprintf(fid,'%s\t%s\n', 'AngleUnit', 'GON');
fprintf(fid,'%s\t%s\n', 'LinearUnit', 'm  1.0000000000000  METER');
fprintf(fid,'%s\t%s\n', 'IterMax', '3');
fprintf(fid,'%s\t%s\n', 'Epsilon', '0.0001');
fprintf(fid,'%s\t%s\n', 'Delta', '1.0e-006');
fprintf(fid,'%s\t%s\n', 'CovMatrix', 'FULL');
fprintf(fid,'%s\t%s\n', 'APostVarFac', 'ALWAYS');
fprintf(fid,'%s\t%s\n', 'VarComponent', 'NONE');
fprintf(fid,'%s\t%s\n', 'VarIterMax', '5');
fprintf(fid,'%s\t%s\n', 'VarEpsilon', '0.0100');
fprintf(fid,'%s\t%s\n', 'FilterFreeStations', 'FALSE');
fprintf(fid,'%s\t%s\n', 'EstAddTrf', 'FREENET');
fprintf(fid,'%s\n', '$ PRECISION AND TESTING PARAMETERS');
fprintf(fid,'%s\t%s\n', 'Sigma0', '1.0e+000');
fprintf(fid,'%s\t%s\n', 'Alfa0', '0.0010');
fprintf(fid,'%s\t%s\n', 'Beta', '0.80');
fprintf(fid,'%s\t%s\n', 'ConfidenceLevel1D', '0.683');
fprintf(fid,'%s\t%s\n', 'ConfidenceLevel2D', '0.394');
fprintf(fid,'%s\t%s\n', 'C0', '0.000');
fprintf(fid,'%s\t%s\n', 'C1', '1.000');
fprintf(fid,'%s\n', '$ DEFAULT STANDARD DEVIATIONS');
fprintf(fid,'%s\t%s\n', 'SigmaAbsR', '0.00100'); % variabel niet?
fprintf(fid,'%s\t%s\n', 'SigmaRelR', '0.00000');
fprintf(fid,'%s\t%s\n', 'SigmaAbsS', '0.0100');
fprintf(fid,'%s\t%s\n', 'SigmaRelS', '0.0');
fprintf(fid,'%s\t%s\n', 'SigmaAbsZ', '0.00100');
fprintf(fid,'%s\t%s\n', 'SigmaRelZ', '0.00000');
fprintf(fid,'%s\t%s\n', 'SigmaAbsA', '0.00100');
fprintf(fid,'%s\t%s\n', 'SigmaRelA', '0.00000');
fprintf(fid,'%s\t%s\n', 'SigmaDHA', '0.00');
fprintf(fid,'%s\t%s\n', 'SigmaDHB', '1.00');
fprintf(fid,'%s\t%s\n', 'SigmaDHC', '0.00');
fprintf(fid,'%s\t%s\n', 'SigmaAbsDX', '0.0100');
fprintf(fid,'%s\t%s\n', 'SigmaRelDX', '1.0');
fprintf(fid,'%s\t%s\n', 'SigmaAbsX', '0.0100');
fprintf(fid,'%s\t%s\n', 'SigmaLatLon', '0.0100');
fprintf(fid,'%s\t%s\n', 'SigmaHgt', '0.01000');
fprintf(fid,'%s\t%s\n', 'SigmaXYZ', '0.0100');% variabel niet?
fprintf(fid,'%s\t%s\n', 'SigmaCentr', '0.0000');
fprintf(fid,'%s\t%s\n', 'SigmaInstr', '0.00000');
fprintf(fid,'%s\t%s\n', 'SigmaTape', '0.0100');
fprintf(fid,'%s\t%s\n', 'SigmaOrthogonal', '0.0100');% variabel niet?
fprintf(fid,'%s\t%s\n', 'SigmaDistLine', '0.0150');
fprintf(fid,'%s\t%s\n', 'SigmaAngle', '0.10000');
fprintf(fid,'%s\t%s\n', 'SigmaIdealXY', '0.0000');% variabel niet?
fprintf(fid,'%s\t%s\n', 'SigmaIdealH', '0.0000');
fprintf(fid,'%s\n', '$ ADDITIONAL PARAMETERS');
fprintf(fid,'%s\t%s\t%1.7f\n', 'ScaleFac0', 'FREE', 1);
fprintf(fid,'%s\t%s\t%1.7f\n', 'ScaleFac1', 'FREE', 1);
fprintf(fid,'%s\t%s\t%1.7f\n', 'ScaleFac2', 'FREE', 1);
fprintf(fid,'%s\t%s\t%1.7f\n', 'ScaleFac3', 'FREE', 1);
fprintf(fid,'%s\t%s\t%1.7f\n', 'ScaleFac4', 'FREE', 1);
fprintf(fid,'%s\t%s\t%1.7f\n', 'ScaleFac5', 'FREE', 1);
fprintf(fid,'%s\t%s\t%1.7f\n', 'ScaleFac6', 'FREE', 1);
fprintf(fid,'%s\t%s\t%1.7f\n', 'ScaleFac7', 'FREE', 1);
fprintf(fid,'%s\t%s\t%1.7f\n', 'ScaleFac8', 'FREE', 1);
fprintf(fid,'%s\t%s\t%1.7f\n', 'ScaleFac9', 'FREE', 1);
fprintf(fid,'%s\t%s\t%1.3f\n', 'VertRefr0', 'FIXED', 0.13);
fprintf(fid,'%s\t%s\t%1.3f\n', 'VertRefr1', 'FIXED', 0.13);
fprintf(fid,'%s\t%s\t%1.3f\n', 'VertRefr2', 'FIXED', 0.13);
fprintf(fid,'%s\t%s\t%1.3f\n', 'VertRefr3', 'FIXED', 0.13);
fprintf(fid,'%s\t%s\t%1.3f\n', 'VertRefr4','FIXED', 0.13);
fprintf(fid,'%s\t%s\t%1.3f\n', 'VertRefr5','FIXED', 0.13);
fprintf(fid,'%s\t%s\t%1.3f\n', 'VertRefr6','FIXED', 0.13);
fprintf(fid,'%s\t%s\t%1.3f\n', 'VertRefr7','FIXED', 0.13);
fprintf(fid,'%s\t%s\t%1.3f\n', 'VertRefr8','FIXED', 0.13);
fprintf(fid,'%s\t%s\t%1.3f\n', 'VertRefr9','FIXED', 0.13);
fprintf(fid,'%s\t%s\t%1.5f\n', 'AzimOffs0', 'FIXED', 0);
fprintf(fid,'%s\t%s\t%1.5f\n', 'AzimOffs1', 'FIXED', 0);
fprintf(fid,'%s\t%s\t%1.5f\n', 'AzimOffs2', 'FIXED', 0);
fprintf(fid,'%s\t%s\t%1.5f\n', 'AzimOffs3', 'FIXED', 0);
fprintf(fid,'%s\t%s\t%1.5f\n', 'AzimOffs4', 'FIXED', 0);
fprintf(fid,'%s\t%s\t%1.5f\n', 'AzimOffs5', 'FIXED', 0);
fprintf(fid,'%s\t%s\t%1.5f\n', 'AzimOffs6', 'FIXED', 0);
fprintf(fid,'%s\t%s\t%1.5f\n', 'AzimOffs7', 'FIXED', 0);
fprintf(fid,'%s\t%s\t%1.5f\n', 'AzimOffs8', 'FIXED', 0);
fprintf(fid,'%s\t%s\t%1.5f\n', 'AzimOffs9', 'FIXED', 0);
fprintf(fid,'%s\t%s\t%1.4f\t%1.4f\n', 'GPSTrfTX', 'FREE', 0, 0);
fprintf(fid,'%s\t%s\t%1.4f\t%1.4f\n', 'GPSTrfTY', 'FREE', 0, 0);
fprintf(fid,'%s\t%s\t%1.4f\t%1.4f\n', 'GPSTrfTZ', 'FREE', 0, 0);
fprintf(fid,'%s\t%s\t%1.4f\t%1.4f\n', 'GPSTrfRX', 'FREE', 0, 0);
fprintf(fid,'%s\t%s\t%1.4f\t%1.4f\n', 'GPSTrfRY', 'FREE', 0, 0);
fprintf(fid,'%s\t%s\t%1.4f\t%1.4f\n', 'GPSTrfRZ', 'FREE', 0, 0);
fprintf(fid,'%s\t%s\t%1.4f\t%1.4f\n', 'GPSTrfSc', 'FREE', 1, 0);
fprintf(fid,'%s\t%s\n', 'LocalTrfType', 'NONE'); % ff kijken wat dit is
fprintf(fid,'%s\n', '$ PRINT OUTPUT SWITCHES');
fprintf(fid,'%s\t%s\n', 'PrProjConst', 'YES');
fprintf(fid,'%s\t%s\n', 'PrInpCoords', 'NO');
fprintf(fid,'%s\t%s\n', 'PrAddParms', 'YES');
fprintf(fid,'%s\t%s\n', 'PrInpObsv', 'NO');
fprintf(fid,'%s\t%s\n', 'PrAdjCoords', 'NO');
fprintf(fid,'%s\t%s\n', 'PrExtReliab', 'YES'); % if baak is only on change
fprintf(fid,'%s\t%s\n', 'PrAbsStandEll', 'YES');
fprintf(fid,'%s\t%s\n', 'PrRelStandEll', 'NO');
fprintf(fid,'%s\t%s\n', 'PrTestCoords', 'NO');
fprintf(fid,'%s\t%s\n', 'PrErrCoords', 'NO');
fprintf(fid,'%s\t%s\n', 'PrAdjParms', 'YES');
fprintf(fid,'%s\t%s\n', 'PrAdjObsv', 'NO');
fprintf(fid,'%s\t%s\n', 'PrTestObsv', 'NO');
fprintf(fid,'%s\t%s\n', 'PrErrObsv', 'NO');
fprintf(fid,'%s\t%s\n', 'LogFile', 'ASCII');
fprintf(fid,'%s\n', '$');
fclose(fid);

cd(workDir);
%                 dx = 2*tan(1)*(m(1)/focal)*(m(2)/focal) + ...
%                     tan(2)*(r2 + 2*((m(1)/focal)).^2);
%                 xD = (1+ rad(1)*r2 + rad(2)*r2^2 + rad(3)*r2^4).* (m(1)/focal) + dx;
%                 dy = 2*tan(2)*(m(1)/focal)*(m(2)/focal) + ...
%                     tan(1)*(r2 + 2*((m(2)/focal)).^2);
%                 yD = (1+ rad(1)*r2 + rad(2)*r2^2 + rad(3)*r2^4).*
%                 (m(2)/focal) + dy;

%                 %% Construct Hessian in different scales and find maximum 
%                 octaves = 12;
%                 intervals = 2;
%                 sigma = 2;
%                 
%                 % construct second moment matrix
%                 Gauss = {}; 
%                 DoG = zeros(size(Isift,1), size(Isift,2), octaves-1);
%                 sample = 7.0/2.0;
%                 for k = 1:octaves
%                     % Determine the number of filter taps.
%                     n = 2*round(sample * sigma)+1;
%                     % Generate the x values.
%                     x=1:n;
%                     x=x-ceil(n/2);
%                     % Sample the gaussian function to generate the filter taps.
%                     gaussianFilter = exp(-(x.^2)/(2*sigma^2))/(sigma*sqrt(2*pi));
%                     % Build gaussian pyramid
%                     Gauss{k} = conv2(gaussianFilter, gaussianFilter, Isift);
%                     sizeDiff = (size(Gauss{k})-size(Isift))/2;
%                     Gauss{k} = Gauss{k}(1+sizeDiff:end-sizeDiff,1+sizeDiff:end-sizeDiff);
%                     sigma = sigma+1;
%                     if k ~= 1
%                         DoG(:,:,k-1) = Gauss{k} - Gauss{k-1};
%                     end
%                 end
%                 
%                 DoGrow = reshape(shiftdim(DoG,2), octaves-1,[]);
%                 [blobMax blobIdx] = max(DoGrow,[],1);
%                 blobScale = reshape(blobIdx, size(I,1), size(I,2));
% %                 [feature, discripter] = vl_sift(I);
% %                 % 1st $ 2nd entry x,y in image
% %                 % 3rd scale
% %                 % 4th orientation in radians

%             % 2nd derivative kernels 
%             xx = [ 1 -2  1 ];
%             yy = xx';
%             xy = [ 1 0 -1; 0 0 0; -1 0 1 ]/4;


%             % Compute the entries of the Hessian matrix at the extrema location.
%             Dxx = sum(DoG{blobScale(keyX,keyY)}(keyY,keyX-1:keyX+1,interval) .* xx);
%             Dyy = sum(DoG{blobScale(keyX,keyY)}(keyY-1:keyY+1,keyX,interval) .* yy);
%             Dxy = sum(sum(DoG{blobScale(keyX,keyY)}(keyY-1:keyY+1,keyX-1:keyX+1,interval) .* xy));
% 
%             % Compute the trace and the determinant of the Hessian.
%             Tr_H = Dxx + Dyy;
%             Det_H = Dxx*Dyy - Dxy^2;

            
            
%             % nearest SIFT keypoint
%             delta = feature(1:2,:)'-repmat([keyX keyY], length(feature),1);
%             [C,K] = min(sqrt((sum(delta.^2,2)).^0.5));
%             centroid = feature(:,K);
%             
%             if keyPoint(1) == 5007
%                 disp('5007 punt gevonden')
%             end

% fid = fopen('kiezegem100_internals.cam');
% 
% xCounter = 1;
% yCounter = 1;
% while ~feof(fid)
%     tline=fgetl(fid);
%     sline = regexp(tline, ' ', 'split');
%     switch sline{1}
%         case 'FOCAL'
%             focal = cellfun(@str2num, sline(end));
%         case 'XPOFF'
%             xCounter = xCounter + 1;
%             if xCounter == 2
%                 xPoff =  cellfun(@str2num, sline(end));
%             end
%         case 'YPOFF'
%             yCounter = yCounter + 1;
%             if yCounter == 2
%                 yPoff =  cellfun(@str2num, sline(end));
%             end
%         case 'SYM_DIST'
%             rad = cellfun(@str2num, sline(3:end));
%         case 'DEC_DIST'
%             tan = cellfun(@str2num, sline(2:end));
%     end
% end
% fclose(fid);