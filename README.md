AAAVision: Symposiun on industrial Applications of Advanced computer vision Algorithms
======================================================================================

All information related to the the AAA Vision Symposium 2014, which was organised at KULeuven, Technology Campus De Nayer on Thursday, the 18th of September. All this information was distributed using a symposium USB stick which could be retrieved in the goodie bag.

Important links:

* AAA Vision Symposium 2014: http://www.eavise.be/AAAVision/
* IWT-TETRA 3D4SURE project page: http://www.eavise.be/3D4SURE/
* IWT-TETRA TOBCAT project page: http://www.eavise.be/tobcat/
