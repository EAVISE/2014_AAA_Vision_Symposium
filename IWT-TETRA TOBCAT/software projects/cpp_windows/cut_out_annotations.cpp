/***************************************************************************************************
Copyright (c) 2013 EAVISE, KU Leuven, Campus De Nayer

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

	1. The above copyright notice and this permission notice shall be included in
	   all copies or substantial portions of the Software.
    2. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
       THE SOFTWARE.
	3. A notice shall be sent to - steven.puttemans[at]kuleuven.be - to inform the 
       software was used in further development.
*****************************************************************************************************

Cut out annotations from their larger images in order to make a positive set of data

*****************************************************************************************************/

#include "stdafx.h"
#include <windows.h>

#include <opencv/cv.h>
#include <opencv/cvaux.h>

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>
#include <stdio.h>
#include <sstream>
#include <fstream>

#include <dirent.h>
#include <ctime>

using namespace std;
using namespace cv;


int _tmain(int argc, _TCHAR* argv[])
{
	//-- 0. Check if arguments are given correct
	if( argc == 1 ){
		printf( "Usage of annotations segmentation: \n"  
				"--> cut_out_annotations.exe <annotationsfile.txt> <outputfolder> <boundingBoxCenter [true/false]> [show] \n");
		return 0;
	}
	
	
	// Load segmentation file with object annotations
	string fileString = argv[1];
	string outputFolder = argv[2];
		
	// Create output folder for segmented output objects
	// If the specified output folder doesn't exist yet, create it
	if (GetFileAttributes(outputFolder.c_str()) == INVALID_FILE_ATTRIBUTES) {
		CreateDirectory(outputFolder.c_str(),NULL);
	}

	// Open annotations file
	ifstream myFile; myFile.open(fileString);
	string readString;
	int image_counter = 0;

	while(!myFile.eof()){
		// Read the first line - define end of line by \n character
		getline(myFile,readString,'\n');
		if (readString == ""){ break; }
		
		// Split the string into parameters
		vector<string> data;
		string element = "";
		stringstream tempStream(readString);
		while(getline(tempStream,element,' ')){
			data.push_back (element);
		}

		// Read in the original image
		string location = data[0];
		Mat img = imread(location);
		Mat output_img;

		namedWindow("Segment", WINDOW_AUTOSIZE);

		// Retrieve rectangle bounding box parameters
		// Parameterposition ignores the location and amount of detections and is then used for looping over bounding boxes
		int parameterPosition = 2;
		int amountDetections = atoi(data[1].c_str());
		int x0 = 0; int y0 = 0; int width = 0; int height = 0; int centerX = 0; int centerY = 0;
		for(int count = 0; count < amountDetections; count ++){
			// Snip bounding box from original image
			x0 = atoi(data[parameterPosition].c_str());
			y0 = atoi(data[parameterPosition+1].c_str());
			width = atoi(data[parameterPosition+2].c_str());
			height = atoi(data[parameterPosition+3].c_str());

			// Create a new image based on this parameter
			output_img = img(Rect(x0,y0,width,height));

			// Save this image towards a solution folder
			stringstream ss;
			stringstream date;

			// Create a unique blueprint for the name!
			time_t t = time(0);   // get time now
			struct tm * now = localtime( & t );
			date << (now->tm_year + 1900) << (now->tm_mon + 1) << now->tm_mday << now->tm_hour << now->tm_min << now->tm_sec;

			ss << outputFolder.c_str() << "image_"<< date.str() << "_" << image_counter << "_sample_" << count << ".png";
			string out = ss.str();
			imwrite(out, output_img);

			if (argv[4]){
				string show = argv[4];
				if (!strcmp(show.c_str(), "show")){
					imshow("Segment",output_img);
					cvWaitKey(200);
				}	
			}
			
			// Add value to the parameterPosition for next retrieval of bounding box
			// If the annotations file doesn't include the centers, then a value of 4 has to be added
			string boundingBox = argv[3];
			if (!strcmp(boundingBox.c_str(), "true")){
				parameterPosition = parameterPosition + 6;
			}
			if (!strcmp(boundingBox.c_str(), "false")){
				parameterPosition = parameterPosition + 4;
			}							
		}

		// Add a value to the image counter
		image_counter = image_counter + 1;
	}


	return 0;
}

