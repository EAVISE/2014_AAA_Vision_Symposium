/***************************************************************************************************
Copyright (c) 2013 EAVISE, KU Leuven, Campus De Nayer

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

	1. The above copyright notice and this permission notice shall be included in
	   all copies or substantial portions of the Software.
    2. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
       THE SOFTWARE.
	3. A notice shall be sent to - steven.puttemans[at]kuleuven.be - to inform the 
       software was used in further development.
*****************************************************************************************************

Software for performing object detection based on a trained object model.
Detection based on a complete folder of images.

*****************************************************************************************************/

// Standard windows includes + input output file processing
#include "stdafx.h"
#include <windows.h>
#include <fstream>
#include <strsafe.h>

// OpenCV includes needed for correct functionality to work
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

// Including the used namespaces
using namespace std;
using namespace cv;

// Function headers
void detect_and_display( Mat frame, string window );

// Global variables
CascadeClassifier cascade;
string window_name = "Cascade classifier object detector";

int main( int argc, const char** argv )
{
	// Check if arguments are given correct
	if( argc == 1 || argc != 5){
		printf( "Usage of model detection software: \n"  
			"detect_objects_folder.exe <object_model.xml> <folder_location> <amount_input_images> <image extension - DEFAULT .jpg>\n");
		return 0;
	}

	// Software parameters
	string model_name = argv[1];
	string folder = argv[2];
	int number_images = atoi(argv[3]);
	string extension = argv[4];

	// Load the cascade model into the model container
	if( !cascade.load( model_name ) ){ 
		printf("Error loading the trained model from the provided model file!\n"); 
		return -1; 
	};

	// If input is video, a basic frame capture is applied, else we use the image filenames
	for(int i = 0; i < number_images; i++){
		// Read current test image
		stringstream location;
		location << folder << "\\" << i << extension;
		Mat image = imread(location.str());
		
		// Detect and display image
		string window = "50 km/u detection sign";
		detect_and_display( image, window );
		waitKey();
	}

	return 0;
}

// FUNCTION : Detects and displays an object detection on the current frame
// WITHOUT SAVE FUNCTIONALITY
void detect_and_display( Mat frame, string window )
{
	// Function specific parameters
	vector<Rect> objects;
	Mat frame_gray;

	// Convert the input frame to grayscale image and apply lightning normalization using histogram equilization
	cvtColor( frame, frame_gray, CV_BGR2GRAY );
	equalizeHist( frame_gray, frame_gray );

	// Detect object in a given image
	cascade.detectMultiScale( frame_gray, objects, 1.05, 1);

	// Visualize detections on the input frame and show in the given window
	for( int i = 0; i < objects.size(); i++ )
	{
		// Use a rectangle representation on the frame
		// Frame width 3 pixels in color red (BGR format)
		rectangle(frame, Point(objects[i].x, objects[i].y), Point(objects[i].x + objects[i].width, objects[i].y + objects[i].height), Scalar(0, 0, 255), 3);
	}
   
	// Show the result
	imshow( window, frame );
}

