/***************************************************************************************************
Copyright (c) 2013 EAVISE, KU Leuven, Campus De Nayer

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

	1. The above copyright notice and this permission notice shall be included in
	   all copies or substantial portions of the Software.
    2. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
       THE SOFTWARE.
	3. A notice shall be sent to - steven.puttemans[at]kuleuven.be - to inform the 
       software was used in further development.
*****************************************************************************************************

Compare two possible approach for detecting pedestrians that are not straight up positioned in the image
  	1. Rotate the image 1� each time and then perform a new detection over the frame, hope to find a result
    2. Make the image rotation invariant by performing the dominant orientation approach

Functionality for rotating an image stepwise will be provided also

Applied OpenCV technique - HOG+SVM

*****************************************************************************************************/

#include "stdafx.h"

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/gpu/gpu.hpp"

#include <iostream>
#include <fstream>
#include <time.h>

#ifdef HAVE_TBB
	#include "tbb/task_scheduler_init.h"
#endif

using namespace std;
using namespace cv;

struct outputGradient{
	Mat smoothedImage;
	Mat gradientX;
	Mat gradientY;
	Mat gradientTotal;
};

struct dominantGradient{
	float orientation;
	float magnitude;
};

struct detectMat{
	Mat outputImage;
	string debugInfo;
};

outputGradient calculateGradientImage(Mat inputImage, Size kernelSize, int sigma){
	// Create image of the same size as the inputImage for output and smoothing
	Mat grayImage, smoothedImage;
		
	// Some basic smoothing on the image to remove pixelation
	GaussianBlur( inputImage, smoothedImage, kernelSize, sigma);

	// Convert RBG towards grayscale image
	cvtColor( smoothedImage, grayImage, CV_RGB2GRAY );

	// Calculate gradient image
	Mat grad_X = Mat(inputImage.rows, inputImage.cols, CV_32F);
	Mat grad_Y = Mat(inputImage.rows, inputImage.cols, CV_32F);

	// Sobel(src, dst, ddepth, dx, dy, ksize, scale, delta, BORDER_DEFAULT)
	// Gradient for the X direction
	Sobel( grayImage, grad_X, CV_32F, 1, 0, 3, 1, 0, BORDER_DEFAULT );

	/// Gradient for the Y direction
	Sobel( grayImage, grad_Y, CV_32F, 0, 1, 3, 1, 0, BORDER_DEFAULT );

	// Calculate magnitudes
	Mat magnitudes = Mat(grayImage.rows, grayImage.cols, CV_64F);
	Mat prodX = Mat(grayImage.rows, grayImage.cols, CV_64F);
	Mat prodY = Mat(grayImage.rows, grayImage.cols, CV_64F);
	Mat sum = Mat(grayImage.rows, grayImage.cols, CV_64F);
	Mat gradient = Mat(grayImage.rows, grayImage.cols, CV_64F);
	
	// Combine both gradient functions together
	// total = sqrt(dx� + dy�) <-- more correct than just adding both values weighted together
	prodX = grad_X.mul(grad_X);
	prodY = grad_Y.mul(grad_Y);
	sum = prodX + prodY;
	sqrt(sum, gradient);
		
	// Create an output structure
	outputGradient result;
	result.smoothedImage = smoothedImage;
	result.gradientX = grad_X;
	result.gradientY = grad_Y;
	result.gradientTotal = gradient;

	// Push back the converted image
	return result;
};

Mat calculateOrientations(Mat gradientX, Mat gradientY){
	// Create container element
	Mat orientation = Mat(gradientX.rows, gradientX.cols, CV_32F);

	// Calculate orientations of gradients --> in degrees
	// Loop over all matrix values and calculate the accompagnied orientation
	for(int i = 0; i < gradientX.rows; i++){
		for(int j = 0; j < gradientX.cols; j++){
			// Retrieve a single value
			float valueX = gradientX.at<float>(i,j);
			float valueY = gradientY.at<float>(i,j);
			// Calculate the corresponding single direction, done by applying the arctangens function
			float result = fastAtan2(valueX,valueY);
			// Store in orientation matrix element
			orientation.at<float>(i,j) = result;
		}
	}
	
	return orientation;
}

dominantGradient calculateDominantGradient(Mat orientations, Mat magnitudes, int stepDegrees){
	// Perform some postprocessing on the orientations, these now loop from 0 - 360�
	// Create bin information - add + 1 to cover for the case that division is not clear
	int amountOfBins = 360 / stepDegrees + 1;
	
	// Create the assignment of bins to each element
	Mat assignedBin, assignedBinInt;
	divide(orientations,stepDegrees,assignedBin);

	// Switch the float value towards int values, in order to be sure that a single bin is possible
	assignedBin.convertTo(assignedBinInt,CV_16U);

	// Create storage for storing the summed magnitudes in each orientation bin	
	vector<float> valuesSummed(amountOfBins,0), summedFirstHalf(amountOfBins/2,0), summedSecondHalf(amountOfBins/2,0), summed(amountOfBins/2,0);

	// Loop over all orientation elements
	for(int i = 0; i < magnitudes.rows; i++){
		for(int j = 0; j < magnitudes.cols; j++){
			short valueBin = assignedBinInt.at<short>(i,j);
			float valueMagnitude = magnitudes.at<float>(i,j);
			valuesSummed[valueBin] = valuesSummed[valueBin] + valueMagnitude;
		}
	}

	// Return to 0 - 180 degrees in order for double edges not to counter eachother, but rather fortify eachother
	int size = amountOfBins/2;
	copy(valuesSummed.begin(),valuesSummed.begin() + size, summedFirstHalf.begin());
	copy(valuesSummed.begin() + size + 1,valuesSummed.end(), summedSecondHalf.begin());
	
	for(int i = 0; i < summedFirstHalf.size(); i++){
		summed[i] = summedFirstHalf[i] + summedSecondHalf[i];
	}
	
	// Retrieve the maximum value index, in order to define the dominant gradient
	float valueMaximum = 0;
	int indexMaximum = 0;
	for(int i = 0; i < summed.size(); i++){
		if(summed[i] > valueMaximum){
			valueMaximum = summed[i];
			indexMaximum = i;
		}
	}

	// Calculate the dominant orientation angle
	float angleDominantGradient = (indexMaximum * stepDegrees) + ((float)stepDegrees / 2);

	// Prepare the output
	dominantGradient result;
	result.magnitude = valueMaximum;
	result.orientation = angleDominantGradient;

	return result;
}

Mat rotateImageStep(Mat source, double angle){
	double margin = 1.3;
	
	// Choose destination center point
	double dimension;
	if(source.rows > source.cols){
		dimension = source.rows;
	}
	else{
		dimension = source.cols;
	}
	double newCenterX = (dimension*margin)/2.0F;
	double newCenterY = (dimension*margin)/2.0F;
	Point2f dst_center(newCenterX, newCenterY);
	
	// Define three points for calculating the treanslation
	// Use upper left, upper right and center
	Point2f srcTri[3];
	Point2f dstTri[3];

	srcTri[0] = Point2f( 0, 0 );
	srcTri[1] = Point2f( source.cols, 0 );
	srcTri[2] = Point2f( 0, source.rows );

	dstTri[0] = Point2f( newCenterX - (source.cols/2.0F), newCenterY - (source.rows/2.0F) );
	dstTri[1] = Point2f( newCenterX + (source.cols/2.0F), newCenterY - (source.rows/2.0F) );
	dstTri[2] = Point2f( newCenterX - (source.cols/2.0F), newCenterY + (source.rows/2.0F) );

	Mat trs_mat = getAffineTransform( srcTri, dstTri );

	// Choose rotation center point
	Point2f trs_center(newCenterX, newCenterY);
    // Define affine rotation and translation matrix
    Mat rot_mat = getRotationMatrix2D(trs_center, angle, 1.0);

	// Define output, rotate image and return
	Mat temp, dst;
	warpAffine(source, temp, trs_mat, Size(dimension*margin, dimension*margin));
	warpAffine(temp, dst, rot_mat, temp.size());

    return dst;
}

detectMat detectRotatedCPU(Mat inputImage, double angle){
	
	// Initiate HOG descriptor approach
	HOGDescriptor hog;
    hog.setSVMDetector(HOGDescriptor::getDefaultPeopleDetector());

	// Perform detections
	vector<Rect> found;
	hog.detectMultiScale(inputImage, found, 0, Size(8,8), Size(32,32), 1.1, 2);

	// Create output
	Mat outputImage = inputImage;
	for(int i = 0; i < found.size(); i++ )
	{
		Rect r = found[i];
      	// the HOG detector returns slightly larger rectangles than the real objects.
		// so we slightly shrink the rectangles to get a nicer output.
		r.x += cvRound(r.width*0.1);
		r.width = cvRound(r.width*0.8);
		r.y += cvRound(r.height*0.07);
		r.height = cvRound(r.height*0.8);
		
		// draw rectangle that is detected using tl as top left corner and br as bottom right corner
		// push the rectangle on top of the original image
		rectangle(outputImage, r.tl(), r.br(), cv::Scalar(255,0,0), 1);
	}

	detectMat result;
	result.debugInfo = "";
	result.outputImage = outputImage;

	return result;
}


detectMat detectRotatedGPU(Mat inputImage, double angle){
	
	// Initiate HOG descriptor approach
	gpu::HOGDescriptor hogGPU;
    hogGPU.setSVMDetector(gpu::HOGDescriptor::getDefaultPeopleDetector());

	// Perform detections
	vector<Rect> found;
	// vector<gpu::HOGConfidence> confidenceScores;

	gpu::GpuMat imgGpu;
	imgGpu.upload(inputImage);

	hogGPU.detectMultiScale(imgGpu, found, 0, Size(8,8), Size(0,0), 1.1);
	// hogGPU.computeConfidenceMultiScale(imgGpu, found, 0, Size(0,0), Size(0,0), confidenceScores,2);

	// Create output
	Mat outputImage = inputImage;
	for(int i = 0; i < found.size(); i++ )
	{
		Rect r = found[i];
		// the HOG detector returns slightly larger rectangles than the real objects.
		// so we slightly shrink the rectangles to get a nicer output.
		r.x += cvRound(r.width*0.1);
		r.width = cvRound(r.width*0.8);
		r.y += cvRound(r.height*0.07);
		r.height = cvRound(r.height*0.8);
		
		// draw rectangle that is detected using tl as top left corner and br as bottom right corner
		// push the rectangle on top of the original image
		rectangle(outputImage, r.tl(), r.br(), cv::Scalar(255,0,0), 1);
	}

	detectMat result;
	result.debugInfo = "";
	result.outputImage = outputImage;

	return result;
}

int _tmain(int argc, _TCHAR* argv[])
{
	// Read in original image
	// Convert it immediatly towards a greyscale 1 channel CV_8UC1 image since the algorithms require this
	Mat imgIN = imread("D:\\data\\reference_image.png", CV_8UC1);
	Mat imgINoriginal = imread("D:\\data\\reference_image.png");
	
	// ---------------------------------------------------------------------------------------------------------------------------
	// STEP 0 : General parameters in order to have a correct comparison
	// ---------------------------------------------------------------------------------------------------------------------------
	// No visualizations will be done, since these require processing time. The previous projects visualize what is done.
	// Here we focus on getting exact performance times.

	// Define angle step size
	int stepAngle = 1;
	int amountOfRotations = (360 / stepAngle);

	// ---------------------------------------------------------------------------------------------------------------------------
	// STEP 1A : Perform detection of angle and rotation - rotate single time - perform detection - CPU
	// ---------------------------------------------------------------------------------------------------------------------------
	printf("-----------------------------------------------------\n");
	printf("Single orientation - rectification - CPU\n");
	printf("-----------------------------------------------------\n");
	
	double time1 = (double)getTickCount();

	// Calculate the gradient image using Sobel operators
	outputGradient result = calculateGradientImage(imgINoriginal,Size(11,11),5);
	
	// Apply calculation of the dominant gradient and angle to rotate
	Mat orientation = calculateOrientations(result.gradientX, result.gradientY);
	Mat magnitude = result.gradientTotal;
	dominantGradient result2 = calculateDominantGradient(orientation, magnitude, 5);

	// Rotate input image into
	double angleRotated = 90.00 - (double)result2.orientation;
	Mat imgRotated = rotateImageStep(imgIN, angleRotated);

	// Switch towards a format suitable for detection and perform detection
	Mat imgDetect;
	detectMat imgRotatedDetected1 = detectRotatedCPU(imgRotated, angleRotated);

	time1 = (double)getTickCount() - time1;
	printf("Total CPU processing time for a rectification + rotation + detection = %0.2f seconds  \n", time1/cv::getTickFrequency());

	// ---------------------------------------------------------------------------------------------------------------------------
	// STEP 1B : Perform detection of angle and rotation - rotate single time - perform detection - GPU
	// ---------------------------------------------------------------------------------------------------------------------------
	
	printf("-----------------------------------------------------\n");
	printf("Single orientation - rectification - GPU\n");
	printf("-----------------------------------------------------\n");
	
	double time1b = (double)getTickCount();

	// Calculate the gradient image using Sobel operators
	outputGradient resultB = calculateGradientImage(imgINoriginal,Size(11,11),5);
	
	// Apply calculation of the dominant gradient and angle to rotate
	Mat orientationB = calculateOrientations(resultB.gradientX, resultB.gradientY);
	Mat magnitudeB = resultB.gradientTotal;
	dominantGradient result2B = calculateDominantGradient(orientationB, magnitudeB, 5);

	// Rotate input image into
	double angleRotatedB = 90.00 - (double)result2.orientation;
	Mat imgRotatedB = rotateImageStep(imgINoriginal, angleRotatedB);

	// Switch towards a format suitable for detection and perform detection
	Mat imgDetectB;
	cvtColor( imgRotatedB, imgDetectB, CV_8UC1 );
	detectMat imgRotatedDetected1B = detectRotatedGPU(imgDetectB, angleRotatedB);

	time1b = (double)getTickCount() - time1b;
	printf("Total CPU processing time for a rectification + rotation + detection = %0.2f seconds  \n", time1b/cv::getTickFrequency());

	// ---------------------------------------------------------------------------------------------------------------------------
	// STEP 2 : Rotate image through all ori�ntations - perform detection - CPU
	// ---------------------------------------------------------------------------------------------------------------------------
	printf("-----------------------------------------------------\n");
	printf("All orientations - CPU\n");
	printf("-----------------------------------------------------\n");
	double time2 = (double)getTickCount();
	
	// Rotate image for each ori�ntation, step by step, able to perform detection on each image
	detectMat imgRotatedDetected2;
	for(int i = 1; i <= amountOfRotations; i++){
		double angleRotated = stepAngle*i;
		Mat imgRotated = rotateImageStep(imgIN, angleRotated);
		imgRotatedDetected2 = detectRotatedCPU(imgRotated, angleRotated);
	}

	time2 = (double)getTickCount() - time2;
	printf("Total CPU processing time for 360 degrees rotations + detections = %0.2f seconds \n", time2/cv::getTickFrequency());

	// ---------------------------------------------------------------------------------------------------------------------------
	// STEP 3 : Rotate image through all ori�ntations - perform detection - GPU
	// ---------------------------------------------------------------------------------------------------------------------------
	printf("-----------------------------------------------------\n");
	printf("All orientations - GPU\n");
	printf("-----------------------------------------------------\n");
	
	double time3 = (double)getTickCount();

	// Rotate image for each ori�ntation, step by step, able to perform detection on each image
	detectMat imgRotatedDetected3;
	for(int i = 1; i <= amountOfRotations; i++){
		double angleRotated = stepAngle*i;
		Mat imgRotated = rotateImageStep(imgIN, angleRotated);
		imgRotatedDetected3 = detectRotatedGPU(imgRotated, angleRotated);
	}

	time3 = (double)getTickCount() - time3;
	printf("Total GPU processing time for 360 degrees rotations + detections = %0.2f seconds \n", time3/cv::getTickFrequency());

	// Visualize the 4 latest detections
	imshow("STEP 1 A", imgRotatedDetected1.outputImage);
	imshow("STEP 1 B", imgRotatedDetected1B.outputImage);
	imshow("STEP 2", imgRotatedDetected2.outputImage);
	imshow("STEP 3", imgRotatedDetected3.outputImage);
	
	// Wait for a keystroke after last image
	cvWaitKey(0);
}

