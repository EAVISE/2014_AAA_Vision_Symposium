/***************************************************************************************************
Copyright (c) 2013 EAVISE, KU Leuven, Campus De Nayer

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

*****************************************************************************************************

This project is part of the channel features approach implemented by Doll�r.
This part implements the different filters that are used for processing images using OpenCV
Addition of some extra filters compared to filters.cpp

*****************************************************************************************************/

#include "stdafx.h"

#include "opencv2/core/core.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>
#include <stdio.h>
#include <sstream>
#include <math.h>
#include <time.h>

using namespace std;
using namespace cv;

// Structure defined for output of channel calculation
struct color_data{
	Mat grayscale;
	Mat hsv;
	Mat luv;
	vector<Mat> img_channels;
	vector<Mat> hsv_channels;
	vector<Mat> luv_channels;
};

struct channel_data{
	color_data color_channels;
	vector<Mat> gradient_magnitude;
	vector<Mat> gradient_magnitude_channels;
	vector<Mat> hog_grayscale;
};

// Print a given matrix to look for errors
void print_matrix(Mat input, int decimals){
	for(int i = 0; i < input.rows; i++){
		for(int j = 0; j < input.cols; j++){
			stringstream ss;
			ss << "%." << decimals << "lf";
			printf(ss.str().c_str(), input.at<double>(i,j));
			cout << " ";
		}
		cout << endl;
	}
}

// For debug purposes : visualize the output of a double matrix
void visualize_double_image(Mat input, string name){
	input.convertTo(input, CV_32F);
	normalize(input, input, 1, 0, CV_MINMAX);
	imshow(name, input); cvWaitKey(20);
}

// Find the index of the maximum of a set of data points delivered as a vector
int find_index_max(vector<double> data){
	int index = 0;
	double max = 0.0;
	
	for (int i = 0; i < data.size(); i++){
		if (data[i] > max){
			index = i;
			max = data[i];
		}
	}

	return index;
}

// Basic function to calculate gradient magnitude and angle matrix based on input
vector<Mat> calculate_gradient(Mat input){
	Mat img_smooth;
	GaussianBlur( input, img_smooth, Size(11,11), 5);

	Mat grad_x = Mat(img_smooth.rows, img_smooth.cols, CV_64F);
	Mat grad_y = Mat(img_smooth.rows, img_smooth.cols, CV_64F);

	Sobel( img_smooth, grad_x, CV_64F, 1, 0, 3, 1, 0, BORDER_DEFAULT );
	Sobel( img_smooth, grad_y, CV_64F, 0, 1, 3, 1, 0, BORDER_DEFAULT );

	Mat magnitude = Mat(img_smooth.rows, img_smooth.cols, CV_64F);
	sqrt(grad_x.mul(grad_x) + grad_y.mul(grad_y), magnitude);

	Mat orientations = Mat(img_smooth.rows, img_smooth.cols, CV_64F);

	for(int i = 0; i < img_smooth.rows; i++){
		for(int j = 0; j < img_smooth.cols; j++){
			orientations.at<double>(i,j) = fastAtan2(grad_x.at<double>(i,j), grad_y.at<double>(i,j));
		}
	}

	vector<Mat> output;
	output.push_back(magnitude);
	output.push_back(orientations);

	return output;
}

// Create all possible color adaptations of the image
// Parameters given determine what is whanted
// Parameters vector<string>
color_data calculate_color_channels(Mat input, vector<string> parameters){
	// Convert the original to CV_64F type to get a desired result for each transform
	input.convertTo(input, CV_64F);
	
	// Create output parameter
	color_data result;

	// Operation 1 : grayscale conversion + normalization
	Mat grayscale; cvtColor(input, grayscale, CV_BGR2GRAY);
	normalize(grayscale, grayscale, 1, 0, CV_MINMAX); 
	result.grayscale = grayscale;
    
	// Operation 2 : split into B, G & R channels
	vector<Mat> img_channels;
	split(input, img_channels);
	normalize(img_channels[0], img_channels[0], 1, 0, CV_MINMAX);
	normalize(img_channels[1], img_channels[1], 1, 0, CV_MINMAX);
	normalize(img_channels[2], img_channels[2], 1, 0, CV_MINMAX); 
	result.img_channels = img_channels;

	// Operation 3 : convert to HSV space
	Mat hsv;
	vector<Mat> hsv_channels;
	cvtColor(input, hsv, CV_BGR2HSV);
	split(hsv, hsv_channels);
	normalize(hsv, hsv, 1, 0, CV_MINMAX); 
	normalize(hsv_channels[0], hsv_channels[0], 1, 0, CV_MINMAX);
	normalize(hsv_channels[1], hsv_channels[1], 1, 0, CV_MINMAX);
	normalize(hsv_channels[2], hsv_channels[2], 1, 0, CV_MINMAX); 
	result.hsv = hsv;
	result.hsv_channels  = hsv_channels;

	// Operation 4 : convert to LUV space
	Mat luv;
	vector<Mat> luv_channels;
	cvtColor(input, luv, CV_BGR2Luv);
	split(luv, luv_channels);
	normalize(luv, luv, 1, 0, CV_MINMAX); 
	normalize(luv_channels[0], luv_channels[0], 1, 0, CV_MINMAX);
	normalize(luv_channels[1], luv_channels[1], 1, 0, CV_MINMAX);
	normalize(luv_channels[2], luv_channels[2], 1, 0, CV_MINMAX); 
	result.luv = luv;
	result.luv_channels  = luv_channels;

	return result;
}

// Calculate the original channels for a single scale
channel_data calculate_channels(Mat input, int bin_size, int number_of_orientations){
	channel_data output;
	Mat img_grayscale;
	vector<Mat> img_channels;

	/*
	Given an input image I, a corresponding channel is a registered map of I,
	where the output pixels are computed from corresponding patches of input
	pixels (thus preserving overall image layout).

	Channels that were chosen are the original Piotr Doll�r channels which seems usefull for person detection
	However, any channel could be added in this function.
	*/
	
	// -----------------------------------------------------------------------------------------------------------
	// Channel 1 & 2 combined: color range operations
	vector<string> parameters;
	color_data color_output = calculate_color_channels(input, parameters);
	
	// -----------------------------------------------------------------------------------------------------------
	// Channel 3 : gradient magnitude channel
	// Keeps gradient magnitude and direction at each location, maximum of three color channels
	// First apply for grayscale image
	vector<Mat> img_gradients_grayscale = calculate_gradient(img_grayscale);
	
	// Secondly apply for color channels
	// Each channel could be handled as a grayscale value
	vector<Mat> img_gradients_blue = calculate_gradient(img_channels[0]);
	vector<Mat> img_gradients_green = calculate_gradient(img_channels[1]);
	vector<Mat> img_gradients_red = calculate_gradient(img_channels[2]);
	
	// Choose the maximum value for the magnitude over the 3 color channels and take accompagnied angle
	vector<Mat> img_gradients_color;
	Mat gradients_color = Mat(input.rows, input.cols, CV_64F);
	Mat angles_color = Mat(input.rows, input.cols, CV_64F);
	for(int i = 0; i < input.rows; i++){
		for(int j = 0; j < input.cols; j++){
			double mag_blue = img_gradients_blue[0].at<double>(i,j);
			double mag_green = img_gradients_green[0].at<double>(i,j);
			double mag_red = img_gradients_red[0].at<double>(i,j);
			
			vector<double> mag_values; mag_values.push_back(mag_blue); mag_values.push_back(mag_green); mag_values.push_back(mag_red);
			int index = find_index_max(mag_values);

			if (index == 0){
				gradients_color.at<double>(i,j) = img_gradients_blue[0].at<double>(i,j);
				angles_color.at<double>(i,j) = img_gradients_blue[1].at<double>(i,j);
			}
			if (index == 1){
				gradients_color.at<double>(i,j) = img_gradients_green[0].at<double>(i,j);
				angles_color.at<double>(i,j) =	img_gradients_green[1].at<double>(i,j);
			}
			if (index == 2){
				gradients_color.at<double>(i,j) = img_gradients_red[0].at<double>(i,j);
				angles_color.at<double>(i,j) = img_gradients_red[1].at<double>(i,j);
			}
		}
	}
	img_gradients_color.push_back(gradients_color);
	img_gradients_color.push_back(angles_color);

	// -----------------------------------------------------------------------------------------------------------
	// Channel 4 : quantized gradient channels - gradient oriented histograms - HOG

	int degrees_per_bin = 360 / number_of_orientations;
	
	// First apply for grayscale image
	Mat magnitude_grayscale = img_gradients_grayscale[0];
	Mat angle_grayscale = img_gradients_grayscale[1];

	int bins_horizontal = magnitude_grayscale.rows / bin_size;
	int bins_vertical = magnitude_grayscale.cols / bin_size;

	// Create quantization into bins for each position
	// Preallocation of memory
	vector<Mat> magnitudes_quantized, magnitudes_hog;
	for (int i = 0; i < number_of_orientations; i++){
		Mat temp = Mat::zeros(magnitude_grayscale.rows, magnitude_grayscale.cols, CV_64F);
		Mat temp2 = Mat::zeros(bins_horizontal, bins_vertical, CV_64F);
		magnitudes_quantized.push_back(temp);
		magnitudes_hog.push_back(temp2);
	}

	// Loop over each orientation, decide which bin to take and copy the magnitude value to it
	for (int i = 0; i < magnitude_grayscale.rows; i ++){
		for (int j = 0; j < magnitude_grayscale.cols; j ++){
			// Define which bin to use for the current pixel
			int orientation = (int)angle_grayscale.at<double>(i,j);
			int bin = 0;
			double modulo_bin = 0.0;
			double half_step = degrees_per_bin / 2.0;
			// If the case of 360� exist, than just take bin 0.
			// For the others, just do integer division by the amount of degrees of each bin
			if( orientation == 360 ){
				bin = 0;
			}else{
				bin = orientation / degrees_per_bin;
				modulo_bin = orientation % degrees_per_bin;
			}
			
			// Add magnitude value to the two closest bins (Piotr Doll�r approach)
			// First add the value to the correct bin, by adding it to the bin, not replacing the content
			magnitudes_quantized[bin].at<double>(i,j) = magnitudes_quantized[bin].at<double>(i,j) + magnitude_grayscale.at<double>(i,j);
			
			// Secondly add to the second closest angle bin
			// Maximum index is amount of bins - 1 due to starting at position 0
			if (bin == (magnitudes_quantized.size() - 1) ){
				// If at last bin, take previous bin or starting bin to add second closest angle
				if (modulo_bin <= half_step){
					magnitudes_quantized[bin - 1].at<double>(i,j) = magnitudes_quantized[bin - 1].at<double>(i,j) + magnitude_grayscale.at<double>(i,j);
				}else{
					magnitudes_quantized[0].at<double>(i,j) = magnitudes_quantized[0].at<double>(i,j) + magnitude_grayscale.at<double>(i,j);
				}
			}else if(bin == 0){
				// If at first bin, take last bin or next bin to add second closest angle
				if (modulo_bin <= half_step){
					magnitudes_quantized[magnitudes_quantized.size() - 1].at<double>(i,j) = magnitudes_quantized[magnitudes_quantized.size() - 1].at<double>(i,j) + magnitude_grayscale.at<double>(i,j);
				}else{
					magnitudes_quantized[bin + 1].at<double>(i,j) = magnitudes_quantized[bin + 1].at<double>(i,j) + magnitude_grayscale.at<double>(i,j);
				}
			}else{
				// In all other cases, just look at the modulo value and decide
				if (modulo_bin <= half_step){
					magnitudes_quantized[bin - 1].at<double>(i,j) = magnitudes_quantized[bin - 1].at<double>(i,j) + magnitude_grayscale.at<double>(i,j);
				}else{
					magnitudes_quantized[bin + 1].at<double>(i,j) = magnitudes_quantized[bin + 1].at<double>(i,j) + magnitude_grayscale.at<double>(i,j);
				}
			}
		}
	}

	// Create HOG representation of the vectorized histograms
	// Using bin_size x bin_size windows, loosing tiny bit of information on the right & bottom edge, but this can be ignored for now
	for (int layer = 0; layer < magnitudes_quantized.size(); layer++){
		// Go through given layer, and loop through bins, current bin given by (i, j)
		for (int i = 0; i < bins_horizontal; i = i++){
			for(int j = 0; j < bins_vertical; j = j++){
				Rect roi(j * bin_size, i * bin_size, bin_size, bin_size);
				Mat bin = magnitudes_quantized[layer]( roi );
				Scalar temp = sum(bin);
				magnitudes_hog[layer].at<double>(i,j) = temp[0];
			}
		}
	}

	// For output, it would be nice if data could actually be formatted towards the [0-1] data region, in order to have an easy approach
	// This is better for visualization purposes, but then you should remember to multiply with 360 to get actual degrees
	normalize(img_gradients_grayscale[0], img_gradients_grayscale[0], 1, 0, CV_MINMAX);
	normalize(img_gradients_grayscale[1], img_gradients_grayscale[1], 1, 0, CV_MINMAX);

	normalize(img_gradients_color[0], img_gradients_color[0], 1, 0, CV_MINMAX);
	normalize(img_gradients_color[1], img_gradients_color[1], 1, 0, CV_MINMAX);

	// Normalization of the HOG data should be done a bit smarter. Actually the data that you want to spread over the [0-1] range are
	// the magnitudes of all angles for a single square bin region. So we need to reformat our data to get correct normalization.
	// Create a bin looper, and for each bin normalize the magnitudes
	for(int i = 0; i < magnitudes_hog[0].rows; i++){
		for(int j = 0; j < magnitudes_hog[1].cols; j++){
			// For each point create a third dimension matrix for normalizing
			Mat temp_norm = Mat(1, magnitudes_hog.size(), CV_64F);
			// Add the elements to the matrix
			for(int layer = 0; layer < magnitudes_hog.size(); layer ++){
				temp_norm.at<double>(0,layer) = magnitudes_hog[layer].at<double>(i,j);
			}
			// Perform normalization
			normalize(temp_norm, temp_norm, 1, 0, CV_MINMAX);
			// Add them back to the original data container
			for(int layer = 0; layer < magnitudes_hog.size(); layer ++){
				magnitudes_hog[layer].at<double>(i,j) = temp_norm.at<double>(0,layer);
			}
		}
	}
	
	// -----------------------------------------------------------------------------------------------------------
	// Creation of output object
		
	output.color_channels = color_output;
	output.gradient_magnitude = img_gradients_grayscale;
	output.gradient_magnitude_channels = img_gradients_color;
	output.hog_grayscale = magnitudes_hog;

	return output;
}

void visualize_channels(Mat original, channel_data input){
	// Combine the actual channels into a single output image for visualization purposes
	Mat image_grayscale = input.color_channels.grayscale; image_grayscale.convertTo(image_grayscale, CV_32F); image_grayscale = image_grayscale / 255;
	Mat image_blue = input.color_channels.img_channels[0]; image_blue.convertTo(image_blue, CV_32F); image_blue = image_blue / 255;
	Mat image_green = input.color_channels.img_channels[1]; image_green.convertTo(image_green, CV_32F); image_green = image_green / 255;
	Mat image_red = input.color_channels.img_channels[2]; image_red.convertTo(image_red, CV_32F); image_red = image_red / 255;
	
	Mat image_grad_magni = input.gradient_magnitude[0];
	Mat image_grad_angle = input.gradient_magnitude[1];
	Mat image_grad_color_magni = input.gradient_magnitude_channels[0];
	Mat image_grad_color_angle = input.gradient_magnitude_channels[1];

	// Create a container matrix that can keep all images
	// From the 8 input images, we want a 2 x 4 image result
	// Using a float image, which means all values need to be between [0 - 1]
	int buffer = 10;
	Mat output = Mat(original.rows * 2 + buffer, original.cols * 4 + 3 * buffer, CV_32F);
	
	// Set the original image in seperate window
	imshow("Original Image", original);

	// Copy all data to correct location
	image_grayscale.copyTo( output( Rect(0, 0, original.cols, original.rows) ) );
	image_blue.copyTo( output( Rect(original.cols + buffer, 0, original.cols, original.rows) ) );
	image_green.copyTo( output( Rect(original.cols * 2 + buffer * 2, 0, original.cols, original.rows) ) );
	image_red.copyTo( output( Rect(original.cols * 3 + buffer * 3, 0, original.cols, original.rows) ) );

	image_grad_magni.convertTo(image_grad_magni, CV_32F);
	image_grad_magni.copyTo( output( Rect(0, original.rows + buffer, original.cols, original.rows) ) );

	image_grad_angle.convertTo(image_grad_angle, CV_32F);
	image_grad_angle.copyTo( output( Rect(original.cols + buffer, original.rows + buffer, original.cols, original.rows) ) );

	image_grad_color_magni.convertTo(image_grad_color_magni, CV_32F);
	image_grad_color_magni.copyTo( output( Rect(original.cols * 2 + 2 * buffer, original.rows + buffer, original.cols, original.rows) ) );

	image_grad_color_angle.convertTo(image_grad_color_angle, CV_32F);
	image_grad_color_angle.copyTo( output( Rect(original.cols * 3 + 3 * buffer, original.rows + buffer, original.cols, original.rows) ) );

	// Visualize the output image into a window
	imshow("Result channels", output);
}

// Based on radial co�rdinates (angle and magnitude) calculate corresponding carthesian co�rdinates (x,y)
// Specific for OpenCV co�rdinate system
vector<Point> radial_to_carthesian(Point start, double angle, double magnitude){
	const double PI = 3.141592;
	
	// Since sin and cos functions already return values between [-1,1] we do not need to calculate signs for quadrants
	// However, this corner is still given a standard 
	double angle_rad = angle * PI / 180;
	double x_temp = cos(angle_rad) * magnitude;
	double y_temp = sin(angle_rad) * magnitude;
	double x_2 = start.x + x_temp;
	double y_2 = start.y + y_temp;

	// Create points
	vector<Point> result;
	result.push_back(start);
	result.push_back(Point(x_2, y_2));

	return result;
}

void visualize_gradients(vector<Mat> gradients, Mat input, int step, int magnitude){
	Mat result = Mat(input.rows, input.cols, input.type());
	input.copyTo(result);
	for(int i = 3; i < input.rows; i = i + step){
		for(int j = 3; j < input.cols; j = j + step){
			// the points (i,j) now loop through the image with points to draw
			// check in which quadrant the angle lies and then compute the correct x and y length
			// Since data is now provided as [0-1] ranges, we need to multiply with 360 to get the actual angle
			double angle = gradients[1].at<double>(i,j) * 360;
			vector<Point> line_positions = radial_to_carthesian(Point(j,i), angle, magnitude);
			line(result, line_positions[0], line_positions[1], Scalar(255,0,0), 1);
		}
	}
	imshow("Gradients visualized", result);
}

// Function that allows the drawing of HOG features in OpenCV
void visualize_HOG(Mat original_grey, vector<Mat> hog_input, int max_size){
	Mat result = Mat(original_grey.rows, original_grey.cols, original_grey.type());
	Mat result_clean = Mat::zeros(original_grey.rows, original_grey.cols, original_grey.type());
	
	original_grey.copyTo(result);
	
	// Read out the amount of orientations that exist
	int bin_size = original_grey.rows / hog_input[0].rows;
	
	// Angle step between bins
	int step_angle = 360 / hog_input.size();

	// Visualizing the HOG features	
	for(int layer = 0; layer < hog_input.size(); layer++){
		for(int i = 0; i < hog_input[layer].rows; i ++){
			for(int j = 0; j < hog_input[layer].cols; j++){
				// Retrieve the magnitude value between [0-1]
				double magnitude = hog_input[layer].at<double>(i,j);
				double angle = layer * step_angle;
				// Always start the drawing from the bin center point
				Point start(j * bin_size + bin_size / 2, i * bin_size + bin_size / 2);
				vector<Point> line_positions = radial_to_carthesian(start, angle, magnitude * max_size);
				line(result, line_positions[0], line_positions[1], Scalar(255,0,0), 1);
				line(result_clean, line_positions[0], line_positions[1], Scalar(255,0,0), 1);
			}
		}
	}

	// Show the result
	imshow("HOG features visualized", result);
	imshow("HOG features visualized - clean", result_clean);
}

int _tmain(int argc, _TCHAR* argv[])
{
	// Import an image for testing purposes - KOALA!
	//Mat img = imread("d:/data/integral_channels/eye.png");
	//Mat img = imread("d:/data/integral_channels/koala.png");
	Mat img = imread("d:/data/arrow.jpg");

	// Start clock to capture calculation time
	const clock_t begin_time = clock();
	cout << "Creation of integral channels started" << endl;

	// Create image channels for effective sliding window detection
	int bin_size = 25;
	int orientations = 60;
	channel_data channels = calculate_channels(img, bin_size, orientations);

	// Output total processing time
	float milliseconds = (float( clock () - begin_time ) / CLOCKS_PER_SEC) * 1000;
	int minutes = (int(milliseconds) / 1000 /60) % 60;
	int seconds = (int(milliseconds) / 1000) % 60;
	int milliseconds_left = int(milliseconds) % 1000;

	cout << "Processing took: " << minutes << " minutes, " << seconds << " seconds and " << milliseconds_left << " milliseconds." << endl;
	
	const clock_t begin_time2 = clock();
	
	// Visualize the complete channels
	visualize_channels(img, channels);

	// Visualize gradient image+
	visualize_gradients(channels.gradient_magnitude, channels.color_channels.grayscale, 10, 5);

	// Visualize the HOG features
	visualize_HOG(channels.color_channels.grayscale, channels.hog_grayscale, bin_size / 2);

	milliseconds = (float( clock () - begin_time2 ) / CLOCKS_PER_SEC) * 1000;
	minutes = (int(milliseconds) / 1000 /60) % 60;
	seconds = (int(milliseconds) / 1000) % 60;
	milliseconds_left = int(milliseconds) % 1000;
	
	cout << "Visualization took: " << minutes << " minutes, " << seconds << " seconds and " << milliseconds_left << " milliseconds." << endl;

	waitKey(0);

	return 0;
}

