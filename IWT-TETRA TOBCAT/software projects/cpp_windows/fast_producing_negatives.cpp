/***************************************************************************************************
Copyright (c) 2013 EAVISE, KU Leuven, Campus De Nayer

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

	1. The above copyright notice and this permission notice shall be included in
	   all copies or substantial portions of the Software.
    2. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
       THE SOFTWARE.
	3. A notice shall be sent to - steven.puttemans[at]kuleuven.be - to inform the 
       software was used in further development.
*****************************************************************************************************

Supply the algorithm with a folder of negative images and cut it down to separate 
preselected sized negatives if you want to explicitly address the negative data.

*****************************************************************************************************/

// fast_cutting_negatives_folder.cpp : Supply the algorithm with a folder of negative images and cut it down to seperate preselected sized negatives

#include "stdafx.h"
#include <windows.h>

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>
#include <stdio.h>
#include <sstream>
#include <fstream>

#include <dirent.h>
#include <time.h>
#include <strsafe.h>

using namespace std;
using namespace cv;

int RandomInt(int floor, int ceiling)
{
	int range = (ceiling - floor);
	int rnd = floor + int((range * rand()) / (RAND_MAX + 1.0));
	return rnd;
}

int _tmain(int argc, _TCHAR* argv[])
{
	//-- 0. Check if arguments are given correct
	if( argc == 1 ){
		printf( "Usage of cutting negatives algorithm: \n"  
				"--> fast_cutting_negatives.exe <negatives_file.txt> <outputfolder> <amount samples> <width sample> <heigth sample> <output format>\n");
		return 0;
	}
	
	
	// Load segmentation file with object annotations
	string negatives_file = argv[1];
	string outputFolder = argv[2];
	int sample_amount = atoi(argv[3]);
	int sample_width = atoi(argv[4]);
	int sample_heigth = atoi(argv[5]);
	string format_type = argv[6];
		
	// Create output folder for segmented output objects
	// If the specified output folder doesn't exist yet, create it
	if (GetFileAttributes(outputFolder.c_str()) == INVALID_FILE_ATTRIBUTES) {
		CreateDirectory(outputFolder.c_str(),NULL);
	}

	// Read in all files from input negatives file
	vector<string> filenames;
	ifstream myFile; myFile.open(negatives_file);
	string readString;

	while(!myFile.eof()){
		// Read the first line - define end of line by \n character
		getline(myFile,readString,'\n');
		// Store the first element into filenames
		filenames.push_back(readString);
	}

	// Calculate how many samples need to be randomly taken from each negative image
	int amount_negatives = filenames.size();
	int negative_per_sample = (sample_amount / amount_negatives) + 1;

	// Loop over each actual image and cut away the correct amount of subimages
	for(int i = 0; i < filenames.size(); i++){
		Mat original = imread(filenames[i]);
		// If a wrong image would be read, break out of loop rather then crash
		if ( original.empty() ) { break;}
		// Output the current image used
		cout << filenames[i] << endl;
		for(int j = 0; j < negative_per_sample; j++){

			// Create a random x and y co�rdinate for the sample, with a maximum of the original width and heigth in both directions
			int sample_x = RandomInt(0, original.cols);
			int sample_y = RandomInt(0, original.rows);

			// Make sure that the sample will not go over the actual image boundaries or negative coordinates
			if (sample_x > (original.cols - sample_width)){
				sample_x = sample_x - sample_width;
				if (sample_x < 0){ sample_x = 0;}
			}
			if (sample_y > (original.rows - sample_heigth)){
				sample_y = sample_y - sample_heigth;
				if (sample_y < 0){ sample_y = 0;}
			}

			// Create region of interest and save to output folder
			Mat roi = original( Rect(sample_x, sample_y, sample_width, sample_heigth) );
			stringstream outputFile;
			outputFile << outputFolder << "sample_" << i << "_" << j << "." << format_type;
			imwrite(outputFile.str(), roi);
		}
	}

	return 0;
}

