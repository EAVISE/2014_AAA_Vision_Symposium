/***************************************************************************************************
Copyright (c) 2013 EAVISE, KU Leuven, Campus De Nayer

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

	1. The above copyright notice and this permission notice shall be included in
	   all copies or substantial portions of the Software.
    2. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
       THE SOFTWARE.
	3. A notice shall be sent to - steven.puttemans[at]kuleuven.be - to inform the 
       software was used in further development.
*****************************************************************************************************

Remove overburden based on average dimensions of sample data

*****************************************************************************************************/

#include "stdafx.h"

#include <windows.h>

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

// for filelisting
#include <stdio.h>
#include <io.h>

// for fileoutput
#include <string>
#include <fstream>
#include <sstream>
#include <dirent.h>
#include <sys/types.h>

#include <windows.h>
#include <tchar.h> 
#include <strsafe.h>

using namespace std;
using namespace cv;

Mat cut(Mat img, int ratio){
	Mat out;

	// Functionality for reducing, will be simply applying a mask
	int hImg = img.rows;
	int wImg = img.cols;
	int wImgNew = hImg / ratio;
	int xImgNew = wImg/2 - wImgNew / 2;

	out = img(Rect(xImgNew,0,wImgNew,hImg));

	return out;
}

int _tmain(int argc, _TCHAR* argv[])
{
	// Check if arguments are given correct
	if( argc == 1 ){
		printf( "Usage of removing overburden software: \n"  
				"--> remove_overburden.exe <location of data> <ratio width/height>\n");
		return 0;
	}
	
	// Retrieve data from folder that was given
	// Read in batch of image data
	string location_data = argv[1];
	string ratioString = argv[2];
	int ratio = atoi(ratioString.c_str());

	// list every filename from the annotated folder
	WIN32_FIND_DATA ffd;
	TCHAR szDir[MAX_PATH];
	vector<string> filenames;
	HANDLE hFind = INVALID_HANDLE_VALUE;

	/* Copy string into buffer and append /* for searching in folder */
	StringCchCopy(szDir, MAX_PATH, location_data.c_str());
	StringCchCat(szDir, MAX_PATH, TEXT("*"));

	/* Get first file - to use in do while loop */
	hFind = FindFirstFile(szDir, &ffd);
	do
	{
		// Can now use filename by ffd.cFileName
		filenames.push_back (ffd.cFileName);	
	}
	while (FindNextFile(hFind, &ffd) != 0);
	// Remove the current and parent directory name (. and ..)
	filenames.erase(filenames.begin(),filenames.begin()+2);
		
	FindClose( hFind );

	// Based on the ratio, load each image, get the height, then define what width should be and cut from the center
	// Average size of car defined as H 56 W 28 --> ratio car = (double) 2.0
	for(int i = 0; i < filenames.size(); i++){
		stringstream file;
		file << location_data << filenames[i];
		Mat imgIN = imread(file.str());
		
		// Create reducing of overburden
		Mat imgOUT = cut(imgIN, ratio);
		
		// Save adaptations
		imwrite(file.str(),imgOUT);
	}


	return 0;
}

