/***************************************************************************************************
Copyright (c) 2013 EAVISE, KU Leuven, Campus De Nayer

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

	1. The above copyright notice and this permission notice shall be included in
	   all copies or substantial portions of the Software.
    2. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
       THE SOFTWARE.
	3. A notice shall be sent to - steven.puttemans[at]kuleuven.be - to inform the 
       software was used in further development.
*****************************************************************************************************

Calculates dominant orientation of a function and returns the rotated angle.

REMEMBER : since the rotation can be up or down, a complete horizontal flip of the image needs 
to be put to be entirely sure to detect all possible objects.

*****************************************************************************************************/

#include "stdafx.h"

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

using namespace std;
using namespace cv;

struct outputGradient{
	Mat smoothedImage;
	Mat gradientX;
	Mat gradientY;
	Mat gradientTotal;
};

struct dominantGradient{
	float orientation;
	float magnitude;
};

outputGradient calculateGradientImage(Mat inputImage, Size kernelSize, int sigma){
	// Create image of the same size as the inputImage for output and smoothing
	Mat grayImage, smoothedImage;
		
	// Some basic smoothing on the image to remove pixelation
	GaussianBlur( inputImage, smoothedImage, kernelSize, sigma);

	// Convert RBG towards grayscale image
	cvtColor( smoothedImage, grayImage, CV_RGB2GRAY );

	// Calculate gradient image
	Mat grad_X = Mat(inputImage.rows, inputImage.cols, CV_32F);
	Mat grad_Y = Mat(inputImage.rows, inputImage.cols, CV_32F);

	// Sobel(src, dst, ddepth, dx, dy, ksize, scale, delta, BORDER_DEFAULT)
	// Gradient for the X direction
	Sobel( grayImage, grad_X, CV_32F, 1, 0, 3, 1, 0, BORDER_DEFAULT );

	/// Gradient for the Y direction
	Sobel( grayImage, grad_Y, CV_32F, 0, 1, 3, 1, 0, BORDER_DEFAULT );

	// Calculate magnitudes
	Mat magnitudes = Mat(grayImage.rows, grayImage.cols, CV_64F);
	Mat prodX = Mat(grayImage.rows, grayImage.cols, CV_64F);
	Mat prodY = Mat(grayImage.rows, grayImage.cols, CV_64F);
	Mat sum = Mat(grayImage.rows, grayImage.cols, CV_64F);
	Mat gradient = Mat(grayImage.rows, grayImage.cols, CV_64F);
	
	// Combine both gradient functions together
	// total = sqrt(dx� + dy�) <-- more correct than just adding both values weighted together
	prodX = grad_X.mul(grad_X);
	prodY = grad_Y.mul(grad_Y);
	sum = prodX + prodY;
	sqrt(sum, gradient);
		
	// Create an output structure
	outputGradient result;
	result.smoothedImage = smoothedImage;
	result.gradientX = grad_X;
	result.gradientY = grad_Y;
	result.gradientTotal = gradient;

	// Push back the converted image
	return result;
};

Mat calculateOrientations(Mat gradientX, Mat gradientY){
	// Create container element
	Mat orientation = Mat(gradientX.rows, gradientX.cols, CV_32F);

	// Calculate orientations of gradients --> in degrees
	// Loop over all matrix values and calculate the accompagnied orientation
	for(int i = 0; i < gradientX.rows; i++){
		for(int j = 0; j < gradientX.cols; j++){
			// Retrieve a single value
			float valueX = gradientX.at<float>(i,j);
			float valueY = gradientY.at<float>(i,j);
			// Calculate the corresponding single direction, done by applying the arctangens function
			float result = fastAtan2(valueX,valueY);
			// Store in orientation matrix element
			orientation.at<float>(i,j) = result;
		}
	}
	
	return orientation;
}

dominantGradient calculateDominantGradient(Mat orientations, Mat magnitudes, int stepDegrees){
	// Perform some postprocessing on the orientations, these now loop from 0 - 360�
	// Create bin information - add + 1 to cover for the case that division is not clear
	int amountOfBins = 360 / stepDegrees + 1;
	
	// Create the assignment of bins to each element
	Mat assignedBin, assignedBinInt;
	divide(orientations,stepDegrees,assignedBin);

	// Switch the float value towards int values, in order to be sure that a single bin is possible
	assignedBin.convertTo(assignedBinInt,CV_16U);

	// Create storage for storing the summed magnitudes in each orientation bin	
	vector<float> valuesSummed(amountOfBins,0), summedFirstHalf(amountOfBins/2,0), summedSecondHalf(amountOfBins/2,0), summed(amountOfBins/2,0);

	// Loop over all orientation elements
	for(int i = 0; i < magnitudes.rows; i++){
		for(int j = 0; j < magnitudes.cols; j++){
			short valueBin = assignedBinInt.at<short>(i,j);
			float valueMagnitude = magnitudes.at<float>(i,j);
			valuesSummed[valueBin] = valuesSummed[valueBin] + valueMagnitude;
		}
	}

	// Return to 0 - 180 degrees in order for double edges not to counter eachother, but rather fortify eachother
	int size = amountOfBins/2;
	copy(valuesSummed.begin(),valuesSummed.begin() + size, summedFirstHalf.begin());
	copy(valuesSummed.begin() + size + 1,valuesSummed.end(), summedSecondHalf.begin());
	
	for(int i = 0; i < summedFirstHalf.size(); i++){
		summed[i] = summedFirstHalf[i] + summedSecondHalf[i];
	}
	
	// Retrieve the maximum value index, in order to define the dominant gradient
	float valueMaximum = 0;
	int indexMaximum = 0;
	for(int i = 0; i < summed.size(); i++){
		if(summed[i] > valueMaximum){
			valueMaximum = summed[i];
			indexMaximum = i;
		}
	}

	// Calculate the dominant orientation angle
	float angleDominantGradient = (indexMaximum * stepDegrees) + ((float)stepDegrees / 2);

	// Prepare the output
	dominantGradient result;
	result.magnitude = valueMaximum;
	result.orientation = angleDominantGradient;

	return result;
}

Mat rotateImage(Mat source, double angle)
{
    double margin = 1.3;
	
	// Choose destination center point
	double dimension;
	if(source.rows > source.cols){
		dimension = source.rows;
	}
	else{
		dimension = source.cols;
	}
	double newCenterX = (dimension*margin)/2.0F;
	double newCenterY = (dimension*margin)/2.0F;
	Point2f dst_center(newCenterX, newCenterY);
	
	// Define three points for calculating the treanslation
	// Use upper left, upper right and center
	Point2f srcTri[3];
	Point2f dstTri[3];

	srcTri[0] = Point2f( 0, 0 );
	srcTri[1] = Point2f( source.cols, 0 );
	srcTri[2] = Point2f( 0, source.rows );

	dstTri[0] = Point2f( newCenterX - (source.cols/2.0F), newCenterY - (source.rows/2.0F) );
	dstTri[1] = Point2f( newCenterX + (source.cols/2.0F), newCenterY - (source.rows/2.0F) );
	dstTri[2] = Point2f( newCenterX - (source.cols/2.0F), newCenterY + (source.rows/2.0F) );

	Mat trs_mat = getAffineTransform( srcTri, dstTri );

	// Choose rotation center point
	Point2f trs_center(newCenterX, newCenterY);
    // Define affine rotation and translation matrix
    Mat rot_mat = getRotationMatrix2D(trs_center, angle, 1.0);

	// Define output, rotate image and return
	Mat temp, dst;
	warpAffine(source, temp, trs_mat, Size(dimension*margin, dimension*margin));
	warpAffine(temp, dst, rot_mat, temp.size());

    return dst;
}

Mat calculateEdges(Mat original){
	Mat result;
	Canny( original, result, 100, 100 * 3, 5 );
	return result;
}

int _tmain(int argc, _TCHAR* argv[])
{
	// Read in data input image
	Mat imgIN = imread("D:/data/manta_turn/segment1.png");
	
	// Visualise input image and wait for keystroke
	imshow( "Original Image", imgIN );
	int i = cvWaitKey(0);
	
	// Calculate the gradient image using Sobel operators
	outputGradient result = calculateGradientImage(imgIN,Size(11,11),5);

	// Visualise smoothed image
	imshow( "Smoothed image", result.smoothedImage);
	i = cvWaitKey(0);

	// Visualise gradient image
	Mat imgGradient; 
	Mat gradientTotal = result.gradientTotal;
	convertScaleAbs( gradientTotal, imgGradient );
	imshow( "Gradient image", imgGradient );
	i = cvWaitKey(0);

	// Apply calculation of the gradient orientations and fetch gradient magnitudes
	Mat orientation = calculateOrientations(result.gradientX, result.gradientY);
	Mat magnitude = result.gradientTotal;
	
	// Calculate dominant gradients
	dominantGradient result2 = calculateDominantGradient(orientation, magnitude, 5);

	// Rotate input image into
	Mat imgRotated = rotateImage(imgIN,90.00 - (double)result2.orientation);

	// Visualise rotated image
	imshow( "Rotated image", imgRotated );
	i = cvWaitKey(0);
	
	return 0;
}

