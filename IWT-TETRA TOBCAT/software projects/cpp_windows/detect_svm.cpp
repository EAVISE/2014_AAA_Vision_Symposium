/***************************************************************************************************
Copyright (c) 2013 EAVISE, KU Leuven, Campus De Nayer

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

	1. The above copyright notice and this permission notice shall be included in
	   all copies or substantial portions of the Software.
    2. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
       THE SOFTWARE.
	3. A notice shall be sent to - steven.puttemans[at]kuleuven.be - to inform the 
       software was used in further development.
*****************************************************************************************************

Software for loading in an SVM model and using it with the HOGDescriptor functionality
INPUT: SVM model / detection_folder
OUTPUT: detections visualized

*****************************************************************************************************/

#include "stdafx.h"

#include <fstream>
#include <sstream>

// OpenCV include all functionality
#include "opencv2/opencv.hpp";

// Include LinearSVM, which enables you to create a CvSVM class with retrieval of the support vector for HOGDescriptor functionality
#include "LinearSVM.h";

using namespace std;
using namespace cv;

int _tmain(int argc, _TCHAR* argv[])
{
	// Check if arguments are given correct
	if( argc == 1 || argc != 4){
		printf( "Usage of SVM detection software: \n"  
				"svm_load_and_detect.exe <SVM_model.xml> <test_data_listing.txt> <detectiontype: 1 = SS, 2 = MS>\n");
		return 0;
	}
	
	// ****************************************************************************************************************************************
	// RETRIEVE SOME BASIC INFO
	// ****************************************************************************************************************************************
	string model = argv[1];
	string test_data = argv[2];
	int detection_type = atoi(argv[3]);

	// Load in the correct model
	LinearSVM SVM_model;
	SVM_model.load( model.c_str() );
	
	// ****************************************************************************************************************************************
	// CHANGE THE TRAINED SVM TO PRIMAL FORM + CONFIGURE THE HOG DESCRIPTOR
	// ****************************************************************************************************************************************
	// Retrieve the support vector in the correct format
	vector<float> support_vector;
	SVM_model.get_primal_form_support_vector(support_vector);

	// Create the HOG descriptor configuration
	HOGDescriptor hog;
	Size window_size = Size(48,96); hog.winSize = window_size;
	Size cell_size = Size(8,8); hog.cellSize = cell_size; hog.blockStride = cell_size;
	Size block_size = Size(16,16); hog.blockSize = block_size;

	// Set the SVM vector
	hog.setSVMDetector(support_vector);

	// ****************************************************************************************************************************************
	// TRY TO PERFORM A DETECTION ON TRAINING DATA - USING THE PROVIDED TEST SET DATA
	// ****************************************************************************************************************************************
	
	// Retrieve a list of test file names
	ifstream input (test_data);
	vector<string> filenames;
	string current_line;
	while ( getline(input, current_line) ){
		vector<string> line_elements;
		stringstream temp (current_line);
		string first_element;
		getline(temp, first_element, ' ');
		filenames.push_back(first_element);
	}
	int number_test_samples = filenames.size();
	input.close();
	
	// For each image in filenames, perform the singlescale/multiscale detection and return the result
	for(int i = 0; i < filenames.size(); i ++){
		Mat test_image = imread(filenames[i]);
		vector<Point> detections;
		vector<Rect> locations;
		
		// Check if singlescale detection is required
		if ( detection_type == 1 ){
			hog.detect(test_image, detections);

			// Create rectangles from detection points
			vector<Rect> rectangles;
			for(int j = 0; j < detections.size(); j ++){
				Rect region (detections[j].x, detections[j].y, window_size.width, window_size.height);
				rectangles.push_back(region);
			}

			//groupRectangles(rectangles, 3, 0.3);

			// Add the detecttions to the image
			for(int j = 0; j < rectangles.size(); j ++){
				rectangle(test_image, rectangles[j], Scalar(255,0,0), 2);
			}
		}

	    // Check of multiscale detection is required
		if ( detection_type == 2 ){
			hog.detectMultiScale(test_image, locations, 0.0, Size(), Size(), 1.01);

			// Add the detections to the image
			for(int j = 0; j < locations.size(); j++){
				rectangle(test_image, locations[j], Scalar(0,0,255), 2);
			}
		}

		// Show the end result
		imshow("SVM detection model result", test_image);
		int key = waitKey(0);
		if (key == 27){ break;}
	}

	return 0;
}

