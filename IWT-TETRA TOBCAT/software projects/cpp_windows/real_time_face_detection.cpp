// real_time_face_detection.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#include <iostream>

using namespace cv;
using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	// Standard interface for executing the program
	if( argc == 1 ){
		printf( "Simple real time face detection demo. \n"
			    "real_time_face_detection.exe <webcam number - default = 0> <face_model.xml> <# of overlaps - default = 15>\n");
		return 0;
	}
	
	VideoCapture capture(atoi(argv[1]));

	if(!capture.isOpened()){
		cout << "Could not open webcam input, make sure a webcam is connected to the system." << endl;
		return -1;
	}

	Mat frame, grayscale;
	string window_name = "Face detection demo, please smile";

	CascadeClassifier cascade;
	cascade.load(argv[2]);

	int number_overlaps = atoi(argv[3]);

	while( true ){
		capture >> frame;
		if( !frame.empty() ){

		}else{
			cout << "Bad frame received, closing down application!";
			break;
		}

		// Process frame and perform detection
		cvtColor(frame, grayscale, COLOR_BGR2GRAY);
		equalizeHist(grayscale, grayscale);

		vector<Rect> faces;

		cascade.detectMultiScale(grayscale, faces, 1.05, number_overlaps);

		// Add rectangle around each result and display
		for( int i = 0; i < faces.size(); i++ ){
			rectangle(frame, faces[i], Scalar(0,0,255), 2);
			stringstream face;
			face << "Face " << (i + 1);
			putText(frame, face.str(), Point(faces[i].x, faces[i].y - 15), 1, 2, Scalar(0, 0, 255), 1 );
		}

		imshow(window_name, frame);

		int key = waitKey(25);
		if(key == 27 ) { break;}
	}
	
	return 0;
}

