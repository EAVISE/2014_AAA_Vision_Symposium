/***************************************************************************************************
Copyright (c) 2013 EAVISE, KU Leuven, Campus De Nayer

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

	1. The above copyright notice and this permission notice shall be included in
	   all copies or substantial portions of the Software.
    2. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
       THE SOFTWARE.
	3. A notice shall be sent to - steven.puttemans[at]kuleuven.be - to inform the 
       software was used in further development.
*****************************************************************************************************

Flip samples by 90 or 180 degrees

*****************************************************************************************************/

#include "stdafx.h"
#include <windows.h>

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

// for filelisting
#include <stdio.h>
#include <io.h>

// for fileoutput
#include <string>
#include <fstream>
#include <sstream>
#include <dirent.h>
#include <sys/types.h>

#include <tchar.h> 
#include <strsafe.h>

using namespace std;
using namespace cv;

Mat rotateImage(Mat source, double angle)
{
    double margin = 2;
	
	// Choose destination center point
	double dimension;
	if(source.rows > source.cols){
		dimension = source.rows;
	}
	else{
		dimension = source.cols;
	}
	double newCenterX = (dimension*margin)/2.0F;
	double newCenterY = (dimension*margin)/2.0F;
	Point2f dst_center(newCenterX, newCenterY);
	
	// Define three pointsfor calculating the treanslation
	// Use upper left, upper right and center
	Point2f srcTri[3];
	Point2f dstTri[3];

	srcTri[0] = Point2f( 0, 0 );
	srcTri[1] = Point2f( source.cols, 0 );
	srcTri[2] = Point2f( 0, source.rows );

	dstTri[0] = Point2f( newCenterX - (source.cols/2.0F), newCenterY - (source.rows/2.0F) );
	dstTri[1] = Point2f( newCenterX + (source.cols/2.0F), newCenterY - (source.rows/2.0F) );
	dstTri[2] = Point2f( newCenterX - (source.cols/2.0F), newCenterY + (source.rows/2.0F) );

	Mat trs_mat = getAffineTransform( srcTri, dstTri );

	// Choose rotation center point
	Point2f trs_center(newCenterX, newCenterY);
    // Define affine rotation and translation matrix
    Mat rot_mat = getRotationMatrix2D(trs_center, angle, 1.0);

	// Define output, rotate image and return
	Mat temp, dst;
	warpAffine(source, temp, trs_mat, Size(dimension*margin, dimension*margin));
	warpAffine(temp, dst, rot_mat, temp.size());

	// In order to be sure that the size of the training sample doesn't change, retrieve the original width and height.
	// Then perform rotating, take center of new image, and overlay a segment based on the original distances
	// BUT, THIS SHOULD ALWAYS BE DONE IN FUNCTION OF THE LARGEST DIMENSION IN ORDER TO HAVE BETTER RESULTS

	// KEEP IN MIND THAT OBJECTS THAT ARE DIAGONAL, ARE ACTUALLY LARGER THAN THE LARGEST DIMENSION, THEREFORE, WE SHOULD INCREASE THIS DIMENSION WITH A FACTOR 
	Size original = source.size();
	Size rotated = dst.size();

	Mat returnImage;

	if (source.rows >= source.cols){
		int leftCornerX = rotated.width/2 - original.width/2;
		int leftCornerY = rotated.height/2 - (original.height)/2;
		int resultW = original.width;
		int resultH = original.height;

		returnImage = dst( Rect(leftCornerX, leftCornerY, resultW, resultH) );
	}

	if (source.rows < source.cols){
		int leftCornerX = rotated.height/2 - original.height/2;
		int leftCornerY = rotated.width/2 - original.width/2;
		int resultW = original.height;
		int resultH = original.width;

		returnImage = dst( Rect(leftCornerX, leftCornerY, resultW, resultH) );
	}

    return returnImage;
}

Mat flip_90(Mat inputImg){
	Mat outputImg;
	// Third argument gives direction - 0 = horizontal / 1 = vertical / -1 = both
	flip(inputImg,outputImg, 0);
	return outputImg;
}

Mat flip_45(Mat inputImg){
	Mat outputImg;

	outputImg = rotateImage(inputImg,90);
	
	return outputImg;
}

int _tmain(int argc, _TCHAR* argv[])
{
	// Check if arguments are given correct
	if( argc == 1 ){
		printf( "Usage of flipping segmentations software: \n"  
				"--> flip_images.exe <location of data> \n");
		return 0;
	}
	
	// Retrieve data from folder that was given
	// Read in batch of image data
	string location_data = argv[1];

	// list every filename from the annotated folder
	WIN32_FIND_DATA ffd;
	TCHAR szDir[MAX_PATH];
	vector<string> filenames;
	HANDLE hFind = INVALID_HANDLE_VALUE;

	/* Copy string into buffer and append /* for searching in folder */
	StringCchCopy(szDir, MAX_PATH, location_data.c_str());
	StringCchCat(szDir, MAX_PATH, TEXT("*"));

	/* Get first file - to use in do while loop */
	hFind = FindFirstFile(szDir, &ffd);
	do
	{
		// Can now use filename by ffd.cFileName
		filenames.push_back (ffd.cFileName);	
	}
	while (FindNextFile(hFind, &ffd) != 0);
	// Remove the current and parent directory name (. and ..)
	filenames.erase(filenames.begin(),filenames.begin()+2);
		
	FindClose( hFind );

	bool stop = false, next = false;
	
	Mat outputImg;
	
	string window = "Current Image";
	namedWindow(window, CV_WINDOW_AUTOSIZE);

	// Loop over images
	// Display them and make it possible to perform actions based on the keystrokes
	for(int i = 0; i < filenames.size(); i++){
		next = false;
		
		stringstream file;
		file << location_data << filenames[i];
		Mat img = imread(file.str());

		// Delete window each time something is added - so there is no overlap
		imshow(window, img); cvWaitKey(15);
		
		while (true){
			int key = cvWaitKey(0);
			switch(key){
				case 13:
					next = true;
					break;
				// ESC pressed - close the applicationzz
				case 27:
					stop = true;
					break;
				// A pressed - flip the image 90�
				case 97:
					outputImg = flip_90(img);
					img = outputImg;
					imshow(window, img); cvWaitKey(15);
					break;
				// Z pressed - flip the image 45�
				case 122:
					outputImg = flip_45(img);
					img = outputImg;
					imshow(window, img); cvWaitKey(15);
					break;
				}

				if (stop){
					// if ESC pressed, jump out of the while loop
					break;
				}

				if (next){
					// Save adapted picture to the same location
					imwrite(file.str(),img);
					// if ENTER pressed, jump out of while loop and go to next image
					break;
				}
		}

		if (stop){
			// if ESC pressed, jump out of the for loop
			break;
		}
		
	}

	return 0;
}

