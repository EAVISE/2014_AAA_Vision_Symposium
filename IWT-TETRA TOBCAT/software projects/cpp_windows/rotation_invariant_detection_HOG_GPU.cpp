/***************************************************************************************************
Copyright (c) 2013 EAVISE, KU Leuven, Campus De Nayer

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

	1. The above copyright notice and this permission notice shall be included in
	   all copies or substantial portions of the Software.
    2. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	   IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	   FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	   AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	   LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
       THE SOFTWARE.
	3. A notice shall be sent to - steven.puttemans[at]kuleuven.be - to inform the 
       software was used in further development.
*****************************************************************************************************

Compare two possible approach for detecting pedestrians that are not straight up positioned in the image
  	1. Rotate the image 1� each time and then perform a new detection over the frame, hope to find a result
    2. Make the image rotation invariant by performing the dominant orientation approach

Functionality for rotating an image stepwise will be provided also

Applied OpenCV technique - HOG+SVM on GPU

*****************************************************************************************************/

#include "stdafx.h"

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/gpu/gpu.hpp"

#include <iostream>
#include <fstream>
#include <time.h>

#ifdef HAVE_TBB
	#include "tbb/task_scheduler_init.h"
#endif

using namespace std;
using namespace cv;

struct detectMat{
	Mat outputImage;
	string debugInfo;
};

Mat rotateImageStepwise(Mat source, double angle){
	double margin = 1.3;
	
	// Choose destination center point
	double dimension;
	if(source.rows > source.cols){
		dimension = source.rows;
	}
	else{
		dimension = source.cols;
	}
	double newCenterX = (dimension*margin)/2.0F;
	double newCenterY = (dimension*margin)/2.0F;
	Point2f dst_center(newCenterX, newCenterY);
	
	// Define three points for calculating the treanslation
	// Use upper left, upper right and center
	Point2f srcTri[3];
	Point2f dstTri[3];

	srcTri[0] = Point2f( 0, 0 );
	srcTri[1] = Point2f( source.cols, 0 );
	srcTri[2] = Point2f( 0, source.rows );

	dstTri[0] = Point2f( newCenterX - (source.cols/2.0F), newCenterY - (source.rows/2.0F) );
	dstTri[1] = Point2f( newCenterX + (source.cols/2.0F), newCenterY - (source.rows/2.0F) );
	dstTri[2] = Point2f( newCenterX - (source.cols/2.0F), newCenterY + (source.rows/2.0F) );

	Mat trs_mat = getAffineTransform( srcTri, dstTri );

	// Choose rotation center point
	Point2f trs_center(newCenterX, newCenterY);
    // Define affine rotation and translation matrix
    Mat rot_mat = getRotationMatrix2D(trs_center, angle, 1.0);

	// Define output, rotate image and return
	Mat temp, dst;
	warpAffine(source, temp, trs_mat, Size(dimension*margin, dimension*margin));
	warpAffine(temp, dst, rot_mat, temp.size());

    return dst;
}

detectMat detectRotated(Mat inputImage, double angle){
	string debugInfo;
	stringstream debug;

	debug << "rotation " << angle << " ";

	// Initiate HOG descriptor approach
	gpu::HOGDescriptor hogGPU;
    hogGPU.setSVMDetector(gpu::HOGDescriptor::getDefaultPeopleDetector());

	// Perform detections
	vector<Rect> found;
	vector<gpu::HOGConfidence> confidenceScores;

	gpu::GpuMat imgGpu;
	imgGpu.upload(inputImage);

	//hogGPU.detectMultiScale(imgGpu, found, 0, Size(8,8), Size(0,0), 1.1);
	hogGPU.computeConfidenceMultiScale(imgGpu, found, 0, Size(8,8), Size(0,0), confidenceScores,2);
	debug << "detection " << found.size() << " ";

	// Create output
	Mat outputImage = inputImage;
	for(int i = 0; i < found.size(); i++ )
	{
		Rect r = found[i];
        debug << r << " ";
		// the HOG detector returns slightly larger rectangles than the real objects.
		// so we slightly shrink the rectangles to get a nicer output.
		r.x += cvRound(r.width*0.1);
		r.width = cvRound(r.width*0.8);
		r.y += cvRound(r.height*0.07);
		r.height = cvRound(r.height*0.8);
		
		// draw rectangle that is detected using tl as top left corner and br as bottom right corner
		// push the rectangle on top of the original image
		rectangle(outputImage, r.tl(), r.br(), cv::Scalar(255,0,0), 1);
	}

	detectMat result;
	result.debugInfo = debug.str();
	result.outputImage = outputImage;

	return result;
}

int _tmain(int argc, _TCHAR* argv[])
{
	double time = (double)getTickCount();
	// Create output file for debugging and analysation purposes
	ofstream myfile;
	myfile.open("D:\\data\\output_rotation_detection.txt");

	// Read in original image
	// Convert it immediatly towards a greyscale 1 channel CV_8UC1 image since the algorithms require this
	Mat imgIN = imread("D:\\data\\personDoubleSquare.png", CV_8UC1);
	
	// Define angle step size
	int stepAngle = 1;

	// Rotate image for each ori�ntation, step by step, able to perform detection on each image
	int amountOfRotations = (360 / stepAngle);
	for(int i = 1; i <= amountOfRotations; i++){
		double angle = stepAngle*i;
		Mat imgRotated = rotateImageStepwise(imgIN, angle);
		detectMat imgRotatedDetected = detectRotated(imgRotated, angle);
		
		// Visualise rotated image
		imshow( "Rotated image", imgRotatedDetected.outputImage );
		int a = cvWaitKey(30);

		// Write output debug data to file
		myfile << imgRotatedDetected.debugInfo << "\n";
	}

	time = (double)getTickCount() - time;
	printf("Total processing time %0.2f seconds", time/cv::getTickFrequency());

	int a = cvWaitKey(0);
	destroyWindow("Rotated image");
	myfile.close();
}

