/***************************************************************************************************
Copyright (c) 2013 EAVISE, KU Leuven, Campus De Nayer

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

    1. The above copyright notice and this permission notice shall be included in
       all copies or substantial portions of the Software.
    2. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
       IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
       FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
       AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
       LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
       OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
       THE SOFTWARE.
    3. A notice shall be sent to - steven.puttemans[at]kuleuven.be - to inform the
       software was used in further development.
*****************************************************************************************************

This OpenCV software allows people to generate the OpenCV needed data structure for
object model training. The classic OpenCV interface has been cut down to the essential stuff.

*****************************************************************************************************/

#include <cstdio>
#include <cstring>
#include <cstdlib>
#include <cmath>
#include <ctime>

using namespace std;

#include "cvhaartraining.h"

int main( int argc, char* argv[] )
{
    int i = 0;
    char* nullname   = (char*)"(NULL)";
    char* vecname    = NULL; /* .vec file name */
    char* infoname   = NULL; /* file name with marked up image descriptions */
    char* imagename  = NULL; /* single sample image */
    char* bgfilename = NULL; /* background */
    int num = 1000;
    int bgcolor = 0;
    int bgthreshold = 80;
    int invert = 0;
    int maxintensitydev = 40;
    double maxxangle = 1.1;
    double maxyangle = 1.1;
    double maxzangle = 0.5;
    int showsamples = 0;
    /* the samples are adjusted to this scale in the sample preview window */
    double scale = 4.0;
    int width  = 24;
    int height = 24;

    srand((unsigned int)time(0));

    // FOR TOBCAT - removed all parameters that should not be specified for using this interface in TOBCAT workshop
    if( argc == 1 )
    {
        printf( "---- [ADAPTED INTERFACE FOR TOBCAT PROJECT] ----\n"
                "Usage: %s\n"
                "  [-info <collection_file_name>]\n"
                "  [-vec <vec_file_name>]\n"
                "  [-num <number_of_samples = %d>]\n"
                "  [-w <sample_width = %d>]\n"
                "  [-h <sample_height = %d>]\n",
                argv[0], num, width, height );

        return 0;
    }

    for( i = 1; i < argc; ++i )
    {
        if( !strcmp( argv[i], "-info" ) )
        {
            infoname = argv[++i];
        }
        else if( !strcmp( argv[i], "-img" ) )
        {
            imagename = argv[++i];
        }
        else if( !strcmp( argv[i], "-vec" ) )
        {
            vecname = argv[++i];
        }
        else if( !strcmp( argv[i], "-bg" ) )
        {
            bgfilename = argv[++i];
        }
        else if( !strcmp( argv[i], "-num" ) )
        {
            num = atoi( argv[++i] );
        }
        else if( !strcmp( argv[i], "-bgcolor" ) )
        {
            bgcolor = atoi( argv[++i] );
        }
        else if( !strcmp( argv[i], "-bgthresh" ) )
        {
            bgthreshold = atoi( argv[++i] );
        }
        else if( !strcmp( argv[i], "-inv" ) )
        {
            invert = 1;
        }
        else if( !strcmp( argv[i], "-randinv" ) )
        {
            invert = CV_RANDOM_INVERT;
        }
        else if( !strcmp( argv[i], "-maxidev" ) )
        {
            maxintensitydev = atoi( argv[++i] );
        }
        else if( !strcmp( argv[i], "-maxxangle" ) )
        {
            maxxangle = atof( argv[++i] );
        }
        else if( !strcmp( argv[i], "-maxyangle" ) )
        {
            maxyangle = atof( argv[++i] );
        }
        else if( !strcmp( argv[i], "-maxzangle" ) )
        {
            maxzangle = atof( argv[++i] );
        }
        else if( !strcmp( argv[i], "-show" ) )
        {
            showsamples = 1;
            if( i+1 < argc && strlen( argv[i+1] ) > 0 && argv[i+1][0] != '-' )
            {
                double d;
                d = strtod( argv[i+1], 0 );
                if( d != -HUGE_VAL && d != HUGE_VAL && d > 0 ) scale = d;
                ++i;
            }
        }
        else if( !strcmp( argv[i], "-w" ) )
        {
            width = atoi( argv[++i] );
        }
        else if( !strcmp( argv[i], "-h" ) )
        {
            height = atoi( argv[++i] );
        }
    }

    printf( "Info file name: %s\n", ((infoname == NULL) ?   nullname : infoname ) );
    printf( "Img file name: %s\n",  ((imagename == NULL) ?  nullname : imagename ) );
    printf( "Vec file name: %s\n",  ((vecname == NULL) ?    nullname : vecname ) );
    printf( "BG  file name: %s\n",  ((bgfilename == NULL) ? nullname : bgfilename ) );
    printf( "Num: %d\n", num );
    printf( "BG color: %d\n", bgcolor );
    printf( "BG threshold: %d\n", bgthreshold );
    printf( "Invert: %s\n", (invert == CV_RANDOM_INVERT) ? "RANDOM"
                                : ( (invert) ? "TRUE" : "FALSE" ) );
    printf( "Max intensity deviation: %d\n", maxintensitydev );
    printf( "Max x angle: %g\n", maxxangle );
    printf( "Max y angle: %g\n", maxyangle );
    printf( "Max z angle: %g\n", maxzangle );
    printf( "Show samples: %s\n", (showsamples) ? "TRUE" : "FALSE" );
    if( showsamples )
    {
        printf( "Scale: %g\n", scale );
    }
    printf( "Width: %d\n", width );
    printf( "Height: %d\n", height );

    /* determine action */
    if( imagename && vecname )
    {
        printf( "Create training samples from single image applying distortions...\n" );

        cvCreateTrainingSamples( vecname, imagename, bgcolor, bgthreshold, bgfilename,
                                 num, invert, maxintensitydev,
                                 maxxangle, maxyangle, maxzangle,
                                 showsamples, width, height );

        printf( "Done\n" );
    }
    else if( imagename && bgfilename && infoname )
    {
        printf( "Create test samples from single image applying distortions...\n" );

        cvCreateTestSamples( infoname, imagename, bgcolor, bgthreshold, bgfilename, num,
            invert, maxintensitydev,
            maxxangle, maxyangle, maxzangle, showsamples, width, height );

        printf( "Done\n" );
    }
    else if( infoname && vecname )
    {
        int total;

        printf( "Create training samples from images collection...\n" );

        total = cvCreateTrainingSamplesFromInfo( infoname, vecname, num, showsamples,
                                                 width, height );

        printf( "Done. Created %d samples\n", total );
    }
    else if( vecname )
    {
        printf( "View samples from vec file (press ESC to exit)...\n" );

        cvShowVecSamples( vecname, width, height, scale );

        printf( "Done\n" );
    }
    else
    {
        printf( "Nothing to do\n" );
    }

    return 0;
}
