/***************************************************************************************************
Copyright (c) 2013 EAVISE, KU Leuven, Campus De Nayer

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

    1. The above copyright notice and this permission notice shall be included in
       all copies or substantial portions of the Software.
    2. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
       IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
       FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
       AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
       LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
       OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
       THE SOFTWARE.
    3. A notice shall be sent to - steven.puttemans[at]kuleuven.be - to inform the
       software was used in further development.
*****************************************************************************************************

Software for performing object detection based on a trained object model.
Viola and Jones framework with score thresholding.

*****************************************************************************************************/

// includes needed for reading in files correctly and creating output
#include <fstream>
#include <iostream>

// OpenCV includes needed for correct functionality to work
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

// Including the used namespaces
using namespace std;
using namespace cv;

// Function headers
void detect_and_display( Mat frame, string window );

// Global variables
CascadeClassifier cascade;
string window_name = "Cascade classifier object detector";
string model_fullbody = "/data/models/hogcascade_pedestrians.xml";

int main( int argc, const char** argv )
{
	// Load the cascade models into the model containers
	if( !cascade.load( model_fullbody ) ){
		cout << "Error loading the trained pedestrian HOG model from the provided model file! " << endl;
		return -1;
	};

	// If input type is an image sequence, then load in all filenames for processing
	vector<string> filenames;

    // Location of the image data - in a txt file containing subsequent images by filename
    string location_data = "/data/datasets/grontmij_ladybug/grontmij_list.txt";

    // list every filename from the annotated folder
    ifstream input (location_data.c_str());
    string current_line;
    while ( getline(input, current_line) ){
        vector<string> line_elements;
        stringstream temp (current_line);
        string first_element;
        getline(temp, first_element, ' ');
        filenames.push_back(first_element);
    }
    input.close();

    // Loop over each image in the sequence and perform detection
    int counter = 1;
    for(int i = 0; i < 1; i++){
        // Read in the first image & flip it immediatly for the profile face
        Mat current_frame = imread(filenames[i]);

        vector<Rect> objects;
        vector<int> reject_levels;
        vector<double> level_weights;

        Mat frame_gray;

        cvtColor( current_frame, frame_gray, CV_BGR2GRAY );
        equalizeHist( frame_gray, frame_gray );

        // Perform detection of the frontal face detector - LBP
        cascade.detectMultiScale( frame_gray, objects, reject_levels, level_weights, 1.05, 1, 0, Size(), Size(), true );

        for( int j = 0; j < (int)objects.size(); j++ )
        {
            // adapt weight here!
            if (level_weights[j] > 2){
                cout << objects[j] << " " << reject_levels[j] << " " << level_weights[j] << endl;
                rectangle(current_frame, Point(objects[j].x, objects[j].y), Point(objects[j].x + objects[j].width, objects[j].y + objects[j].height), Scalar(0, 0, 255), 3);
            }
        }

        // Save the processed image
        stringstream name;
        name << "/data/datasets/grontmij_ladybug/output_VJ_score/processed_image_b_" << counter << ".jpg";
        counter ++;
        imwrite(name.str(), current_frame);

        cerr << "* ";
    }

	return 0;
}




