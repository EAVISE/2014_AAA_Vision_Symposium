/***************************************************************************************************
Copyright (c) 2013 EAVISE, KU Leuven, Campus De Nayer

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

    1. The above copyright notice and this permission notice shall be included in
       all copies or substantial portions of the Software.
    2. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
       IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
       FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
       AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
       LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
       OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
       THE SOFTWARE.
    3. A notice shall be sent to - steven.puttemans[at]kuleuven.be - to inform the
       software was used in further development.
*****************************************************************************************************

If you have precut positives you can use this interface to directly generate your annotations.txt

*****************************************************************************************************/

#include <opencv2/core/core.hpp>
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

// for file input and output
#include <fstream>
#include <iostream>

using namespace std;
using namespace cv;

int main( int argc, const char** argv )
{
	// Check if arguments are given correct
	if( argc == 1 ){
		printf( "Usage of general positive txt file software: \n"
				"--> create_general_positives.exe <data.txt> <annotations.txt>\n");
		return 0;
	}

	// Retrieve data from folder that was given
	// Read in batch of image data
	string location_data = argv[1];
	string location_annotations = argv[2];

    // Retrieve filenames
	// Use the positive sample file to generate a filenames element
    vector<string> filenames;
	stringstream in;
	in << location_data;
	ifstream input ( in.str().c_str() );
    string current_line;
    while ( getline(input, current_line) ){
        vector<string> line_elements;
        stringstream temp (current_line);
        string first_element;
        getline(temp, first_element, ' ');
        filenames.push_back(first_element);
    }
    input.close();

	// Data structure for outputting towards txt file is stringstream
	// Read in image, retrieve data, add too stringstream and go to following
	stringstream output;

	for(int i = 0; i < (int)filenames.size(); i++){
		Mat img = imread( filenames[i] );

		Size data = img.size();

		output << filenames[i] << " 1" << " 0 0 " << data.width << " " << data.height << "\n";
	}

	stringstream out;
	out << location_annotations;
	ofstream outFile ( out.str().c_str() );
	outFile << output.str();
	outFile.close();

	return 0;
}

