/***************************************************************************************************
Copyright (c) 2013 EAVISE, KU Leuven, Campus De Nayer

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

    1. The above copyright notice and this permission notice shall be included in
       all copies or substantial portions of the Software.
    2. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
       IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
       FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
       AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
       LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
       OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
       THE SOFTWARE.
    3. A notice shall be sent to - steven.puttemans[at]kuleuven.be - to inform the
       software was used in further development.
*****************************************************************************************************

Software for performing real time face detection in any orientation

0 /home/steven/Documents/data/face_detection/face_model.xml 15 360 10

*****************************************************************************************************/

// Includes for openCV functionality
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

// Includes for outputting information
#include <iostream>

#include <math.h>

using namespace cv;
using namespace std;

// Return the rotation matrices for each rotation
void rotate(Mat& src, double angle, Mat& dst)
{
    Point2f pt(src.cols/2., src.rows/2.);
    Mat r = getRotationMatrix2D(pt, angle, 1.0);
    warpAffine(src, dst, r, cv::Size(src.cols, src.rows));
}

int main(int argc, const char** argv)
{
	// Standard interface for executing the program
	if( argc == 1 ){
		cout << "Simple real time face detection demo." << endl;
		cout << "real_time_face_detection.exe <webcam number - default = 0> <face_model.xml> <# of overlaps - default = 15> <total degrees> <degree step>" << endl;
		return 0;
	}

	// Retrieve the correct capturing device
	int device_number = atoi(argv[1]);
	VideoCapture capture( device_number );

	if(!capture.isOpened()){
		cout << "Could not open webcam input, make sure a webcam is connected to the system." << endl;
		return -1;
	}

	Mat frame, grayscale;
	string window_name = "Face detection demo, please smile";

	CascadeClassifier cascade;
	cascade.load(argv[2]);

	int number_overlaps = atoi(argv[3]);
	int degree_total = atoi(argv[4]);
	int degree_step = atoi(argv[5]);

	while( true ){
		capture >> frame;
		if( !frame.empty() ){

		}else{
			cout << "Bad frame received, closing down application!";
			break;
		}

		// For each frame, store angles that triggered a detection
		vector<int> angles;

		// Before detection, rotate the image of the angle step and append it to an image list.
        int number_of_steps = degree_total / degree_step;
        vector<Mat> rotated_matrices;
        vector<Mat> all_rotation_matrices;
        for ( int i = 0; i < number_of_steps; i ++ ){
            // Rotate the image
            Mat rotated = frame.clone();
            rotate(frame, (i+1)*degree_step, rotated);
            // Preprocess the images already, since we will need it
            cvtColor(rotated, rotated, COLOR_BGR2GRAY);
            equalizeHist(rotated, rotated);
            // Add to the collection of rotated images
            rotated_matrices.push_back(rotated);
        }

		// Perform detection on each image seperately
        // Store all detection in a vector of vector detections
        bool detected = false;
        int x_c, y_c;
        vector< vector<Rect> > objects;
        for (int i = 0; i < (int)rotated_matrices.size(); i++){
            vector<Rect> current_objects;
            cascade.detectMultiScale(rotated_matrices[i], current_objects, 1.05, number_overlaps, 0, Size(114,114), Size(243,243));
            objects.push_back(current_objects);
            // if a detectio happended and current objects is not empty, then store the current angle
            if( current_objects.size() != 0 ){
                if (!detected){
                    x_c = current_objects[0].x + (current_objects[0].width / 2);
                    y_c = current_objects[0].y + (current_objects[0].height / 2);
                    detected = true;
                }
                angles.push_back( (i+1)*degree_step );
            }
        }

        // Calculate average angle retrieved
        double total = 0;
        for (int i = 0; i < angles.size(); i ++){
            total += angles[i];
        }
        if (total != 0 && angles.size() != 0){
            total = total / angles.size();
        }

		// Visualize detections on the input frame and show in the given window
        // Select a color for each orientation
        RNG rng(12345);
        bool draw_arrow = false;
        for( int i = 0; i < (int)objects.size(); i++ ){
            // Visualize detections in the first image
            //int i = 8;
            vector<Rect> temp = objects[i];
            Scalar color(rng.uniform(0,255), rng.uniform(0, 255), rng.uniform(0, 255));
            for( int j = 0; j < (int)temp.size(); j++ )
            {
                    // Use a rectangle representation on the frame but warp back the coordinates
                    // Retrieve the 4 corners detected in the rotation image
                    Point p1 ( temp[j].x, temp[j].y ); // Top left
                    Point p2 ( (temp[j].x + temp[j].width), temp[j].y ); // Top right
                    Point p3 ( (temp[j].x + temp[j].width), (temp[j].y + temp[j].height) ); // Down right
                    Point p4 ( temp[j].x, (temp[j].y + temp[j].height) ); // Down left

                    // Add the 4 points to a matrix structure
                    Mat coordinates = (Mat_<double>(3,4) << p1.x, p2.x, p3.x, p4.x,\
                                                            p1.y, p2.y, p3.y, p4.y,\
                                                            1   , 1  ,  1   , 1    );

                    // Apply a new inverse tranformation matrix
                    Point2f pt(frame.cols/2., frame.rows/2.);
                    Mat r = getRotationMatrix2D(pt, -(degree_step*(i+1)), 1.0);
                    Mat result = r * coordinates;

                    // Retrieve the ew coordinates from the tranformed matrix
                    Point p1_back, p2_back, p3_back, p4_back;
                    p1_back.x=(int)result.at<double>(0,0);
                    p1_back.y=(int)result.at<double>(1,0);

                    p2_back.x=(int)result.at<double>(0,1);
                    p2_back.y=(int)result.at<double>(1,1);

                    p3_back.x=(int)result.at<double>(0,2);
                    p3_back.y=(int)result.at<double>(1,2);

                    p4_back.x=(int)result.at<double>(0,3);
                    p4_back.y=(int)result.at<double>(1,3);

                    // Draw a rotated rectangle by lines, using the reverse warped points
                    line(frame, p1_back, p2_back, color, 2);
                    line(frame, p2_back, p3_back, color, 2);
                    line(frame, p3_back, p4_back, color, 2);
                    line(frame, p4_back, p1_back, color, 2);

                    draw_arrow = true;
                }
        }

        // If a detection has hapened, an arrow can be drawn, else not
        // TODO --> check why the arrow is not what was expected!!!
        Point start_arrow, end_arrow;
        if (detected) {
            start_arrow.x = x_c;
            start_arrow.y = y_c;

            end_arrow.x = start_arrow.x + 35 * cos( total * 3.14/180.0 );
            end_arrow.y = start_arrow.y + 35 * sin( total * 3.14/180.0 );

            line(frame, start_arrow, end_arrow, Scalar(255,255,255), 2);
        }

        // store the result and the detections
		imshow(window_name, frame);

		int key = waitKey(25);
		if(key == 27 ) { break;}
	}

	return 0;
}

