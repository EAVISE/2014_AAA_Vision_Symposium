/***************************************************************************************************
Copyright (c) 2013 EAVISE, KU Leuven, Campus De Nayer

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

    1. The above copyright notice and this permission notice shall be included in
       all copies or substantial portions of the Software.
    2. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
       IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
       FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
       AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
       LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
       OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
       THE SOFTWARE.
    3. A notice shall be sent to - steven.puttemans[at]kuleuven.be - to inform the
       software was used in further development.
*****************************************************************************************************

Software for performing object detection based on a trained object model.
Viola and Jones framework with overlapping detections thresholding.

*****************************************************************************************************/
// includes needed for reading in files correctly and creating output
#include <fstream>
#include <iostream>

// OpenCV includes needed for correct functionality to work
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

// Including the used namespaces
using namespace std;
using namespace cv;

// Function headers
void detect_and_display( Mat frame, string window );

// Global variables
CascadeClassifier cascade1, cascade2, cascade3;
string window_name = "Cascade classifier object detector";
string model_face = "/data/models/lbpcascade_frontalface.xml";
string model_fullbody = "/data/models/hogcascade_pedestrians.xml";

int main( int argc, const char** argv )
{
	// Load the cascade models into the model containers
	if( !cascade1.load( model_face ) ){
		cout << "Error loading the trained frontal face model from the provided model file! " << endl;
		return -1;
	};
	if( !cascade3.load( model_fullbody ) ){
		cout << "Error loading the trained pedestrian HOG model from the provided model file! " << endl;
		return -1;
	};

	// If input type is an image sequence, then load in all filenames for processing
	vector<string> filenames;

    // Location of the image data - in a txt file containing subsequent images by filename
    string location_data = "/data/datasets/grontmij_ladybug/grontmij_list.txt";

    // list every filename from the annotated folder
    ifstream input (location_data.c_str());
    string current_line;
    while ( getline(input, current_line) ){
        vector<string> line_elements;
        stringstream temp (current_line);
        string first_element;
        getline(temp, first_element, ' ');
        filenames.push_back(first_element);
    }
    input.close();

    // Loop over each image in the sequence and perform detection
    int counter = 1;
    for(int i = 0; i < (int)filenames.size(); i++){
        // Read in the first image & flip it immediatly for the profile face
        Mat current_frame = imread(filenames[i]);

        vector<Rect> objects1, objects2, objects3, objects4;
        Mat frame_gray;

        cvtColor( current_frame, frame_gray, CV_BGR2GRAY );
        equalizeHist( frame_gray, frame_gray );

        // Perform detection of the frontal face detector - LBP
        cascade1.detectMultiScale( frame_gray, objects1, 1.05, 15);
        for( int i = 0; i < (int)objects1.size(); i++ )
        {
            rectangle(current_frame, Point(objects1[i].x, objects1[i].y), Point(objects1[i].x + objects1[i].width, objects1[i].y + objects1[i].height), Scalar(0, 0, 255), 3);
        }

        // Perform detection of the full body detector - HOG
        cascade3.detectMultiScale( frame_gray, objects3, 1.05, 15);
        for( int i = 0; i < (int)objects3.size(); i++ )
        {
            rectangle(current_frame, Point(objects3[i].x, objects3[i].y), Point(objects3[i].x + objects3[i].width, objects3[i].y + objects3[i].height), Scalar(0, 255, 0), 3);
        }

        // Save the processed image
        stringstream name;
        name << "/data/output/processed_image_" << counter << ".jpg";
        counter ++;
        imwrite(name.str(), current_frame);

        cerr << "* ";
    }

	return 0;
}




