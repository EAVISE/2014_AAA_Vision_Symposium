/***************************************************************************************************
Copyright (c) 2013 EAVISE, KU Leuven, Campus De Nayer

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

    1. The above copyright notice and this permission notice shall be included in
       all copies or substantial portions of the Software.
    2. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
       IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
       FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
       AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
       LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
       OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
       THE SOFTWARE.
    3. A notice shall be sent to - steven.puttemans[at]kuleuven.be - to inform the
       software was used in further development.
*****************************************************************************************************

Software for performing object detection based on a trained object model.
Dallal and Triggs HOG + SVM object detection approach.

*****************************************************************************************************/

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/contrib/contrib.hpp"

#include <fstream>
#include <iostream>
#include <string.h>

using namespace std;
using namespace cv;

vector<string> filenames;
string image_file;

int main(int argc, char** argv)
{
	// Retrieve the filenames
	vector<string> filenames;

    // Location of the image data - in a txt file containing subsequent images by filename
    string location_data = "/data/datasets/grontmij_ladybug/grontmij_list.txt";

    // list every filename from the annotated folder
    ifstream input (location_data.c_str());
    string current_line;
    while ( getline(input, current_line) ){
        vector<string> line_elements;
        stringstream temp (current_line);
        string first_element;
        getline(temp, first_element, ' ');
        filenames.push_back(first_element);
    }
    input.close();

	// Use this information to create a HOG detector
	HOGDescriptor hog;
    hog.setSVMDetector(HOGDescriptor::getDefaultPeopleDetector());

    // Process images and detections
	for(int i = 0; i < (int)filenames.size(); i++){
        Mat img = imread(filenames[i]);
        Mat clean = img.clone();

        // Perform the detection
        vector<Rect> found;
        vector<double> foundWeights;
        hog.detectMultiScale(img, found, foundWeights, 0, Size(8,8), Size(32,32), 1.05, 1);

        for(int j = 0; j < (int)found.size(); j++ ){
            if (foundWeights[j] > 0.49){
                Rect r = found[j];
                r.x += cvRound(r.width*0.1);
                r.width = cvRound(r.width*0.8);
                r.y += cvRound(r.height*0.07);
                r.height = cvRound(r.height*0.8);
                rectangle(clean, r.tl(), r.br(), cv::Scalar(0,255,0), 3);
            }
        }

        // Save the detection result
        stringstream name;
        name << "/data/datasets/grontmij_ladybug/output_HOGSVM/processed_image_" << i << ".jpg";
        imwrite( name.str() , clean );
        cerr << "* ";
    }

	return 0;
}
