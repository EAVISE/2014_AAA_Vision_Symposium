/***************************************************************************************************
Copyright (c) 2013 EAVISE, KU Leuven, Campus De Nayer

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

    1. The above copyright notice and this permission notice shall be included in
       all copies or substantial portions of the Software.
    2. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
       IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
       FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
       AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
       LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
       OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
       THE SOFTWARE.
    3. A notice shall be sent to - steven.puttemans[at]kuleuven.be - to inform the
       software was used in further development.
*****************************************************************************************************

Software for performing object detection based on a trained object model.
LatentSVM1.0 approach with part based modeling.

*****************************************************************************************************/

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/contrib/contrib.hpp"

#include <fstream>
#include <iostream>
#include <string.h>

using namespace std;
using namespace cv;

vector<string> filenames;
string image_file;

int main(int argc, char** argv)
{
    // Threshold for non maxima suppression
    float overlapThreshold = 0.5f;

	// Retrieve the filenames
	vector<string> filenames;

    // Location of the image data - in a txt file containing subsequent images by filename
    string location_data = "/data/datasets/grontmij_ladybug/grontmij_list_backup.txt";

    // list every filename from the annotated folder
    ifstream input (location_data.c_str());
    string current_line;
    while ( getline(input, current_line) ){
        vector<string> line_elements;
        stringstream temp (current_line);
        string first_element;
        getline(temp, first_element, ' ');
        filenames.push_back(first_element);
    }
    input.close();

	// Add all the models that need to be detected to the filenames
	// Added all models that are usefull for trafic and remote sensing object detection
	string model_person = "/data/models/person.xml"; filenames.push_back(model_person);

	// Use this information to create a LatentSVM detector
	// Add the models to the interface
	LatentSvmDetector detector(filenames);

	// Return the auto generated class names by the openCV implementation
	const vector<string>& classNames = detector.getClassNames();

    // Process images and detections
    int counter = 41;
	for(int i = 0; i < (int)filenames.size(); i++){
        Mat img = imread(filenames[i]);
        Mat clean = img.clone();

        //Actual detections
        vector<LatentSvmDetector::ObjectDetection> detections;
        detector.detect(img, detections, overlapThreshold, 15);

        //Visualise the actual detections
        vector<Scalar> colors;
        generateColors( colors, detector.getClassNames().size() );

        for( int j = 0; j < (int)detections.size(); j++ )
        {
            const LatentSvmDetector::ObjectDetection& od = detections[j];
            //Only draw items larger than a specific score -- situation specific
            if (od.score > 0){
                rectangle( img, od.rect, colors[od.classID], 1 );
                putText( img, classNames[od.classID], Point(od.rect.x+4,od.rect.y+13), FONT_HERSHEY_SIMPLEX, 0.55, colors[od.classID], 1 );

                stringstream ss;
                ss << od.score;
                putText( img, ss.str(), Point(od.rect.x+4,od.rect.y+35), FONT_HERSHEY_SIMPLEX, 0.55, colors[od.classID], 1 );
            }
        }
        stringstream name;
        name << "/data/datasets/grontmij_ladybug/output_LSVM/processed_image_" << counter << ".jpg";
        counter ++;
        imwrite( name.str() , img );
        cerr << "* ";

    }
	return 0;
}
