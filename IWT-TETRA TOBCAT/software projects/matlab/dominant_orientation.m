%***************************************************************************************************
% Copyright (c) 2013 EAVISE, KU Leuven, Campus De Nayer
%
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% 1. The above copyright notice and this permission notice shall be included in
%    all copies or substantial portions of the Software.
% 2. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
%    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
%    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
%    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
%    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
%    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
%    THE SOFTWARE.
% 3. A notice shall be sent to - steven.puttemans[at]kuleuven.be - to inform the 
%    software was used in further development.
%*****************************************************************************************************

% Software for defining the dominant orientation of the image provided.
% For testing purposes, do not forget to specify your own test image location.

%*****************************************************************************************************

clc;
clear all;
close all;

img = imread('C:/data/test.png');
[row colTemp] = size(img);
col = colTemp/3;
kernel = fspecial('gaussian',[10 10],5);

figure();
subplot(2,3,1); imshow(img, []); title('Original');

imgGS = double(img);
smoothed = imfilter(imgGS,kernel,'replicate','full');
subplot(2,3,4); imshow(imgGS, []); title('Grayscale');
subplot(2,3,6); imshow(smoothed, []); title('Smoothed Grayscale');

% Creation of gradients plot of the input image to determine the dominant
% orientation. This orientation will always be perpendicular towards the
% largest gradient component.
[Gx Gy] = gradient(smoothed);
subplot(2,3,2); imshow(Gx, []); title('Gradient X');
subplot(2,3,5); imshow(Gy, []); title('Gradient Y');

% Combine gradients
grad = sqrt(Gx.^2+Gy.^2);
subplot(2,3,3); imshow(Gx, []); title('Combined Gradient');

% Creation of HOG direction maps
stepHOG = 5;
figure();
subplot(1,2,1);
xw = 1:size(Gx,1);
yw = 1:size(Gx,2);
imgWhite = double(ones(row,col));
imshow(imgWhite, []);
hold on; axis image;
% First y values, which are the rows, then x values which are the columns
quiver(yw(1:stepHOG:size(Gx,2)), ...
    xw(1:stepHOG:size(Gx,1)), ...
    Gx(1:stepHOG:size(Gx,1), 1:stepHOG:size(Gx,2)), ...
    Gy(1:stepHOG:size(Gx,1), 1:stepHOG:size(Gx,2)));
title('Gradient direction mapping');

% First calculate the size of the vectors and the angles that they are
% under using the arctangens function. 
arrowLength = sqrt(Gx.^2 + Gy.^2);
% Angle will be dependant of which quadrant will be available and can be
% between 0 and 360�.
% - Angle 0� and 90� -> arctan(y/x) will give the angle of the arrow
% - Angle 90� and 180� -> 90� + arctan(-x/y); ABS needed!
% - Angle 180� and 270� -> 180� + arctan(-y/-x); 
% - Angle 270� and 360� -> 270� + arctan(x/-y); ABS needed!
% However negative values here show that the actual value will be negative
% since values can range from -255 to 255 for each gradient component
arrowAngle = zeros(size(arrowLength,1),size(arrowLength,2));
for i=1:size(arrowLength,1)
    for j=1:size(arrowLength,2)
        % First quadrant 
        if(Gx(i,j) > 0 && Gy(i,j) > 0)
            arrowAngle(i,j) = atand(Gy(i,j)/Gx(i,j));
        end
        % Second quadrant
        if(Gx(i,j) < 0 && Gy(i,j) > 0)
            arrowAngle(i,j) = 90 + abs(atand(Gx(i,j)/Gy(i,j)));
        end
        % Third quadrant
        if(Gx(i,j) < 0 && Gy(i,j) < 0)
            arrowAngle(i,j) = 180 + atand(Gy(i,j)/Gx(i,j));
        end
        % Fourth quadrant
        if(Gx(i,j) > 0 && Gy(i,j) < 0)
            arrowAngle(i,j) = 270 + abs(atand(Gx(i,j)/Gy(i,j)));
        end
    end
end

% Create histogram bins
% Angle step size needs to be defined
angleStep = 5;
bins = 0:angleStep:360;
amountBins = size(bins,2) - 1;

% Go through all arrow values, add to the corresponding bin to define the
% largest/dominant ori�ntation
assignedBin = floor(arrowAngle ./ angleStep);
% Make sure that a number 0 doesnt exist, because this is the first bin!
% This is to make the values mean more, keeping in mind to do +1 each time
% is too hard
assignedBin = assignedBin + 1;
valuesBin = zeros(1,size(bins,2)-1);

for i = 1:size(arrowLength,1)
    for j = 1:size(arrowLength,2)
        index = assignedBin(i,j);
        valuesBin(1,index) = valuesBin(1,index) + arrowLength(i,j);
    end
end

% Since we now got ranges of 0� - 360� but we want counterpart edges to be
% placed together. For example, +45� and +225�, are contradicting arrows,
% BUT they show the same direction. Therefore, we will reduce the range to
% 0-180� which can contain each possible orientation of an object not
% taking into account top or bottom of the object.

% However due to the stepSize, this arent always the first 180 elements,
% this should be written in function of this value.

firstPartDegrees = valuesBin(1,1:amountBins/2);
secondPartDegrees = valuesBin(1,(amountBins/2)+1:amountBins);
totalValues = firstPartDegrees + secondPartDegrees;

% We should be sure to demp the first bin, because all no value gradients
% go there, and this can never be a maximum gradient, therefore replace the
% value by an arbitrarily 0. Same goes for the last value.
totalValues(1,1) = 0;
totalValues(1,size(totalValues,2)) = 0;

% Display the results into a histogram plot
subplot(1,2,2); 
stem(totalValues); title('Histogram full image');

% Look for the largest direction value
[value index] = max(totalValues);

% Use the index to retrieve the direction
direction = (index * angleStep) + (angleStep / 2);

% Now use these degrees to calculate an arrow indicating 
lengthArrowToDraw = col / 2;
xDominantGradient = lengthArrowToDraw * cos((direction * pi / 180));
yDominantGradient = lengthArrowToDraw * sin((direction * pi / 180));
xDominantOrientation = lengthArrowToDraw * cos(((direction - 90) * pi / 180));
yDominantOrientation = lengthArrowToDraw * sin(((direction - 90) * pi / 180));
figure();
subplot(1,2,1);
imshow(smoothed, []);
hold on; axis image;
quiver(col/2,row/2,xDominantGradient,yDominantGradient,'b','LineWidth', 3); 
quiver(col/2,row/2,xDominantOrientation,yDominantOrientation,'r','LineWidth', 3);
hold off;
title('Dominating gradient (blue) and orientation (red) in the input image');

% Now rotate the image around it's centerpoint, based on the most
% dominating orientation. This means that the most dominant orientation
% needs to be around the vertical or horizontal axes depending on the application.
% --> in order to rotate towards vertical axes apply direction
% --> in order to rotate towards horizontal axrs apply -(90� - direction) 
rotatedImage = imrotate(imgGS, direction);
subplot(1,2,2);
imshow(rotatedImage, []);
title('Rotated original image - ready for detection');