%***************************************************************************************************
% Copyright (c) 2013 EAVISE, KU Leuven, Campus De Nayer
%
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% 1. The above copyright notice and this permission notice shall be included in
%    all copies or substantial portions of the Software.
% 2. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
%    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
%    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
%    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
%    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
%    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
%    THE SOFTWARE.
% 3. A notice shall be sent to - steven.puttemans[at]kuleuven.be - to inform the 
%    software was used in further development.
%*****************************************************************************************************

% Testing texture filtering for effective segmentatiom

%*****************************************************************************************************

close all;
clear all;
clc;

% Read in original image and create a grayscale version of it
I = imread('test.jpg');
grayI = rgb2gray(I);

% Get image and window sizes
[w, h] = size(I);

% Create a texture image based on this input and rescale towards a standard
% double ranged image
E = entropyfilt(I);
Eim = mat2gray(E);
gEim = rgb2gray(Eim);

S = stdfilt(I,true(5));
Sim = mat2gray(S);
gSim = rgb2gray(Sim);

R = rangefilt(I,ones(5));
Rim = mat2gray(R);
gRim = rgb2gray(Rim);

subplot(2,3,1); imshow(Eim, [0 255]); title('Entropy filter');
subplot(2,3,2); imshow(Sim, [0 255]); title('Local standard deviation filter'); 
subplot(2,3,3); imshow(Rim, [0 255]); title('Local range filter');

subplot(2,3,4); imshow(gEim, []); title('GS - Entropy filter');
subplot(2,3,5); imshow(gSim, []); title('GS - Local standard deviation filter'); 
subplot(2,3,6); imshow(gRim, []); title('GS - Local range filter');