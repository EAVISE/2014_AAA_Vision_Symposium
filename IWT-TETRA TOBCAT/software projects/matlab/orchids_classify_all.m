%***************************************************************************************************
% Copyright (c) 2013 EAVISE, KU Leuven, Campus De Nayer
%
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% 1. The above copyright notice and this permission notice shall be included in
%    all copies or substantial portions of the Software.
% 2. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
%    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
%    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
%    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
%    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
%    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
%    THE SOFTWARE.
% 3. A notice shall be sent to - steven.puttemans[at]kuleuven.be - to inform the 
%    software was used in further development.
%*****************************************************************************************************

% Performs an orchids classification test on an entire directory of orchid images. 
% Output: images are copied to directories "egaal", "lip", ... (to be prepared beforehand)

%*****************************************************************************************************

%load SVMs
load SVMs.mat

% load all images of a directory full of orchid images
maindir='G:\research\TOBCAT\aris\grotetest';
%egaal
imagenames=dir([maindir '\*.jpg']);
for (i=1:length(imagenames)),
    im=[maindir '\' imagenames(i).name];
    features=preprocess(im)

%classify SVM1: egaal or textured
%egaalornot=svmclassify(SVM1, features(1:2), 'showplot', true);
%hold on; plot(features(1), features(2), 'ro', 'MarkerSize', 12); hold off
egaalornot=svmclassify(SVM1, features);

if (egaalornot)
    display(['SVM1 RESULT: orchidee ' im ' is egaal!']);
    saveasim=[maindir '\egaal\' imagenames(i).name];
    imwrite(imread(im),saveasim,'jpg');
else
    
    %classify SVM2: grof or fijn patroon
    grofornot=svmclassify(SVM2, features);
    
    if (grofornot)
        display(['SVM2 RESULT: orchidee ' im ' heeft een grof patroon!'])
        
        %classify SVM3: gevlekt or lip
        gevlektornot=svmclassify(SVM3, features);
        
        if (gevlektornot)
            display(['SVM3 RESULT: orchidee ' im ' is gevlekt!'])
            saveasim=[maindir '\gevlekt\' imagenames(i).name];
            imwrite(imread(im),saveasim,'jpg');
        else
            display(['SVM3 RESULT: orchidee ' im ' heeft een lip!'])
            saveasim=[maindir '\lip\' imagenames(i).name];
            imwrite(imread(im),saveasim,'jpg');
        end
    else
        display(['SVM2 RESULT: orchidee ' im '  heeft een fijn patroon!'])
        
        %classify SVM4: gespikkeld or gestreept
        gespikkeldornot=svmclassify(SVM4, features);
        
        if (gespikkeldornot)
            display(['SVM4 RESULT: orchidee ' im ' is gespikkeld!']);
            saveasim=[maindir '\gespikkeld\' imagenames(i).name];
            imwrite(imread(im),saveasim,'jpg');
        else
            display(['SVM4 RESULT: orchidee ' im ' is gestreept!']);
            saveasim=[maindir '\gestreept\' imagenames(i).name];
            imwrite(imread(im),saveasim,'jpg');
        end
    end
end
    
end