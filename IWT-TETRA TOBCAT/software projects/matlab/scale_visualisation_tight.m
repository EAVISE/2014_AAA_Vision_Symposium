%***************************************************************************************************
% Copyright (c) 2013 EAVISE, KU Leuven, Campus De Nayer
%
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% 1. The above copyright notice and this permission notice shall be included in
%    all copies or substantial portions of the Software.
% 2. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
%    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
%    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
%    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
%    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
%    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
%    THE SOFTWARE.
% 3. A notice shall be sent to - steven.puttemans[at]kuleuven.be - to inform the 
%    software was used in further development.
%*****************************************************************************************************

% Software for visualising the scale location relation in detection data with fixed camera position

%*****************************************************************************************************

clear all;
close all;
clc;

%-------------------------------------------------------------------------
% Adjust the code so that more tighter boundaries can be defined
% Manually defined on paper --> later look for data relation
%-------------------------------------------------------------------------

% Read in the annotations file, create a file handler
fid = fopen('annotaties/positives_original.txt');

% Get a first line from file
readLine = fgets(fid);

% Create data storage
allBoxesHeight = [];
allCenterY = [];
totalAnnotations = 0;

% Process line, continue until there is no more line to fetch
while ischar(readLine)
    % Split char array based on space
    elements = regexp(readLine, ' ', 'split');
    filename = elements{1,1}(1,:);
    amount = str2num(elements{1,2}(1,1));
   
    % Reset parameters
    count = 0;
    h = 0;
    middleY = 0;
    for i=1:amount
        h(i,:) = str2double(elements{1,6 + count});
        middleY(i,:) =str2double( elements{1,8 + count});
        
        count = count + 6;
    end
    
    % Process the data, in order to store the
    allBoxesHeight = [allBoxesHeight; h];
    allCenterY = [allCenterY; middleY];
    totalAnnotations = totalAnnotations + amount;
    
    % Get the next line that needs to be processed
    readLine = fgets(fid);
end

% Plot the points together
figure();
scatter(allCenterY(:,1), allBoxesHeight(:,1), 5,'filled');
maxHeightValue = max(allBoxesHeight(:));
axis([0 2400 0 maxHeightValue]);
set(gca,'XTick',0:100:2400);
set(gca,'YTick',0:25:maxHeightValue);
% possible fittings = poly1, smoothingspline
[polygon data output] = fit(allCenterY(:,1), allBoxesHeight(:,1), 'poly1');
% polygon contains the data needed to reconstruct the curve
% form equals p1*x + p2 where these co�fficients can be retrieved by
% polygon.p1 and polygon.p2
hold on; 
plot(polygon); 

% Manually defined
marker1 = line([1180 1320], [0 maxHeightValue], 'LineWidth', 1, 'Color', 'g'); plot(marker1);
marker2 = line([1225 1635], [0 maxHeightValue], 'LineWidth', 1, 'Color', 'g'); plot(marker2);

hold off;
title('Curve fitting on annotated image data');
xlabel('Location of appearance in the input image');
ylabel('Height of the corresponding bounding scale box - defining detection scale');

hold off;

