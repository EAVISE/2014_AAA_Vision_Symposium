%***************************************************************************************************
% Copyright (c) 2013 EAVISE, KU Leuven, Campus De Nayer
%
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% 1. The above copyright notice and this permission notice shall be included in
%    all copies or substantial portions of the Software.
% 2. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
%    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
%    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
%    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
%    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
%    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
%    THE SOFTWARE.
% 3. A notice shall be sent to - steven.puttemans[at]kuleuven.be - to inform the 
%    software was used in further development.
%*****************************************************************************************************

% Software for visualising the scale location relation in detection data with fixed camera position

%*****************************************************************************************************

clear all;
close all;
clc;

% Read in the annotations file, create a file handler
fid = fopen('annotaties/positives.txt');

% Get a first line from file
readLine = fgets(fid);

% Create data storage
allBoxesHeight = [];
allCenterY = [];
totalAnnotations = 0;

% Process line, continue until there is no more line to fetch
while ischar(readLine)
    % Split char array based on space
    elements = regexp(readLine, ' ', 'split');
    filename = elements{1,1};
    amount = str2num(elements{1,2});
   
    % Reset parameters
    count = 0;
    h = 0;
    middleY = 0;
    for i=1:amount
        h(i,:) = str2double(elements{1,6 + count});
        middleY(i,:) =str2double( elements{1,8 + count});
        
        count = count + 6;
    end
    
    % Process the data, in order to store the
    allBoxesHeight = [allBoxesHeight; h];
    allCenterY = [allCenterY; middleY];
    totalAnnotations = totalAnnotations + amount;
    
    % Get the next line that needs to be processed
    readLine = fgets(fid);
end

% Plot the points together
figure();
scatter(allCenterY(:,1), allBoxesHeight(:,1), 5,'filled');
maxHeightValue = max(allBoxesHeight(:));
axis([0 2400 0 maxHeightValue]);
set(gca,'XTick',0:100:2400);
set(gca,'YTick',0:25:maxHeightValue);
% possible fittings = poly1, smoothingspline
[polygon data output] = fit(allCenterY(:,1), allBoxesHeight(:,1), 'poly1');
% polygon contains the data needed to reconstruct the curve
% form equals p1*x + p2 where these co�fficients can be retrieved by
% polygon.p1 and polygon.p2
hold on; 
plot(polygon); 

% Reason of spread
% - how more to the top, the smaller the boxes are going to get, reaching
% the vanishing points
% - however, individual variances of persons on the boxes, are larger when
% the actual person is closer to the camera. At smaller boxes, these
% variations tend to disappear
% - this causes the typical spread of points towards larger bounding boxes

% Try to make more realistic borders know this
% out = a * in + b;
% in = (out - b)/a;
a = polygon.p1;
b = polygon.p2;

xStart = (0 - b)/a;
xEnd = (maxHeightValue - b)/a;

% (-3*stdError to +3*stdError) covers 99.8% of the spread
maxSpread = 3 * data.rmse;

marker3 = line([xStart-maxSpread xEnd-maxSpread],[0 maxHeightValue], 'LineStyle', '-', 'Color', 'g'); 
marker4 = line([xStart+maxSpread xEnd+maxSpread],[0 maxHeightValue], 'LineStyle', '-', 'Color', 'g'); 

aNew = a;
bNewUpper = b - a*maxSpread;
bNewLower = b + a*maxSpread;

upperBorder = (maxHeightValue - bNewUpper)/aNew;
lowerBorder = (0 - bNewLower)/aNew;

marker1 = line([lowerBorder lowerBorder],[0 maxHeightValue], 'LineStyle', ':');
marker2 = line([upperBorder upperBorder],[0 maxHeightValue], 'LineStyle', ':'); 

% Lets assume that the distribution isn't the same over the complete set of
% data, therefore we can define adjusted borders, letting them run from
% -sigma to +sigma as start and from -3*sigma to +3*sigma as end value
minSpread = data.rmse;

marker5 = line([xStart-minSpread xEnd-maxSpread],[0 maxHeightValue], 'LineStyle', '-', 'Color', 'm'); 
marker6 = line([xStart+minSpread xEnd+maxSpread],[0 maxHeightValue], 'LineStyle', '-', 'Color', 'm');

hold off;
title('Curve fitting on annotated image data');
xlabel('Location of appearance in the input image');
ylabel('Height of the corresponding bounding scale box - defining detection scale');

% Plot the same borders on an original example
img = imread('data/original.jpg');
figure();
imshow(img);
hold on;
marker1 = line([0 4800], [lowerBorder lowerBorder], 'LineWidth', 3); plot(marker1);
marker2 = line([0 4800], [upperBorder upperBorder], 'LineWidth', 3); plot(marker2);

% Calculate the values, the plot a scale range map if that is possible
inputValues = 1:maxHeightValue;
outputValues = (inputValues - b)/a;

hold off;

