%***************************************************************************************************
% Copyright (c) 2013 EAVISE, KU Leuven, Campus De Nayer
%
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% 1. The above copyright notice and this permission notice shall be included in
%    all copies or substantial portions of the Software.
% 2. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
%    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
%    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
%    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
%    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
%    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
%    THE SOFTWARE.
% 3. A notice shall be sent to - steven.puttemans[at]kuleuven.be - to inform the 
%    software was used in further development.
%*****************************************************************************************************

% Looking at specific circular information concerning the texture.

%*****************************************************************************************************

close all;
clear all;
clc;

% Perform texture segmentation on original image
image = imread('structure.jpg');
imageGRAY = rgb2gray(image);

% Get image and window sizes
[imageWidth, imageHeight] = size(image);

% Search for texture using edge detectors
% A first test can be done by using canny edge detection
imageEDGE = edge(imageGRAY,'canny', [0.002 0.17],0.5);
subplot(2,2,1);
imshow(imageEDGE);

% Edge detection pushes too much information, blob detection is created to
% search for blob like structures. Lets see if we can tweak blob detection
% in order to retrieve the structures of the berries.
[accum, circen, cirrad] = CircularHough_Grd(imageGRAY, [1 5]);
if any(cirrad <= 0)
    inds = find(cirrad>0);
    cirrad = cirrad(inds);
    circen = circen(inds,:);
end
subplot(2,2,2);
imshow(imageGRAY);
hold on;
plot(circen(:,1), circen(:,2), 'r+');
for ii = 1 : size(circen, 1)
    rectangle('Position',[circen(ii,1) - cirrad(ii), circen(ii,2) - cirrad(ii), 2*cirrad(ii), 2*cirrad(ii)], 'Curvature', [1,1], 'edgecolor', 'b', 'linewidth', 1.5);
end
hold off;
