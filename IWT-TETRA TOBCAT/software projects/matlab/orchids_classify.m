%***************************************************************************************************
% Copyright (c) 2013 EAVISE, KU Leuven, Campus De Nayer
%
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% 1. The above copyright notice and this permission notice shall be included in
%    all copies or substantial portions of the Software.
% 2. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
%    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
%    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
%    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
%    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
%    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
%    THE SOFTWARE.
% 3. A notice shall be sent to - steven.puttemans[at]kuleuven.be - to inform the 
%    software was used in further development.
%*****************************************************************************************************

% Individual orchids classification test. Program produces a file selector window where user
% can choose repeatedly an orchid image to classify it. Once cancel is clicked the program stops.

%*****************************************************************************************************

%load SVMs
load SVMs.mat

while(1)

%get image
 [filename, pathname] = uigetfile('*.jpg', 'Select image');
 im=[pathname filename];
 features=preprocess(im)

%classify SVM1: egaal or textured
%egaalornot=svmclassify(SVM1, features(1:2), 'showplot', true);
%hold on; plot(features(1), features(2), 'ro', 'MarkerSize', 12); hold off
egaalornot=svmclassify(SVM1, features);

if (egaalornot)
    display(['SVM1 RESULT: orchidee ' im ' is egaal!'])
else
    
    %classify SVM2: grof or fijn patroon
    grofornot=svmclassify(SVM2, features);
    
    if (grofornot)
        display(['SVM2 RESULT: orchidee ' im ' heeft een grof patroon!'])
        
        %classify SVM3: gevlekt or lip
        gevlektornot=svmclassify(SVM3, features);
        
        if (gevlektornot)
            display(['SVM3 RESULT: orchidee ' im ' is gevlekt!'])
        else
            display(['SVM3 RESULT: orchidee ' im ' heeft een lip!'])
        end
    else
        display(['SVM2 RESULT: orchidee ' im '  heeft een fijn patroon!'])
        
        %classify SVM4: gespikkeld or gestreept
        gespikkeldornot=svmclassify(SVM4, features);
        
        if (gespikkeldornot)
            display(['SVM4 RESULT: orchidee ' im ' is gespikkeld!'])
        else
            display(['SVM4 RESULT: orchidee ' im ' is gestreept!'])
        end
    end
end
    
end