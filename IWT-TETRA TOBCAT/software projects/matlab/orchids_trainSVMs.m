%***************************************************************************************************
% Copyright (c) 2013 EAVISE, KU Leuven, Campus De Nayer
%
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% 1. The above copyright notice and this permission notice shall be included in
%    all copies or substantial portions of the Software.
% 2. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
%    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
%    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
%    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
%    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
%    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
%    THE SOFTWARE.
% 3. A notice shall be sent to - steven.puttemans[at]kuleuven.be - to inform the 
%    software was used in further development.
%*****************************************************************************************************

% Training the SVM's for orchids classification
% The input images are sorted out in directories for each class

%*****************************************************************************************************

% process each class of orchid images DONT FORGET TO ADAPT!
maindir='G:\research\TOBCAT\aris';

%egaal
egaal= dir([maindir '\egaal\*.jpg']);
egaalfeatures=[];
for (i=1:length(egaal)),
    im=[maindir '\egaal\' egaal(i).name];
    egaalfeatures(:,i)=preprocess(im);
end

%gespikkeld
gespikkeld= dir([ maindir '\gespikkeld\*.jpg']);
gespikkeldfeatures=[];
for (i=1:length(gespikkeld)),
    im=[maindir '\gespikkeld\' gespikkeld(i).name];
    gespikkeldfeatures(:,i)=preprocess(im);
end

%gestreept
gestreept= dir([maindir '\gestreept\*.jpg']);
gestreeptfeatures=[];
for (i=1:length(gestreept)),
    im=[maindir '\gestreept\' gestreept(i).name];
    gestreeptfeatures(:,i)=preprocess(im);
end

%gevlekt
gevlekt= dir([maindir '\gevlekt\*.jpg']);
gevlektfeatures=[];
for (i=1:length(gevlekt)),
    im=[maindir '\gevlekt\' gevlekt(i).name];
    gevlektfeatures(:,i)=preprocess(im);
end

%lip
lip= dir([maindir '\lip\*.jpg']);
lipfeatures=[];
for (i=1:length(lip)),
    im=[maindir '\lip\' lip(i).name];
    lipfeatures(:,i)=preprocess(im);
end

figure(10)
plot3(egaalfeatures(1,:),egaalfeatures(2,:),egaalfeatures(3,:), 'b*');
hold on
plot3(gespikkeldfeatures(1,:),gespikkeldfeatures(2,:),gespikkeldfeatures(3,:), 'g+');
plot3(gestreeptfeatures(1,:),gestreeptfeatures(2,:),gestreeptfeatures(3,:), 'g+');
plot3(gevlektfeatures(1,:),gevlektfeatures(2,:),gevlektfeatures(3,:), 'md');
plot3(lipfeatures(1,:),lipfeatures(2,:),lipfeatures(3,:), 'ro');
hold off


%train SVMs%
%%%%%%%%%%%%


%SVM1: egaal or patroon
data1=[egaalfeatures'; gespikkeldfeatures'; gestreeptfeatures'; gevlektfeatures'; lipfeatures']; %each row is one observation, each column is one feature
groups1=[ones(length(egaal),1); zeros(length(gespikkeld),1); zeros(length(gestreept),1); zeros(length(gevlekt),1); zeros(length(lip),1);];
%SVM1=svmtrain(data(:,1:2), groups1, 'ShowPlot', true);
SVM1=svmtrain(data1, groups1);

%SVM2: grof or fijn
data2=[gespikkeldfeatures'; gestreeptfeatures'; gevlektfeatures'; lipfeatures']; %each row is one observation, each column is one feature
groups2=[zeros(length(gespikkeld),1); zeros(length(gestreept),1); ones(length(gevlekt),1); ones(length(lip),1);];
SVM2=svmtrain(data2, groups2);

%SVM3: gevlekt or lip
data3=[gevlektfeatures'; lipfeatures']; %each row is one observation, each column is one feature
groups3=[ones(length(gevlekt),1); zeros(length(lip),1);];
SVM3=svmtrain(data3, groups3);

%SVM4: gespikkeld or gestreept
data4=[gespikkeldfeatures'; gestreeptfeatures']; %each row is one observation, each column is one feature
groups4=[ones(length(gespikkeld),1); zeros(length(gestreept),1);];
SVM4=svmtrain(data4, groups4);

save('SVMs.mat', 'SVM1', 'SVM2', 'SVM3', 'SVM4');
