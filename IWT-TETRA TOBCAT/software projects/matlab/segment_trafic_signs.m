%***************************************************************************************************
% Copyright (c) 2013 EAVISE, KU Leuven, Campus De Nayer
%
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% 1. The above copyright notice and this permission notice shall be included in
%    all copies or substantial portions of the Software.
% 2. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
%    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
%    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
%    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
%    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
%    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
%    THE SOFTWARE.
% 3. A notice shall be sent to - steven.puttemans[at]kuleuven.be - to inform the 
%    software was used in further development.
%*****************************************************************************************************

% Functionality to segment out yellow trafic signs
% Using this, detections of non humans containing these signs and thus
% valuabale information, won't be blurred

%*****************************************************************************************************

clear all
close all;
clc;

% Configuration parameters
amount_images = 25;
file_extension = '.png';

% In order to be easy to work, all images should be resized to a general
% width and heigth - defined by the first input image, all others will be
% resized to that size
img_reference = imread('D:\data\traffic_signs\1.png');
[rows cols] = size(img_reference(:,:,1));

% Create a vector of the images
for i = 1 : amount_images
    file = ['D:\data\traffic_signs\' int2str(i) file_extension];
    img = imread(file);
    img_resized = imresize(img, [rows cols]);
    % General structure for input image data, easy to reuse
    % 4D array = 2D image data, 3 layers R G B, image number
    images(:,:,:,i) = img_resized;
end

% First step is to switch all images to the YUV color space, since yellow
% will be much more visible there
count = amount_images / 5;
for i = 1 : amount_images
    % Retrieve original channels
    R = images(:,:,1,i);
    G = images(:,:,2,i);
    B = images(:,:,3,i);
    
    % Convert to YUV channels
    Y = 0.299 * R + 0.587 * G + 0.114 * B;
    U = -0.14713 * R - 0.28886 * G + 0.436 * B;
    V = 0.615 * R - 0.51499 * G - 0.10001 * B;
    img_YUV = cat(3,Y,U,V); 
    
    subplot(count,5,i);  
    % Only display the Y - channel
    imshow(img_YUV(:,:,2));
    
    % Combine together again
    images_YUV(:,:,:,i) = img_YUV;
end

% Lets see what this does for the HSV color space, it should be possible to
% define the region for yellow coloring there
figure();
count = amount_images / 5;
for i = 1 : amount_images
    % Convert to HSV channels
    img_HSV = rgb2hsv(images(:,:,:,i));
    
    subplot(count,5,i);  
    % Only display the H - channel
    imshow(img_HSV(:,:,1) .* 360, [0 360]);
    
    % Combine together again
    images_HSV(:,:,:,i) = img_HSV;
end

% Lets perform some segmentation, to define only the yellow color region
% H value needs to be between 49 and 51
figure();
count = amount_images / 5;
for i = 1 : amount_images
    subplot(count,5,i);
    hue_channels = images_HSV(:,:,1,:) .* 360;
    segment = (hue_channels(:,:,:,i) < 48 & hue_channels(:,:,:,i) > 40);
    imshow(segment, []);
end

% Look at blue channel
figure();
for i = 1 : amount_images
    red = double(images(:,:,1,i));
    green = double(images(:,:,2,i));
    blue = double(images(:,:,3,i));
    
    denominator = 0.299 * red + 0.587 * green + 0.114 * blue;
    
    subplot(count,5,i);
    result = blue ./ denominator ./ 3;
    result_cleaned = result > 0.6;
    resultsBlue(:,:,i) = result_cleaned;
    imshow(result_cleaned, [0 1]);
end

% Yellow segmentation based on CMYK image space
figure();
for i = 1 : amount_images
    image_CMYK = images(:,:,:,i);
    image_CMYK = rgb2cmyk(image_CMYK);
    subplot(count,5,i);
    segment = image_CMYK(:,:,3) > 100;
    segments(:,:,i) = segment;
    imshow(segment);
end

% Percentage of white in a certain region
for i = 1 : amount_images
    % Check if it is a yellow traffic sign, by counting the yellow
    % percentages of pixels in the image
    [x y] = size(segments(:,:,i));
    totalPixelsYellow = double(x * y);
    pixelsWhiteYellow = double(nnz(segments(:,:,i)));
    percentageYellow = double(pixelsWhiteYellow/totalPixelsYellow*100);
    if(percentageYellow > 5)
        % We should check here if each segmented region is actually
        % dimensionally a possible trafic sign
        [BW amount] = bwlabel(segments(:,:,i));
        properties = regionprops(BW, 'BoundingBox');
        properties2 = regionprops(BW, 'Area');
        wrong_size = 0;
        for i = 1:amount
            data = properties(i,1);
            dimensions = data.BoundingBox;
            ratio = dimensions(1,3) / dimensions(1,4);
            % Firstly a larger ration means that it cannot be a sign
            if ratio > 0.4
                wrong_size = 1;
            else
                % Check if area is still large enough
                data2 = properties2(i,1);
                area = data2.Area;
                if area < 2000
                    wrong_size = 1;
                end
            end
        end
        if (wrong_size == 0)
            disp('This is a yellow trafic sign!');
        else
            disp('This is a valid detection!');
        end
    else
        % Check if it is a normal trafic sign, with blue lid
        % check for upper blue in the above half
        [x2 y2] = size(resultsBlue(:,:,i));
        half_image = resultsBlue(1:x2/2,:,i);
        totalPixelsBlue = double(x2/2 * y2);
        pixelsWhiteBlue = double(nnz(half_image));
        percentageBlue = double(pixelsWhiteBlue/totalPixelsBlue*100);
        if(percentageBlue > 5)
            disp('This is a blue trafic sign!');
        else
            disp('This is a valid detection!');
        end
    end    
end
