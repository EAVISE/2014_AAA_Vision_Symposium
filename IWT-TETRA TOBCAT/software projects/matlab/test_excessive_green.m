%***************************************************************************************************
% Copyright (c) 2013 EAVISE, KU Leuven, Campus De Nayer
%
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% 1. The above copyright notice and this permission notice shall be included in
%    all copies or substantial portions of the Software.
% 2. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
%    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
%    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
%    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
%    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
%    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
%    THE SOFTWARE.
% 3. A notice shall be sent to - steven.puttemans[at]kuleuven.be - to inform the 
%    software was used in further development.
%*****************************************************************************************************

% Testing the excessive green segmentation approach

%*****************************************************************************************************

close all;
clear all;
clc;

% Perform texture segmentation on original image
image = imread('test.jpg');
grayImage = rgb2gray(image);

% Get image and window sizes
[imageWidth, imageHeight] = size(image);

% Apply the ExcessiveGreen formula on the input image to accentuate the
% green information, then look at the green channel to see if details
% become more clear.
red = image(:,:,1);
green = image(:,:,2);
blue = image(:,:,3);

% Intensity normalised color channels
redN = double(red)./double(grayImage);
greenN = double(green)./double(grayImage);
blueN = double(blue)./double(grayImage);

subplot(1,3,1); imshow(redN, []);
subplot(1,3,2); imshow(greenN, []);
subplot(1,3,3); imshow(blueN, []);

% Calculate the excessive green index
figure();
subplot(2,3,1);
ExG = 2.*double(green) - double(red) - double(blue);
imshow(ExG,[]); title('Excessive Green Approach (light pixels)');

% Calculate the normalized difference index
NDI = (double(green) - double(red))./(double(green) + double(red));
subplot(2,3,2);
imshow(NDI,[]); title('Normalized Difference Index (light pixels)');

% Marchant index
rM = double(red) ./ double(blue);
gM = double(green) ./ double(blue);
A = 0.1;
F = rM ./(gM .^ A);
subplot(2,3,3);
imshow(F,[]); title('Marchant index F (dark pixels)');

% Calculate the excessive green index - Normalized Colors
subplot(2,3,4);
ExG = 2.*greenN - redN - blueN;
imshow(ExG,[]); title('Normalized Colors - Excessive Green Approach');

% Calculate the normalized difference index - Normalized Colors
NDIN = (greenN - redN)./(greenN + redN);
subplot(2,3,5);
imshow(NDIN,[]); title('Normalized Colors - Normalized Difference Index');

% Marchant index - Normalized Colors
rM = redN ./ blueN;
gM = greenN ./ blueN;
A = 1;
F = rM ./(gM .^ A);
subplot(2,3,6);
imshow(F,[]); title('Normalized Colors - Marchant index F');

% Basic setup

temp = double(red).*double(red) + double(green).*double(green) + double(blue).*double(blue);
denominator = sqrt(double(temp));

figure();
subplot(3,3,1); imshow(red, []); title('Red');
subplot(3,3,2); imshow(green, []); title('Green');
subplot(3,3,3); imshow(blue, []); title('Blue');
subplot(3,3,4); imshow(redN, []); title('Red Eye Norm');
subplot(3,3,5); imshow(greenN, []); title('Green Eye Norm');
subplot(3,3,6); imshow(blueN, []); title('Blue Eye Norm');
subplot(3,3,7); imshow(double(red)./denominator.*255, [0 255]); title('Red Contrast');
subplot(3,3,8); imshow(double(green)./denominator.*255, [0 255]); title('Green Contrast');
subplot(3,3,9); imshow(double(blue)./denominator.*255, [0 255]); title('Blue Contrast');


