function [features]=preprocess(im)
% fuction that processes an orchid image and extract the features

%show figures?
showfigs=0;

%read image
he = imread(im);
if (showfigs), figure(1), imshow(he); end

%mask background
bgcolor=255;
mask=(he(:,:,1)~=255)&(he(:,:,2)~=255)&(he(:,:,3)~=255);
se=strel('disk',3);
mmask=medfilt2(mask, [5 5]);
cleanmask=imerode(mmask,se);
maskindex=find(cleanmask);

%convert to lab space
cform = makecform('srgb2lab');
lab_he = applycform(he,cform);

%classify the colors in a*b* space using k-means clustering
ab = double(lab_he(:,:,2:3));
nrows = size(ab,1);
ncols = size(ab,2);
ab = reshape(ab,nrows*ncols,2);
abmasked=ab(maskindex,:);

nColors = 2;
% repeat the clustering 3 times to avoid local minima
[cluster_idx cluster_center] = kmeans(abmasked,nColors,'distance','sqEuclidean', ...
                                      'Replicates',3);

% Label Every Pixel in the Image Using the Results from KMEANS 
cluster_idx_unmasked=zeros(nrows*ncols,1); %zeros outside mask
cluster_idx_unmasked(maskindex)=cluster_idx;
pixel_labels = reshape(cluster_idx_unmasked,nrows,ncols);
if (showfigs), figure(2), imshow(pixel_labels,[]), title('image labeled by cluster index'); end

%Compute average color for each segment and construct colored segmented im
colored=zeros(size(he));
meancolors=[];
segmentcolored=[];
for k = 1:nColors
    segment=(pixel_labels==k);
    segmentidx=find(segment);
    r=double(he(:,:,1));
    meanr=mean(mean(r(segmentidx)));
    g=double(he(:,:,2));
    meang=mean(mean(g(segmentidx)));
    b=double(he(:,:,3));
    meanb=mean(mean(b(segmentidx)));
    meancolor=[meanr; meang; meanb];
    meancolors=[meancolors meancolor];
    segmentcolored(:,:,1)=segment*meanr;
    segmentcolored(:,:,2)=segment*meang;
    segmentcolored(:,:,3)=segment*meanb;
    colored=colored+segmentcolored;
end

if (showfigs), figure(3), imshow(colored./256), title('image labeled by mean color cluster'); end

%Compute feature measurements for each cluster
% measures is a matrix with numcluster columns and each measure in the rows
measurements=zeros(8,nColors);
% 1: mean l
% 2: mean a
% 3: mean b
% 4: mean intensity
% 5: x centroid
% 6: y centroid
% 7: area
% 8: number of blobs

for k = 1:nColors
    segmentidx=find(pixel_labels==k);
    segment=(pixel_labels==k);
    r=double(he(:,:,1));
    meanr=mean(mean(r(segmentidx))); %mean r
    g=double(he(:,:,2));
    meang=mean(mean(g(segmentidx))); %mean g
    b=double(he(:,:,3));
    meanb=mean(mean(b(segmentidx))); %mean b
    measurements(1,k)=meanr;
    measurements(2,k)=meang;
    measurements(3,k)=meanb;
    measurements(4,k)=0.2989*meanr+0.5870*meang+0.1140*meanb; %mean intensity 

    X_hist=sum(segment,1); 
    Y_hist=sum(segment,2); 
    X=1:ncols; Y=1:nrows; 
    measurements(5,k)=sum(X.*X_hist)/sum(X_hist); % x centroid
    measurements(6,k)=sum(Y'.*Y_hist)/sum(Y_hist); % y centroid
     
    measurements(7,k)=sum(sum(segment)); % area
    
    %eventueel eerst wat opkuisen met dilatie?
    CC = bwconncomp(segment); 
    measurements(8,k)=CC.NumObjects; %number of blobs
    
end


%sort colors based on number of pixels and construct labeled FG/BG image
FG=find(pixel_labels==1);
BG=find(pixel_labels==2);
if (measurements(7,1)>measurements(7,2))
    measurements_swap(:,1)=measurements(:,2);
    measurements_swap(:,2)=measurements(:,1);
    measurements=measurements_swap;
    FG=find(pixel_labels==2);
    BG=find(pixel_labels==1);
end

labeledimage=zeros(size(pixel_labels));
labeledimage(FG)=1;
labeledimage(BG)=0.5;
if (showfigs), figure(4), imshow(labeledimage), title('image labeled by FG (white) and BG (grey)'); end


%derive relative features
features=zeros(1,4);
% 1: color difference between FG and BG
% 2: relative y-position of clusters
% 3: number of foreground blobs + background blobs
features(1)=abs(measurements(4,1)-measurements(4,2))/255; %intensity difference 
features(2)=(measurements(6,2)-measurements(6,1))/nrows; %difference in y-pos
features(3)=measurements(8,1)+measurements(8,2); %measurements(8,2)

display(['*************************************************']);
display(['Features measured on image ' im ':']);
display(['Color difference between FG and BG: ' num2str(features(1))]);
display(['relative y-position of clusters: ' num2str(features(2))]);
display(['number of foreground blobs: ' num2str(features(3))]);


%line texture detection
%%%%%%%%%%%%%%%%%%%%%%%

%unwarp image
grey=rgb2gray(he); %convert to grayscale image
[h,w]=size(grey);
xc=w/2; %flower centre coordinates
yc=h/2;

%unwarp image from polar to rectangular coordinates
maxalpha=360;
maxr=50;
unwarped=zeros(maxr, maxalpha);
for (alpha=1:maxalpha),
    for (r=1:maxr)
        x=round(xc+r*sind(alpha));
        y=round(yc+r*cosd(alpha));
        unwarped(r,alpha)=grey(y,x);
    end
end

if (showfigs), figure(5), imshow(mat2gray(unwarped)), end

%filter unwarped image with vertical and horizontal Sobel filter
fi = fspecial('sobel');
v_edges = filter2(fi',unwarped,'valid');
h_edges = filter2(fi,unwarped,'valid');

%computer ratio of amount of edges in vertical and horizontal direction
v_edginess=sum(sum(abs(v_edges)))/sum(sum(abs(h_edges)));

display(['vertical edginess: ' num2str(v_edginess)]);
features(4)=v_edginess;

