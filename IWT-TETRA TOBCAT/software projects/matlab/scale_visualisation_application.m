%***************************************************************************************************
% Copyright (c) 2013 EAVISE, KU Leuven, Campus De Nayer
%
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% 1. The above copyright notice and this permission notice shall be included in
%    all copies or substantial portions of the Software.
% 2. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
%    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
%    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
%    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
%    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
%    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
%    THE SOFTWARE.
% 3. A notice shall be sent to - steven.puttemans[at]kuleuven.be - to inform the 
%    software was used in further development.
%*****************************************************************************************************

% Software for visualising the scale location relation in detection data with fixed camera position

%*****************************************************************************************************

% -------------------------------------------------------------------------
% Same functionality as scaleVisualisation, however it was optimized to
% display the difference between the annotated data and the actual
% measurements. Also the code was adapted to read in the files provided as
% output from the detection algorithm.
% -------------------------------------------------------------------------

clear all;
close all;
clc;

% Read in the annotations files, create a file handler
fid = fopen('annotaties/positives_original.txt');
fid2 = fopen('annotaties/interval05.txt');

% -------------------------------------------------------------------------
% First block of code does exactly as the previous version, reading the
% manual data in and creating the accompagnied boundaries, visualising only
% what we need to evaluate the new data.
% -------------------------------------------------------------------------

% Get a first line from file
readLine = fgets(fid);

% Create data storage
allBoxesHeight = [];
allCenterY = [];
totalAnnotations = 0;

% Process line, continue until there is no more line to fetch
while ischar(readLine)
    % Split char array based on space
    elements = regexp(readLine, ' ', 'split');
    filename = elements{1,1}(1,:);
    amount = str2num(elements{1,2}(1,1));
   
    % Reset parameters
    count = 0;
    h = 0;
    middleY = 0;
    for i=1:amount
        h(i,:) = str2double(elements{1,6 + count});
        middleY(i,:) =str2double( elements{1,8 + count});
        
        count = count + 6;
    end
    
    % Process the data, in order to store the
    allBoxesHeight = [allBoxesHeight; h];
    allCenterY = [allCenterY; middleY];
    totalAnnotations = totalAnnotations + amount;
    
    % Get the next line that needs to be processed
    readLine = fgets(fid);
end

fclose(fid);

% Plot the points together
figure();
maxHeightValue = max(allBoxesHeight(:));
axis([0 2400 0 maxHeightValue]);
set(gca,'XTick',0:100:2400);
set(gca,'YTick',0:25:maxHeightValue);

hold on;

[polygon data output] = fit(allCenterY(:,1), allBoxesHeight(:,1), 'poly1');
% Plot center of polygon

plot(polygon); 

a = polygon.p1;
b = polygon.p2;

xStart = (0 - b)/a;
xEnd = (maxHeightValue - b)/a;

maxSpread = 3 * data.rmse;

% Markers sigma based
marker3 = line([xStart-maxSpread xEnd-maxSpread],[0 maxHeightValue], 'LineStyle', '-', 'Color', 'g'); 
marker4 = line([xStart+maxSpread xEnd+maxSpread],[0 maxHeightValue], 'LineStyle', '-', 'Color', 'g'); 

aNew = a;
bNewUpper = b - a*maxSpread;
bNewLower = b + a*maxSpread;

upperBorder = (maxHeightValue - bNewUpper)/aNew;
lowerBorder = (0 - bNewLower)/aNew;

% Vertical markers
marker1 = line([lowerBorder lowerBorder],[0 maxHeightValue], 'LineStyle', ':');
marker2 = line([upperBorder upperBorder],[0 maxHeightValue], 'LineStyle', ':'); 

title('Curve fitting on annotated image data');
xlabel('Location of appearance in the input image');
ylabel('Height of the corresponding bounding scale box - defining detection scale');

% -------------------------------------------------------------------------
% Second block of code reads and adds the new data, in order to be able to
% compare with the theoretic boundaries.
% -------------------------------------------------------------------------

% Read in the data of the new file and create new points data
% Get a first line from file and ignore it, skip to second line by
% performing the operation a second time
readLineNew = fgets(fid2);
readLineNew = fgets(fid2);

% Create data storage
allBoxesHeightNew = [];
allCenterYNew = [];
allScoresNew = [];
allFilenamesNew = [];
totalAnnotationsNew = 0;

% Process line, continue until there is no more line to fetch
while ischar(readLineNew)
    % Split char array based on space
    elementsNew = regexp(readLineNew, '\t', 'split');
    filenameNew = elementsNew{1,1};
    yNew = str2double(elementsNew{1,3});
    hightNew = str2double(elementsNew{1,5});
    scoreNew = str2double(elementsNew{1,6});
    
    middleYNew = yNew + (hightNew / 2);
    
    % Store the data
    allBoxesHeightNew = [allBoxesHeightNew; hightNew];
    allCenterYNew = [allCenterYNew; middleYNew];
    allScoresNew = [allScoresNew; scoreNew];
    allFilenamesNew = [allFilenamesNew; filenameNew];
     
    totalAnnotationsNew = totalAnnotationsNew + 1;
   
    % Get the next line that needs to be processed
    readLineNew = fgets(fid2);
end

fclose(fid2);

scatter(allCenterYNew(:,1), allBoxesHeightNew(:,1), 5,'filled','b');

% We can first start by forcing the blue borders, removing all detections
% that are in positions that are physically impossible like the sky.
flags = ones(size(allCenterYNew,1), 1);
for i = 1:size(allCenterYNew,1)
    if( (allCenterYNew(i,1) < lowerBorder) || (allCenterYNew(i,1) > upperBorder))
        flags(i,1) = 0;
    end
end

temp1 = allCenterYNew(:,1) .* flags(:,1);
temp2 = allBoxesHeightNew(:,1) .* flags(:,1);

scatter(temp1, temp2, 5,'filled','b');

% We will now enforce the green borders
for i = 1:size(allCenterYNew,1)
    centerLow = aNew * allCenterYNew(i,1) + bNewUpper;
    centerHigh = aNew * allCenterYNew(i,1) + bNewLower;
    if( (allBoxesHeightNew(i,1) < centerLow) || (allBoxesHeightNew(i,1) > centerHigh))
        flags(i,1) = 0;
    end
end

temp3 = allCenterYNew(:,1) .* flags(:,1);
temp4 = allBoxesHeightNew(:,1) .* flags(:,1);

scatter(temp3, temp4, 5,'filled','b');

% Manually defined
marker1 = line([1180 1320], [0 maxHeightValue], 'LineWidth', 1, 'Color', 'm'); plot(marker1);
marker2 = line([1225 1635], [0 maxHeightValue], 'LineWidth', 1, 'Color', 'm'); plot(marker2);

% Retrieve the arguments of the line equations through these points
% Form y - y1 = m * (x - x1) with m = (y2 - y1)/(x2 - x1)
% Form y = m * (x - x1) + y1 with m = (y2 - y1)/(x2 - x1)  
m1 = (maxHeightValue - 0)/(1320 - 1180);
m2 = (maxHeightValue - 0)/(1635 - 1225);

% We will now enforce the purple borders on the data to create a new mask
for i = 1:size(allCenterYNew,1)
    centerHigh = m1 * (allCenterYNew(i,1) - 1180) + 0;
    centerLow = m2 * (allCenterYNew(i,1) - 1225) + 0;
    if( (allBoxesHeightNew(i,1) < centerLow) || (allBoxesHeightNew(i,1) > centerHigh))
        flags(i,1) = 0;
    end
end

temp5 = allCenterYNew(:,1) .* flags(:,1);
temp6 = allBoxesHeightNew(:,1) .* flags(:,1);

scatter(temp5, temp6, 5,'filled','m');

% Remove data points that have a to small confidence value. We will take as
% border a confidence score of 0.
for i = 1:size(allCenterYNew,1)
    if( allScoresNew(i,1) < 0)
        flags(i,1) = 0;
    end
end

temp7 = allCenterYNew(:,1) .* flags(:,1);
temp8 = allBoxesHeightNew(:,1) .* flags(:,1);

scatter(temp7, temp8, 5,'filled','g');

% % Visualize the lowest possible scale that the model is able to detect
% % based on the data run on the set
minimalDetectionHeigth = min(allBoxesHeightNew(:));
stringDisplay = sprintf('%s %0.2f %s','The lowest possible scale is = ', minimalDetectionHeigth, ' pixels.');
markerBorder = line([0 4800], [minimalDetectionHeigth minimalDetectionHeigth], 'LineWidth', 1, 'Color', 'r'); plot(marker1);
text(50,25,stringDisplay)

hold off;

% -------------------------------------------------------------------------
% Third block of code will use the seperated data to create a new file
% with annotations for correct visualization purposes.
% -------------------------------------------------------------------------

% Read in everything from the original file
fid3 = fopen('annotaties/interval05.txt');
fid4 = fopen('annotaties/interval05_cleaned.txt','w');
readLineFinal = fgets(fid3);
readLineFinal = fgets(fid3);
count = 1;
while ischar(readLineFinal)
    finalData(count,:) = regexp(readLineFinal, '\t', 'split');
    % Go over the flags, if the flag is 1, then the annotation can remain
    if flags(count,1) == 1
       % Copy line towards new file
       fprintf(fid4,'%s',readLineFinal);
    end
    % Get the next line that needs to be processed
    readLineFinal = fgets(fid3);
    count = count + 1;
end

fclose(fid3);
fclose(fid4);