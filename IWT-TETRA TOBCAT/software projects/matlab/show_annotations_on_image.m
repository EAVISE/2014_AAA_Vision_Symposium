%***************************************************************************************************
% Copyright (c) 2013 EAVISE, KU Leuven, Campus De Nayer
%
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% 1. The above copyright notice and this permission notice shall be included in
%    all copies or substantial portions of the Software.
% 2. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
%    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
%    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
%    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
%    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
%    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
%    THE SOFTWARE.
% 3. A notice shall be sent to - steven.puttemans[at]kuleuven.be - to inform the 
%    software was used in further development.
%*****************************************************************************************************

% Functionality to visualize the cleaned annotations file

%*****************************************************************************************************

clear all
close all;
clc;

% Open file, couple a file handler
fid = fopen('annotaties/interval05.txt');

% Get a first line from file
readLine = fgets(fid);

% Process line, continue until there is no more line to fetch
filenameReference = '';
while ischar(readLine)
    % Split char array based on backslash
    elements = regexp(readLine, '\t', 'split');
    filename = elements{1,1};
    
    % If an image has multiple selections, retrieve the already adapted
    % imaged to add another rectangle region as bounding box
    if strcmp(filename,filenameReference)
        locationIN = ['annotated_result_cleaned/' filename];
    else
        locationIN = ['annotated_part_original/' filename];
        filenameReference = filename;
    end
    locationOUT = ['annotated_result_cleaned/' filename];
    
    % Add the rectangles to the image and save
    img = imread(locationIN);
    xCorner = uint16(str2double(elements{1,2}));
    yCorner = uint16(str2double(elements{1,3}));
    width = uint16(str2double(elements{1,4}));
    heigth = uint16(str2double(elements{1,5}));
    
    % Matlab reference axes are different from openCV
    % We need to apply some switching of data
    temp = xCorner;
    xCorner = yCorner;
    yCorner = temp;
    
    temp = width;
    width = heigth;
    heigth = temp;
    
    % Adding borders, 2 pixels wide to visualize better, adding extra pixel
    % towards the outside of the bounding box
    % Horizontal top
    img(xCorner:xCorner+width,yCorner,1) = 76;
    img(xCorner:xCorner+width,yCorner,2) = 255;
    img(xCorner:xCorner+width,yCorner,3) = 0;
    % Horizontal bottom
    img(xCorner:xCorner+width,yCorner+heigth,1) = 76;
    img(xCorner:xCorner+width,yCorner+heigth,2) = 255;
    img(xCorner:xCorner+width,yCorner+heigth,3) = 0;
    % Vertical left
    img(xCorner,yCorner:yCorner+heigth,1) = 76;
    img(xCorner,yCorner:yCorner+heigth,2) = 255;
    img(xCorner,yCorner:yCorner+heigth,3) = 0;
    % Vertical right
    img(xCorner+width,yCorner:yCorner+heigth,1) = 76;
    img(xCorner+width,yCorner:yCorner+heigth,2) = 255;
    img(xCorner+width,yCorner:yCorner+heigth,3) = 0;
    
    % Save the image with added borders
    locationOUTSave = locationOUT(1:end-4);
    % Replace .jpg by .png if you need image data for further processing
    locationOUTSave = [locationOUTSave '.jpg'];
    imwrite(img,locationOUTSave);
    
    % Get the next line that needs to be processed
    readLine = fgets(fid);
end

% Close the handler to the file
fclose(fid);