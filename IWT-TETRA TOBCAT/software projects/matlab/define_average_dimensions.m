%***************************************************************************************************
% Copyright (c) 2013 EAVISE, KU Leuven, Campus De Nayer
%
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% 1. The above copyright notice and this permission notice shall be included in
%    all copies or substantial portions of the Software.
% 2. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
%    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
%    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
%    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
%    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
%    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
%    THE SOFTWARE.
% 3. A notice shall be sent to - steven.puttemans[at]kuleuven.be - to inform the 
%    software was used in further development.
%*****************************************************************************************************

% Software for defining average dimensions based on annotated data or detection outputs

%*****************************************************************************************************

clear all
close all;
clc;

%-------------------------------------------------------------------------
% Functionality to visualize the cleaned annotations file
%-------------------------------------------------------------------------

% Open file, couple a file handler
fid = fopen('C:\data\cookie_detector\cookies_svm\positives.txt');

% Get a first line from file
readLine = fgets(fid);

% Process line, continue until there is no more line to fetch
filenameReference = '';

% Create a vector format to store the width and height values
width = [];
height = [];

while ischar(readLine)
    % Split char array based on tab
    elements = regexp(readLine, ' ', 'split');
    filename = elements{1,1};
    
    % Count the amount of detections on the line
    amountDetections = uint8(str2double(elements{1,2}));
    
    % Add the width and height of each detection to the array
    addValue = 0;
    for i=1:amountDetections
        width = [width uint16(str2double(elements{1,5+addValue}))];
        height = [height uint16(str2double(elements{1,6+addValue}))];
        addValue = addValue + 4;
    end
      
    % Get the next line that needs to be processed
    readLine = fgets(fid);
end

% Get the average width and height
avW = floor(mean(width))
avH = floor(mean(height))
    
% Close the handler to the file
fclose(fid);