%***************************************************************************************************
% Copyright (c) 2013 EAVISE, KU Leuven, Campus De Nayer
%
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% 1. The above copyright notice and this permission notice shall be included in
%    all copies or substantial portions of the Software.
% 2. THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
%    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
%    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
%    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
%    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
%    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
%    THE SOFTWARE.
% 3. A notice shall be sent to - steven.puttemans[at]kuleuven.be - to inform the 
%    software was used in further development.
%*****************************************************************************************************

% Testing color information to reduce the object search space.

%*****************************************************************************************************

close all;
clear all;
clc;

file = 'test.jpg';

% Read in image
% Seperate into color layers
img = imread(file);
[hImg,wTemp] = size(img);
wImg = wTemp/3;
red = double(img(:,:,1));
green = double(img(:,:,2));
blue = double(img(:,:,3));

figure()
% Present the different color layers
% Value here can be maximum of 1, result will be between 0&1
% Therefore multiply the result * 255 to get grayscale values
temp = red.*red + green.*green + blue.*blue;
denominator = sqrt(double(temp));

% Here scaling perfect is another issue, since the size can be variating
% But it will never be larger then 3
denominator2 = (red + green + blue)./3;
SE = strel('square', 10);
SE2 = strel('square', 50);

subplot(3,4,1);
imshow(img);
title('Color');

% Work on the red channel information
subplot(3,4,5);
imshow(red./denominator.*255, [0 255]);
title('Red/Denominator');

subplot(3,4,6);
imshow(((red./denominator2)./3).*255, [0 255]);
title('Red/Denominator2');

subplot(3,4,7);
BWred = im2bw(red./denominator, 0.75);
imshow(BWred);
title('Red - segmented');

subplot(3,4,8);
BWcleanRed = imopen(BWred, SE);
BWconnectedRed = imclose(BWcleanRed,SE2);
imshow(BWconnectedRed);
title('Red - clean segment');

% Work on the green channel information
subplot(3,4,9);
imshow(green./denominator.*255, [0 255]);
title('Green/Denominator');

subplot(3,4,10);
imshow(((green./denominator2)./3).*255, [0 255]);
title('Green/Denominator2');

subplot(3,4,11);
% invert because the strawberrys are darker in the green space
BWgreen = im2bw(imcomplement(green./denominator), 0.5);
imshow(BWgreen);
title('Green - segmented');

subplot(3,4,12);
BWcleanGreen = imopen(BWgreen, SE);
BWconnectedGreen = imclose(BWcleanGreen,SE2);
imshow(BWconnectedGreen);
title('Red - clean segment');

% Position bounding boxes around the retrieved areas
% Making them as larges as 150% in order to be sure that these
% contain the actual fruits
% --> also use this information to create masks for further processing
subplot(3,4,3);
LabelRed = logical(BWconnectedRed);
boxesRed = regionprops(LabelRed,'BoundingBox');
areasRed = regionprops(LabelRed,'Area');
% define the minimum size of fruits to be detected in input images
% if we consider 20x20 pixels as smallest ones --> 400
minArea = 400;
amountR = size(boxesRed);
imshow(img);
hold on;
for i=1:amountR
    boxR = boxesRed(i,1).BoundingBox;
    if (areasRed(i,1).Area < minArea)
        continue
    end
    % In proportion to the fruit, enlarge the box by 50% to 150% of its
    % size in each dimension
    xR = boxR(1) - boxR(3)/4;
    yR = boxR(2) - boxR(4)/4;
    wR = boxR(3) + boxR(3)/2;
    hR = boxR(4) + boxR(4)/2;
    % Need correction if co�rdinates go outside the image dimensions, which
    % is possible when growing the found positions. This needs to lead to a
    % reducing of the width or height of that specific rectangle
    if(xR + wR > wImg)
        reduceW = xR + wR - wImg;
        wR = wR - reduceW;
    end
    if(yR + hR > hImg)
        reduceH = yR + hR - hImg;
        hR = hR - reduceH;
    end
    rectangle('position', [xR yR wR hR], 'EdgeColor','r');
    % Save the possibly adapted sizes of the rectangles, so we do not need
    % to do the check and resize again for the mask creation
    boxesRed(i,1).BoundingBox = abs([xR yR wR hR]);
end
hold off;
title('Red - bounding box');

subplot(3,4,4);
LabelGreen = bwlabel(BWconnectedGreen);
boxesGreen = regionprops(LabelGreen,'BoundingBox');
areasGreen = regionprops(LabelGreen,'Area');
amountG = size(boxesGreen);
imshow(img);
hold on;
for i=1:amountG
    boxG = boxesGreen(i,1).BoundingBox;
    if (areasGreen(i,1).Area < minArea)
        continue
    end
    xG = boxG(1) - boxG(3)/4;
    yG = boxG(2) - boxG(4)/4;
    wG = boxG(3) + boxG(3)/2;
    hG = boxG(4) + boxG(4)/2;
    if(xG + wG > wImg)
        reduceW = xG + wG - wImg;
        wG = wG - reduceW;
    end
    if(yG + hG > hImg)
        reduceH = yG + hG - hImg;
        hG = hG - reduceH;
    end
    rectangle('position', [xG yG wG hG], 'EdgeColor','g');
    boxesGreen(i,1).BoundingBox = abs([xG yG wG hG]);
end
hold off;
title('Green - bounding box');

% Create mask images for further processing
% These will indicate regions that will be used for object detection
% These can also reduce scale pyramid, considering that an object cannot be
% larger than a mask region, so these scales can be ignored
figure();
subplot(2,2,1);
maskR = zeros(hImg,wImg);
count = 0;
for i=1:amountR
    count = count + 1;
    boxR = boxesRed(i,1).BoundingBox;
    if (areasRed(i,1).Area < minArea)
        continue
    end
    % Fill the position of the box inside the array
    maskR(boxR(2):boxR(2)+boxR(4),boxR(1):boxR(1)+boxR(3)) = 1;
end
maskR = logical(maskR);
imshow(maskR);
title('Red - mask for processing');

subplot(2,2,2);
maskG = maskR;
for i=1:amountG
    boxG = boxesGreen(i,1).BoundingBox;
    if (areasGreen(i,1).Area < minArea)
        continue
    end
    % Fill the position of the box inside the array
    maskG(boxG(2):boxG(2)+boxG(4),boxG(1):boxG(1)+boxG(3)) = 1;
end
maskG = logical(maskG);
imshow(maskG);
title('Green - mask for processing');

subplot(2,2,3:4);
maskTotal = maskR | maskG;
imshow(maskTotal);
title('Total combined mask over both color dimensions');

% Lets see what the information gain is when switching towards the HSV
% color space in stead of the RGB color space.
% Hue values can be seen as degrees between 0 and 360, therefore we will
% multiply the values between 0 and 1 with 360
imgHSV = rgb2hsv(img);
hue = imgHSV(:,:,1).*360;
saturation = imgHSV(:,:,2);
value = imgHSV(:,:,3);

figure();
% Display the possible channels in the HSV color space
subplot(2,3,1); imshow(imgHSV); title('HSV Color space');
subplot(2,3,2); imshow(hue,[0 360]); title('Hue Channel');
subplot(2,3,4); imshow(saturation); title('Saturation Channel');
subplot(2,3,5); imshow(value); title('Value Channel');

% Perform some thresholding to define the red color in hue
% Values lie somewhere between 0 - 15� and 345� - 360�
strawberryHUE = (hue < 15 & hue > 0) | (hue > 345 & hue < 360);
subplot(2,3,3); imshow(strawberryHUE); title('Hue fixed on good berries');

% When looking at the color space, we notice that we can reduce combined
% with the HUE, the darker spots and the whitened spots by limiting
% thresholds for value and saturation
% Saturation between 0.5 and 1
% Value between 0.3 and 1
segmentHUE = (hue < 15 & hue > 0) | (hue > 345 & hue < 360);
segmentSATURATION = (saturation > 0.5 & saturation < 1);
segmentVALUE = (value > 0.5 & value < 1);
complete = segmentHUE & segmentSATURATION & segmentVALUE;
subplot(2,3,6); imshow(complete); title('Restrictions on 3 channels');

% In RGB color space it is difficult to define the yellow/white -
% green/white color shade of the unripe strawberries. Maybe HSV space can
% offer a solution here.
figure();
subplot(1,2,1);
segmentHUE = (hue > 45 & hue < 80);
segmentSATURATION = (saturation > 0.20 & saturation < 0.55);
segmentVALUE = (value > 0.45);
complete2 = segmentHUE & segmentSATURATION & segmentVALUE;

% Clean some rubbish elements
SE3 = strel('square', 2);
SE4 = strel('square', 4);
complete2clean = imopen(complete2, SE3);
complete2clean2 = imclose(complete2clean, SE4);
imshow(complete2clean2); 
title('Result for detecting unripe strawberries - no artefacts');

% Assign regions, use region properties to remove regions that do not fit
LabelUnripe = logical(complete2clean2);
propertiesBerry = regionprops(LabelUnripe,'Area','Orientation','BoundingBox');

subplot(1,2,2);
[a b] = size(propertiesBerry);
imshow(img);
hold on;
for i=1:a
    area = propertiesBerry(i,1).Area;
    orientation = propertiesBerry(i,1).Orientation;
    box = propertiesBerry(i,1).BoundingBox;
    if( area < 350 )
        continue
    end
    xU = box(1) - box(3)/4;
    yU = box(2) - box(4)/4;
    wU = box(3) + box(3)/2;
    hU = box(4) + box(4)/2;
    if(xU + wU > wImg)
        reduceW = xU + wU - wImg;
        wU = wU - reduceW;
    end
    if(yU + hU > hImg)
        reduceH = yU + hU - hImg;
        hU = hU - reduceH;
    end
    rectangle('position', [xU yU wU hU] , 'EdgeColor','b');
    propertiesBerry(i,1).BoundingBox = abs([xU yU wU hU]);
end
hold off;
title('Unripe berry regions detected');
% This already gives exact regions of where scanning can be applied. Again
% these regions could be grown larger in order to have an exact region to
% scan.

% A last part can be to apply the mask on the image
figure();
gray = rgb2gray(img);
result = uint8(gray) .* uint8(maskTotal);
subplot(1,3,1);
imshow(img, [0 255]);
title('Original input - grayscale');
subplot(1,3,2);
imshow(result,[0 255]);
title('Original input - mask red strawberries');

maskU = zeros(hImg,wImg);
for i=1:a
    boxU = propertiesBerry(i,1).BoundingBox;
    areaU = propertiesBerry(i,1).Area;
    if ( areaU < 350 )
        continue
    end
    % Fill the position of the box inside the array
    maskU(boxU(2):boxU(2)+boxU(4),boxU(1):boxU(1)+boxU(3)) = 1;
end
maskU = logical(maskU);

subplot(1,3,3);
result = uint8(gray) .* uint8(maskU);
imshow(result,[0 255]);
title('Original input - mask unripe strawberries');

figure()
imshow(maskTotal);
%subplot(1,2,1); imshow(BWred); title('segment red');
%subplot(1,2,2); imshow(BWgreen); title('segment green');
